//
//  AppDelegate.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import iCloudDocumentSync
import Fabric
import Crashlytics
import SwiftKeychainWrapper
import RealmSwift
import Firebase
import ok_ios_sdk
import VK_ios_sdk
import WebRTC
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var providerDelegate: CallKitProviderDelegate?
    var account: VSLAccount!
    lazy var presenter: Presenter = PresenterImpl()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        RTCInitializeSSL()
        UIApplication.shared.statusBarStyle = .lightContent
        
        Fabric.with([Crashlytics.self])
        let settings = OKSDKInitSettings()
        settings.appId = "1256302592"
        settings.appKey = "CBAJKCOLEBABABABA"
        settings.controllerHandler = {
            return self.window?.rootViewController
        }
        OKSDK.initWith(settings)
        VKSdk.initialize(withAppId: "6201411")


        let config = Realm.Configuration(
            schemaVersion: 10,
            migrationBlock: { migration, oldSchemaVersion in
        })
        
        Realm.Configuration.defaultConfiguration = config
        let _ = try! Realm()
        
        KeychainWrapper.standard.removeObject(forKey: Constants.expertCallId)
        KeychainWrapper.standard.removeObject(forKey: Constants.dialog_id)
        KeychainWrapper.standard.removeObject(forKey: Constants.timestamp)
        
        if let _ = UserEntity.current {
            let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BaseMenuViewController")
            let navigationController = Smart24NavigationController(rootViewController: controller)
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        } else {
            let vc = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "GreetingViewController")
            let navigationController = Smart24NavigationController(rootViewController: vc)
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }
        
        
        // icloud init
        iCloud.shared().setupiCloudDocumentSync(withUbiquityContainer: nil)
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        // init SIP
        setupVialerEndpoint()
        setupSIPAccount()
        
        // Firabase init
        ChattingService.shared.configurateFirebaseMessaging(application: application)
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if ApplicationDelegate.shared.application(application, open: url, options: options) {
            return true
        }
        if GIDSignIn.sharedInstance().handle(url) {
            return true
        }
        
        if OKSDK.open(url) {
            return true
        }
        
        let app = options[.sourceApplication] as? String
        if VKSdk.processOpen(url, fromApplication: app) {
            return true
        }
        
        return true
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Swift.Void) {
        // Must be called when finished
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification){
        
        if application.applicationState == .inactive {
            presenter.openChatVC()
        }
        
        if application.applicationState == .active {
            if let containerVC = UIApplication.containerViewController() as? ContainerViewController {
                
                let alertController = UIAlertController(title: Constants.newMessageFromOperator, message: "", preferredStyle: .alert)
                let goToChat = UIAlertAction(title: Constants.goToChat, style: .default) { [weak self] action in
                    self?.presenter.openChatVC()
                }
                alertController.addAction(goToChat)
                alertController.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
                containerVC.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        ChattingService.shared.receiveMessage()
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        debugPrint("applicationDidEnterBackground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        debugPrint("applicationDidBecomeActive")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        RTCCleanupSSL();
    }
    
    private func setupVialerEndpoint() {
        DDLogWrapper.setup()
        
        let endpointConfiguration = VSLEndpointConfiguration()
        endpointConfiguration.logLevel = 3
        endpointConfiguration.userAgent = "VialerSIPLib Example App"
        if let transportType = VSLTransportConfiguration(transportType: .UDP) {
            endpointConfiguration.transportConfigurations = [transportType]
        }
        
        do {
            try VialerSIPLib.sharedInstance().configureLibrary(withEndPointConfiguration: endpointConfiguration)
        } catch let error {
            debugPrint("Error setting up VialerSIPLib: \(error)")
        }
    }
    
    private func setupSIPAccount() {
        do {
            account = try VialerSIPLib.sharedInstance().createAccount(withSip: SipUser())
        } catch let error {
            debugPrint("Could not create account. Error:\(error)\nExiting")
            assert(false)
        }
    }
}

