//
//  ForgotPasswordViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveCocoa
import ReactiveSwift
import ObjectMapper
import RealmSwift
import SwiftKeychainWrapper
import enum Result.NoError
import Fabric


class ForgotPasswordViewModel: GeneralViewModel {
    
    var phoneProperty = MutableProperty<String?>("")
    
    var cocoaActionForgotPassword: CocoaAction<Any>!
    var forgotPasswordSignalProducer: Action<(), Any, ErrorEntity>!
    var phoneSignalProducer: SignalProducer<Bool, Never>!
    
    private let errorFactory = ErrorFactory()
    
    override func configureSignals() {
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.count == 13 else {
                    return false
                }
                return true
        }
        
        // MARK forgot password Action
        forgotPasswordSignalProducer = Action<(), Any, ErrorEntity> { [unowned self] _ in
            return self.forgotPassword(code: nil)
        }
        
        cocoaActionForgotPassword = CocoaAction(forgotPasswordSignalProducer, input:())
    }
    
//    private func getPassword(phone: String, otp: String?) {
//        
//        networkProvider.request(ApiManager.forgotPassword(phone, otp)) { (result) in
//            parseResponse(moyaResult: result, complition: { (dict) in
//                if let errors = dict["errors"] as? [[String : Any]], errors.count > 0 {
//                    if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                        var message = ""
//                        if let _ = errors.first(where: {$0["code"] as? Int == 1011 }) {
//                            message = "Неверный код"
//                        }
//                        let alert = UIAlertController(title: NSLocalizedString("needSMSConfirm", comment: ""), message: message, preferredStyle: .alert)
//                        alert.addTextField { (textField) in
//                            textField.keyboardType = .numberPad
//                            if #available(iOS 12.0, *) {
//                                textField.textContentType = UITextContentType.oneTimeCode
//                            }
//                        }
//                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
//                            self.getPassword(phone: phone, otp: alert?.textFields?.first?.text)
//                        }))
//                        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    } else {
//                        guard let message = errors.first?["message"] as? String else { return }
//                        self.showAlertWithMessage(message: message)
//                    }
//                } else {
//                    guard let message = dict["message"] as? String else { return }
//                    self.showAlertWithMessage(message: message, complition: {
//                        if let user = User.current() {
//                            PersistanceService.shared.change(object: user, changes: { (user) in
//                                user.password = ""
//                            })
//                        }
//                        self.dismiss(animated: true, completion: nil)
//                    })
//                }
//                MBProgressHUD.hide(for: self.view, animated: true)
//            }, failure: { (error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
//                self.showAlertWithMessage(message: error.localizedDescription)
//            })
//        }
//    }
    
    
    
    func forgotPassword(code: String?) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.phoneProperty.value!.replacingOccurrences(of: "+", with: "")
            
            networkProvider.request(.forgotPassword(phone, code)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String: AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                observer.sendCompleted()
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                    
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func loginUser(password: String, otpCode: String?) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.phoneProperty.value!.replacingOccurrences(of: "+", with: "")
            
//            networkProvider.request(.login(phone, password, otpCode)) { [weak self] result in
//                
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//                            
//                            if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                                if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                                    observer.send(value: BPLoginAction.needSMSConfirm)
//                                } else {
//                                    observer.send(error: ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError)
//                                }
//                            } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
//                                UserDefaults.standard.set(phone, forKey: Constants.savedPhone)
//                                UserDefaults.standard.set(password, forKey: Constants.savedPassword)
//                                
//                                //MARK:- remove DB data if it isn't current user
//                                if let _ = self?.currentId {
//                                    do {
//                                        let realm = try Realm()
//                                        try realm.write {
//                                            realm.deleteAll()
//                                        }
//                                    } catch {
//                                        debugPrint("can't delete data from DB")
//                                    }
//                                }
//                                
//                                KeychainWrapper.standard.set(item.uid, forKey: Constants.currentUserId)
//                                self?.setDataInDB(data: item)
//                                
//                                observer.send(value: item)
//                                observer.sendCompleted()
//                                
//                              //  self?.loginObserver.send(value: ())
//                                self?.openContainerVC()
//                            } else {
//                                observer.send(error: ErrorFactory.serverError)
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
        }
    }
}

