//
//  OrderCallViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Павел Останин on 29.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import SWRevealViewController

class OrderCallViewController: UIViewController, UITextFieldDelegate, SWRevealViewControllerDelegate {
    
    @IBOutlet weak var bottomCheckMark: UIButton!
    @IBOutlet weak var topCheckMark: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var callMeNowTextField: UITextField!
    @IBOutlet weak var callMeInTimeTextField: UITextField!
    @IBOutlet weak var startCallTextField: UITextField!
    @IBOutlet weak var endCallTextField: UITextField!
    @IBOutlet weak var selectTimePicker: UIDatePicker!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var constaraintHeightTimerView: NSLayoutConstraint!
    
    let profileViewModel = ProfileViewModel()
    var dateFormatter: DateFormatter = DateFormatter()
    var startTime = Date()
    var endTime: Date?
    
    var isCallNow: Bool = true
    var isSelectStartTime = true
    
    lazy var control : UIControl = {
        let control = UIControl()
        
        return control
    }()
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.control.isHidden = false
        return true
    }
    
    @objc fileprivate func p_tapDismissKeyboard(_ sender: Any?) {
        self.nameTextField.resignFirstResponder()
        self.phoneNumberTextField.resignFirstResponder()
        self.commentTextField.resignFirstResponder()
        self.callMeNowTextField.resignFirstResponder()
        self.callMeInTimeTextField.resignFirstResponder()
        self.startCallTextField.resignFirstResponder()
        self.endCallTextField.resignFirstResponder()
        self.control.isHidden = true
    }
    
    func revealControllerPanGestureShouldBegin(_ revealController: SWRevealViewController!) -> Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomCheckMark.isHidden = true
        self.control.addTarget(self, action: #selector(p_tapDismissKeyboard(_:)), for: .touchUpInside)
        self.nameTextField.delegate = self
        self.phoneNumberTextField.delegate = self
        self.commentTextField.delegate = self
        self.callMeNowTextField.delegate = self
        self.callMeInTimeTextField.delegate = self
        self.startCallTextField.delegate = self
        self.endCallTextField.delegate = self
        self.selectTimePicker.date = Date()
        revealViewController().delegate = self
        self.scrollView.addSubview(self.control)
        self.control.isHidden = true
        initUI()
        
        changeCallType(now: true)
        self.nameTextField.text = profileViewModel.getSelfEntity()!.firstname
        self.phoneNumberTextField.text = profileViewModel.getSelfEntity()!.phone
        dateFormatter.dateFormat = "HH:mm"
        configTimeTextFields()
        self.timePickerDidChange(self.selectTimePicker)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.control.frame = self.scrollView.bounds
    }
    
    private func configTimeTextFields() {
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: Calendar.Component.minute, value: 30, to: startDate)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        startCallTextField.text = dateFormatter.string(from: startDate)
        endCallTextField.text = dateFormatter.string(from: endDate)
    }
    
    func initUI(){
        sendButton.layer.masksToBounds = true
        self.sendButton.layer.borderWidth = 1.0
        self.sendButton.layer.cornerRadius = 4.0
        self.sendButton.backgroundColor = .white
        self.sendButton.layer.borderColor = UIColor(red: 221.0/255.0, green: 0.0/255.0, blue: 28.0/255.0, alpha: 1.0).cgColor
        //.setClearBackgrounStyle()
//        self.sendButton.setTitleColor(UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0), for: .normal)
    }
    
    func changeCallType(now: Bool){
        endCallTextField.backgroundColor = .white
        startCallTextField.backgroundColor = .white
        isCallNow = now
        callMeNowTextField.text = now ? callMeNowTextField.placeholder : ""
        callMeInTimeTextField.text = now ? "" : callMeInTimeTextField.placeholder
        constaraintHeightTimerView.constant = now ? 0 : 120
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func send(_ sender: Any) {
        var errors = ""
        var isError = false
        if nameTextField.text?.count == 0 {
            errors = "Укажите имя"
            isError = true
        }
        if phoneNumberTextField.text?.count == 0 {
            errors = "\(errors)\(isError ? "\n" : "")Укажите номер телефона"
            isError = true
        }
        if !isCallNow && (endTime == nil){
            errors = "\(errors)\(isError ? "\n" : "")Укажите в какое время Вы будете ожидать звонка"
            isError = true
        }
        if !isCallNow && endTime != nil{
            if startTime > endTime!{
                errors = "\(errors)\(isError ? "\n" : "")Укажите в какое время Вы будете ожидать звонка"
                isError = true
            }
        }
        if isError{
            let alert = UIAlertController(title: "", message: errors, preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        } else {
            networkProvider.request(ApiManager.orderCallBack(uid: (profileViewModel.getSelfEntity()?.uid)!,
                                                             serviceID: "",
                                                             username: nameTextField.text!,
                                                             phone: phoneNumberTextField.text!,
                                                             comment: commentTextField.text!,
                                                             timeFrom: startCallTextField.text!,
                                                             timeTo: endCallTextField.text!)) { [weak self] (result) in
                                                                switch result {
                                                                case .success(let response):
                                                                    do {
                                                                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                                                                           if json["success"] as? Bool == true {
                                                                            let alert = UIAlertController(title: "", message: "Ваша заявка принята, ожидайте звонка от консультанта", preferredStyle: .alert)
                                                                            alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { (_) in
                                                                                self?.navigationController?.popViewController(animated: true)
                                                                            }))
                                                                            self?.present(alert, animated: true, completion: nil)
                                                                            } else if let errors = json["errors"] as? [[String: AnyObject]], errors.count > 0 {
                                                                            self?.showAlertMessage(title: "", message: errors.first!["data"] as! String)
                                                                            }
                                                                        }
                                                                    } catch {
                                                                            self?.showAlertMessage(title: "", message: "При заказе звонка произошла ошибка")
                                                                        }
                                                                    
                                                                
                                                                case .failure(let error):
                                                                    self?.showSmart24Error(error: error)
                                                                }
            }
            
        }
    }
    
    @IBAction func callMeNow(_ sender: Any) {
        self.bottomCheckMark.isHidden = true
        self.topCheckMark.isHidden = false
        changeCallType(now: true)
    }
    
    @IBAction func callMeAtTime(_ sender: Any) {
        self.topCheckMark.isHidden = true
        self.bottomCheckMark.isHidden = false
        changeCallType(now: false)
    }
    
    @IBAction func selectStartTime(_ sender: Any) {
        selectTimePicker.minimumDate = Date()
        startCallTextField.backgroundColor = UIColor(red: 0.8, green: 0.95, blue: 0.8, alpha: 1.0)
        endCallTextField.backgroundColor = .white
        isSelectStartTime = true
        constaraintHeightTimerView.constant = 220
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func selectEndTime(_ sender: Any) {
        if startTime != nil{
            selectTimePicker.minimumDate = startTime
        }
        else{
            selectTimePicker.minimumDate = Date()
        }
        isSelectStartTime = false
        endCallTextField.backgroundColor = UIColor(red: 0.8, green: 0.95, blue: 0.8, alpha: 1.0)
        startCallTextField.backgroundColor = .white
        constaraintHeightTimerView.constant = 220
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func timePickerDidChange(_ sender: UIDatePicker) {
        let selectDate = sender.date
        if isSelectStartTime{
            startTime = selectDate
            startCallTextField.text = dateFormatter.string(from: selectDate)
        } else {
            endTime = selectDate
            endCallTextField.text = dateFormatter.string(from: selectDate)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nameTextField.resignFirstResponder()
        self.phoneNumberTextField.resignFirstResponder()
        self.commentTextField.resignFirstResponder()
        self.callMeNowTextField.resignFirstResponder()
        self.callMeInTimeTextField.resignFirstResponder()
        self.startCallTextField.resignFirstResponder()
        self.endCallTextField.resignFirstResponder()
        self.control.isHidden = true
        return true
    }


}
