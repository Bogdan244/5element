//
//  ChangeUserDataViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/30/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveCocoa
import ReactiveSwift
import ObjectMapper
import enum Result.NoError
import RealmSwift

class ChangeUserDataViewModel: GeneralViewModel {
    
    var userAvatarImage: UIImage?
    var cocoaActionChangeData: CocoaAction<Any>!
    var changeDataSignalProducer: Action<(), Any, ErrorEntity>!
    var inputValidData = MutableProperty<Bool>(false);
    
    let nameProperty            = MutableProperty<String?>("")
    let surnameProperty         = MutableProperty<String?>("")
    let phoneProperty           = MutableProperty<String?>("")
    let passwordProperty        = MutableProperty<String?>("")
    let confirmPasswordProperty = MutableProperty<String?>("")
    let otpProperty             = MutableProperty<String?>(nil)
    
    var nameSignalProducer: SignalProducer<Bool, Never>!
    var surnameSignalProducer: SignalProducer<Bool, Never>!
    var phoneSignalProducer: SignalProducer<Bool, Never>!
    var passwordSignalProducer: SignalProducer<Bool, Never>!
    var confirmPaswordSignalProducer: SignalProducer<Bool, Never>!
    
    private let errorFactory = ErrorFactory()
    
    override func configureSignals() {
        
        // MARK Register Producers
        nameSignalProducer = nameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        surnameSignalProducer = surnameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 || value == "" else {
                    return false
                }
                return true
        }
        
        passwordSignalProducer = passwordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 32 || value == "" else {
                    return false
                }
                return true
        }
        
        confirmPaswordSignalProducer = confirmPasswordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value == self.passwordProperty.value || value == "" else {
                    return false
                }
                return true
        }
        
        inputValidData <~ SignalProducer.combineLatest([nameSignalProducer, surnameSignalProducer, phoneSignalProducer, passwordSignalProducer, confirmPaswordSignalProducer])
            .flatMap((.latest)) { credentionals in
                return SignalProducer<Bool, Never> { [weak self] observer, _ in
                    guard let strongSelf = self else { return }
                    
                    if strongSelf.isTextFieldEmpty() && !credentionals.contains(false){
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
        }
        
        // MARK Registration Action
        
        changeDataSignalProducer = Action<(), Any, ErrorEntity> { [unowned self] _ in
            return self.changeUserData()
        }
        
        cocoaActionChangeData = CocoaAction(changeDataSignalProducer, input:())
    }
    
    var validateUserData: (isValid: Bool, errorText: String) {
        var isValid = true
        var text = ""
        if let surname = surnameProperty.value, surname.count == 0 {
            isValid = false
            text += "Фамилия обязательное поле\n"
        }
        if let name = nameProperty.value, name.count == 0 {
            isValid = false
            text += "Имя обязательное поле\n"
        }
        if let phone = phoneProperty.value, phone.count == 0 {
            isValid = false
            text += "Номер телефона обязательное поле"
        }
        
        return (isValid, text)
    }
    
    func changeUserData() -> SignalProducer<Any, ErrorEntity> {
        if !validateUserData.isValid {
            return SignalProducer<Any, ErrorEntity> { [weak self] observer, disposable in
                observer.send(error: ErrorEntity(text: (self?.validateUserData.errorText)!))
            }
        }
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let name            = strongSelf.nameProperty.value!
            let surname         = strongSelf.surnameProperty.value!
            var phone: String?  = strongSelf.phoneProperty.value!.replacingOccurrences(of: "+", with: "")
            let currentPhone    = self?.getSelfEntity()?.phone.replacingOccurrences(of: "+", with: "")
            let password        = strongSelf.passwordProperty.value!
        //    let confirmPassword = strongSelf.confirmPasswordProperty.value!
        //    let stringUserId    = String(strongSelf.currentId)
            let otp             = strongSelf.otpProperty.value
            var base64ImageData: String?
            
            if phone == currentPhone {
                phone = nil
            }
            
            if let userImage = strongSelf.userAvatarImage, let imageData = userImage.pngData() {
                base64ImageData = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            
//            networkProvider.request(ApiManager.editUserData(firstname: name,
//                                                            surname: surname,
//                                                            password: password,
//                                                            photo: base64ImageData,
//                                                            phone: phone,
//                                                            otp: otp),
//                                    completion: { (result) in
//                switch result {
//                case .success(let response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//                            
//                            if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                                if let _ = errors.first(where: {$0["code"] as? Int == 1014 }) {
//                                    observer.send(value: BPLoginAction.needSMSConfirm)
//                                } else {
//                                    observer.send(error: ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError)
//                                }
//                            } else if response.statusCode == 200 {
//                                strongSelf.updateUserInfo().start()
//                                observer.sendCompleted()
//                            } else {
//                                observer.send(error: ErrorFactory.serverError)
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(let error):
//                    print(error)
//                }
//            })
            
//            networkProvider.request(.changeUserData(stringUserId, base64ImageData, name, surname, phone, password, confirmPassword)) { result in
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String:AnyObject] {
//
//                            if let result = json[MapperKey.success] as? Bool, result == true {
//                                self?.updateUserInfo()
//                                    .start { events in
//                                        switch events {
//                                        case .completed:
//                                            observer.sendCompleted()
//                                        case .interrupted:
//                                            observer.sendInterrupted()
//                                        case .failed(let error):
//                                            observer.send(error: error)
//                                        case .value(_):
//                                            break
//                                        }
//                                }
//                            } else {
//                                if let error = self?.errorFactory.createEntity(dictionary: json) {
//                                    observer.send(error: error)
//                                } else {
//                                    observer.sendInterrupted()
//                                }
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
        }
        
    }
    
    private func updateUserInfo() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let stringUserId    = String(strongSelf.currentId)
            
            networkProvider.request(ApiManager.userInfo(), completion: { (result) in
                switch(result) {
                case .success(let response):
//                    do {
//                        let item = try response.map(UserEntity.self)
//                    } catch {
//                        print(error)
//                    }
                    guard let item = try? response.map(UserEntity.self), let user = self?.getSelfEntity() else { return }
                    RealmResultsCache().change(object: user, changes: { (user) in
                        user.firstname = item.firstname
                        user.surname = item.surname
                        user.phone = item.phone
                        user.userPhoto = item.userPhoto
                    })
                case .failure(let error):
                    print(error)
                }
            })
            
//            networkProvider.request(.getUserInfo(stringUserId)) { result in
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String:AnyObject] {
//                            if let result = json[MapperKey.success] as? Bool, result == true {
//
////                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
////                                    return
////                                }
//
////                                item.uid = stringUserId
////                                self?.setDataInDB(data: item)
//
//                                observer.sendCompleted()
//                            } else {
//                                if let error = self?.errorFactory.createEntity(dictionary: json) {
//                                    observer.send(error: error)
//                                } else {
//                                    observer.sendInterrupted()
//                                }
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
        }
    }
    
    private func isTextFieldEmpty() -> Bool {
        if  self.nameProperty.value! != "" &&
            self.surnameProperty.value! != ""
//            && self.phoneProperty.value! != ""
        {
            return true
        } else {
            return false
        }
    }
}
