//
//  ProfileViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveSwift
import ObjectMapper
import RealmSwift

class ProfileViewModel: GeneralViewModel {
    
    var notifications: ReactiveSwift.Property<[NotificationEntity]?> { return Property(_notifications) }
    var activeServices: ReactiveSwift.Property<[ActiveServicesEntity]?> { return Property(_activeServices) }
    var activeServicesProfile: ReactiveSwift.Property<[ActiveServicesEntity]?> { return Property(_activeServicesProfile) }
    var bonuses: ReactiveSwift.Property<BonusesEntity?> { return Property(_bonuses) }
    
    var statuses : [String:AnyObject]?
    var history : [String:AnyObject]?
    
    private var _notifications = MutableProperty<[NotificationEntity]?>(nil)
    private var _activeServices = MutableProperty<[ActiveServicesEntity]?>(nil)
    private var _activeServicesProfile = MutableProperty<[ActiveServicesEntity]?>(nil)
    private var _bonuses = MutableProperty<BonusesEntity?>(nil)
    private let errorFactory = ErrorFactory()
    
    func getNotifications(userId: String) -> SignalProducer<NotificationEntity, ErrorEntity> {
        
        return SignalProducer<NotificationEntity, ErrorEntity> { [weak self]
            observer, disposable in
            
            self?._notifications.value = self?.getNotifications()
            
            networkProvider.request(.notifications(userId)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let jsonArray = json[MapperKey.notifications] as? [AnyObject] {
                                
                                guard let items = Mapper<NotificationEntity>().mapArray(JSONObject: jsonArray) else {
                                    return
                                }
                                
                                //MARK:- add new messages in DB
                                items.forEach { [weak self] element in
                                    if self?._notifications.value == nil {
                                        self?.setDataInDB(data: element)
                                        self?._notifications.value = [element]
                                    } else if let isContains = self?._notifications.value?.contains(element), isContains == false {
                                        self?.setDataInDB(data: element)
                                        self?._notifications.value?.append(element)
                                    }
                                }
                                
                                observer.sendCompleted()
                                
                            } else {
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func postHistory(uid: String, username: String, phone: String, comment: String, serviceId: Int) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.post_history(uid,username,phone,comment,serviceId)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                debugPrint("success activation")
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func rate(uid: String, requestId: Int, rating: Int, comment: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.rate(uid,comment,requestId,rating)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        observer.sendCompleted()
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func activateService(userId: String, activationCode: String) -> SignalProducer<Bool, ErrorEntity> {
        
        return SignalProducer<Bool, ErrorEntity> { observer, disposable in
            
            networkProvider.request(.activateService(userId, activationCode)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let gift = json[MapperKey.gift] as? Bool {
                                observer.send(value: gift)
                            }
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                debugPrint("success activation")
                                observer.sendCompleted()
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func activateServiceProfile(userId: String, activationCode: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.activateServiceProfile(userId, activationCode)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                debugPrint("success activation")
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func getStatuses(userId: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            networkProvider.request(.getStatus(userId)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
//                                guard let jsonArray = json[MapperKey.services] as? [AnyObject],
//                                    let item = Mapper<ActiveServicesEntity>().mapArray(JSONObject: jsonArray) else {
//                                        observer.sendInterrupted()
//                                        return
//                                }
                                
                                self?.statuses = json
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func getActiveServicesProfile(userId: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            networkProvider.request(.getActiveServicesProfile(userId, density)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                guard let jsonArray = json[MapperKey.services] as? [AnyObject],
                                    let item = Mapper<ActiveServicesEntity>().mapArray(JSONObject: jsonArray) else {
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                self?._activeServicesProfile.value = item
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func getActiveServices(userId: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            networkProvider.request(.getActiveServices(userId, density)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                guard let jsonArray = json[MapperKey.services] as? [AnyObject],
                                    let item = Mapper<ActiveServicesEntity>().mapArray(JSONObject: jsonArray) else {
                                        observer.sendInterrupted()
                                        return
                                }

                                self?._activeServices.value = item
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func getHistory(uid: String, serviceId: Int) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            networkProvider.request(.history(uid, serviceId)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                guard let jsonArray = json["requests"] as? [[String:AnyObject]] else {
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                self?.history = jsonArray.first
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func syncNotifications(userId: Int, notificationId: Int, action: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.syncNotifications(userId, notificationId, action)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func deleteNotification(userId: Int, notificationId: Int, action: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            networkProvider.request(.syncNotifications(userId, notificationId, action)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                self?.deleteNotificationFromDB(notificationId: String(notificationId))
                                observer.sendCompleted()
                                
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func readNotification(message: NotificationEntity) {
        do {
            let realm = try Realm()
            try realm.write {
                message.readStatus = true
                realm.add(message, update: .all) 
            }
        } catch {}
    }
    
    func unreadMessagesCount() -> Int? {
        return _notifications.value?.filter({ !$0.readStatus }).count
    }
    
    private func deleteNotificationFromDB(notificationId: String) {
        do {
            let realm = try Realm()
            if let object = realm.object(ofType: NotificationEntity.self, forPrimaryKey: notificationId) {
                try realm.write {
                    realm.delete(object)
                }
            }
        } catch { }
        
        _notifications.value = getNotifications()
    }
    
    private func getNotifications() -> [NotificationEntity]? {
        do {
            let realm = try Realm()
            let notificationsEntity = realm.objects(NotificationEntity.self).toArray(ofType: NotificationEntity.self)
            return notificationsEntity.count > 0 ? notificationsEntity : nil
        } catch {
            return nil
        }
    }
    
    func getGifts() -> SignalProducer<[GiftEntity], ErrorEntity> {
        
        return SignalProducer<[GiftEntity], ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.getGifts(), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    guard let jsonArray = json[MapperKey.types] as? [AnyObject],
                                        let item = Mapper<GiftEntity>().mapArray(JSONObject: jsonArray) else {
                                            observer.sendInterrupted()
                                            return
                                    }
                                    observer.send(value: item)
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }

            })
            
        }
    }
    
    func activateGift(id: Int) -> SignalProducer<GiftCodeEntity, ErrorEntity> {
        
        return SignalProducer<GiftCodeEntity, ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.activateGiftCode(id), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                   guard let item = Mapper<GiftCodeEntity>().map(JSONObject: json) else {
                                            observer.sendInterrupted()
                                            return
                                    }
                                    observer.send(value: item)
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
                
            })
            
        }
    }
    
    func allUserGifts() -> SignalProducer<[GiftCodeUserEntity], ErrorEntity> {
        
        return SignalProducer<[GiftCodeUserEntity], ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.allUserGifts(), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    guard let jsonArray = json[MapperKey.data],
                                        let item = Mapper<GiftCodeUserEntity>().mapArray(JSONObject: jsonArray) else {
                                        observer.sendInterrupted()
                                        return
                                    }
                                    observer.send(value: item)
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
                
            })
            
        }
    }
    
    func viewGiftCode(code: Int) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.viewGiftCode(code), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
        }
    }
    
    func clearGifts() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.clearGifts(), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
        }
    }
    
    func showMartletCodes() -> SignalProducer<[MartletCodeEntity], ErrorEntity> {
        
        return SignalProducer<[MartletCodeEntity],ErrorEntity> { observer, disposable in
            
            networkProvider.request(ApiManager.showMartletCode(), completion: { (result) in
                switch (result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    guard let jsonArray = json[MapperKey.data],
                                        let item = Mapper<MartletCodeEntity>().mapArray(JSONObject: jsonArray) else {
                                            observer.sendInterrupted()
                                            return
                                    }
                                    observer.send(value: item)
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
        }
    }
    
    func activateMartletCode(code: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer,disposable in
            
            networkProvider.request(ApiManager.activateMartletCode(code), completion: { (result) in
                switch (result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let error = self.errorFactory.createEntity(dictionary: json) {
                                observer.send(error: error)
                            } else {
                                observer.sendCompleted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            })
        }
    }
}
