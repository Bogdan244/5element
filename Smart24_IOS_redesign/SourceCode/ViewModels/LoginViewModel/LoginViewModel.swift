//
//  LoginViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveCocoa
import ReactiveSwift
import SwiftKeychainWrapper
import ObjectMapper
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

import RealmSwift
import enum Result.NoError

class LoginViewModel: GeneralViewModel {
    
    var cocoaActionLogin: CocoaAction<Any>!
    var loginSignalProducer: Action<(), Any, ErrorEntity>!
    
    let userPhoneProperty = MutableProperty<String?>(nil)
    let userPasswordProperty = MutableProperty<String?>(nil)
    let loginInputValid = MutableProperty<Bool>(false)
    let otpCode = MutableProperty<String?>(nil)
    let phoneSocial = MutableProperty<String?>(nil)
    
    let (loginSignal, loginObserver) = Signal<Any, ErrorEntity>.pipe()
    
    private let errorFactory = ErrorFactory()
    
    override func configureSignals() {
        let userPhoneSignalProducer = userPhoneProperty
            .producer
            .map { (text: String?) -> String in
                if let returnValue = text {
                    return returnValue
                }
                return ""
        }
        
        let userPassworSignalProducer = userPasswordProperty
            .producer
            .map { (text: String?) -> String in
                if let returnValue = text {
                    return returnValue
                }
                return ""
        }
        
        loginInputValid <~ SignalProducer.combineLatest([userPhoneSignalProducer, userPassworSignalProducer])
            .flatMap(.latest) { credentionals in
                return SignalProducer<Bool, Never> { observer, disposable in
                    
                    if let phone = credentionals.first, phone.count == 13,
                        let password = credentionals.last, password.count >= 1 {
                        observer.send(value: true)
                    } else if let phone = self.userPhoneProperty.value, phone.count == 13,
                        let password = self.userPasswordProperty.value, password.count >= 1 {
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
        }
        
        // MARK Login Action
        loginSignalProducer = Action<(), Any, ErrorEntity>(enabledIf: loginInputValid) { [unowned self] _ in
            return self.loginUser(otpCode: self.otpCode.value)
        }
        
        cocoaActionLogin = CocoaAction(loginSignalProducer, input:())
    }
    
    func loginUser(otpCode: String?) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.userPhoneProperty.value!.replacingOccurrences(of: "+", with: "")
            let password = strongSelf.userPasswordProperty.value!
            
//            networkProvider.request(.login(phone, password, otpCode)) { [weak self] result in
//                
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//                            
//                            if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                                if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                                    observer.send(value: BPLoginAction.needSMSConfirm)
//                                } else {
//                                    observer.send(error: ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError)
//                                }
//                            } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
//                                UserDefaults.standard.set(phone, forKey: Constants.savedPhone)
//                                UserDefaults.standard.set(password, forKey: Constants.savedPassword)
//                                
//                                //MARK:- remove DB data if it isn't current user
//                                if let _ = self?.currentId {
//                                    do {
//                                        let realm = try Realm()
//                                        try realm.write {
//                                            realm.deleteAll()
//                                        }
//                                    } catch {
//                                        debugPrint("can't delete data from DB")
//                                    }
//                                }
//                                
//                                KeychainWrapper.standard.set(item.uid, forKey: Constants.currentUserId)
//                                self?.setDataInDB(data: item)
//                                
//                                observer.send(value: item)
//                                observer.sendCompleted()
//                                
//                                self?.loginObserver.send(value: ())
//                                self?.openContainerVC()
//                            } else {
//                                observer.send(error: ErrorFactory.serverError)
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
        }
    }
    
//    func registrationWithFacebook(viewController: UIViewController) {
//
//        GraphRequest(graphPath: Constants.fbGraphPath, parameters: Constants.fbParameters).start { [weak self] (_, result) in
//            switch result {
//            case .success(let response):
//                if let json = response.dictionaryValue, let item = Mapper<FaceBookEntity>().map(JSON: json) {
//
//                    self?.authWithSocial(socialName: SocialName.facebook, accesToken: , serverAuthCode: <#T##String?#>, phone: <#T##String?#>, otp: <#T##String?#>, fcmToken: <#T##String?#>)
//                    self?.registrationWithSocial(photo: item.photo, username: item.name, type: SocialType.facebook.rawValue, id: item.id)
//                        .start { [weak self] event in
//                            if event.isCompleted {
//                                self?.loginWithFacebook(viewController: viewController)
//                            } else {
//                                self?.loginObserver.sendInterrupted()
//                            }
//                    }
//                }
//
//            case .failed(let error):
//                debugPrint("registrationWithFacebook Request Failed: \(error)")
//            }
//        }
//    }
    
    func loginWithFacebook(viewController: UIViewController, loginResult: @escaping LoginManagerLoginResultBlock) {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["publish_actions"], from: viewController, handler: loginResult)
    }
    
//    func loginWithVkontakte(userId: String?) -> SignalProducer<UserEntity, ErrorEntity>  {
//        
//        return SignalProducer<UserEntity, ErrorEntity> { [weak self] observer, disposable in
//            guard let userId = userId, let strongSelf = self else {
//                return
//            }
//            
//            VK.API.Users.get([VK.Arg.userId: userId, VK.Arg.fields: Constants.vkAvatarSize]).send(
//                onSuccess: { response in
//                    
//                    if let json = response.arrayObject?.first as? [String: AnyObject] , let item = Mapper<VKEntity>().map(JSON: json) {
//                        strongSelf.registrationWithSocial(photo: item.photo_50,
//                                                          username: item.getFullName(),
//                                                          type: SocialType.vk.rawValue,
//                                                          id: item.id).startWithSignal { signal, _ in
//                                                            signal.observeCompleted {
//                                                                observer.sendCompleted()
//                                                            }
//                        }
//                    } else {
//                        debugPrint("can't parse data")
//                    }
//            },
//                onError: { _ in
//                    debugPrint("incorrect vk data")}
//            )
//        }
//    }
    
    func authWithSocial(socialName: SocialName,
                        accessToken: String? = nil,
                        serverAuthCode: String? = nil,
                        phone: String? = nil,
                        otp: String? = nil) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
        
//            networkProvider.request(.authWithSocial(socialName: socialName,
//                                                    accessToken: accessToken,
//                                                    serverAuthCode: serverAuthCode,
//                                                    phone: phone,
//                                                    otp: otp),
//                                    completion: { (result) in
//                                        switch result {
//                                        case .success(let response):
//                                            do {
//                                                if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//                                                    
//                                                    if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                                                        if let _ = errors.first(where: {$0["code"] as? Int == 1016 }) {
//                                                            observer.send(value: BPLoginAction.needPhoneNumber)
//                                                        } else if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                                                            observer.send(value: BPLoginAction.needSMSConfirm)
//                                                        } else {
//                                                            observer.send(error: ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError)
//                                                        }
//                                                    } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
//                                                        UserDefaults.standard.set(phone, forKey: Constants.savedPhone)
//                                                        
//                                                        
//                                                        //MARK:- remove DB data if it isn't current user
//                                                        if let _ = self?.currentId {
//                                                            do {
//                                                                let realm = try Realm()
//                                                                try realm.write {
//                                                                    realm.deleteAll()
//                                                                }
//                                                            } catch {
//                                                                debugPrint("can't delete data from DB")
//                                                            }
//                                                        }
//                                                        
//                                                        KeychainWrapper.standard.set(item.uid, forKey: Constants.currentUserId)
//                                                        self?.setDataInDB(data: item)
//                                                        
//                                                        observer.send(value: item)
//                                                        observer.sendCompleted()
//                                                        
//                                                        self?.loginObserver.send(value: ())
//                                                        self?.openContainerVC()
//                                                    } else {
//                                                        observer.send(error: ErrorFactory.serverError)
//                                                    }
//                                                }
//                                            } catch {
//                                                observer.sendInterrupted()
//                                            }
//                                        case .failure(let error):
//                                            print(error)
//                                        }
//            })
        }
    }
    
//    func loginWithSocial(type: Int, id: String?) -> SignalProducer<UserEntity, ErrorEntity> {
//
//        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
//            observer, disposable in
//
//            guard let id = id else { return }
//
//            networkProvider.request(.loginWithSocial(type, id)) { [weak self] result in
//
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//
//                            if let status = json[MapperKey.success] as? Bool, status == true,
//                                let userId = json[MapperKey.uid] as? String {
//
////                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
////                                    return
////                                }
//
//                                //MARK:- remove DB data if it isn't current user
//                                if let currentId = self?.currentId, String(currentId) != userId {
//
//                                    KeychainWrapper.standard.removeObject(forKey: Constants.expertCallId)
//                                    KeychainWrapper.standard.removeObject(forKey: Constants.dialog_id)
//                                    KeychainWrapper.standard.removeObject(forKey: Constants.timestamp)
//                                    do {
//                                        let realm = try Realm()
//                                        try realm.write {
//                                            realm.deleteAll()
//                                        }
//                                    } catch {
//                                        debugPrint("can't delete data from DB")
//                                    }
//                                }
//
//                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
//                              //  self?.setDataInDB(data: item)
//
//                                observer.sendCompleted()
//
//                                self?.loginObserver.send(value: ())
//                                self?.openContainerVC()
//
//                            } else {
//                                if let error = self?.errorFactory.createEntity(dictionary: json),
//                                    error.code == Constants.userNotFoundError {
//                                    observer.send(error: error)
//                                } else {
//                                    observer.sendInterrupted()
//                                }
//
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
//        }
//    }
    
//    func registrationWithSocial(photo: String, username: String, type: Int, id: String) -> SignalProducer<UserEntity, ErrorEntity> {
//
//        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
//            observer, disposable in
//
//            networkProvider.request(.registerWithSocial(photo, username, type, id)) { [weak self] result in
//
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//
//                            if let status = json[MapperKey.success] as? Bool, status == true,
//                                let userId = json[MapperKey.uid] as? String {
//
//                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
//
////                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
////                                    return
////                                }
//
//                              //  observer.send(value: item)
//                                observer.sendCompleted()
//
//                                self?.loginObserver.send(value: ())
//                                self?.openContainerVC()
//                            } else {
//                                observer.sendInterrupted()
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
//        }
//    }

}
