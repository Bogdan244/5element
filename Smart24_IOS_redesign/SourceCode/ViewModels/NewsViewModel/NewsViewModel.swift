//
//  NewsViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveSwift
import ObjectMapper
import Moya

class NewsViewModel: GeneralViewModel {
    
    var news: ReactiveSwift.Property<[NewsEntity]?> { return Property(_news) }
    var closeRequest: Cancellable?
    
    private var _news = MutableProperty<[NewsEntity]?>(nil)
    
    func getNews(userId: String) -> SignalProducer<NewsEntity, ErrorEntity> {
        
        return SignalProducer<NewsEntity, ErrorEntity> { [weak self]
            observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            self?.closeRequest = networkProvider.request(.news(userId, density)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let jsonArray = json[MapperKey.news] as? [AnyObject] {
                                
                                guard let items = Mapper<NewsEntity>().mapArray(JSONObject: jsonArray) else {
                                    return
                                }
                                
                                self?._news.value = items
                                observer.sendCompleted()
                                
                            } else {
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
}
