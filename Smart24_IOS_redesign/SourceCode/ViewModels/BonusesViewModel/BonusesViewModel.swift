//
//  BonusesViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 14.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa
import ObjectMapper

class BonusesViewModel: GeneralViewModel {
    
    var cocoaActionActivate: CocoaAction<Any>!
    
    let cardNumberProperty = MutableProperty<String?>(nil)
    let phoneProperty = MutableProperty<String?>(nil)
    
    var bonuses: ReactiveSwift.Property<BonusesEntity?> { return Property(_bonuses) }
    
    private var _bonuses = MutableProperty<BonusesEntity?>(nil)
    private let errorFactory = ErrorFactory()

    func getBonuses() -> SignalProducer<BonusesEntity?, ErrorEntity> {
        
        return SignalProducer<BonusesEntity?, ErrorEntity> { [weak self] observer, disposable in
            
            networkProvider.request(ApiManager.getAllBonuses(), completion: { [weak self] (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                guard let jsonArray = json[MapperKey.cards] as? [AnyObject],
                                    let item = Mapper<BonusesEntity>().mapArray(JSONObject: jsonArray) else {
                                        observer.sendInterrupted()
                                        return
                                }
                                observer.send(value: item.first)
                                observer.sendCompleted()
                            } else {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
        }
    }
    
    func activateBonuseSignal() -> SignalProducer<BonusesEntity?, ErrorEntity> {
        
        return SignalProducer<BonusesEntity?, ErrorEntity> { [weak self] observer, disposable in
            
            guard let card = self?.cardNumberProperty.value else {
                observer.sendInterrupted()
                return
            }
            guard let phone = self?.phoneProperty.value else {
                observer.sendInterrupted()
                return
            }
            
            networkProvider.request(ApiManager.addNewBonus(card, phone), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                if let error = self?.errorFactory.createEntity(dictionary: json) {
                                    observer.send(error: error)
                                } else {
                                    guard let jsonArray = json[MapperKey.cards] as? [AnyObject],
                                        let item = Mapper<BonusesEntity>().mapArray(JSONObject: jsonArray) else {
                                            observer.sendInterrupted()
                                            return
                                    }
                                    observer.send(value: item.first)
                                    observer.sendCompleted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
        }
    }
    
    func deactivateBonuseSignal(id: Int) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
         
            networkProvider.request(ApiManager.deactivateBonuse(id), completion: { (result) in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                observer.sendCompleted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    break
                }
            })
            
        }
    }
}
