//
//  GeneralViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveSwift
import RealmSwift
import SwiftKeychainWrapper
import enum Result.NoError

protocol ViewModelTargetType {
    func configureSignals()
    
    var currentId: Int { get set }
    var dialogId: String? { get set }
    var dialogTimestamp: Int? { get set }
    var expertCallId: String? { get set }
    var lastBackupDate: Int? { get set }
}

extension ViewModelTargetType {
    
    var currentId: Int {
        
        get {
            guard let value = KeychainWrapper.standard.string(forKey: Constants.currentUserId) else {
                return 0
            }
            guard let returnValue = Int(value) else {
                return 0
            }
            return returnValue
        }
        
        set {
            self.currentId = newValue
        }
    }
    
    var dialogId: String? {
        
        get {
            guard let value = KeychainWrapper.standard.string(forKey: Constants.dialog_id) else {
                return nil
            }
            return value
        }
        
        set {
            self.dialogId = newValue
        }
    }
    
    var dialogTimestamp: Int? {
        
        get {
            guard let value = KeychainWrapper.standard.integer(forKey: Constants.timestamp) else {
                return nil
            }
            return value
        }
        
        set {
            self.dialogTimestamp = newValue
        }
    }
    
    var expertCallId: String? {
        
        get {
            guard let value = KeychainWrapper.standard.string(forKey: Constants.expertCallId) else {
                return nil
            }
            return value
        }
        
        set {
            self.expertCallId = newValue
        }
    }
    
    var lastBackupDate: Int? {
        
        get {
            guard let value = KeychainWrapper.standard.integer(forKey: Constants.lastBackupDate) else {
                return nil
            }
            return value
        }
        
        set {
            self.lastBackupDate = newValue
        }
    }
}

protocol NavigationStackProtocol {
    
    func presentLoginVC()
    func presentChatVC()
    func backToPreviousVC()
    func openRootVC()
    
    func openLoginVC()
    func openRegistrationVC()
    func openForgotPasswordVC()
    func openContainerVC()
    func openNewsVC()
    func openUtilityVC()
    func openProfileVC()
    func openNotificationVC(message: NotificationEntity)
    func openChangeDataVC()
    func showAlertController(alertViewController: UIAlertController)
    func openBackupVC()
    func openChatVC()
    func openHowItWorkTBC()
    func openDevelopersVC()
    func openBackupProgressVC(status: BackupState)
    func openBackupPerionVC()
    func openMakeCallVC(callId: String?)
    func openNewsDescriptionVC(news: NewsEntity)
    func openVideoVC(call: VSLCall?)
    func openMainVC()
    func openNewContainer()
    func openChattingVC()
    func openCloseChattingVC(complition: (() -> ())?) 
}

class GeneralViewModel: ViewModelTargetType, NavigationStackProtocol {
    
    lazy var presenter: Presenter = PresenterImpl()
    let expertId                  = "0"
    
    init() {
        configureSignals()
    }
    
    func racTextProducer(textField: UITextField) -> Signal<String, Never> {
        return textField.reactive.textValues
    }
    
    func configureSignals(){}
    
    func getSelfEntity() -> UserEntity? {
        do {
            let realm = try Realm()
            let selfEntity = realm.object(ofType: UserEntity.self, forPrimaryKey: Constants.userPrimaryKey)
            return selfEntity
        } catch {
            return nil
        }
    }
    
    func setDataInDB(data: Object) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(data, update: .all)
            }
        } catch {}
    }
    
    func openLoginVC() {
        presenter.openLoginVC()
    }
    
    func openRegistrationVC() {
        presenter.openRegistrationVC()
    }
    
    func openForgotPasswordVC() {
        presenter.openForgotPasswordVC()
    }
    
    func backToPreviousVC() {
        presenter.backToPreviousVC()
    }
    
    func openContainerVC() {
        presenter.openContainerVC()
    }
    
    func presentLoginVC() {
        presenter.presentLoginVC()
    }
    
    func openNewsVC() {
        presenter.openNewsVC()
    }
    
    func openUtilityVC() {
        presenter.openUtilityVC()
    }
    
    func openProfileVC() {
        presenter.openProfileVC()
    }
    
    func openNotificationVC(message: NotificationEntity) {
        presenter.openNotificationVC(message: message)
    }
    
    func openChangeDataVC() {
        presenter.openChangeDataVC()
    }
    
    func showAlertController(alertViewController: UIAlertController) {
        presenter.openAlertVC(alertViewController: alertViewController)
    }
    
    func openBackupVC() {
        presenter.openBackupVC()
    }
    
    func openChatVC() {
        presenter.openChatVC()
    }
    
    func openHowItWorkTBC() {
        presenter.openHowItWorkTBC()
    }
    
    func openDevelopersVC() {
        presenter.openDevelopersVC()
    }
    
    func openBackupProgressVC(status: BackupState) {
        presenter.openBackupProgressVC(status: status)
    }
    
    func openBackupPerionVC() {
        presenter.openBackupPerionVC()
    }
    
    func openMakeCallVC(callId: String?) {
        presenter.openMakeCallVC(callId: callId)
    }
    
    func openNewsDescriptionVC(news: NewsEntity) {
        presenter.openNewsDescriptionVC(news: news)
    }
    
    func openVideoVC(call: VSLCall?) {
        presenter.openVideoVC(call: call)
    }
    
    func openMainVC() {
        presenter.openMainVC()
    }
    
    func openRootVC() {
        presenter.openRootVC()
    }
    
    func presentChatVC() {
        presenter.presentChatVC()
    }
    
    func openActivateBonuseVC() {
        presenter.openActivateBonusVC()
    }
    
    func openBonusesVC(bonuses: BonusesEntity) {
        presenter.openBonusesVC(bonuses: bonuses)
    }
    
    func openOrderCallVC() {
        presenter.openOrderCallVC()
    }
    
    func presentGiftsVC(gifts: [Gift], didSelectGift: @escaping GiftsViewController.selectGift) {
        presenter.presentGiftsVC(gifts: gifts, didSelectGift: didSelectGift)
    }
    
    func presentGiftDetailVC(giftCodeEntity: GiftCodeEntity) {
        presenter.presentGiftDetailVC(giftCode: giftCodeEntity)
    }
    
    func openMyGiftsVC() {
        presenter.openMyGiftsVC()
    }
    
    func openMyGiftDetailVC(gift: GiftCodeUserEntity) {
        presenter.openMyGiftDetailVC(giftCode: gift)
    }
    
    func openMyServices() {
        presenter.openMyServicesVC()
    }
    
    func openMyServiceDetailVC(martletCodeEntity: MartletCodeDependencyEntity) {
        presenter.openMyServiceDetailVC(martletCodeEntity: martletCodeEntity)
    }
    
    func openRepairsStatusVC() {
        presenter.openRepairsStatusVC()
    }
    
    func openNewLoginVC() {
        presenter.openNewLoginVC()
    }
    
    func openNewRegistrationVC() {
        presenter.openNewRegistrationVC(authService: nil, user: nil, state: .registration, needBPM: false, phone: nil, password: nil)
    }
    
    func openNewForgotPasswordVC() {
        presenter.openNewForgotPassword()
    }
    
    func openNewContainer() {
        presenter.openNewContainer()
    }
    
    func openCallMeVC() {
        presenter.openCallMeVC()
    }
    
    func openChattingVC() {
        presenter.openChattingVC()
    }
    
    func openCloseChattingVC(complition: (() -> ())?) {
        presenter.openCloseChattingVC(complition: complition)
    }
    
    func openActivateBonussesVC() {
        presenter.openActivateBonusses()
    }
    
    func openActivateBonuseAlert(complition: ( (_ cardNumber: String, _ phone: String) -> ())?) {
        presenter.openActivateBonuseAlert(complition: complition)
    }
    
    func openBonussesVC(bonuses: BonusCardEntity) {
        presenter.openBonussesVC(bonuse: bonuses)
    }
}
