//
//  RegistrationViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveSwift
import ReactiveCocoa
import ObjectMapper
import RealmSwift
import SwiftKeychainWrapper
import enum Result.NoError

enum BPLoginAction {
    case needSMSConfirm
    case needPhoneNumber
}

enum BPRegistrationAction {
    case needSMS
    case wrongOTP
}

class RegistrationViewModel: GeneralViewModel {
    
    var cocoaActionRegistration: CocoaAction<Any>!
    var registrationSignalProducer: Action<(), Any, ErrorEntity>!
    var inputValidData = MutableProperty<Bool>(false)
    
    let nameProperty            = MutableProperty<String?>("")
    let surnameProperty         = MutableProperty<String?>("")
    let phoneProperty           = MutableProperty<String?>("")
    let passwordProperty        = MutableProperty<String?>("")
    let confirmPasswordProperty = MutableProperty<String?>("")
    let otp                     = MutableProperty<String?>(nil)
    
    var nameSignalProducer: SignalProducer<Bool, Never>!
    var surnameSignalProducer: SignalProducer<Bool, Never>!
    var phoneSignalProducer: SignalProducer<Bool, Never>!
    var passwordSignalProducer: SignalProducer<Bool, Never>!
    var confirmPaswordSignalProducer: SignalProducer<Bool, Never>!
    
    private let errorFactory = ErrorFactory()
    
    override func configureSignals() {
        
        // MARK Register Producers
        nameSignalProducer = nameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.count >= 1 && value.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        surnameSignalProducer = surnameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.count >= 1 && value.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.count == 13 || value == "" else {
                    return false
                }
                return true
        }
        
        passwordSignalProducer = passwordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.count >= 1 && value.count <= 32 || value == "" else {
                    return false
                }
                return true
        }
        
        confirmPaswordSignalProducer = confirmPasswordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value == self.passwordProperty.value && value.count >= 1 || value == "" else {
                    return false
                }
                return true
        }
        
        inputValidData <~ SignalProducer.combineLatest([nameSignalProducer, surnameSignalProducer, phoneSignalProducer, passwordSignalProducer, confirmPaswordSignalProducer])
            .flatMap((.latest), { credentionals in
                return SignalProducer<Bool, Never> { [weak self] observer, _ in
                    guard let strongSelf = self else { return }
                    
                    if strongSelf.isTextFieldEmpty() && !credentionals.contains(false) {
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
            })
        
        // MARK Registration Action
        registrationSignalProducer = Action<(),Any, ErrorEntity> { [unowned self] _ in
            return self.registerUser(otp: self.otp.value)
        }
        
        cocoaActionRegistration = CocoaAction(registrationSignalProducer, input:())
    }
    
    func registerUser(otp: String?) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let name            = strongSelf.nameProperty.value!
            let surname         = strongSelf.surnameProperty.value!
            let phone           = strongSelf.phoneProperty.value!.replacingOccurrences(of: "+", with: "")
            let password        = strongSelf.passwordProperty.value!
            //let confPass        = strongSelf.confirmPasswordProperty.value!
            let phoneModel      = Constants.phoneModel
            //let otp = strongSelf.otpCode.value?
            
//            networkProvider.request(.registration(name, surname, phone, password, phoneModel, otp)) { result in
//                switch(result) {
//                case let .success(response):
//                    do {
//                        let dictionary = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String:AnyObject]
//                        if let errors = dictionary!["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                            if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                                if let _ = errors.first(where: {$0["code"] as? Int == 1011 }) {
//                                    observer.send(value: BPRegistrationAction.wrongOTP)
//                                } else {
//                                    observer.send(value: BPRegistrationAction.needSMS)
//                                }
//                            } else {
//                                observer.send(error: ErrorFactory.createEntity(dictionary: dictionary!) ?? ErrorFactory.serverError)
//                            }
//                        } else {
//                            if response.statusCode == 200 {
//                                observer.sendCompleted()
//                            } else {
//                                observer.send(error: ErrorFactory.serverError)
//                            }
//                        }
//                    } catch let error {
//                        observer.send(error: error as! ErrorEntity)
//                    }
//                case let .failure(error):
//                    observer.send(error: ErrorFactory.error(text: error.localizedDescription))
//                }
//            }
        }
        
    }
    
    func loginUser(otpCode: String?) -> SignalProducer<Any, ErrorEntity> {
        
        return SignalProducer<Any, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.phoneProperty.value!.replacingOccurrences(of: "+", with: "")
            let password = strongSelf.passwordProperty.value!
            
//            networkProvider.request(.login(phone, password, otpCode)) { [weak self] result in
//                
//                switch(result) {
//                case let .success(response):
//                    do {
//                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
//                            
//                            if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
//                                if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
//                                    observer.send(value: BPLoginAction.needSMSConfirm)
//                                } else {
//                                    observer.send(error: ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError)
//                                }
//                            } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
//                                UserDefaults.standard.set(phone, forKey: Constants.savedPhone)
//                                UserDefaults.standard.set(password, forKey: Constants.savedPassword)
//                                
//                                //MARK:- remove DB data if it isn't current user
//                                if let _ = self?.currentId {
//                                    do {
//                                        let realm = try Realm()
//                                        try realm.write {
//                                            realm.deleteAll()
//                                        }
//                                    } catch {
//                                        debugPrint("can't delete data from DB")
//                                    }
//                                }
//                                
//                                KeychainWrapper.standard.set(item.uid, forKey: Constants.currentUserId)
//                                self?.setDataInDB(data: item)
//                                
//                                observer.send(value: item)
//                                observer.sendCompleted()
//                                
//                              //  self?.loginObserver.send(value: ())
//                                self?.openContainerVC()
//                            } else {
//                                observer.send(error: ErrorFactory.serverError)
//                            }
//                        }
//                    } catch {
//                        observer.sendInterrupted()
//                    }
//                case .failure(_):
//                    observer.sendInterrupted()
//                }
//            }
        }
    }
    
    private func isTextFieldEmpty() -> Bool {
        if  nameProperty.value! != "" &&
            surnameProperty.value! != "" &&
            phoneProperty.value! != "" &&
            passwordProperty.value! != "" &&
            confirmPasswordProperty.value! != "" {
            return true
        }
        
        return false
    }
}
