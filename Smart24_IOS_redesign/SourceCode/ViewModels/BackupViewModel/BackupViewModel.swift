//
//  BackupViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/1/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import ReactiveSwift
import SwiftKeychainWrapper
import iCloudDocumentSync
import Contacts

class BackupViewModel: GeneralViewModel {
    
    var isContactsSaved = false
    
    private var userContacts = [CNContact]()
    private let contactStore = CNContactStore()
    
    override func configureSignals() {
        getContacts()
    }
    
    func getInfoBackup() -> SignalProducer<Int, ErrorEntity> {
        
        return SignalProducer<Int, ErrorEntity> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let currentUserId = String(describing: strongSelf.currentId)
            
            networkProvider.request(.getInfoBackup(currentUserId)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject],
                            let status = json[MapperKey.success] as? Bool, status == true {
                            
                            guard let isBackup = json[MapperKey.isbackup] as? String, isBackup == IsBackupExist.exist.rawValue,
                                let date = json[MapperKey.date] as? Int else {
                                    observer.sendInterrupted()
                                    return
                            }
                            
                            KeychainWrapper.standard.set(date, forKey: Constants.lastBackupDate)
                            observer.send(value: date)
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    private func putInfoBackup(isBackup: Bool, date: Int) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let currentUserId = String(describing: strongSelf.currentId)
            
            networkProvider.request(.putInfoBackup(currentUserId, isBackup, date)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String: AnyObject] = try response.mapJSON() as? [String: AnyObject],
                            let status = json[MapperKey.success] as? Bool, status == true {
                            
                            observer.sendCompleted()
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func createBackupAddressBook() -> SignalProducer<(), CloudSyncType> {
        return SignalProducer<(), CloudSyncType> { [weak self] observer, disposable in
            
            if iCloud.shared().checkUbiquityContainer() {
                let backupTimeInterval = Int64(NSDate().timeIntervalSince1970)
                var contactsData = Data()
                
                do {
                    if let contacts = self?.userContacts {
                        try contactsData = CNContactVCardSerialization.data(with: contacts)
                    }
                } catch {
                    observer.sendInterrupted()
                    return
                }
                
                iCloud.shared().saveAndCloseDocument(withName: String(backupTimeInterval), withContent: contactsData) {
                    [weak self] (document, data, error) in
                    
                    if error == nil {
                        debugPrint("Document saved \(document?.fileURL)")
                        
                        self?.saveBackupDate(date: Int(backupTimeInterval))
                            .start { event in
                                if event.isCompleted {
                                    observer.sendCompleted()
                                } else {
                                    observer.sendInterrupted()
                                }
                        }
                    } else {
                        debugPrint("Document save Error")
                        observer.send(error: .error)
                    }
                }
            } else {
                observer.send(error: .unavailable)
            }
        }
    }
    
    func getLastBackup() {
        if let lastBackupDate = lastBackupDate {
            restoreLastBackupAddressBook(backupName: String(lastBackupDate))
        }
    }
    
    private func saveBackupDate(date: Int) -> SignalProducer<(), CloudSyncType> {
        return SignalProducer<(), CloudSyncType> { [weak self]
            observer, disposable in
            
            self?.putInfoBackup(isBackup: true, date: date)
                .start{ [weak self] event in
                    if event.isCompleted {
                        self?.isContactsSaved = true
                        observer.sendCompleted()
                    } else {
                        observer.send(error: .error)
                    }
            }
        }
    }
    
    private func restoreLastBackupAddressBook(backupName: String) {
        
        if iCloud.shared().checkUbiquityContainer() {
            
            iCloud.shared().retrieveCloudDocument(withName: backupName) { [weak self] (document, data, error) in
                guard let strongSelf = self, let data = data else {
                    return
                }
                
                do {
                    let icloudContacts = try CNContactVCardSerialization.contacts(with: data)
                    
                    icloudContacts.forEach { contact in
                        let isContains = strongSelf.userContacts.contains { $0.givenName == contact.givenName }
                        
                        if !isContains, let mutableContact = contact.mutableCopy() as? CNMutableContact {
                            let saveRequest = CNSaveRequest()
                            saveRequest.add(mutableContact, toContainerWithIdentifier: nil)
                            
                            do {
                                try self?.contactStore.execute(saveRequest)
                                self?.isContactsSaved = true
                                debugPrint("Success, You saved users \(contact.givenName)")
                            } catch {
                                self?.isContactsSaved = false
                            }
                        }
                    }
                } catch {
                    self?.isContactsSaved = false
                }
            }
        }
    }
    
    private func getContacts() {
        var allContainers: [CNContainer] = []
        let key = CNContactVCardSerialization.descriptorForRequiredKeys()
        
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            debugPrint("Error fetching containers")
        }
        
        // Iterate all containers and append their contacts to our results array
        userContacts.removeAll()
        allContainers.forEach { container in
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: [key])
                userContacts.append(contentsOf: containerResults)
            } catch {
                debugPrint("Error fetching results for container")
            }
        }
    }
}
