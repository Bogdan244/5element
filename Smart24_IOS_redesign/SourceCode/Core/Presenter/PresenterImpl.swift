//
//  PresenterImpl.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import BarcodeScanner

final class PresenterImpl: Presenter {
    
    private let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    private let router: Router
    
    
    init() {
        guard let topController = UIApplication.topNavigationViewController() else {
            fatalError("Couldn't init view stack")
        }
        router = RouterImpl(rootController: topController as! Smart24NavigationController);
    }
    
    func openLoginVC() {
        if let vc = R.storyboard.login.loginViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openRegistrationVC() {
        if let vc = R.storyboard.login.registrationViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openForgotPasswordVC() {
        if let vc = R.storyboard.login.forgotPasswordViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func backToPreviousVC() {
        router.popController(animated: true)
    }
    
    func openRootVC() {
        router.popToRootViewController(animated: true)
    }
    
    func openContainerVC() {
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BaseMenuViewController")
        let navigationController = Smart24NavigationController(rootViewController: controller)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        
    }
    
    func openMainVC() {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainViewController")
        router.present(controller: vc, animated: true)
    }
    
    func presentLoginVC() {
        if let vc = R.storyboard.login.loginViewController() {
            router.changeRootViewController(controller: vc)
        }
    }
    
    func openNewsVC() {
        if let vc = R.storyboard.menu.newsViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openUtilityVC() {
        if let vc = R.storyboard.menu.utilityViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openProfileVC() {
        if let vc = R.storyboard.profile.profileViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openNotificationVC(message: NotificationEntity) {
        if let vc = R.storyboard.profile.notificationViewController() {
            vc.message = message
            router.push(controller: vc, animated: true)
        }
    }
    
    func openChangeDataVC() {
        if let vc = R.storyboard.profile.changeDataViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupVC() {
        if let vc = R.storyboard.backup.backupViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openAlertVC(alertViewController: UIAlertController) {
        router.openAlertViewController(alertViewController: alertViewController)
    }
    
    func openChatVC() {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatViewController")
        router.push(controller: vc, animated: true)
        
    }
    
    func presentChatVC() {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatViewController")
        router.pushVCToRoot(viewController: vc, animated: true)
        
    }
    
    func openHowItWorkTBC() {
        if let vc = R.storyboard.howItWork.howItWorkController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openDevelopersVC() {
        if let vc = R.storyboard.menu.developersViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupProgressVC(status: BackupState) {
        if let vc = R.storyboard.backup.backupProgressViewController() {
            vc.backupStatus = status
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupPerionVC() {
        if let vc = R.storyboard.backup.backupPerionViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openMakeCallVC(callId: String?) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MakeCallViewController") as! MakeCallViewController
        if let callId = callId {
            vc.sipCallNumber = String(callId)
        }
        router.push(controller: vc, animated: true)
        
    }
    
    func openNewsDescriptionVC(news: NewsEntity) {
        if let vc = R.storyboard.menu.newsDescriptionViewController() {
            vc.news = news
            router.push(controller: vc, animated: true)
        }
    }
    
    func openVideoVC(call: VSLCall?) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
        vc.activeCall = call
        router.push(controller: vc, animated: true)
    }
    
    func openActivateBonusVC() {
        if let vc = R.storyboard.bonuses.activateBonuseViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBonusesVC(bonuses: BonusesEntity) {
        if let vc = R.storyboard.bonuses.bonusesViewController() {
            vc.bonuses = bonuses
            router.push(controller: vc, animated: true)
        }
    }
    
    func openOrderCallVC() {
        if let vc = R.storyboard.menu.orderCallViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func presentGiftsVC(gifts:[Gift], didSelectGift: @escaping GiftsViewController.selectGift) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GiftsViewController") as! GiftsViewController
        vc.gifts = gifts
        vc.didSelectGift = didSelectGift
        vc.modalPresentationStyle = .overFullScreen
        router.present(controller: vc, animated: false)
    }
    
    func presentGiftDetailVC(giftCode: GiftCodeEntity) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GiftDetailViewController") as! GiftDetailViewController
        vc.giftCode = giftCode
        vc.modalPresentationStyle = .overFullScreen
        router.present(controller: vc, animated: false)
    }

    func openMyGiftsVC() {
        if let vc = R.storyboard.menu.myGiftsViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openMyGiftDetailVC(giftCode: GiftCodeUserEntity) {
        if let vc = R.storyboard.menu.myGiftDetailViewController() {
            vc.gift = giftCode
            router.push(controller: vc, animated: true)
        }
    }
    
    func openMyServicesVC() {
        if let vc = R.storyboard.menu.myServicesViewController() {
            router.push(controller: vc)
        }
    }
    
    func openMyServiceDetailVC(martletCodeEntity: MartletCodeDependencyEntity) {
        if let vc = R.storyboard.menu.myServiceDetailViewController() {
            vc.martletCodeEntity = martletCodeEntity
            router.push(controller: vc, animated: true)
        }
    }
    
    func openRepairsStatusVC() {
        let storyboard = UIStoryboard.init(name: "RepairsStatus", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "RepairsStatusViewController")
        router.push(controller: vc)
    }
    
    func openNewLoginVC() {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewLoginViewController")
        router.push(controller: vc)
    }
    
    func openNewLoginVC(phone: String?, authService: AuthorizationServices) {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewLoginViewController") as! NewLoginViewController
        vc.authService = authService
        vc.phone = phone
        router.push(controller: vc)
       }
    
    func openNewForgotPassword() {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewForgotPasswordViewController")
        router.push(controller: vc)
    }
    
    func openNewRegistrationVC(authService: AuthorizationServices? = nil,
                               user: ShortUserEntity? = nil,
                               state: NewRegistrationViewController.State,
                               needBPM: Bool = false,
                               phone: String?,
                               password: String?) {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewRegistrationViewController") as! NewRegistrationViewController
        if let authService = authService {
            vc.authService = authService
        }
        vc.state = state 
        vc.user = user
        vc.needBPM = needBPM
        vc.phone = phone
        vc.password = password
        router.push(controller: vc)
    }
    
    func openSMSConfirmVC(authService: AuthorizationServices, phone: String) {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "SMSConfirmViewController") as! SMSConfirmViewController
        vc.authService = authService
        vc.phone = phone
        
        router.push(controller: vc)
    }
    
    func openNewContainer() {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    func openCallMeVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallMeViewController")
        router.push(controller: vc)
    }
    
    func openChattingVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChattingViewController")
        router.push(controller: vc)
    }
    
    func openCloseChattingVC(complition: (() -> ())?) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "CloseChatViewController") as! CloseChatViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.handler = complition
        router.present(controller: vc)
    }
    
    func openActivateBonusses() {
        let storyboard = UIStoryboard(name: "Bonuses", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ActivateBonussesViewController")
        router.push(controller: vc)
    }
    
    func openActivateBonuseAlert(complition: ((_ cardNumber: String, _ phone: String) -> ())?) {
        let storyboard = UIStoryboard(name: "Bonuses", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "ActivateBonuseAlertViewController") as! ActivateBonuseAlertViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.handler = complition
        router.present(controller: vc)
    }
    
    func openBonussesVC(bonuse: BonusCardEntity) {
        let storyboard = UIStoryboard(name: "Bonuses", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewBonussesViewController") as! NewBonussesViewController
        vc.bonusCardEntity = bonuse
        router.push(controller: vc)
    }
    
    func openGiftVC(gift: GiftDetailUIProtocol, giftID: Int?, didRemoveService: (() -> ())? = nil) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewGiftDetailViewController") as! NewGiftDetailViewController
        vc.gift = gift
        vc.giftID = giftID
        vc.didRemoveService = didRemoveService
        router.push(controller: vc)
    }
    
    func openGiftsListVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewGiftsListViewController") as! NewGiftsListViewController
        router.push(controller: vc)
    }
    
    func openNotificationsVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewNotificationsViewController") as!
        NewNotificationsViewController
        router.push(controller: vc)
    }
    
    func openNotificationDetailVC(notification: NewNotificationEntity) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewNotificationDetailViewController") as! NewNotificationDetailViewController
        vc.notification = notification
        router.push(controller: vc)
    }
    
    func presentGreetingVC() {
        let vc = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "GreetingViewController")
        let navigationController = Smart24NavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        //router.present(controller: navigationController, animated: true)
    }
    
    func openNewProfileVC() {
        let vc = UIStoryboard(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewProfileViewController")
        router.push(controller: vc)
    }
    
    func openImagePickerVC(makePhotoHandler: (() -> ())?, galleryHandler: (() -> ())?) {
        let vc = UIStoryboard(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePickerViewController") as! ImagePickerViewController
        vc.makePhotoHandler = makePhotoHandler
        vc.galleryHandler = galleryHandler
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        router.present(controller: vc)
    }
    
    func openRequestService(didEnterEquipmentCode: ((String) -> ())?, didCreateRequestForInstall: (() -> ())?) {
        let vc = UIStoryboard(name: "RequestService", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateServiceRequestViewController") as! CreateServiceRequestViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.didEnterCode = didEnterEquipmentCode
        vc.didCreateRequestForInstall = didCreateRequestForInstall
        router.present(controller: vc)
    }
    
    func openRequestInstallEquipmentVC() {
        let vc = UIStoryboard(name: "RequestService", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestInstallEquipmentViewController") as! RequestInstallEquipmentViewController
        
        router.push(controller: vc)
    }
    
    func openRequestServicesListVC(servicesList: [ServiceEntity]) {
        let vc = UIStoryboard(name: "RequestService", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestServicesListViewController") as! RequestServicesListViewController
        vc.dataSource = servicesList
        router.push(controller: vc)
    }
    
    func openRequestHistoryVC(service: ServiceEntity) {
        let vc = UIStoryboard(name: "RequestService", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestServiceDetailViewController") as! RequestServiceDetailViewController
        vc.service = service
        router.push(controller: vc)
    }
    
    func openNewMyServicesVC() {
        let vc = UIStoryboard(name: "MyServices", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewMyServicesViewController") as! NewMyServicesViewController
        router.push(controller: vc)
    }
    
    func openNewActivateMyServiceVC(didActivateService: ((Bool) -> ())?) {
        let vc = UIStoryboard(name: "MyServices", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewActivateMyServiceViewController") as! NewActivateMyServiceViewController
        vc.didActivateServcie = didActivateService
        router.push(controller: vc)
    }
    
    func openBarcodeScannerVC<T: BarcodeScannerCodeDelegate & BarcodeScannerErrorDelegate & BarcodeScannerDismissalDelegate>(delegate: T) {
        let vc = BarcodeVC()
        vc.codeDelegate = delegate
        vc.dismissalDelegate = delegate
        vc.errorDelegate = delegate
        router.push(controller: vc)
    }
    
    func openContractWebVC(urlString: String) {
        let vc = UIStoryboard(name: "MyServices", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.urlString = urlString
        router.push(controller: vc)
    }
    
    func openFeedbackVC(state: FeedbackType) {
        let vc = UIStoryboard(name: "Feedback", bundle: Bundle.main).instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        vc.state = state
        router.push(controller: vc)
    }
    
    func openFeedbackMenuVC() {
        let vc = UIStoryboard(name: "Feedback", bundle: Bundle.main).instantiateViewController(withIdentifier: "FeedbackMenuViewController")
        router.push(controller: vc)
    }
    
    func openRequestRepairEquipmentVC(didCreateRequestForInstall: (() -> ())?, technicParkPRSEntity: TechnicParkPRSEntity? = nil) {
        let vc = UIStoryboard(name: "RequestService", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestRepairEquipmentViewController") as! RequestRepairEquipmentViewController
        vc.didCreateRequestForInstall = didCreateRequestForInstall
        vc.technicParkPRSEntity = technicParkPRSEntity
        router.push(controller: vc)
    }
    
    func openTechnicParkVC() {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "TechnicParkViewController")
        router.push(controller: vc)
    }
    
    func openTechnicParkDetailVC(technicParkList: [TechnicParkEntity], delegate: TechnicParkDetailViewControllerDelegate, scrollToIndex: IndexPath) {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "TechnicParkDetailViewController") as! TechnicParkDetailViewController
        vc.delegate = delegate
        vc.dataSource = technicParkList
        vc.startIndex = scrollToIndex
        router.push(controller: vc)
    }
    
    func openAddTechnicToParkViewController() {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTechnicToParkViewController") as! AddTechnicToParkViewController
        router.push(controller: vc)
    }
    
    func openAddTechnicToParkDetailVC(technikParkInfo: TechnikParkInfoEntity, technicCategories: TechnicCategories, technicParkEntity: TechnicParkEntity? = nil, didEdit: (() -> Void)? = nil, state: AddTechnicToParkDetailViewController.State = .defaultState) {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTechnicToParkDetailViewController") as! AddTechnicToParkDetailViewController
        vc.technikParkInfo = technikParkInfo
        vc.technicCategories = technicCategories
        vc.technicParkEntity = technicParkEntity
        vc.didEdit = didEdit
        vc.state = state
        router.push(controller: vc)
    }
    
    func addTechnikSuccessVC(imageUrlString: String) {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTechnikSuccessViewController") as! AddTechnikSuccessViewController
        vc.imageString = imageUrlString
        router.push(controller: vc)
    }
    
    func pushTechnicParkVCToRoot(in navigationViewController: UINavigationController?) {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "TechnicParkViewController")
        navigationViewController?.viewControllers.insert(vc, at: 1)
        popTechnicParkVCIfInStack()
    }
    
    @discardableResult
    func popTechnicParkVCIfInStack() -> Bool {
        return router.popToVCofClass(classType: TechnicParkViewController.self)
    }
    
    @discardableResult
    func popAddTechnicToParkVCIfInStack() -> Bool {
        return router.popToVCofClass(classType: AddTechnicToParkViewController.self)
    }
    
    @discardableResult
    func popChattingVCIfInStack() -> Bool {
        return router.popToVCofClass(classType: ChattingContainerViewController.self)
    }
    
    @discardableResult 
    func popCallMeVCIfInStack() -> Bool {
        return router.popToVCofClass(classType: CallMeViewController.self)
    }
    
    func openSearchVC(items: [String], delegate: SearchViewControllerDelegate) {
        let vc = UIStoryboard(name: "TechnicPark", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        vc.delegate = delegate
        vc.items = items
        router.push(controller: vc)
    }
    
    func openSearchCitiesVC(delegate: SearchCityViewControllerDelegate) {
        let vc = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchCityViewController") as! SearchCityViewController
        vc.delegate = delegate
        router.push(controller: vc)
    }
    
    func presentCallPopup(didTapCallRequest: (() -> Void)?, didTapWriteToChat: (() -> Void)?) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallPopupViewController") as! CallPopupViewController
        vc.writeToChat = didTapWriteToChat
        vc.requestCall = didTapCallRequest
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        router.present(controller: vc)
    }
    
    func openInsuranceActivateVC(insurance: InsuranceEntity, notification: NewNotificationEntity) {
        let vc = UIStoryboard(name: "Insurance", bundle: Bundle.main).instantiateViewController(withIdentifier: "InsuranceActivateViewController") as! InsuranceActivateViewController
        vc.insurance = insurance
        vc.notification = notification
        router.push(controller: vc)
    }
    
    func openInsuranceVC(insurance: MyInsuranceServiceEntity, didRemoveInsurance: (() -> ())? = nil) {
        let vc = UIStoryboard(name: "Insurance", bundle: Bundle.main).instantiateViewController(withIdentifier: "InsuranceViewController") as! InsuranceViewController
        vc.insurance = insurance
        vc.didRemoveInsurance = didRemoveInsurance
        
        router.push(controller: vc)
    }
    
    func openRegistrationStart(state: NewRegistrationViewController.State = .registration, authService: AuthorizationServices? = nil) {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "StartRegistrationViewController") as! StartRegistrationViewController
        vc.state = state
        if let authService = authService {
            vc.authService = authService
        }
        
        router.push(controller: vc)
    }
}

