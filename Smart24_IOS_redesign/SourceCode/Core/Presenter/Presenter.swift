//
//  Presenter.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

protocol Presenter {
    
    func presentLoginVC()
    func backToPreviousVC()
    func openRootVC()
    
    func openLoginVC()
    func openRegistrationVC()
    func openForgotPasswordVC()
    func openContainerVC()
    func openMainVC()
    func openNewsVC()
    func openUtilityVC()
    func openProfileVC()
    func openNotificationVC(message: NotificationEntity)
    func openChangeDataVC()
    func openBackupVC()
    func openAlertVC(alertViewController: UIAlertController)
    func openChatVC()
    func presentChatVC()
    func openHowItWorkTBC()
    func openDevelopersVC()
    func openBackupProgressVC(status: BackupState)
    func openBackupPerionVC()
    func openMakeCallVC(callId: String?)
    func openNewsDescriptionVC(news: NewsEntity)
    func openVideoVC(call: VSLCall?)
    func openActivateBonusVC()
    func openBonusesVC(bonuses:BonusesEntity)
    func openOrderCallVC()
    func presentGiftsVC(gifts:[Gift], didSelectGift: @escaping GiftsViewController.selectGift)
    func presentGiftDetailVC(giftCode: GiftCodeEntity)
    func openMyGiftsVC()
    func openMyGiftDetailVC(giftCode: GiftCodeUserEntity)
    func openMyServicesVC()
    func openMyServiceDetailVC(martletCodeEntity: MartletCodeDependencyEntity) 
    func openRepairsStatusVC()
    func openNewLoginVC()
    func openNewForgotPassword()
    func openNewRegistrationVC(authService: AuthorizationServices?, user: ShortUserEntity?, state: NewRegistrationViewController.State, needBPM: Bool, phone: String?, password: String?)
    func openNewContainer()
    func openCallMeVC()
    func openChattingVC()
    func openCloseChattingVC(complition: (() -> ())?)
    func openActivateBonusses()
    func openActivateBonuseAlert(complition: ((_ cardNumber: String, _ phone: String) -> ())?)
    func openBonussesVC(bonuse: BonusCardEntity)
    func openGiftVC(gift: GiftDetailUIProtocol, giftID: Int?, didRemoveService: (() -> ())?)
    func openGiftsListVC()
    func openNotificationsVC()
    func openNotificationDetailVC(notification: NewNotificationEntity)
    func presentGreetingVC()
    func openNewProfileVC()
    func openImagePickerVC(makePhotoHandler: (() -> ())?, galleryHandler: (() -> ())?)
    func openRequestService(didEnterEquipmentCode: ((String) -> ())?, didCreateRequestForInstall: (() -> ())?)
    func openRequestInstallEquipmentVC()
    func openRequestServicesListVC(servicesList: [ServiceEntity])
    func openNewActivateMyServiceVC(didActivateService: ((Bool) -> ())?)
}
