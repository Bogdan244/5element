//
//  RouterImpl.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import SWRevealViewController

final class RouterImpl: Router {
    
    private(set) weak var rootController: Smart24NavigationController?
    
    init(rootController: Smart24NavigationController) {
        self.rootController = rootController
    }
    
    func present(controller: UIViewController) {
        present(controller: controller, animated: true)
    }
    
    func present(controller: UIViewController, animated: Bool) {
        
        //self.navigationRootViewController()?.popToRootViewController(animated: false)
        self.navigationRootViewController()?.present(controller, animated: animated)
    }
    
    func present(controller: UIViewController?, animated: Bool, completition: @escaping RouterHandler) {
        guard let controller = controller else { return }
        DispatchQueue.main.async { [weak self] in
           self?.navigationRootViewController()?.present(controller, animated: animated, completion: completition)
        }
    }

    func push(controller: UIViewController?)  {
        push(controller: controller, animated: true)
    }
    
    func push(controller: UIViewController?, animated: Bool)  {
        guard let controller = controller else { return }
        
        navigationRootViewController()?.pushViewController(controller, animated: true)
    }
    
    func popController()  {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.navigationRootViewController()?.popViewController(animated: false)
        }
    }
    
    func popController(animated: Bool ) {
        self.navigationRootViewController()?.popViewController(animated: animated)
    }
    
    func dismissController() {
        dismissController(animated: true)
    }
    
    func dismissController(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.navigationRootViewController()?.dismiss(animated: animated, completion: nil)
        }
    }
    

    func dismissController(animated: Bool, completition: @escaping RouterHandler) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.navigationRootViewController()?.dismiss(animated: animated, completion: completition)
        }
    }

    func openOnMenu(controller: UIViewController?) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if let vc = controller, let containerViewController = UIApplication.topViewController() as? SWRevealViewController {
                let navigationController = Smart24NavigationController(rootViewController: vc)
                containerViewController.frontViewController = navigationController
            }
        }
    }
    
    func navigationRootViewController() -> UINavigationController? {
        if let containerViewController = UIApplication.topViewController() as? NewSideMenuViewController {
            return containerViewController.navigationController
        }
        
        if let navigationController = UIApplication.topViewController() as? Smart24NavigationController {
            return navigationController
        }
        
        return self.rootController
    }

    func changeRootViewController(controller: UIViewController) {
        if let containerVC = UIApplication.topViewController() {
            containerVC.dismiss(animated: true, completion: nil)
        }
    }
    
    func openAlertViewController(alertViewController: UIAlertController) {
        if let containerViewController = UIApplication.topViewController() as? SWRevealViewController {
            containerViewController.present(alertViewController, animated: true, completion:nil)
        }
    }
    
    func getRoot() -> Smart24NavigationController? {
        return rootController
    }
    
    func popToRootViewController(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            _ = self?.navigationRootViewController()?.popToRootViewController(animated: animated)
        }
    }
    
    func pushVCToRoot(viewController: UIViewController, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            _ = self?.navigationRootViewController()?.popToRootViewController(animated: animated)
            self?.push(controller: viewController)
        }
    }

    func popToVCofClass(classType: AnyClass) -> Bool {
        guard let navigationController = navigationRootViewController() else { return false }
        let viewControllers = navigationController.viewControllers
        if let vc = viewControllers.first(where: {$0.isKind(of: classType)}) {
            DispatchQueue.main.async {
                navigationController.popToViewController(vc, animated: true)
            }
            return true
        }
        return false
    }
    
}
