//
//  Router.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

typealias RouterHandler = () -> ()

protocol Router: class {
    
    var rootController: Smart24NavigationController? { get }
    
    func present(controller: UIViewController)
    func present(controller: UIViewController, animated: Bool)
    func present(controller: UIViewController?, animated: Bool, completition: @escaping RouterHandler)
    
    func push(controller: UIViewController?)
    func push(controller: UIViewController?, animated: Bool)
    
    func popController()
    func popController(animated: Bool)
    
    func dismissController()
    func dismissController(animated: Bool)
    func dismissController(animated: Bool, completition: @escaping RouterHandler)
    
    func openOnMenu(controller: UIViewController?)
    
    func changeRootViewController(controller: UIViewController)
    func openAlertViewController(alertViewController: UIAlertController)
    
    func getRoot() -> Smart24NavigationController?

    func popToRootViewController(animated: Bool)
    
    func pushVCToRoot(viewController: UIViewController, animated: Bool)
    
    func popToVCofClass(classType: AnyClass) -> Bool
}
