//
//  ApiManager.swift
//  ULC
//
//  Created by Alex on 6/4/16.
//  Copyright © 2016 wezom.com.ua. All rights reserved.
//

import Foundation
import Moya
import Firebase

// MARK: - Provider setup

private func JSONResponseDataFormatter(data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data as Data, options: [])
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData as Data
    } catch {
        return data // fallback to original data if it cant be serialized
    }
}

// MARK: - Provider support
private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

public enum ApiManager {
    case login(phone: String, password: String, otpCode: String?, needBPM: Bool?, gender: Gender?, cityID: String?, birthday: Int?, name: String?, surname: String?)
    case registration(name: String?, surname: String?, phone: String, password: String?, phoneModel: String?, otp: String?, needBPM: Bool?, gender: Gender?, cityID: String?, birthday: Int?)
    case authWithSocial(socialName: SocialName, accessToken: String?, serverAuthCode: String?, phone: String?, otp: String?, joinAccount: Bool?, needBPM: Bool?, gender: Gender?, cityID: String?, birthday: Int?, name: String?, surname: String?)
    case confirmRegistration(String, String)
    case forgotPassword(String, String?)
    case loginWithSocial(Int, String)
    case registerWithSocial(String, String, Int, String)
    case news(String, Int)
    case post_history(String, String, String, String, Int)
    case history(String, Int)
    case notifications(String)
    case syncNotifications(Int, Int, String)
    case activateService(String, String)
    case activateServiceProfile(String, String)
    case getActiveServices(String, Int)
    case getActiveServicesProfile(String, Int)
    case changeUserData(String, String?, String, String, String, String, String)
    case getUserInfo(String)
    case getStatus(String)
    case rate(String, String, Int, Int)
    case putInfoBackup(String, Bool, Int)
    case getInfoBackup(String)
    case sendMessage(String, String, Int?)
    case getAnswer(String, String, Int?, Int?)
    case messagesRead(String, String, Int)
    case closeDialog(String, String, String?, String?)
    case getAllBonuses()
    case addNewBonus(String, String)
    case deactivateBonuse(Int)
    case getGifts()
    case activateGiftCode(Int)
    case allUserGifts()
    case viewGiftCode(Int)
    case clearGifts()
    case removeGift(id: Int)
    case showMartletCode()
    case activateMartletCode(String)
    case orderCallBack(uid: String, serviceID: String, username: String, phone: String, comment: String, timeFrom: String, timeTo: String)
    case technicStatusList(page: String?, count: String?)
    case technicStatusAdd(code: String)
    case editUserData(firstname: String, surname: String, password: String, photo: String?, phone: String?, otp: String?, gender: Gender?, cityID: String?, birthday: String?, joinAccount: Bool?)
    case userInfo()
    case forgotPasswordRes(phone: String, code: String?)
    case logOut()
    case getPRSInfo()
    case getMessages(Int?)
    case uploadFile(dialogID: Int, file: String)
    case getNotifications()
    case readNotification(id: Int)
    case remoeNotification(id: Int)
    case getRequestServiceInfo()
    case getRequestServices()
    case getServices()
    case removeService(id: Int)
    case addReview(type:FeedbackType, email: String, description: String)
    case addTechnicToRepair(code: String)
    case addRequestToRepair(name: String, phone: String, technicCategory: String, requestReason: String, requestComment: String, serviceID: Int?)
    case addRequestToInstall(name: String, phone: String, technicCategory: String, address: String, date: String)
    case addRequestToMasterInstall(serviceNumber: String, name: String, model: String, phone: String, technicCategory: String, date: String)
    case getProductListFromTechnicPark()
    case getTechnicParkInfo()
    case addProductToTechnikPark(categoryID: Int, brand: String, model: String, exploitationTime: String, repaired: Int)
    case removeMartletService(id: Int)
    case removeProductFromTechnikPark(id: Int)
    case searchProduct(field: Field)
    case editTechnicProduct(id: Int, categoryID: Int, brand: String, model: String, exploatitionTime: String, repaired: Int)
    case activateInsurance(id: Int, imei: String, name: String, middleName: String, surname: String, notificationId: Int)
    case removeInsurance(id: Int)
    case cities(query: String)
}

extension ApiManager: TargetType {
    public var headers: [String : String]? {
        return ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                "Authorization": UserEntity.current?.token ?? "",
                "version-app": Bundle.main.appVersion,
                "app-os": "iOS",
                "Accept": "application/json"]
    }	
    
    /// The method used for parameter encoding.
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    public var baseURL: URL {
        switch self {
        case .getAllBonuses(), .addNewBonus(_, _), .deactivateBonuse(_):
            return URL(string: Constants.BONUS_URL)!
        case .getGifts(), .activateGiftCode(_), .allUserGifts(), .viewGiftCode(_), .clearGifts(), .removeGift(_):
            return URL(string: Constants.GIFTS_URL)!
        case .showMartletCode(),.activateMartletCode(_):
            return URL(string: Constants.MARTLET_URL)!
        case .orderCallBack(_,_,_,_,_,_,_):
            return URL(string: Constants.CALLBACK_URL)!
        case .login(phone: _, password: _, otpCode: _, needBPM: _, gender: _, cityID: _, birthday: _, name: _, surname: _),
             .registration(name: _, surname: _, phone: _, password: _, phoneModel: _, otp: _, needBPM: _, gender: _, cityID: _, birthday: _),
             .technicStatusList(page: _, count: _),
             .technicStatusAdd(code: _),
             .authWithSocial(socialName: _, accessToken: _, serverAuthCode: _, phone: _, otp: _, joinAccount: _, needBPM: _, gender: _, cityID: _, birthday: _, name: _, surname: _),
             .editUserData(_, _, _, _, _, _, _, _, _, _),
             .userInfo(),
             .forgotPasswordRes(phone: _, code: _),
             .logOut,
             .getPRSInfo,
             .getMessages(_),
             .uploadFile(dialogID: _, file: _),
             .addTechnicToRepair(code: _),
             .getProductListFromTechnicPark(),
             .getTechnicParkInfo(),
             .addProductToTechnikPark(categoryID: _, brand: _, model: _, exploitationTime: _, repaired: _),
             .removeProductFromTechnikPark(id: _),
             .editTechnicProduct(id: _, categoryID: _, brand: _, model: _, exploatitionTime: _, repaired: _):
            return URL(string: Constants.AUTH_URL)!
        case .getNotifications(), .readNotification(id: _), .remoeNotification(id: _):
            return URL(string: Constants.NOTIFICATIONS_URL)!
        case .getRequestServiceInfo(), .getRequestServices(), .getServices(), .removeService(id: _), .addReview(type: _, email: _, description: _), .addRequestToRepair(_, _, _, _, _, _), .addRequestToInstall(_, _, _, _, _), .removeMartletService(id: _), .searchProduct(field: _), .addRequestToMasterInstall(serviceNumber: _, name: _, model: _, phone: _, technicCategory: _, date: _), .activateService(_, _), .activateInsurance(id: _, imei: _, name: _, middleName: _, surname: _, notificationId: _), .removeInsurance(id: _), .cities(query: _):
            return URL(string: Constants.BASE5ELEMEMT_URL)!
        default:
            return URL(string: Constants.BASE_URL)!
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .login, .registration, .loginWithSocial, .changeUserData, .getUserInfo, .sendMessage, .syncNotifications, .confirmRegistration, .forgotPassword, .history, .post_history, .getStatus, .rate, .activateService, .activateServiceProfile, .getActiveServicesProfile, .addNewBonus, .deactivateBonuse, .activateGiftCode, .viewGiftCode, .clearGifts, .activateMartletCode, .orderCallBack, .technicStatusAdd, .authWithSocial, .editUserData, .forgotPasswordRes, .getMessages, .uploadFile, .addReview, .addTechnicToRepair, .addRequestToRepair, .addRequestToInstall, .addProductToTechnikPark, .searchProduct, .editTechnicProduct, .addRequestToMasterInstall, .activateInsurance, .cities:
            return .post
        default:
            return .get
        }
    }
    
    public var path: String {
        switch self {
        case .login(phone: _, password: _, otpCode: _, needBPM: _, gender: _, cityID: _, birthday: _, name: _, surname: _):
            return "auth/login_password"
        case .registration(name: _, surname: _, phone: _, password: _, phoneModel: _, otp: _, needBPM: _, gender: _, cityID: _, birthday: _):
            return "auth/register"
        case .authWithSocial(socialName: _, accessToken: _, serverAuthCode: _, phone: _, otp: _, joinAccount: _, needBPM: _, gender: _, cityID: _, birthday: _, name: _, surname: _):
            return "auth/login_social"
        case .confirmRegistration(_, _):
            return "checkcode"
        case .forgotPassword:
            return "sendpasswd"
        case .loginWithSocial(_, _):
            return "loginSocial"
        case .registerWithSocial(_, _, _, _):
            return "registerSocial"
        case .news(_):
            return "news"
        case .notifications(_):
            return "notifications"
        case .syncNotifications(_, _, _):
            return "sync-notifications"
        case .activateService(_, _):
            return "services/prs/activate"
        case .activateServiceProfile(_, _):
            return "activatecard"
        case .getActiveServices(_, _):
            return "prs_getservices"
        case .getActiveServicesProfile(_, _):
            return "services"
        case .changeUserData(_, _, _, _, _, _, _):
            return "changedata"
        case .getUserInfo(_):
            return "user_info"
        case .putInfoBackup(_):
            return "putinfo_backup"
        case .getInfoBackup(_):
            return "getinfo_backup"
        case .sendMessage(_, _, _):
            return "chat/create_message"
        case .getAnswer(_, _, _, _):
            return "getanswer"
        case .messagesRead(_, _, _):
            return "messagesread"
        case .history(_, _):
            return "prs_getrequests"
        case .post_history(_, _, _, _, _):
            return "prs_sendrequest"
        case .closeDialog(_, _, _, _):
            return "chat/close_chat_with_rating"
        case .getStatus(_):
            return "prs_getstatus"
        case .rate(_, _, _, _):
            return "prs_setgrade"
        case .getAllBonuses():
            return "get"
        case .addNewBonus(_, _):
            return "add"
        case .deactivateBonuse(_):
            return "remove"
        case .getGifts():
            return "get"
        case .activateGiftCode(_):
            return "activate"
        case .allUserGifts():
            return "user/all"
        case .viewGiftCode(_):
            return "view"
        case .clearGifts():
            return "clear"
        case .showMartletCode():
            return "show"
        case .activateMartletCode(_):
            return "store"
        case .orderCallBack(_, _, _, _, _, _, _):
            return "get_callback"
        case .technicStatusList(page: _, count: _):
            return "technic_status/list"
        case .technicStatusAdd(code: _):
            return "technic_status/add"
        case .editUserData(firstname: _, surname: _, password: _, photo: _, phone: _, otp: _, gender: _, cityID: _, birthday: _, joinAccount: _):
            return "client/edit"
        case .userInfo():
            return "client/info"
        case .forgotPasswordRes(phone: _, code: _):
            return "auth/forgot_password"
        case .logOut():
            return "auth/logout"
        case .getPRSInfo():
            return "five_element/requests/info"
        case .getMessages(_):
            return "chat/get_messages"
        case .uploadFile(dialogID: _, file: _):
            return "chat/create_message_with_attachment"
        case .removeGift(id: let id):
            return "remove/\(id)"
        case .getNotifications():
            return "get"
        case .readNotification(id: let id):
            return "read/\(id)"
        case .remoeNotification(id: let id):
            return "remove/\(id)"
        case .getRequestServiceInfo():
            return "requests/info"
        case .getRequestServices():
            return "requests/get"
        case .getServices():
            return "services/get"
        case .removeService(id: let id):
            return "services/prs/remove/\(id)"
        case .removeMartletService(id: let id):
            return "services/martlet/remove/\(id)"
        case .addReview(type: _, email: _, description: _):
            return "add_review"
        case .addTechnicToRepair(code: _):
            return "technic_status/add"
        case .addRequestToRepair(name: _, phone: _, technicCategory: _, requestReason: _, requestComment: _, serviceID: _):
            return "requests/add_prs"
        case .addRequestToInstall(name: _, phone: _, technicCategory: _, address: _, date: _):
            return "requests/add_install"
        case .addRequestToMasterInstall(serviceNumber: _, name: _, model: _, phone: _, technicCategory: _, date: _):
            return "requests/add_master_install"
        case .getProductListFromTechnicPark():
            return "park_technic/get"
        case .getTechnicParkInfo():
            return "park_technic/info"
        case .addProductToTechnikPark(categoryID: _, brand: _, model: _, exploitationTime: _, repaired: _):
            return "park_technic/add"
        case .removeProductFromTechnikPark(id: let id):
            return "park_technic/remove/\(id)"
        case .searchProduct(field: _):
            return "search_products"
        case .editTechnicProduct(id: _, categoryID: _, brand: _, model: _, exploatitionTime: _, repaired: _):
            return "park_technic/edit"
        case .activateInsurance(id: _, imei: _, name: _, middleName: _, surname: _, notificationId: _):
            return "services/insurances/activate"
        case .removeInsurance(id: let id):
            return "services/insurances/hide/\(id)"
        case .cities(query: _):
            return "auth/get_cities_like_string"
        }

    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var parameters: [String : Any] {
        var value = [String: Any]()
        
        switch self {
            
        case .login(let email, let password, let otp, let needBPM, let gender, let cityID, let birthday , let name, let surname):
            value = [ApiManagerKey.phone: email,
                     ApiManagerKey.password: password,
                     ApiManagerKey.partnerName: "5element",
                     ApiManagerKey.appName: "Защита+"]
            if let token = Messaging.messaging().fcmToken {
                value[ApiManagerKey.fcm_token] = token
            }
            if let otpValue = otp {
                value[ApiManagerKey.otp] = otpValue
            }
            if let needBPM = needBPM {
                value[ApiManagerKey.doGluingBpm] = needBPM ? "true" : "false"
            }
            if let gender = gender {
                value[ApiManagerKey.gender] = gender.serverRepresentation
            }
            if let cityID = cityID {
                value[ApiManagerKey.cityID] = cityID
            }
            if let birthday = birthday {
                value[ApiManagerKey.birthday] = birthday
            }
            if let name = name {
                value[ApiManagerKey.firstname] = name
            }
            if let surname = surname {
                value[ApiManagerKey.surname] = surname
            }
        case .registration(let name, let surname, let phone, let password, let phoneModel, let otp, let needBPM, let gender, let cityID, let birthday):
            value = [ApiManagerKey.phone: phone]
            if let name = name {
                value[ApiManagerKey.firstname] = name
            }
            if let surname = surname {
                value[ApiManagerKey.surname] = surname
            }
            if let password = password {
                value[ApiManagerKey.password] = password
            }
            if let phoneModel = phoneModel {
                value[ApiManagerKey.phone_model] = phoneModel
            }
            if let token = Messaging.messaging().fcmToken {
                value[ApiManagerKey.fcm_token] = token
            }
            if let otpValue = otp {
                value[ApiManagerKey.otp] = otpValue
            }
            if let needBPM = needBPM {
                value[ApiManagerKey.doGluingBpm] = needBPM ? "true" : "false"
            }
            if let gender = gender {
                value[ApiManagerKey.gender] = gender.serverRepresentation
            }
            if let cityID = cityID {
                value[ApiManagerKey.cityID] = cityID
            }
            if let birthday = birthday {
                value[ApiManagerKey.birthday] = birthday
            }
        case .authWithSocial(let socialName, let accesToken, let serverAuthCode, let phone, let otp, let joinAccount, let needBPM, let gender, let cityID, let birthday, let name, let surname):
            value[ApiManagerKey.socialName] = socialName.rawValue
            if let accesTokenValue = accesToken { value[ApiManagerKey.accessToken] = accesTokenValue }
            if let serverAuthCodeValue = serverAuthCode { value[ApiManagerKey.serverAuthCode] = serverAuthCodeValue }
            if let phoneValue = phone { value[ApiManagerKey.phone] = phoneValue }
            if let otpValue = otp { value[ApiManagerKey.otp] = otpValue }
            if let token = Messaging.messaging().fcmToken { value[ApiManagerKey.fcm_token] = token }
            if let joinAccount = joinAccount { value[ApiManagerKey.doJoinAccount] = joinAccount ? "true" : "false" }
            if let needBPM = needBPM { value[ApiManagerKey.doGluingBpm] = needBPM ? "true" : "false" }
            if let gender = gender { value[ApiManagerKey.gender] = gender.serverRepresentation }
            if let cityID = cityID { value[ApiManagerKey.cityID] = cityID }
            if let birthday = birthday { value[ApiManagerKey.birthday] = birthday }
            if let name = name { value[ApiManagerKey.firstname] = name }
            if let surname = surname { value[ApiManagerKey.surname] = surname }
            
        case .confirmRegistration(let phone, let code):
            value = [ApiManagerKey.phone: phone,
                     ApiManagerKey.code: code]

        case .forgotPassword(let phone, let code):
            if let code = code {
                value = [ApiManagerKey.phone: phone,
                         ApiManagerKey.code: code]
            } else {
                value = [ApiManagerKey.phone: phone]
            }
        
        case .loginWithSocial(let type, let id):
            value = [ApiManagerKey.social_type: type,
                     ApiManagerKey.social_id: id,
                     ApiManagerKey.partnerName: "5element",
                     ApiManagerKey.appName: "Защита+"]
            if let token = Messaging.messaging().fcmToken {
                value[ApiManagerKey.fcm_token] = token
            }
        
        case .registerWithSocial(let photo, let username, let type, let id):
            value = [ApiManagerKey.user_photo: photo,
                     ApiManagerKey.username: username,
                     ApiManagerKey.social_type: type,
                     ApiManagerKey.social_id: id]
        
        case .post_history(let uid, let username, let phone, let comment, let serviceId):
            value = [ApiManagerKey.uid: uid,
                     ApiManagerKey.username: username,
                     ApiManagerKey.phone: phone,
                     ApiManagerKey.comment: comment,
                     ApiManagerKey.serviceId: serviceId]
        
        case .news(let id, let density):
            value = [ApiManagerKey.uid: id,
                     ApiManagerKey.density: density]
        
        case .history(let uid, let serviceId):
            value = [ApiManagerKey.uid: uid,
                     ApiManagerKey.serviceId: serviceId]
        
        case .notifications(let id):
            value = [ApiManagerKey.uid: id]
            
        case .syncNotifications(let id, let notificationsId, let action):
            value = [ApiManagerKey.uid: id,
                     ApiManagerKey.firstNotification: [ApiManagerKey.id: notificationsId,
                                                       ApiManagerKey.action: action]]
            
        case .activateService(let userId, let activationCode):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.activationCode: activationCode]
            
        case .getActiveServices(let userId, let density):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.density: density]
            
        case .getActiveServicesProfile(let userId, let density):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.density: density]
            
        case .changeUserData(let userId, let userPhoto, let userName, let surname, let phone, let password, let confirmPassword):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.name: userName,
                     ApiManagerKey.surname: surname,
                     ApiManagerKey.phone: phone,
                     ApiManagerKey.passwd: password,
                     ApiManagerKey.passwdcheck: confirmPassword]
            if let userPhoto = userPhoto {
                value[ApiManagerKey.image_person] = "data:image/png;base64,\(userPhoto)"
            }
            
        case .getUserInfo(let userId):
            value = [ApiManagerKey.uid: userId]
        case .userInfo:
            value = [ApiManagerKey.appAction: "edit_form"]
        case .getStatus(let userId):
            value = [ApiManagerKey.uid: userId]
            
        case .rate(let userId, let comment, let requestId, let rate):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.comment: comment,
                     ApiManagerKey.request_id: requestId,
                     ApiManagerKey.rating: rate]
            
        case .putInfoBackup(let userId, let isBackup, let date):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.iSbackup: isBackup,
                     ApiManagerKey.date: date]
            
        case .getInfoBackup(let userId):
            value = [ApiManagerKey.uid: userId]
            
        case .sendMessage(let message, let userId, let dialog_id):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.message: message]
            if let dialogID = dialog_id { value[ApiManagerKey.dialog_id] = dialogID }
            
        case .getAnswer(let userId, let dialog_id, let timestamp, let isVideo):
            if let timestamp = timestamp, let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.timestamp: timestamp,
                         ApiManagerKey.isVideo: isVideo]
            } else if let timestamp = timestamp {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.timestamp: timestamp]
            } else if let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.isVideo: isVideo]
            } else {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id]
            }
            
        case .activateServiceProfile(let userId, let activationCode):
            value = [ApiManagerKey.uid: userId,
                     "activation_code": activationCode]
            
        case .messagesRead(let userId, let dialog_id, let timestamp):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.dialog_id: dialog_id,
                     ApiManagerKey.timestamp: timestamp]
            
        case .closeDialog(let userId, let dialog_id, let grade, let opinion):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.dialog_id: dialog_id]
            if let gradeValue = grade {
                value[ApiManagerKey.grade] = gradeValue
            } else {
                value[ApiManagerKey.grade] = "10"
            }
            if let opinionValue = opinion {
                value[ApiManagerKey.opinion] = opinionValue
            }
            
        case .addNewBonus(let card, let phone):
            value = [ApiManagerKey.card: card,
                     ApiManagerKey.phone: phone]
        
        case .deactivateBonuse(let id):
            value = [ApiManagerKey.card_id: id]
        
        case .activateGiftCode(let id):
            value = [ApiManagerKey.type_id: id]
        
        case .viewGiftCode(let id):
            value = [ApiManagerKey.codeId: id]
            
        case .activateMartletCode(let code):
            value = [ApiManagerKey.code: code]
            
        case .orderCallBack(let uid, let serviceID, let username, let phone, let comment, let timeFrom, let timeTo):
            value = [ApiManagerKey.uid: uid,
                     ApiManagerKey.serviceId: serviceID,
                     ApiManagerKey.username: username,
                     ApiManagerKey.phone: phone,
                     ApiManagerKey.comment: comment,
                     ApiManagerKey.timeFrom: timeFrom,
                     ApiManagerKey.timeTo: timeTo]
        
        case .technicStatusList(let page, let count):
            if let pageValue = page {
                value[ApiManagerKey.page] = pageValue
            }
            if let countValue = count {
                value[ApiManagerKey.count] = countValue
            }
            
        case .technicStatusAdd(let code):
            value[ApiManagerKey.code] = code
            
        case .editUserData(let firstname, let surname, let password, let photo, let phone, let otp, let gender, let cityID, let birthday, let joinAccount):
            value = [ApiManagerKey.firstname: firstname,
                     ApiManagerKey.surname: surname,
                     ApiManagerKey.password: password]
            if let photoValue = photo { value[ApiManagerKey.photo] = photoValue }
            if let phoneValue = phone { value[ApiManagerKey.phone] = phoneValue }
            if let otpValue = otp { value[ApiManagerKey.otp] = otpValue }
            if let joinAccount = joinAccount { value[ApiManagerKey.doJoinAccount] = joinAccount ? "true" : "false" }
            if let gender = gender { value[ApiManagerKey.gender] = gender.serverRepresentation }
            if let cityID = cityID { value[ApiManagerKey.cityID] = cityID }
            if let birthday = birthday { value[ApiManagerKey.birthday] = birthday }
            
        case .forgotPasswordRes(let phone, let code):
            value[ApiManagerKey.phone] = phone
            if let codeValue = code { value[ApiManagerKey.otp] = codeValue }
        
        case .getMessages(let page):
            if let unPage = page { value[ApiManagerKey.page] = unPage }
            
        case .uploadFile(let dialigID, let fileString):
            value = [ApiManagerKey.convID: dialigID,
                     ApiManagerKey.file: fileString]
            
        case .addReview(type: let type, email: let email, description: let description):
            value = [ApiManagerKey.type: type.rawValue,
                     ApiManagerKey.email: email,
                     ApiManagerKey.description: description]
            
        case .addTechnicToRepair(code: let code):
            value = [ApiManagerKey.code: code]
            
        case .addRequestToRepair(name: let name, phone: let phone, technicCategory: let technicCategory, requestReason: let requestReason, requestComment: let requestComment, serviceID: let serviceID):
            value = [ApiManagerKey.contactFio: name,
                     ApiManagerKey.contactPhone: phone,
                     ApiManagerKey.technicCategory: technicCategory,
                     ApiManagerKey.requestReasone: requestReason,
                     ApiManagerKey.requestComment: requestComment]
            if let servcieIDValue = serviceID {
                value[ApiManagerKey.serviceID] = servcieIDValue
            }
            
        case .addRequestToInstall(name: let name, phone: let phone, technicCategory: let technicCategory, address: let address, date: let date):
            value = [ApiManagerKey.contactFio: name,
                     ApiManagerKey.contactPhone: phone,
                     ApiManagerKey.technicCategory: technicCategory,
                     ApiManagerKey.address: address,
                     ApiManagerKey.installDate: date]
            
        case let .addRequestToMasterInstall(serviceNumber: serviceNumber, name: name, model: model, phone: phone, technicCategory: technicCategory, date: date):
            value = [ApiManagerKey.requestNumber: serviceNumber,
                     ApiManagerKey.technicCategory: technicCategory,
                     ApiManagerKey.technicModel: model,
                     ApiManagerKey.contactFio: name,
                     ApiManagerKey.contactPhone: phone,
                     ApiManagerKey.installDate: date]
            
        case .addProductToTechnikPark(categoryID: let id, brand: let brand, model: let model, exploitationTime: let exploitationTime, repaired: let repaired):
            value = [ApiManagerKey.technicCategoryID: id,
                     ApiManagerKey.brand: brand,
                     ApiManagerKey.model: model,
                     ApiManagerKey.exploitationTime: exploitationTime,
                     ApiManagerKey.repaired: repaired]
            
        case .searchProduct(field: let field):
            switch field {
            case .brand(let brand):
                value = [ApiManagerKey.brand: brand,
                         ApiManagerKey.field: field.rawValue]
            case .model(let brand, let model):
                value = [ApiManagerKey.brand: brand,
                         ApiManagerKey.model: model,
                         ApiManagerKey.field: field.rawValue]
            }
        case .editTechnicProduct(id: let id, categoryID: let categoryID, brand: let brand, model: let model, exploatitionTime: let time, repaired: let isRepaired):
            value = [ApiManagerKey.id: id,
                     ApiManagerKey.technicCategoryID: categoryID,
                     ApiManagerKey.brand: brand,
                     ApiManagerKey.model: model,
                     ApiManagerKey.exploitationTime: time,
                     ApiManagerKey.repaired: isRepaired]
        case let .activateInsurance(id: id, imei: imei, name: name, middleName: middleName, surname: surname, notificationId: notificationID):
            value = [ApiManagerKey.prsId: id,
                     ApiManagerKey.imei: imei,
                     ApiManagerKey.name: name,
                     ApiManagerKey.lastName: middleName,
                     ApiManagerKey.surname: surname,
                     ApiManagerKey.notificationId: notificationID
            ]
        case let .cities(query: query):
            value = [ApiManagerKey.string: query]
        default: break
        }
        
        value[ApiManagerKey.clientID] = Constants.client_id
        value[ApiManagerKey.client_secret] = Constants.client_secret
        value[ApiManagerKey.partnerName] = Constants.partnerName
        value[ApiManagerKey.appName] = Constants.appName
        value[ApiManagerKey.versionApp] = Constants.versionApp
        print(value)
        return value
    }
    
    public var task: Moya.Task {
        return Moya.Task.requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
}

let plugins: [PluginType] = [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)]

let networkProvider = MoyaProvider<ApiManager>(endpointClosure: endpointClosure, plugins: plugins)

let backgroundNetworkProvider = MoyaProvider(endpointClosure: endpointClosure,
                                             manager: Manager(configuration: URLSessionConfiguration.background(withIdentifier: "com.example.app.background")),
                                             plugins: plugins)
