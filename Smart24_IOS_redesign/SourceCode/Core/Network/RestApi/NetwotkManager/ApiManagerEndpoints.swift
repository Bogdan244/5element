//
//  ApiManagerEndpoints.swift
//  ULC
//
//  Created by Alex on 6/4/16.
//  Copyright © 2016 wezom.com.ua. All rights reserved.
//

import Foundation
import Moya
import SwiftKeychainWrapper

let endpointClosure = {(target: ApiManager) -> Endpoint in
    let endpoint: Endpoint = Endpoint(url: target.baseURL.appendingPathComponent(target.path).absoluteString,
                                                              sampleResponseClosure: {.networkResponse(200, target.sampleData)},
                                                              method: target.method,
                                                              task: target.task,
                                                              httpHeaderFields: target.headers)
    return endpoint
}

enum ApiManagerKey {
    
    static let email            = "email"
    static let token            = "token"
    static let login            = "login"
    static let serviceId        = "service_id"
    static let passwd           = "passwd"
    static let comment          = "comment"
    static let request_id       = "request_id"
    static let rating           = "rating"
    static let firstname        = "firstname"
    static let username         = "username"
    static let name             = "name"
    static let surname          = "surname"
    static let phone            = "phone"
    static let social_type      = "social_type"
    static let social_id        = "social_id"
    static let user_photo       = "user_photo"
    static let uid              = "uid"
    static let activ_code       = "code"
    static let activationCode   = "activation_code" 
    static let password         = "password"
    static let passwdcheck      = "passwdcheck"
    static let image_person     = "image_person"
    static let iSbackup         = "isbackup"
    static let date             = "date"
    static let dialog_id        = "dialog_id"
    static let message          = "message"
    static let timestamp        = "timestamp"
    static let grade            = "grade"
    static let opinion          = "opinion"
    static let notifications    = "notifications"
    static let id               = "id"
    static let action           = "action"
    static let read             = "read"
    static let delete           = "delete"
    static let firstNotification = "notifications[0]"
    static let density          = "density"
    static let password_confirm = "password_confirm"
    static let phone_model      = "phone_model"
    static let code             = "code"
    static let isVideo          = "isVideo"
    static let card             = "card"
    static let card_id          = "card_id"
    static let type_id          = "type_id"
    static let codeId           = "code_id"
    static let fcm_token        = "fcm_token"
    static let timeFrom         = "time_from"
    static let timeTo           = "time_to"
    static let partnerName      = "partner_name"
    static let appName          = "app_name"
    static let versionApp       = "version_app"
    static let clientID         = "client_id"
    static let client_secret    = "client_secret"
    static let otp              = "otp"
    static let page             = "page"
    static let count            = "count"
    static let socialName       = "provider"
    static let accessToken      = "accessToken"
    static let serverAuthCode   = "serverAuthCode"
    static let photo            = "photo"
    static let convID           = "conv_id"
    static let file             = "file"
    static let description      = "description"
    static let contactFio       = "contact_fio"
    static let contactPhone     = "contact_phone"
    static let serviceID        = "service_id"
    static let technicCategory  = "technic_category"
    static let requestReasone   = "request_reason"
    static let requestComment   = "request_comment"
    static let address          = "address"
    static let installDate      = "install_date"
    static let technicCategoryID = "technic_category_id"
    static let brand            = "brand"
    static let model            = "model"
    static let exploitationTime = "exploitation_time"
    static let repaired         = "repaired"
    static let field            = "field"
    static let type             = "type"
    static let requestNumber    = "request_number"
    static let technicModel     = "technic_model"
    static let imei             = "imei"
    static let prsId            = "prs_id"
    static let lastName         = "last_name"
    static let notificationId   = "notification_id"
    static let string           = "string"
    static let doGluingBpm      = "do_gluing_bpm"
    static let gender           = "gender"
    static let cityID           = "city_external_id"
    static let birthday         = "birthday"
    static let doJoinAccount    = "do_join_account"
    static let appAction        = "app_action"
}
