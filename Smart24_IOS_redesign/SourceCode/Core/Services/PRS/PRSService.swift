//
//  PRSService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/11/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

class PRSService {
  
    func getPRSInfo(complition: @escaping (PRSInfoEntity) -> (), failure: (() -> ())?) {
        networkProvider.request(ApiManager.getPRSInfo()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(PRSInfoEntity.self) else {
                    failure?()
                    return
                }
                complition(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func getUserBonusCard(complition: @escaping (BonusCardEntity) -> (), failure: (() ->())?) {
        networkProvider.request(ApiManager.getAllBonuses()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(BonusCardEntity.self) else {
                    failure?()
                    return
                }
                complition(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func activateBonusCard(cardNumber: String, phone: String, complition: @escaping (BonusCardEntity) -> (), failure: (() ->())?) {
        networkProvider.request(ApiManager.addNewBonus(cardNumber, phone)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(BonusCardEntity.self) else {
                    failure?()
                    return
                }
                complition(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func deactivateBonusCard(cardID: Int, complition: (() -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.deactivateBonuse(cardID)) { (result) in
            switch result {
            case .success(_):
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
    
    func removeGift(id: Int) {
        networkProvider.request(ApiManager.removeGift(id: id)) { (result) in
            switch result {
            case .success(_):
                break
            case .failure(_):
                break
            }
        }
    }
    
    
    
}
