//
//  NotificationsService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class NotificationsService {
    
    static let shared = NotificationsService()
    
    var notViewed: Int {
        guard let user = UserEntity.current, user.notReadNotificationCount > 0 else {
            return 0
        }
        return user.notReadNotificationCount
    }
    
    func getNotification(complition: ((NewNotificationsEntity) -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.getNotifications()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewNotificationsEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
            
        }
    }
    
    func readNotification(id: Int, complition: ((NewNotificationEntity) -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.readNotification(id: id)) { (result) in
            switch result {
            case .success(let response):
                guard let items = try? response.map([NewNotificationEntity].self, atKeyPath: "data"), let item = items.first else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func removeNotification(id: Int, complition: (() -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.remoeNotification(id: id)) { (result) in
            switch result {
            case .success(_):
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
    
}
