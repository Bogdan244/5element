//
//  AuthorizationServices.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/30/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import Realm
import MBProgressHUD

enum BPAuthorizationAction {
    case failureWithString(String)
    case failure(ErrorEntity)
    case succes
    case succesWithMassega(String)
    case needSMSConfirm
    case needPhoneNumber(socialName: SocialName, accessToken: String?, serverAuthCode: String?)
    case wrongOTP
    case showDetail(ShortUserEntity?, phone: String?)
    case login(ShortUserEntity?, phone: String?, password: String?)
    case loginAfterRegistration(ShortUserEntity?, phone: String?, password: String?)
    case loginWithBPM(ShortUserEntity?, phone: String?, password: String?)
    case loginWithSocialBPM(ShortUserEntity?, socialName: SocialName, accessToken: String?, serverAuthCode: String?, phone: String?)
    case loginWithSocialJoinAccount(ShortUserEntity?, socialName: SocialName, accessToken: String?, serverAuthCode: String?, phone: String?)
    case updateUserDataJoinAccount(ShortUserEntity?, name: String, surname: String, password: String, photo: String?, phone: String?, otp: String?, gender: Gender?, cityID: String?, birthday: String?, joinAccount: Bool?)
    case succesEditUser
}

class AuthorizationServices {
    
    var otpCode: String?
    var phoneSocial: String? 
    private var forgotPasswordPhone: String?
    
    func cities(query: String, completion: @escaping ([CityEntity]) -> (), failure: (() ->())?) {
        networkProvider.request(.cities(query: query)) { (result) in
            switch result {
                case .success(let response):
                    guard let item = try? response.map(CitiesEntity.self) else {
                        failure?()
                        return
                    }
                    completion(item.cities ?? [])
                case .failure(_):
                    failure?()
            }
        }
    }
    
    func loginUser(phone: String, password: String, needBPM: Bool?, user: ShortUserEntity?, completion: @escaping (BPAuthorizationAction) -> ()) {
        loginUser(phone: phone,
                  password: password,
                  needBPM: needBPM,
                  gender: user?.gender,
                  cityID: user?.city?.cityExternalID,
                  birthday: user?.birthday.serverTimeInterval,
                  name: user?.firstname,
                  surname: user?.surname,
                  completion: completion)
    }
    
    func loginUser(phone: String, password: String?, needBPM: Bool?, gender: Gender?, cityID: String?, birthday: Int?, name: String?, surname: String?, completion: @escaping (BPAuthorizationAction) -> ()) {
        networkProvider.request(.login(phone: phone,
                                       password: password ?? (otpCode ?? ""),
                                       otpCode: otpCode,
                                       needBPM: needBPM,
                                       gender: gender,
                                       cityID: cityID,
                                       birthday: birthday,
                                       name: name,
                                       surname: surname)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            if let actions = json["actions"] as? [String], actions.count > 0 {
                                if let _ = actions.first(where: { $0 == "go_join_account_bpm" }) {
                                    let item = try? response.map(ShortUserEntity.self)
                                    completion(BPAuthorizationAction.loginWithBPM(item, phone: phone, password: password))
                                    return
                                } else if let _ = actions.first(where: { $0 == "go_to_login" }) {
                                    let item = try? response.map(ShortUserEntity.self)
                                    completion(BPAuthorizationAction.login(item, phone: phone, password: password))
                                    return
                                }
                            }
                            if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
                                if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
                                    if let _ = errors.first(where: {$0["code"] as? Int == 1011 }) {
                                        completion(BPAuthorizationAction.wrongOTP)
                                    } else {
                                        completion(BPAuthorizationAction.needSMSConfirm)
                                    }
                                } else {
                                    completion(BPAuthorizationAction.failure(ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError))
                                }
                            } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
                                UserDefaults.standard.set(phone, forKey: Constants.savedPhone)
                                UserDefaults.standard.set(password, forKey: Constants.savedPassword)
                                
                                let db = RealmResultsCache()
                                db.update(obj: item, isUpdate: true)
                                completion(BPAuthorizationAction.succes)
        
                            } else {
                                completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                            }
                        }
                    } catch {
                        completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                    }
                case .failure(_):
                    completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                }
            }
    }
    
    func registrationUser(name: String? = nil,
                          surname: String? = nil,
                          phone: String,
                          password: String? = nil,
                          needBPM: Bool? = nil,
                          gender: Gender? = nil,
                          cityID: String? = nil,
                          birthday: Int? = nil,
                          completion: @escaping (BPAuthorizationAction) -> ()) {
        networkProvider.request(.registration(name: name,
                                              surname: surname,
                                              phone: phone,
                                              password: password ?? otpCode,
                                              phoneModel: Constants.phoneModel,
                                              otp: otpCode,
                                              needBPM: needBPM,
                                              gender: gender,
                                              cityID: cityID,
                                              birthday: birthday
        )) { result in
            switch(result) {
            case let .success(response):
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String:AnyObject]
                    if let actions = dictionary?["actions"] as? [String], actions.count > 0 {
                        if let _ = actions.first(where: { $0 == "go_join_account_bpm" }) {
                            let item = try? response.map(ShortUserEntity.self)
                            completion(BPAuthorizationAction.showDetail(item, phone: phone))
                            return
                        } else if let _ = actions.first(where: { $0 == "go_to_login" }) {
                            let item = try? response.map(ShortUserEntity.self)
                            completion(BPAuthorizationAction.loginAfterRegistration(item, phone: phone, password: password))
                            return
                        }
                    }
                    if let errors = dictionary!["errors"] as? [[String:AnyObject]], errors.count > 0 {
                        if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
                            if let _ = errors.first(where: {$0["code"] as? Int == 1011 }) {
                                completion(BPAuthorizationAction.wrongOTP)
                            } else {
                                completion(BPAuthorizationAction.needSMSConfirm)
                            }
                        } else {
                            completion(BPAuthorizationAction.failure(ErrorFactory.createEntity(dictionary: dictionary) ?? ErrorFactory.serverError))
                        }
                    } else {
                        if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
                            UserDefaults.standard.set(self.phoneSocial, forKey: Constants.savedPhone)
                            let db = RealmResultsCache()
                            db.update(obj: item, isUpdate: true)
                            completion(BPAuthorizationAction.succes)
                        } else {
                            completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                        }
                    }
                } catch {
                    completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                }
            case let .failure(error):
                completion(BPAuthorizationAction.failure(ErrorFactory.error(text: error.localizedDescription)))
            }
        }
    }
    
    func forgotPassword(phone: String, otpCode: String?, completion: @escaping (BPAuthorizationAction) -> ()) {
        networkProvider.request(ApiManager.forgotPasswordRes(phone: phone, code: otpCode)) { [weak self] (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else { return }
                if item.errors.count > 0 {
                    if (200..<300).contains(response.statusCode) {
                        if let _ = item.errors.first(where: {$0.code == 1015 }) {
                            if let _ = item.errors.first(where: {$0.code == 1011 }) {
                                completion(BPAuthorizationAction.wrongOTP)
                            } else {
                                self?.forgotPasswordPhone = phone
                                completion(BPAuthorizationAction.needSMSConfirm)
                            }
                        } else {
                            completion(BPAuthorizationAction.failure(ErrorFactory.createEntity(error: item.errors.first!)))
                        }
                    } else {
                        completion(BPAuthorizationAction.failureWithString(item.message))
                    }
                } else {
                    self?.otpCode = otpCode
                    completion(BPAuthorizationAction.succesWithMassega(item.message))
                }
    
            case .failure(_):
                completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
            }
        }
    }
    
    func confirmOtpCode(otpCode: String, completion: @escaping (BPAuthorizationAction) -> ()) {
        forgotPassword(phone: forgotPasswordPhone!, otpCode: otpCode, completion: completion)
    }
    
    func authWithSocial(socialName: SocialName,
                        accessToken: String? = nil,
                        serverAuthCode: String? = nil,
                        joinAccount: Bool? = nil,
                        needBPM: Bool? = nil,
                        gender: Gender? = nil,
                        cityID: String? = nil,
                        birthday: Int? = nil,
                        name: String? = nil,
                        surname: String? = nil,
                        phone: String? = nil,
                        completion: @escaping (BPAuthorizationAction) -> ()) {
        backgroundNetworkProvider.request(.authWithSocial(socialName: socialName,
                                                accessToken: accessToken,
                                                serverAuthCode: serverAuthCode,
                                                phone: phone,
                                                otp: otpCode,
                                                joinAccount: joinAccount,
                                                needBPM: needBPM,
                                                gender: gender,
                                                cityID: cityID,
                                                birthday: birthday,
                                                name: name,
                                                surname: surname),
                                completion: { (result) in
                                    switch result {
                                    case .success(let response):
                                        do {
                                            if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                                                if let actions = json["actions"] as? [String], actions.count > 0 {
                                                    if let _ = actions.first(where: { $0 == "go_join_account_bpm" }) {
                                                        let item = try? response.map(ShortUserEntity.self)
                                                        completion(BPAuthorizationAction.loginWithSocialBPM(item,
                                                                                                            socialName: socialName,
                                                                                                            accessToken: accessToken,
                                                                                                            serverAuthCode: serverAuthCode,
                                                                                                            phone: phone ?? item?.phone))
                                                        return
                                                    } else if let _ = actions.first(where: { $0 == "go_join_account" }) {
                                                        let item = try? response.map(ShortUserEntity.self)
                                                        completion(BPAuthorizationAction.loginWithSocialJoinAccount(item,
                                                                                                                    socialName: socialName,
                                                                                                                    accessToken: accessToken,
                                                                                                                    serverAuthCode: serverAuthCode, phone: phone ?? item?.phone))
                                                        return
                                                    }
                                                }
                                                if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
                                                    if let _ = errors.first(where: {$0["code"] as? Int == 1016 }) {
                                                        completion(BPAuthorizationAction.needPhoneNumber(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode))
                                                    } else if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
                                                        completion(BPAuthorizationAction.needSMSConfirm)
                                                    } else {
                                                        completion(BPAuthorizationAction.failure(ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError))
                                                    }
                                                } else if response.statusCode == 200, let item = try? response.map(UserEntity.self) {
                                                    UserDefaults.standard.set(self.phoneSocial, forKey: Constants.savedPhone)
                                                    let db = RealmResultsCache()
                                                    db.update(obj: item, isUpdate: true)
                                                    completion(BPAuthorizationAction.succes)
                                                } else {
                                                    completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                                }
                                            }
                                        } catch {
                                            completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                        }
                                    case .failure(let error):
                                        print(error)
                                        completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                    }
            })
        
    }
    
    func changeUserData(name: String,
                        surname: String,
                        password: String,
                        photo: String?,
                        phone: String?,
                        gender: Gender?,
                        cityID: String?,
                        birthday: String?,
                        joinAccount: Bool?,
                        completion: @escaping (BPAuthorizationAction) -> ()) {
        networkProvider.request(ApiManager.editUserData(firstname: name,
                                                        surname: surname,
                                                        password: password,
                                                        photo: photo,
                                                        phone: phone,
                                                        otp: otpCode,
                                                        gender: gender,
                                                        cityID: cityID,
                                                        birthday: birthday,
                                                        joinAccount: joinAccount),
                                completion: { [weak self] (result) in
                                    switch result {
                                    case .success(let response):
                                        do {
                                            if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                                                if let actions = json["actions"] as? [String], actions.count > 0 {
                                                    if let _ = actions.first(where: { $0 == "go_join_account" }) {
                                                        let item = try? response.map(ShortUserEntity.self)
                                                        completion(BPAuthorizationAction.updateUserDataJoinAccount(item, name: name, surname: surname, password: password, photo: photo, phone: phone, otp: self?.otpCode, gender: gender, cityID: cityID, birthday: birthday, joinAccount: joinAccount))
                                                        return
                                                    }
                                                }
                                                if let errors = json["errors"] as? [[String:AnyObject]], errors.count > 0 {
                                                    if let _ = errors.first(where: {$0["code"] as? Int == 1014 }) {
                                                        completion(BPAuthorizationAction.needSMSConfirm)
                                                    } else {
                                                        completion(BPAuthorizationAction.failure(ErrorFactory.createEntity(dictionary: json) ?? ErrorFactory.serverError))
                                                    }
                                                } else if response.statusCode == 200 {
                                                    completion(BPAuthorizationAction.succesEditUser)
                                                    self?.updateUserInfo()
                                                } else {
                                                    completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                                }
                                            }
                                        } catch {
                                            completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                        }
                                    case .failure(_):
                                        completion(BPAuthorizationAction.failure(ErrorFactory.serverError))
                                    }
        })
    }
 
    func updateUserInfo(didUpdate: (() -> ())? = nil, ads: (([Ads]) -> ())? = nil) {
        networkProvider.request(ApiManager.userInfo(), completion: { (result) in
            switch(result) {
            case .success(let response):
                guard let item = try? response.map(UserEntity.self), let user = UserEntity.current else { return }
                RealmResultsCache().change(object: user, changes: { (user) in
                    user.firstname = item.firstname
                    user.surname = item.surname
                    user.phone = item.phone
                    user.userPhoto = item.userPhoto
                    user.gift = item.gift
                    user.availableGifts = item.availableGifts
                    user.notReadNotificationCount = item.notReadNotificationCount
                    user.bonusCard = item.bonusCard
                    user.city = item.city
                    user.gender = item.gender
                    user.birthday = item.birthday
                })
                didUpdate?()
                ads?(item.ads)
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func logOut(complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.logOut()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    failure?()
                    return
                }
                if let user = UserEntity.current {
                    let db = RealmResultsCache()
                    db.delete(obj: user)
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
}

protocol AuthorizationUIActionsProtocol {
    var authService: AuthorizationServices { get }
}

extension AuthorizationUIActionsProtocol where Self: UIViewController {
    
    func registration(name: String? = nil, surname: String? = nil, phone: String, password: String? = nil, needBPM: Bool? = nil, gender: Gender? = nil, cityID: String? = nil, birthday: Int? = nil) {
        MBProgressHUD.showAdded(to: view, animated: true)
        authService.registrationUser(name: name, surname: surname, phone: phone, password: password, needBPM: needBPM, gender: gender, cityID: cityID, birthday: birthday) { [weak self] (loginAction) in
            self?.handleAuthorizationAction(loginAction: loginAction, completion: { [weak self] in
                self?.registration(name: name, surname: surname, phone: phone, password: password, needBPM: needBPM, gender: gender, cityID: cityID, birthday: birthday)
            })
        }
    }
    
    func login(phone: String, password: String?, needBPM: Bool?, gender: Gender?, cityID: String?, birthday: Int?, name: String?, surname: String?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        authService.loginUser(phone: phone, password: password, needBPM: needBPM, gender: gender, cityID: cityID, birthday: birthday, name: name, surname: surname) { [weak self] (loginAction) in
            self?.handleAuthorizationAction(loginAction: loginAction, completion: { [weak self] in
                self?.login(phone: phone, password: password, needBPM: needBPM, gender: gender, cityID: cityID, birthday: birthday, name: name, surname: surname)
            })
        }
    }
    
    func login(phone: String, password: String, needBPM: Bool?, user: ShortUserEntity?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        authService.loginUser(phone: phone, password: password, needBPM: needBPM, user: user) { [weak self] (loginAction) in
            self?.handleAuthorizationAction(loginAction: loginAction, completion: { [weak self] in
                self?.login(phone: phone, password: password, needBPM: needBPM, user: user)
            })
        }
    }
    
    func changeUserData(name: String, surname: String, password: String, photo: String?, phone: String?, gender: Gender?, cityID: String?, birthday: String?, joinAccount: Bool?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        authService.changeUserData(name: name,
                                   surname: surname,
                                   password: password,
                                   photo: photo,
                                   phone: phone,
                                   gender: gender,
                                   cityID: cityID,
                                   birthday: birthday, joinAccount: joinAccount) { [weak self] (authAction) in
            self?.handleAuthorizationAction(loginAction: authAction, completion: {
                self?.changeUserData(name: name, surname: surname, password: password, photo: photo, phone: phone, gender: gender, cityID: cityID, birthday: birthday, joinAccount: joinAccount)
            })
        }
    }
    
    func loginSocial(socialName: SocialName,
                     token: String?,
                     serverAuthCode: String? = nil,
                     joinAccount: Bool? = nil,
                     needBPM: Bool? = nil,
                     gender: Gender? = nil,
                     cityID: String? = nil,
                     birthday: Int? = nil,
                     name: String? = nil,
                     surname: String? = nil,
                     phone: String? = nil) {
        MBProgressHUD.showAdded(to: view, animated: true)
        authService.authWithSocial(socialName: socialName,
                                   accessToken: token,
                                   serverAuthCode: serverAuthCode,
                                   joinAccount: joinAccount,
                                   needBPM: needBPM,
                                   gender: gender,
                                   cityID: cityID,
                                   birthday: birthday,
                                   name: name,
                                   surname: surname,
                                   phone: phone) { [weak self] (loginAction) in
                                    self?.handleAuthorizationAction(loginAction: loginAction, completion: { [weak self] in
                                        self?.loginSocial(socialName: socialName, token: token, serverAuthCode: serverAuthCode, joinAccount: joinAccount, needBPM: needBPM, gender: gender, cityID: cityID, birthday: birthday, name: name, surname: surname, phone: phone)
            })
        }
    }
    
    func loginSocial(socialName: SocialName, token: String?, serverAuthCode: String? = nil, joinAccount: Bool? = nil, needBPM: Bool? = nil, user: ShortUserEntity?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        loginSocial(socialName: socialName,
                    token: token,
                    serverAuthCode: serverAuthCode,
                    joinAccount: joinAccount,
                    needBPM: needBPM,
                    gender: user?.gender,
                    cityID: user?.city?.cityExternalID,
                    birthday: user?.birthday.serverTimeInterval,
                    name: user?.firstname,
                    surname: user?.surname,
                    phone: user?.phone)
    }
    
    
    func handleAuthorizationAction(loginAction: BPAuthorizationAction, completion: @escaping () -> ()) {
        MBProgressHUD.hide(for: view, animated: true)
        switch loginAction {
        case .failureWithString(let str):
            self.showAlertMessage(title: "", message: str)
        case .failure(let error):
            self.showAlertMessage(title: "", message: error.data )
        case .succes:
            PresenterImpl().openContainerVC()
        case .needSMSConfirm:
            smsConfirm(message: "", completion: completion)
        case let .needPhoneNumber(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode):
            PresenterImpl().openRegistrationStart(state: .socialLogin(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: nil), authService: self.authService)
        case .wrongOTP:
            smsConfirm(message: "Неверный код", completion: completion)
        case .succesWithMassega(let message):
            self.showAlertMessage(title: "", message: message)
        case .showDetail(let user, let phone):
            guard let firstName = user?.firstname, let surname = user?.surname else {
                PresenterImpl().openNewRegistrationVC(authService: self.authService, user: user, state: .registration, needBPM: false, phone: phone, password: nil)
                return
            }
            let nameStr = (firstName) + " " + (surname)
            let str = NSAttributedString(string: nameStr + ", " + "это Вы?")
            showAlertMessage(attributedString: str, firstButtonTitle: "НЕТ", secondButtonTitle: "ДА", complition: { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .registration, needBPM: false, phone: phone, password: nil)
            }) { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .registration, needBPM: true, phone: phone, password: nil)
            }
        case .login(let user, let phone, let password):
            login(phone: user?.phone ?? "", password: password ?? (authService.otpCode ?? ""), needBPM: false, user: nil)
        case .loginWithBPM(let user, let phone, let password):
            guard let firstName = user?.firstname, let surname = user?.surname else {
                PresenterImpl().openNewRegistrationVC(authService: self.authService, user: user, state: .login, needBPM: false, phone: phone, password: password)
                return
            }
            let nameStr = (firstName) + " " + (surname)
            let str = NSAttributedString(string: nameStr + ", " + "это Вы?")
            showAlertMessage(attributedString: str, firstButtonTitle: "НЕТ", secondButtonTitle: "ДА", complition: { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .login, needBPM: false, phone: phone, password: password)
            }) { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .login, needBPM: true, phone: phone, password: password)
            }
        case let .loginWithSocialBPM(user, socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: phone):
            guard let firstName = user?.firstname, let surname = user?.surname else {
                PresenterImpl().openNewRegistrationVC(authService: self.authService, user: user, state: .socialLogin(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: phone), needBPM: false, phone: phone, password: nil)
                return
            }
            let nameStr = (firstName) + " " + (surname)
            let str = NSAttributedString(string: nameStr + ", " + "это Вы?")
            showAlertMessage(attributedString: str, firstButtonTitle: "НЕТ", secondButtonTitle: "ДА", complition: { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .socialLogin(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: phone), needBPM: false, phone: phone, password: nil)
            }) { [weak self] in
                PresenterImpl().openNewRegistrationVC(authService: self?.authService, user: user, state: .socialLogin(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: phone), needBPM: true, phone: phone, password: nil)
            }
            
        case let .loginWithSocialJoinAccount(user, socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: phone):
            guard let firstName = user?.firstname, let surname = user?.surname else {
                loginSocial(socialName: socialName, token: accessToken, serverAuthCode: serverAuthCode, joinAccount: false, needBPM: nil, gender: nil, cityID: nil, birthday: user?.birthdayInt, name: nil, surname: nil, phone: phone)
                return
            }
            let nameStr = (firstName) + " " + (surname)
            let str = NSAttributedString(string: nameStr + ", " + "это Вы?")
            showAlertMessage(attributedString: str, firstButtonTitle: "НЕТ", secondButtonTitle: "ДА", complition: { [weak self] in
                self?.loginSocial(socialName: socialName, token: accessToken, serverAuthCode: serverAuthCode, joinAccount: false, needBPM: nil, gender: user?.gender, cityID: user?.city?.cityExternalID, birthday: user?.birthdayInt, name: firstName, surname: surname, phone: phone)
            }) { [weak self] in
                self?.loginSocial(socialName: socialName, token: accessToken, serverAuthCode: serverAuthCode, joinAccount: true, needBPM: nil, gender: user?.gender, cityID: user?.city?.cityExternalID, birthday: user?.birthdayInt, name: firstName, surname: surname, phone: phone)
            }
        case let .updateUserDataJoinAccount(user, name: name, surname: surname, password: password, photo: photo, phone: phone, otp: _, gender: gender, cityID: cityID, birthday: birthday, joinAccount: _):
            guard let firstName = user?.firstname, let surnameUser = user?.surname else {
                changeUserData(name: name, surname: surname, password: password, photo: photo, phone: phone, gender: gender, cityID: cityID, birthday: birthday, joinAccount: false)
                return
            }
            let nameStr = (firstName) + " " + (surnameUser)
            let str = NSAttributedString(string: nameStr + ", " + "это Вы?")
            showAlertMessage(attributedString: str, firstButtonTitle: "НЕТ", secondButtonTitle: "ДА", complition: { [weak self] in
                self?.changeUserData(name: name, surname: surname, password: password, photo: photo, phone: phone, gender: gender, cityID: cityID, birthday: birthday, joinAccount: false)
            }) { [weak self] in
                self?.changeUserData(name: name, surname: surname, password: password, photo: photo, phone: phone, gender: gender, cityID: cityID, birthday: birthday, joinAccount: true)
            }
        case .succesEditUser:
            self.showAlertMessage(title: "", message: "Данные пользователя изменены", complition: {
                self.navigationController?.popViewController(animated: true)
            }, cancel: nil)
        case .loginAfterRegistration(_, phone: let phone, password: _):
            PresenterImpl().openNewLoginVC(phone: phone, authService: self.authService)
        }
        
    }
    
    func smsConfirm(message: String?, completion: @escaping () -> ()) {
        let alert = UIAlertController(title: "Введите код из смс", message: message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            if #available(iOS 12.0, *) {
                textField.textContentType = UITextContentType.oneTimeCode
            }
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert, weak self] (_) in
            self?.authService.otpCode = alert?.textFields?.first!.text
            completion()
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
