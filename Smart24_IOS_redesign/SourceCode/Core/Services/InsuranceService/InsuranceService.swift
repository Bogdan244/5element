//
//  InsuranceService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/31/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class InsuranceService {
    
    func activateInsurance(id: Int,
                           imei: String,
                           name: String,
                           middleName: String,
                           surname: String,
                           notificationId: Int,
                           complition: ((MyInsuranceServiceEntity) -> ())?,
                           failure: ((Error) -> ())?) {
        networkProvider.request(ApiManager.activateInsurance(id: id,
                                                             imei: imei,
                                                             name: name,
                                                             middleName: middleName,
                                                             surname: surname,
                                                             notificationId: notificationId)) { (result) in
                                                                switch result {
                                                                    case .success(let response):
                                                                    guard let item = try? response.map(MyInsuranceEntity.self) else {
                                                                        failure?(NewError.Errors.cantParsingModel(type: MyInsuranceEntity.self))
                                                                        return
                                                                    }
                                                                    if let error = item.errors.first {
                                                                        failure?(error)
                                                                    } else if let insurance = item.insurance {
                                                                        complition?(insurance)
                                                                    }
                                                                case .failure(let error):
                                                                    failure?(error)
                                                                }
        }
    }
    
    func removeInsurance(id: Int, complition: (() -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.removeInsurance(id: id)) { (result) in
            switch result {
            case .success(_):
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
}
