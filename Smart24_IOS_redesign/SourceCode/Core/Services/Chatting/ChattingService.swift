//
//  ChattingService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/24/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications
import MessageKit

protocol ChattingServiceDelegate: NSObjectProtocol {
    
    func didUpdateImage(forChatMessage: ChattingMessage)
    func didReceiveMessage()
}

class ChattingService: NSObject, UNUserNotificationCenterDelegate {
    
    static let shared = ChattingService()
    
    weak var delegate: ChattingServiceDelegate?
    
    var isDialogExist : Bool {
        guard let id = dialogID, id > 0 else { return false }
        return true
    }
    var inputText = ""
    
    var expertCallId: String?
    private var dialogID: Int?
    private var nextPage: Int?
    private override init() {}
    
    func configurateFirebaseMessaging(application: UIApplication){
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func sendMessage(message: String,
                     complition: @escaping (_ newSendMessageEntity: NewSendMessageEntity) -> Void,
                     failure: @escaping (_ error: NSError) -> Void) {
        networkProvider.request(ApiManager.sendMessage(message, (UserEntity.current?.uid)!, dialogID)) { [unowned self] (result) in
            switch result {
            case .success(let response):
                do {
                    let item = try response.map(NewSendMessageEntity.self)
                    self.expertCallId = item.ccID
                    self.dialogID = Int(item.dialogID)
                    complition(item)
                } catch {
                    print(error)
                    failure(error as NSError)
                }
            case .failure(let error):
                failure(error as NSError)
            }
        }
//        networkProvider.request(ApiManager.sendMessage(message, "ru", dialogID)) { (result) in
//            switch result {
//            case let .success(response):
//                do {
//                    let dictonary = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String:AnyObject]
//                    guard let convId = dictonary!["dialog_id"] as? Int else {
//                        return
//                    }
//                    self.dialogID = convId
//                    complition(dictonary!)
//                } catch let error {
//                    failure(error as NSError)
//                    print(error)
//                }
//            case let .failure(error):
//                failure(error as NSError)
//                print("\(error)")
//            }
//        }
    }

    func uploadFile(imageBase64: String,
                    complition: @escaping () -> (),
                    failure: @escaping (_ error: Error?) -> Void) {
        guard let dialogID = self.dialogID else {
            failure(nil)
            return
        }
        networkProvider.request(ApiManager.uploadFile(dialogID: dialogID, file: imageBase64)) { (result) in
            switch result {
            case .success(let response):
                complition()
            case .failure(let error):
                failure(error)
            }
        }
    }
    
//    func sendImage(imageBase64: String,
//                   complition: @escaping (_ dictionary: [String:AnyObject]) -> Void,
//                   failure: @escaping (_ error: NSError) -> Void) {
//        guard let dialogIDUn = dialogID else {
//            failure(NSError())
//            return
//        }
//        networkProvider.request(ApiManager.sendImage(dialogIDUn, imageBase64)) { (result) in
//            switch result {
//            case let .success(response):
//                do {
//                    if let dictionary = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String:AnyObject] {
//                        complition(dictionary)
//                    }
//                } catch {
//                    failure(error as NSError)
//                }
//            case let .failure(error):
//                failure(error as NSError)
//            }
//        }
//    }
    
    func firstMessages(complition: @escaping (_ messages: [ChattingMessage]) -> Void,
                       failure: @escaping (_ error: NSError) -> Void) {
        getMessages(page: nil, complition: complition, failure: failure)
    }
    
    func nextMessages(complition: @escaping (_ messages: [ChattingMessage]) -> Void,
                      failure: @escaping (_ error: NSError) -> Void) {
        getMessages(page: self.nextPage, complition: complition, failure: failure)
    }
    
    func createChattingMessage(messageEntity: MessageEntity) -> ChattingMessage? {
        if let url = URL(string: messageEntity.attachedFile) {
            return ChattingMessage(url: url,
                                   sender: Sender(id: messageEntity.messageClass, displayName: messageEntity.name),
                                   messageID: String(messageEntity.id),
                                   date: Date(timeIntervalSince1970: TimeInterval(messageEntity.time)))
        } else if messageEntity.messageClass == "notice" || messageEntity.messageClass == "ended" {
            return ChattingMessage(custom: NoticeMessage(text: messageEntity.message),
                                   sender: Sender(id: messageEntity.messageClass, displayName: messageEntity.name),
                                   messageId: String(messageEntity.id),
                                   date: Date(timeIntervalSince1970: TimeInterval(messageEntity.time)))
        } else if MessageValidator.isChatLink(string: messageEntity.message) {
            if dialogID == messageEntity.convid {
                return ChattingMessage(custom: VideoLinkMessage(text: "Показать проблему эксперту", link: MessageValidator.chatLink(string: messageEntity.message)),
                                       sender: Sender(id: messageEntity.messageClass, displayName: messageEntity.name),
                                       messageId: String(messageEntity.id),
                                       date: Date(timeIntervalSince1970: TimeInterval(messageEntity.time)))
            } else {
                return nil
            }
        }
        print(messageEntity.messageClass)
        return ChattingMessage(attributedText: NSAttributedString(string: messageEntity.message, attributes: ChattingMessage.attributedForMessage),
                               sender: Sender(id: messageEntity.messageClass, displayName: messageEntity.name),
                               messageId: String(messageEntity.id),
                               date: Date(timeIntervalSince1970: TimeInterval(messageEntity.time)))
    }
    
    func getMessages(page: Int?,
                     complition: @escaping (_ messages: [ChattingMessage]) -> Void,
                     failure: @escaping (_ error: NSError) -> Void) {
        networkProvider.request(ApiManager.getMessages(page)) { [unowned self] (result) in
            switch result {
            case .success(let response):
                do {
                    let messages = try response.map(UserMessagesEntity.self)
                    self.dialogID = messages.convID
                    var chattingMessages = [ChattingMessage]()
                    for message in messages.data {
                        if let message = self.createChattingMessage(messageEntity: message) {
                            chattingMessages.append(message)
                        }
                    }
                    self.nextPage = messages.currentPage + 1
                    complition(chattingMessages)
                } catch {
                    failure(error as NSError)
                }
                
            case .failure(let error):
                failure(error as NSError)
            }
        }
    }
    
    func closeDialog(grade: String?,
                     opinion: String,
                     complition: @escaping (_ isSucces: Bool) -> Void,
                     failure: @escaping (_ error: Error) -> Void) {
        
        guard let dialogID = dialogID else {
            complition(false)
            return
        }
        
        networkProvider.request(ApiManager.closeDialog((UserEntity.current?.uid)!, "\(dialogID)", grade, opinion)) { (result) in
            switch result {
            case .success(let response):
                do {
                    if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject], let isSuccess = json["success"] as? Bool {
                        complition(isSuccess)
                    } else {
                        complition(false)
                    }
                } catch {
                    failure(error)
                }
            case .failure(_):
                break
            }
        }
        
        
    }
    
    func receiveMessage () {
        delegate?.didReceiveMessage()
    }
    
}

//extension ChattingService: ChatMessageProtocol {
//
//    func didGetImage(image: UIImage, forChatMessage: ChattingMessage) {
//        guard let delegateObject = delegate else { return }
//        delegateObject.didUpdateImage(forChatMessage: forChatMessage)
//    }
//}

extension ChattingService: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }

}
