//
//  MessageValidator.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 3/1/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class MessageValidator {
    
    let str = "<a href=\"https://appr.tc/r/operator_17809_fkokgx7q6co427\" target=\"_blank>Показать проблему эксперту</a>"
    
    static func isChatLink(string: String) -> Bool {
        return matches(for: "(https://appr.tc/r/operator_.+)\" ", in: string).count > 0
    }
    
    static func chatLink(string: String) -> String {
        return firstMatch(for: "(https://appr.tc/r/operator_.+)\" ", in: string).replacingOccurrences(of: "https://appr.tc/r/", with: "").replacingOccurrences(of: "\" ", with: "")
    }
    
    static private func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    static private func firstMatch(for regex: String, in text: String) -> String {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            guard let result = regex.firstMatch(in: text, range: NSRange(text.startIndex..., in: text)) else { return "" }
            return String(text[Range(result.range, in: text)!])
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return ""
        }
    }
    
}
