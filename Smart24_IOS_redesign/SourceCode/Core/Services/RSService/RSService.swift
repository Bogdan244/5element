//
//  RSService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/23/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class RSService {
    
    static let shared = RSService()
    private init() {}
    
    func getRequestServicesInfo(complition: @escaping (RequestServiceInfoEntity) -> (), failure: (() -> ())?) {
        networkProvider.request(ApiManager.getRequestServiceInfo()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(RequestServiceInfoEntity.self) else {
                    failure?()
                    return
                }
                complition(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func getRequestServices(complition: ((ServicesArrayEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.getRequestServices()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(ServicesArrayEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func getServices(complition: ((MyServiceArrayEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.getServices()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(MyServiceArrayEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func removeService(id: Int, complition: (() -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.removeService(id: id)) { (result) in
            switch result {
            case .success(_):
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
    
    func removeMartletService(id: Int, complition: (() -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.removeMartletService(id: id)) { (result) in
            switch result {
            case .success(_):
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
    
    func activateCode(code: String, complition: ((NewPRSEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.activateService((UserEntity.current?.uid)!, code)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewPRSEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func activateMartletCode(code: String, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.activateMartletCode(code)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewPRSEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    private func checkCode(inData data: Data, complition: (() -> ())?, failure: (() -> ())?) {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDict = json as? [String: Any],
            let jsonArray = jsonDict["data"] as? [[String: Any]],
            jsonArray.count > 0 else {
                failure?()
                return
        }
        complition?()
    }
    
    func addReview(type: FeedbackType, email: String, description: String, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.addReview(type: type, email: email, description: description)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func addTechnicToRepair(code: String, complition: (() -> ())?, failure: ((String) -> ())?) {
        networkProvider.request(ApiManager.addTechnicToRepair(code: code)) { (result) in
            switch result {
            case .success(let response):
                self.checkMessage(inData: response.data, complition: complition, failure: failure)
            case .failure(_):
                failure?("Ошибка сервера")
            }
        }
    }
    
    private func checkMessage(inData data: Data, complition: (() -> ())?, failure: ((String) -> ())?) {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDict = json as? [String: Any],
            let errors = jsonDict["errors"] as? [[String: Any]],
            let error = errors.first,
            let string = error["data"] as? String else {
                complition?()
                return
        }
        failure?(string)
    }
    
    func createRequestToRepair(name: String,
                               phone: String,
                               technicCategory: String,
                               requestReason: String,
                               requestComment: String,
                               serviceID: Int?,
                               complition: ((NewBaseEntity) -> ())?,
                               failure: (() -> ())?) {
        networkProvider.request(ApiManager.addRequestToRepair(name: name, phone: phone, technicCategory: technicCategory, requestReason: requestReason, requestComment: requestComment, serviceID: serviceID)) { (result) in
                switch result {
                case .success(let response):
                    guard let item = try? response.map(NewBaseEntity.self) else {
                        return
                    }
                    complition?(item)
                case .failure(_):
                    failure?()
                }
        }
    }
    
    func createRequestToInstall(name: String,
                                phone: String,
                                technicCategory: String,
                                address: String,
                                date: String,
                                complition: ((NewBaseEntity) -> ())?,
                                failure: (() -> ())?) {
        networkProvider.request(ApiManager.addRequestToInstall(name: name, phone: phone, technicCategory: technicCategory, address: address, date: date)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func createRequestToMasterInstall(serviceNumber: String,
                                      name: String,
                                      model: String,
                                      phone: String,
                                      technicCategory: String,
                                      date: String,
                                      complition: ((NewBaseEntity) -> ())?,
                                      failure: (() -> ())?) {
        networkProvider.request(ApiManager.addRequestToMasterInstall(serviceNumber: serviceNumber, name: name, model: model, phone: phone, technicCategory: technicCategory, date: date)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
}
