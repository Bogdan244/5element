//
//  DBManager.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import RealmSwift

class RealmResultsCache {
    
    let realm = try! Realm()
    
    func fetchObjects(type: Object.Type, query: String) -> Results<Object> {
        do {
            let predicate = NSPredicate(format: query)
            return realm.objects(type).filter(predicate)
        }
    }
    
    func updateWithKey<T: Object>(type: T.Type, value: [String: AnyObject], isUpdate: Bool) {
        do {
            try! realm.write {
                realm.create(type, value: value, update: isUpdate ? .all : .error)
            }
        }
    }
    
    func updateOptionally<T: Object>(type: T.Type, value: AnyObject, key: String) {
        do {
            try! realm.write {
                let object = get(type: type)
                object.first?.setValue(value, forKey: key)
            }
        }
    }
    
    func deleteAll() {
        do {
            try! realm.write {
                realm.deleteAll()
            }
        }
    }
    
    func insert(obj: Object) {
        do {
            try! realm.write {
                realm.add(obj)
            }
        }
    }
    
    func delete(obj: Object) {
        do {
            try! realm.write {
                realm.delete(obj)
            }
        }
    }
    
    func update(obj: Object, isUpdate: Bool = true) {
        do {
            try! realm.write {
                realm.add(obj, update: isUpdate ? .all : .error)
            }
            
        }
    }
    
    func get<T: Object>(type: T.Type) -> Results<T> {
        do {
            return realm.objects(type)
        }
    }
    
    func getArray<T: Object>(type: T.Type) -> Array<T> {
        do {
            let realm = try! Realm()
            let array = Array(realm.objects(T.self))
            return array
        }
    }
    
    func change<T: Object>(object: T, changes: (_ object: T) -> Void) {
        do {
            try! realm.write {
                changes(object)
            }
        }
    }
    
}
