//
//  TechnicParkService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class TechnicParkService {
    
    func getProductListFromTechnicPark(complition: ((TechnicParkEntityList) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.getProductListFromTechnicPark()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(TechnicParkEntityList.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func getTechnicParkInfo(complition: ((TechnikParkInfoEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.getTechnicParkInfo()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(TechnikParkInfoEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func addProductToTechnicPark(id: Int, brand: String, model: String, exploitationTime: String, repaired: Int, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.addProductToTechnikPark(categoryID: id, brand: brand, model: model, exploitationTime: exploitationTime, repaired: repaired)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func removeProductFromTechnicPark(technic: TechnicParkEntity, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.removeProductFromTechnikPark(id: technic.id)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func editProduct(id: Int, categoryID: Int, brand: String, model: String, exploatitionTime: String, repaired: Int, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.editTechnicProduct(id: id, categoryID: categoryID, brand: brand, model: model, exploatitionTime: exploatitionTime, repaired: repaired)) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func searchProduct(technic:TechnicParkEntity, complition: ((NewBaseEntity) -> ())?, failure: (() -> ())?) {
        networkProvider.request(ApiManager.searchProduct(field: Field(technic: technic))) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewBaseEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
}

extension Field {
    init(technic:TechnicParkEntity) {
        if !technic.model.isEmpty {
            self = .brand(brand: technic.brand)
        } else {
            self = .model(brand: technic.brand, model: technic.model)
        }
    }
}
