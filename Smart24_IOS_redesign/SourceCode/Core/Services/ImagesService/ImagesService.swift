//
//  ImagesService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/26/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import UIKit

class ImageService {
    
    class func loadImage(fromURL: URL,
                         complition: @escaping (_ image: UIImage) -> Void,
                         failure: @escaping () -> Void) {
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: fromURL), let image = UIImage(data: data) else {
                failure()
                return
            }
            DispatchQueue.main.async {
                complition(image)
            }
        }
    }
    
    class func saveImageToDisk(image: UIImage, path: String) {
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask,
                                               appropriateFor: nil,
                                               create: true)
        let path = path.replacingOccurrences(of: "/", with: "")
        if let fileURL = dir?.appendingPathComponent(path) {
            try? image.pngData()?.write(to: fileURL, options: .atomic)
        }
    }
    
    class func loadImageFromDisk(path: String) -> UIImage? {
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask,
                                               appropriateFor: nil,
                                               create: false)
        let path = path.replacingOccurrences(of: "/", with: "")
        guard let fileURL = dir?.appendingPathComponent(path), let imageData = try? Data(contentsOf: fileURL) else {
            return nil
        }
        return UIImage(data: imageData)
    }
    
    static let cache = NSCache<NSString, UIImage>()
    
    class func cachingImage(image: UIImage, key: String) {
        ImageService.cache.setObject(image, forKey: key as NSString)
    }
    
    class func getImage(key: String) -> UIImage? {
        guard let image = ImageService.cache.object(forKey: key as NSString) else {
            return nil
        }
        return image
    }
}
