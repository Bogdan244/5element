//
//  GiftsService.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/23/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class GiftsService {
    
    static let shared = GiftsService()
    
    private init() {}
    
    var notViewed: Int {
        guard let user = UserEntity.current, user.availableGifts > 0, user.gift == true else {
            return 0
        }
        return user.availableGifts
    }
    
    func getUserGifts(complition: ((UserGifts) -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.allUserGifts()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(UserGifts.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func getAvailableGifts(complition: ((NewGiftsEntity) -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.getGifts()) { (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(NewGiftsEntity.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func activateGift(id: Int, complition: ((ActivateGift) -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.activateGiftCode(id)) { [unowned self] (result) in
            switch result {
            case .success(let response):
                guard let item = try? response.map(ActivateGift.self) else {
                    failure?()
                    return
                }
                complition?(item)
            case .failure(_):
                failure?()
            }
        }
    }
    
    func viewGiftCode(id: Int, complition: (() -> ())?, failure: (() ->())?) {
        networkProvider.request(ApiManager.viewGiftCode(id)) { (result) in
            switch result {
            case .success(let response):
                print(response)
//                                guard let item = try? response.map(ActivateGift.self) else {
//                                    failure?()
//                                    return
//                                }
                complition?()
            case .failure(_):
                failure?()
            }
        }
    }
    
}
