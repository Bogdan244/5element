//
//  GiftUIProtocol.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/12/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation
import MBProgressHUD

protocol GiftUIProtocol {}

extension GiftUIProtocol where Self: UIViewController {
    
    func activateGifts(didActivateGift: (() -> ())?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        GiftsService.shared.getAvailableGifts(complition: { [weak self] (giftEntity) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            GeneralViewModel().presentGiftsVC(gifts: giftEntity.gifts, didSelectGift: { [weak self] (selectedID) in
                guard let self = self else { return }
                MBProgressHUD.showAdded(to: self.view, animated: true)
                GiftsService.shared.activateGift(id: selectedID, complition: { (activeGift) in
                    didActivateGift?()
                    MBProgressHUD.hide(for: self.view, animated: true)
                    PresenterImpl().openGiftVC(gift: activeGift, giftID: nil)
                    }, failure: {
                    MBProgressHUD.hide(for: self.view, animated: true)
                })
                MBProgressHUD.hide(for: self.view, animated: true)
            })
            MBProgressHUD.hide(for: self.view, animated: true)
        }) {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
}
