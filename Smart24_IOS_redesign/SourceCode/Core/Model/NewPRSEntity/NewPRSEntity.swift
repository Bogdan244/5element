//
//  NewPRSEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/12/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class NewPRSEntity: NewBaseEntity {
    
    let success: Bool
    let gift: Bool
    
    enum CodingKeys: String, CodingKey {
        case success
        case gift
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try container.decodeIfPresent(Bool.self, forKey: .success) ?? false
        self.gift = try container.decodeIfPresent(Bool.self, forKey: .gift) ?? false
        try super.init(from: decoder)
    }
    
}
