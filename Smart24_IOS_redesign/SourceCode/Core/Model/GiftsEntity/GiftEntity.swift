//
//  GiftsEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 18.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class GiftEntity: BaseEntity {
    
    dynamic var id = 0
    dynamic var title = ""
    dynamic var giftDescription = ""
    dynamic var imageLink = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id                  <- map[MapperKey.id]
        title               <- map[MapperKey.title]
        giftDescription     <- map[MapperKey.description]
        imageLink           <- map[MapperKey.image_link]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return GiftEntity()
    }
    
}
