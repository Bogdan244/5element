//
//  MyInsuranceEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class MyInsuranceEntity: NewBaseEntity {
    
    let insurance: MyInsuranceServiceEntity?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        insurance = try container.decodeIfPresent(MyInsuranceServiceEntity.self, forKey: .data)
        
        try super.init(from: decoder)
        } catch {
            print(error)
            throw(error)
        }
    }
    
}
