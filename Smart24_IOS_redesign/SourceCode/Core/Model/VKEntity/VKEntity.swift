//
//  VKEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/22/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

@objcMembers class VKEntity: BaseSocialEntity {
    
    dynamic var first_name  = ""
    dynamic var last_name   = ""
    dynamic var photo_50    = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        first_name      <- map[MapperKey.first_name]
        last_name       <- map[MapperKey.last_name]
        photo_50        <- map[MapperKey.photo_50]

    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return VKEntity()
    }
    
    func getFullName() -> String {
        return first_name + " " + last_name
    }
    
}
