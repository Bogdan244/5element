//
//  MyInsuranceServiceEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/3/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class MyInsuranceServiceEntity: Decodable {
    
    let userName: String
    let processDescriptionDefinition: String
    let userLastName: String
    let prsIdForSorting: String
    let statusColor: String
    let imei: String
    let userSurname: String
    let brand: String
    let denied: Int?
    let reported: Int?
    let activatedTime: String
    let name: String
    let insuranceAmount: String
    let type: MyServiceType
    let id: Int
    let prsServiceId: Int
    let payoutDescription: String
    let contractType: String
    let devicePrice: String
    let phone: String?
    let model: String
    let used: Int?
    let createdAtSorting: String
    let processDescriptionTitle: String
    let notActive: Bool?
    let processDescriptionText: String
    let expiredAt: String
    let contractLink: String
    let statusText: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case userName = "user_name"
        case processDescriptionDefinition = "process_description_definition"
        case userLastName = "user_last_name"
        case prsIdForSorting = "prs_id_for_sorting"
        case statusColor = "status_color"
        case userSurname = "user_surname"
        case activatedTime = "activated_time"
        case insuranceAmount = "insurance_amount"
        case prsServiceId = "prs_service_id"
        case payoutDescription = "payout_description"
        case contractType = "contract_type"
        case devicePrice = "device_price"
        case createdAtSorting = "created_at_sorting"
        case processDescriptionTitle = "process_description_title"
        case notActive = "not_active"
        case processDescriptionText = "process_description_text"
        case expiredAt = "expired_at"
        case contractLink = "contract_link"
        case statusText = "status_text"
        case imei, brand, denied, reported, name, type, id, phone, model, used, description
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.userName = try container.decode(String.self, forKey: .userName)
        self.processDescriptionDefinition = try container.decode(String.self, forKey: .processDescriptionDefinition)
        self.userLastName = try container.decode(String.self, forKey: .userLastName)
        self.prsIdForSorting = try container.decode(String.self, forKey: .prsIdForSorting)
        self.statusColor = try container.decode(String.self, forKey: .statusColor)
        self.imei = try container.decode(String.self, forKey: .imei)
        self.userSurname = try container.decode(String.self, forKey: .userSurname)
        self.brand = try container.decode(String.self, forKey: .brand)
        self.denied = try container.decodeIfPresent(Int.self, forKey: .denied)
        self.reported  = try container.decodeIfPresent(Int.self, forKey: .reported)
        self.activatedTime = try container.decode(String.self, forKey: .activatedTime)
        self.name = try container.decode(String.self, forKey: .name)
        self.insuranceAmount = try container.decode(String.self, forKey: .insuranceAmount)
        self.type = try container.decode(MyServiceType.self, forKey: .type)
        self.id = try container.decode(Int.self, forKey: .id)
        self.prsServiceId = try container.decode(Int.self, forKey: .prsServiceId)
        self.payoutDescription = try container.decode(String.self, forKey: .payoutDescription)
        self.contractType = try container.decode(String.self, forKey: .contractType)
        self.devicePrice = try container.decode(String.self, forKey: .devicePrice)
        self.phone = try container.decodeIfPresent(String.self, forKey: .phone)
        self.model = try container.decode(String.self, forKey: .model)
        self.used = try container.decodeIfPresent(Int.self, forKey: .used)
        self.createdAtSorting = try container.decode(String.self, forKey: .createdAtSorting)
        self.processDescriptionTitle = try container.decode(String.self, forKey: .processDescriptionTitle)
        self.notActive = try container.decodeIfPresent(Bool.self, forKey: .notActive)
        self.processDescriptionText = try container.decode(String.self, forKey: .processDescriptionText)
        self.expiredAt = try container.decode(String.self, forKey: .expiredAt)
        self.contractLink = try container.decode(String.self, forKey: .contractLink)
        self.statusText = try container.decode(String.self, forKey: .statusText)
        self.description = try container.decode(String.self, forKey: .description)
        
    }
    
}




