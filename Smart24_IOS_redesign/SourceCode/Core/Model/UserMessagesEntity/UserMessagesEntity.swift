//
//  UserMessagesEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/24/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

class UserMessagesEntity: NewBaseEntity {
    let action: String
    let display: Int
    let data: [MessageEntity]
    let success: Bool
    let convID: Int
    let total: Int
    let currentPage: Int
    let lastPage: Int
    
    private enum CodingKeys: String, CodingKey {
        case action = "action"
        case errors = "errors"
        case display = "display"
        case data = "data"
        case success = "success"
        case convID = "conv_id"
        case total = "total"
        case currentPage = "current_page"
        case lastPage = "last_page"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.action = try container.decode(String.self, forKey: .action)
        self.display = try container.decode(Int.self, forKey: .display)
        self.data = try container.decode([MessageEntity].self, forKey: .data)
        self.success = try container.decode(Bool.self, forKey: .success)
        self.convID = try container.decode(Int.self, forKey: .convID)
        self.total = try container.decode(Int.self, forKey: .total)
        self.currentPage = try container.decode(Int.self, forKey: .currentPage)
        self.lastPage = try container.decode(Int.self, forKey: .lastPage)
      
        try super.init(from: decoder)
    }
    
}

class MessageEntity: Decodable {
    let convid: Int
    let attachedFile: String
    let time: Int
    let id: Int
    let plevel: Int
    let isRead: Int
    let message: String
    let messageClass: String
    let isUread: Int
    let name: String
    let user: String?
    
    enum CodingKeys: String, CodingKey {
        case convid = "convid"
        case attachedFile = "attached_file"
        case time = "time"
        case id = "id"
        case plevel = "plevel"
        case isRead = "is_read"
        case message = "message"
        case messageClass = "class"
        case isUread = "is_uread"
        case name = "name"
        case user = "user"
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let convid = try container.decode(Int.self, forKey: .convid)
        let attachedFile = try container.decode(String.self, forKey: .attachedFile)
        let time = try container.decode(Int.self, forKey: .time)
        let id = try container.decode(Int.self, forKey: .id)
        let plevel = try container.decode(Int.self, forKey: .plevel)
        let isRead = try container.decode(Int.self, forKey: .isRead)
        let message = try container.decode(String.self, forKey: .message)
        let messageClass = try container.decode(String.self, forKey: .messageClass)
        let isUread = try container.decode(Int.self, forKey: .isUread)
        let name = try container.decode(String.self, forKey: .name)
        let user = try container.decodeIfPresent(String.self, forKey: .user)
        self.init(convid: convid,
                  attachedFile: attachedFile,
                  time: time,
                  id: id,
                  plevel: plevel,
                  isRead: isRead,
                  message: message,
                  messageClass: messageClass,
                  isUread: isUread,
                  name: name,
                  user: user)
    }
    
    init(convid: Int, attachedFile: String, time: Int, id: Int, plevel: Int, isRead: Int, message: String, messageClass: String, isUread: Int, name: String, user: String?) {
        self.convid = convid
        self.attachedFile = attachedFile
        self.time = time
        self.id = id
        self.plevel = plevel
        self.isRead = isRead
        self.message = message
        self.messageClass = messageClass
        self.isUread = isUread
        self.name = name
        self.user = user
    }
}

class NewSendMessageEntity: NewBaseEntity {
    
    let expect: String
    let timeOfReceipt: Int
    let notice: String
    let dialogID: Int
    let success: Bool
    let expertID: String?
    let ccID: String?
    let expertName: String?
    
    private enum CodingKeys: String, CodingKey {
        case expect
        case timeOfReceipt = "time_of_receipt"
        case notice = "notice"
        case dialogID = "dialog_id"
        case success
        case expertID = "expert_id"
        case ccID = "cc_id"
        case expertName = "expert_name"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.expect = try container.decode(String.self, forKey: .expect)
        self.timeOfReceipt = try container.decode(Int.self, forKey: .timeOfReceipt)
        self.notice = try container.decode(String.self, forKey: .notice)
        self.dialogID = try container.decode(Int.self, forKey: .dialogID)
        self.success = try container.decode(Bool.self, forKey: .success)
        self.expertID = try container.decodeIfPresent(String.self, forKey: .expertID)
        self.ccID = try container.decodeIfPresent(String.self, forKey: .ccID)
        self.expertName = try container.decodeIfPresent(String.self, forKey: .expertName)
    
        try super.init(from: decoder)
    }
}
