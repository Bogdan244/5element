//
//  MyServiceEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/1/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

enum MyServiceType: String, Decodable {
    case prs
    case martlet
    case insurance
    case undefined
}

enum MyBaseService: Decodable {
    
    case prs(service: MyServiceEntity)
    case martlet(service: MyServiceEntity)
    case insurance(service: MyInsuranceServiceEntity)
    case undefined
    
    init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let identifier = (try? container.decode(MyBaseService.Identifier.self, forKey: .type)) ?? .undefined
            print(identifier.rawValue)
        switch identifier {
        case .prs:
            let service = try MyServiceEntity(from: decoder)
            self = .prs(service: service)
        case .martlet:
            let service = try MyServiceEntity(from: decoder)
            self = .martlet(service: service)
        case .insurance:
            let service = try MyInsuranceServiceEntity(from: decoder)
            self = .insurance(service: service)
        case .undefined:
            self = .undefined
        }
        } catch {
            print(error)
            throw(error)
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case type
    }
    
    enum Identifier: String, Decodable {
        case prs
        case martlet
        case insurance
        case undefined
    }

}

class MyServiceArrayEntity: NewBaseEntity {
    
    var services = [MyBaseService]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.services = try container.decodeIfPresent([MyBaseService].self, forKey: .data) ?? []
        try super.init(from: decoder)
    }
    
}

class MyServiceEntity: Decodable, GiftDetailUIProtocol {
    
    var codeUI: String {
        switch type {
        case .martlet:
            return lincence
        default:
            return code.group(by: 3)
        }
    }
    
    var typeUI: GiftDetailUIType {
        return GiftDetailUIType.myService(type)
    }
    
    var title: String { return name }
    
    var description: String {
        switch type {
        case .martlet:
            return about
        default:
            return productName
        }
    }
    
    let id: Int
    let createdAtSorting: String
    let productName: String
    let salePlace: String
    let shoppingTime: String
    let expired: String
    let productModel: String
    let contractType: String
    let type: MyServiceType
    let code: String
    let productBrand: String
    let name: String
    let contractLink: String

    let lincence: String
    let appLink: String
    let about: String
    let prsIDForSorting: String
    let statusColor: String
    let statusText: String
    let notActive: Bool 
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAtSorting = "created_at_sorting"
        case productName = "product_name"
        case salePlace = "sale_place"
        case shoppingTime = "shopping_time"
        case expired
        case productModel = "product_model"
        case contractType = "contract_type"
        case type
        case code
        case productBrand = "product_brand"
        case name
        case contractLink = "contract_link"
        case lincence = "licence"
        case appLink = "app_link"
        case about = "about"
        case prsIDForSorting = "prs_id_for_sorting"
        case statusColor = "status_color"
        case statusText = "status_text"
        case notActive = "not_active"
    }
    
    required init(from decoder: Decoder) throws {
        
        do {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.createdAtSorting = try container.decodeIfPresent(String.self, forKey: .createdAtSorting) ?? ""
        self.productName = try container.decodeIfPresent(String.self, forKey: .productName) ?? ""
        self.salePlace = try container.decodeIfPresent(String.self, forKey: .salePlace) ?? ""
        self.shoppingTime = try container.decodeIfPresent(String.self, forKey: .shoppingTime) ?? ""
        self.expired = try container.decodeIfPresent(String.self, forKey: .expired) ?? ""
        self.productModel = try container.decodeIfPresent(String.self, forKey: .productModel) ?? ""
        self.contractType = try container.decodeIfPresent(String.self, forKey: .contractType) ?? ""
        self.type = MyServiceType(rawValue: try container.decodeIfPresent(String.self, forKey: .type) ?? "") ?? MyServiceType.undefined
        self.code = try container.decodeIfPresent(String.self, forKey: .code) ?? ""
        self.productBrand = try container.decodeIfPresent(String.self, forKey: .productBrand) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.contractLink = try container.decodeIfPresent(String.self, forKey: .contractLink) ?? ""
        self.lincence = try container.decodeIfPresent(String.self, forKey: .lincence) ?? ""
        self.appLink = try container.decodeIfPresent(String.self, forKey: .appLink) ?? ""
        self.about = try container.decodeIfPresent(String.self, forKey: .about) ?? ""
        self.prsIDForSorting = try container.decodeIfPresent(String.self, forKey: .prsIDForSorting) ?? ""
        self.statusColor = try container.decodeIfPresent(String.self, forKey: .statusColor) ?? ""
        self.statusText = try container.decodeIfPresent(String.self, forKey: .statusText) ?? ""
        self.notActive = try container.decodeIfPresent(Bool.self, forKey: .notActive) ?? false 
        } catch {
            print(error)
            throw error
        }
    }
}


