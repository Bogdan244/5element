//
//  TechnikParkInfoEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/13/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class TechnikParkInfoEntity: NewBaseEntity {
    
    let exploitationTime: [String]
    let brands: [String]
    let technicCategories: [TechnicCategories]
    
    enum CodingKeys: String, CodingKey {
        case exploitationTime = "exploitation_time"
        case brands
        case technicCategories = "technic_categories"
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.exploitationTime = try container.decodeIfPresent([String].self, forKey: .exploitationTime) ?? [String]()
        self.brands = try container.decodeIfPresent([String].self, forKey: .brands) ?? [String]()
        self.technicCategories = try container.decodeIfPresent([TechnicCategories].self, forKey: .technicCategories) ?? [TechnicCategories]()
        try super.init(from: decoder)
    }
}

class TechnicCategories: Decodable {
    
    let id: Int
    let imageBlack: String
    let imageBig: String
    let image: String
    let name: String
    let brands: [String]
    let childrens: [TechnicCategories]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageBlack = "image_black"
        case imageBig = "image_big"
        case image
        case name
        case brands
        case childrens
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.imageBlack = try container.decodeIfPresent(String.self, forKey: .imageBlack) ?? ""
        self.imageBig = try container.decodeIfPresent(String.self, forKey: .imageBig) ?? ""
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.childrens = try container.decodeIfPresent([TechnicCategories].self, forKey: .childrens)
        self.brands = try container.decodeIfPresent([String].self, forKey: .brands) ?? []
    }
}
