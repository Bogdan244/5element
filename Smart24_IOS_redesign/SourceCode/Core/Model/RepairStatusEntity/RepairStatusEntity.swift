//
//  RepairStatusEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/16/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

class RepairStatusEntity: Decodable {
    
    let message: String
    let data: Statuses
    
    enum CodingKeys: String, CodingKey {
        case message 
        case data
    }
    
    init(message: String, data: Statuses) {
        self.message = message
        self.data = data
    }
}

class Statuses: Codable {
    let currentPage: Int
    let lastPage: Int
    let display: Int
    let statuses: [Status]
    let total: Int
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case lastPage = "last_page"
        case display = "display"
        case statuses = "statuses"
        case total = "total"
    }
    
    init(currentPage: Int, lastPage: Int, display: Int, statuses: [Status], total: Int) {
        self.currentPage = currentPage
        self.lastPage = lastPage
        self.display = display
        self.statuses = statuses
        self.total = total
    }
}

class Status: Codable {
    let technic: String
    let status: String
    //let diagnosticResult: String
    
    enum CodingKeys: String, CodingKey {
        case technic = "technic"
        case status = "status"
      //  case diagnosticResult = "diagnostic_result"
    }
    
    init(technic: String, status: String) {
        self.technic = technic
        self.status = status
        //self.diagnosticResult = diagnosticResult
    }
}
