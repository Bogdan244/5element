//
//  Cityentity.swift
//  Smart24_IOS_redesign
//
//  Created by Bogdan on 26.05.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

@objcMembers class CityEntity: Object, Decodable {
    
    dynamic var districtName: String = ""
    dynamic var cityName: String = ""
    dynamic var regionName: String = ""
    dynamic var cityExternalID: String = ""
    
    enum CodingKeys: String, CodingKey {
        case districtName = "district_name"
        case cityName = "city_name"
        case regionName = "region_name"
        case cityExternalID = "city_external_id"
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        districtName = try container.decode(String.self, forKey: .districtName)
        cityName = try container.decode(String.self, forKey: .cityName)
        regionName = try container.decode(String.self, forKey: .regionName)
        cityExternalID = try container.decode(String.self, forKey: .cityExternalID)
    }

    
}

class CitiesEntity: NewBaseEntity {
    
    let cities: [CityEntity]?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self)
            cities = try container.decodeIfPresent([CityEntity].self, forKey: .data)
        
        try super.init(from: decoder)
        } catch {
            print(error)
            throw(error)
        }
    }
    
}
