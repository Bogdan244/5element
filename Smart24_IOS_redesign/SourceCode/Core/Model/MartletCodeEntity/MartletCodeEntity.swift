//
//  MartletCodeEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 29.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class MartletCodeEntity: BaseEntity {
    
    dynamic var code = ""
    dynamic var id = 0
    dynamic var userID = 0
    dynamic var updateAt = ""
    dynamic var createdAt = ""
    dynamic var dependencies: [MartletCodeDependencyEntity]?
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        code                    <- map[MapperKey.code]
        id                      <- map[MapperKey.id]
        userID                  <- map[MapperKey.userID]
        updateAt                <- map[MapperKey.updateAt]
        createdAt               <- map[MapperKey.createdAt]
        dependencies            <- map[MapperKey.dependence]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return MartletCodeEntity()
    }
    
    override static func ignoredProperties() -> [String] {
        return ["dependencies"]
    }
}
