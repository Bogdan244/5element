//
//  FaceBookEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/21/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import RealmSwift
import Realm
import ObjectMapper

@objcMembers class FaceBookEntity: BaseSocialEntity {
    
    dynamic var name    = ""
    dynamic var photo   = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        name    <- map[MapperKey.name]
        photo   <- map[MapperKey.fbPicture]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return FaceBookEntity()
    }

}
