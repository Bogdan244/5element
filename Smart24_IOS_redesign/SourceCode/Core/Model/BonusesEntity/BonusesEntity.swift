//
//  BonusesEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 15.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class BonusesEntity: BaseEntity {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var card = ""
    dynamic var phone = ""
    dynamic var activeBonuses = 0.0
    dynamic var nextActivationDate = ""
    dynamic var nextActivationSum = 0.0
    dynamic var nextCancelingDate = ""
    dynamic var nextCancelingSum = 0.0
    dynamic var nonActiveBonuses = 0.0
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id                          <- map[MapperKey.id]
        name                        <- map[MapperKey.name]
        card                        <- map[MapperKey.card]
        phone                       <- map[MapperKey.phone]
        activeBonuses               <- map[MapperKey.activeBonuses]
        nextActivationDate          <- map[MapperKey.nextActivationDate]
        nextActivationSum           <- map[MapperKey.nextActivationSum]
        nextCancelingDate           <- map[MapperKey.nextCancelingDate]
        nextCancelingSum            <- map[MapperKey.nextCancelingSum]
        nonActiveBonuses            <- map[MapperKey.nonActiveBonuses]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return BonusesEntity()
    }
    
}
