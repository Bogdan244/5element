//
//  InsuranceEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/16/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class InsuranceEntity: Decodable {
    
    var prsId: Int?
    var shoppingTime: Int?
    var surname: String?
    var model: String?
    var price: String?
    var termsTitle: String?
    var brand: String?
    var terms: String?
    var name: String?
    var patronymic: String?
    var title: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case title, description, surname, model, price, brand, name, patronymic, terms
        case prsId = "prs_id"
        case shoppingTime = "shopping_time"
        case termsTitle = "terms_title"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        prsId = try container.decodeIfPresent(Int.self, forKey: .prsId)
        shoppingTime = try container.decodeIfPresent(Int.self, forKey: .shoppingTime)
        surname = try container.decodeIfPresent(String.self, forKey: .surname)
        model = try container.decodeIfPresent(String.self, forKey: .model)
        price = try container.decodeIfPresent(String.self, forKey: .price)
        termsTitle = try container.decodeIfPresent(String.self, forKey: .termsTitle)
        brand = try container.decodeIfPresent(String.self, forKey: .brand)
        terms = try container.decodeIfPresent(String.self, forKey: .terms)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        patronymic = try container.decodeIfPresent(String.self, forKey: .patronymic)
        title = try container.decodeIfPresent(String.self, forKey: .title)
        description = try container.decodeIfPresent(String.self, forKey: .description)
    }
}
