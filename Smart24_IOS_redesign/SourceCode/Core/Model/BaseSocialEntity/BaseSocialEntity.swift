//
//  BaseSocialEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/22/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import RealmSwift
import Realm
import ObjectMapper

@objcMembers class BaseSocialEntity: Object, StaticMappable {
    
    dynamic var id      = ""
    
    func mapping(map: Map) {
        id      <- map[MapperKey.id]
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? BaseSocialEntity else {
            return false;
        }
        return self.id == object.id;
    }
    
    class func objectForMapping(map: Map) -> BaseMappable? {
        return BaseSocialEntity()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
