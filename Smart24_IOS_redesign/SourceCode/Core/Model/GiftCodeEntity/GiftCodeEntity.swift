//
//  GiftCodeEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 21.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class GiftCodeEntity: BaseEntity {
    
    dynamic var code = ""
    dynamic var appLink = ""
    dynamic var activationSuccessText = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        code                    <- map[MapperKey.code]
        appLink                 <- map[MapperKey.app_link]
        activationSuccessText   <- map[MapperKey.activation_success_text]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return GiftCodeEntity()
    }
}
