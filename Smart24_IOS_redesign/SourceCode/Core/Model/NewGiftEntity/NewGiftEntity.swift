//
//  NewGiftEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/9/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class NewGiftsEntity: NewBaseEntity {

    let gifts: [Gift]
    
    private enum CodingKeys: String, CodingKey {
        case gifts = "types"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        gifts = try container.decode([Gift].self, forKey: .gifts)
        
        try super.init(from: decoder)
    }
}

class Gift: Decodable {
    
    let id: Int
    let title: String
    let description: String
    let imageLink: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case imageLink = "image_link"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        description = try container.decode(String.self, forKey: .description)
        imageLink = try container.decode(String.self, forKey: .imageLink)
    }
}

class ActivateGift: NewBaseEntity, GiftDetailUIProtocol {
    var codeUI: String {
        return code
    }
    
    let typeUI = GiftDetailUIType.gift
    let code: String
    let appLink: String
    let imageLink: String
    let activationSuccessText: String
    let description: String
    let title: String
    
    private enum CodingKeys: String, CodingKey {
        case code
        case appLink = "app_link"
        case imageLink = "image_link"
        case activationSuccessText = "activation_success_text"
        case description = "description"
        case title = "title"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(String.self, forKey: .code)
        appLink = try container.decode(String.self, forKey: .appLink)
        imageLink = try container.decode(String.self, forKey: .imageLink)
        activationSuccessText = try container.decode(String.self, forKey: .activationSuccessText)
        description = try container.decode(String.self, forKey: .description)
        title = try container.decode(String.self, forKey: .title)
        
        try super.init(from: decoder)
    }
}

class UserGifts: NewBaseEntity {
    let gifts: [UserGift]
    
    private enum CodingKeys: String, CodingKey {
        case gifts = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        gifts = try container.decode([UserGift].self, forKey: .gifts)
        
        try super.init(from: decoder)
    }
}

class UserGift: Decodable, GiftDetailUIProtocol {
    
    var codeUI: String {
        return code
    }
    
    var typeUI = GiftDetailUIType.gift
    
    var viewed: Int
    let id: Int
    let code: String
    let giftID: Int
    let title: String
    let appLink: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case viewed
        case id
        case code
        case type = "type"
    }
    
    enum CodingKeysGift: String, CodingKey {
        case giftID = "id"
        case title
        case description
        case appLink = "app_link"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        viewed = try container.decode(Int.self, forKey: .viewed)
        id = try container.decode(Int.self, forKey: .id)
        code = try container.decode(String.self, forKey: .code)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeysGift.self, forKey: .type)
        giftID = try nestedContainer.decode(Int.self, forKey: .giftID)
        title = try nestedContainer.decode(String.self, forKey: .title)
        description = try nestedContainer.decode(String.self, forKey: .description)
        appLink = try nestedContainer.decode(String.self, forKey: .appLink)
    }
}
