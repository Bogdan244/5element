//
//  GiftCodeUserEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 22.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class GiftCodeUserEntity: BaseEntity {
    dynamic var viewed = false
    dynamic var id = 0
    dynamic var code = ""
    dynamic var createdAt = ""
    dynamic var userID = 0
    dynamic var updateAt = ""
    dynamic var title = ""
    dynamic var appLink = ""
    dynamic var descriptionGift = ""
    dynamic var imageLink = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    
        viewed              <- map[MapperKey.viewed]
        id                  <- map[MapperKey.id]
        code                <- map[MapperKey.code]
        createdAt           <- map[MapperKey.createdAt]
        userID              <- map[MapperKey.userID]
        updateAt            <- map[MapperKey.updateAt]
        title               <- map[MapperKey.typeTitle]
        appLink             <- map[MapperKey.typeAppLink]
        descriptionGift     <- map[MapperKey.typeDescription]
        imageLink           <- map[MapperKey.typeImageLink]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return GiftCodeUserEntity()
    }
}
