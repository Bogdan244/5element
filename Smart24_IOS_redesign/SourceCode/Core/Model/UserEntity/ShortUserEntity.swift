//
//  ShortUserEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 02.06.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation

public enum Gender: String, CaseIterable, Decodable {
    case men = "Мужской"
    case women = "Женский"
    
    var serverRepresentation: String {
        switch self {
        case .men: return "man"
        case .women: return "woman"
        }
    }
    
    init?(string: String?) {
        switch string {
        case "man":
            self = .men
        case "woman":
            self = .women
        default:
            self.init(rawValue: string ?? "")
        }
    }
}

class ShortUserEntity: Decodable {
    
    var df: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd.mm.yyyy"
        return df
    }()
    
    
    var birthdayInt: Int? {
        guard let birthday = birthday?.timeIntervalSince1970 else { return nil }
        return Int(birthday)
    }
    
    let firstname: String?
    let surname: String?
    let phone: String?
    let gender: Gender?
    let birthday: Date?
    let city: CityEntity?
    
    enum CodingKeys: String, CodingKey {
        case firstname, surname, phone, gender, birthday, city, data
    }
    
    required init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)

        self.firstname = try container.decodeIfPresent(String?.self, forKey: .firstname) ?? nil
        self.surname = try container.decodeIfPresent(String?.self, forKey: .surname) ?? nil
        self.phone = try container.decodeIfPresent(String?.self, forKey: .phone) ?? nil
        let genderStr = try container.decodeIfPresent(String?.self, forKey: .gender) ?? nil
        self.gender = Gender(string: genderStr)
        let date = try container.decodeIfPresent(String?.self, forKey: .birthday) ?? nil
        self.birthday = df.date(from: date ?? "")
        self.city = try container.decodeIfPresent(CityEntity?.self, forKey: .city) ?? nil
            
        } catch {
            print(error)
            throw error
        }
    }

    
}
