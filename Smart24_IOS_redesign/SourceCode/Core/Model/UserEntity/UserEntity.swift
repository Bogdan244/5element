//
//  UserEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class Ads: Decodable {
    
    let redirectLink: String
    let imageLink: String
    
    enum CodingKeys: String, CodingKey {
        case redirectLink = "redirect_link"
        case imageLink = "image_link"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.redirectLink = try container.decodeIfPresent(String.self, forKey: .redirectLink) ?? String()
        self.imageLink = try container.decodeIfPresent(String.self, forKey: .imageLink) ?? String()
    }
}

@objcMembers class UserEntity: Object, Decodable {
    
    dynamic var userKey : String = Constants.userPrimaryKey
    dynamic var expiresIn  = 0
    dynamic var tokenType = ""
    dynamic var uid = ""
    dynamic var patronymic = ""
    dynamic var phone = ""
    dynamic var phoneVerified = 0
    dynamic var isbackup = 0
    dynamic var surname = ""
    dynamic var refreshToken = ""
    dynamic var firstname = ""
    dynamic var backupDate = ""
    dynamic var backupLink = ""
    dynamic var userPhoto = ""
    dynamic var bonusCard = ""
    dynamic var gift = false
    dynamic var email = ""
    dynamic var accessToken = ""
    dynamic var codesNotViewed = 0
    dynamic var availableGifts = 0
    dynamic var notReadNotificationCount = 0
    dynamic var birthday: Date? = nil
    dynamic var gender: String? = nil
    dynamic var city: CityEntity? = nil
    dynamic var ads = [Ads]()

    
    override static func ignoredProperties() -> [String] {
        return ["ads"]
    }
    
    var token: String {
        return "\(tokenType) \(accessToken)"
    }
    
    enum CodingKeys: String, CodingKey {
        case expiresIn = "expires_in"
        case tokenType = "token_type"
        case uid = "id"
        case patronymic = "patronymic"
        case phone = "phone"
        case phoneVerified = "phone_verified"
        case isbackup = "isbackup"
        case surname = "surname"
        case refreshToken = "refresh_token"
        case firstname = "firstname"
        case backupDate = "backup_date"
        case backupLink = "backup_link"
        case userPhoto = "user_photo"
        case bonusCard = "card"
        case gift = "gift"
        case email = "email"
        case accessToken = "access_token"
        case codesNotViewed = "codes_not_viewed"
        case availableGifts = "available_gifts"
        case notReadNotificationCount = "not_read_notifications_count"
        case ads = "ads"
        case birthday = "birthday"
        case gender = "gender"
        case city = "city"
    }
    
    enum DataKey: String, CodingKey {
        case data
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: DataKey.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        expiresIn = try container.decodeIfPresent(Int.self, forKey: .expiresIn) ?? 0
        tokenType = try container.decodeIfPresent(String.self, forKey: .tokenType) ?? ""
        uid = String(try container.decode(Int.self, forKey: .uid))
        patronymic = try container.decodeIfPresent(String.self, forKey: .patronymic) ?? ""
        phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? ""
        phoneVerified = try container.decodeIfPresent(Int.self, forKey: .phoneVerified) ?? 0
        isbackup = try container.decodeIfPresent(Int.self, forKey: .isbackup) ?? 0
        surname = try container.decodeIfPresent(String.self, forKey: .surname) ?? ""
        refreshToken = try container.decodeIfPresent(String.self, forKey: .refreshToken) ?? ""
        firstname = try container.decodeIfPresent(String.self, forKey: .firstname) ?? ""
        backupDate = try container.decodeIfPresent(String.self, forKey: .backupDate) ?? ""
        backupLink = try container.decodeIfPresent(String.self, forKey: .backupLink) ?? ""
        userPhoto = try container.decodeIfPresent(String.self, forKey: .userPhoto) ?? ""
        bonusCard = try container.decodeIfPresent(String.self, forKey: .bonusCard) ?? ""
        gift = try container.decodeIfPresent(Bool.self, forKey: .gift) ?? false
        email = try container.decodeIfPresent(String.self, forKey: .email) ?? ""
        accessToken = try container.decodeIfPresent(String.self, forKey: .accessToken) ?? ""
        codesNotViewed = try container.decodeIfPresent(Int.self, forKey: .codesNotViewed) ?? 0
        availableGifts = try container.decodeIfPresent(Int.self, forKey: .availableGifts) ?? 0
        notReadNotificationCount = try container.decodeIfPresent(Int.self, forKey: .notReadNotificationCount) ?? 0
        let birthdayString = try container.decodeIfPresent(String.self, forKey: .birthday) ?? ""
        birthday = DateFormatter.bithdayFormatter.date(from: birthdayString)
        gender = try container.decodeIfPresent(String.self, forKey: .gender) ?? ""
        city = try container.decodeIfPresent(CityEntity.self, forKey: .city)
        ads = try container.decodeIfPresent([Ads].self, forKey: .ads) ?? [Ads]()
    }
    
    override class func primaryKey() -> String? {
        return "userKey"
    }
        
    static var current: UserEntity? {
        do {
            let realm = try Realm()
            let selfEntity = realm.object(ofType: UserEntity.self, forPrimaryKey: Constants.userPrimaryKey)
            return selfEntity
        } catch {
            return nil
        }
    }
    
}

