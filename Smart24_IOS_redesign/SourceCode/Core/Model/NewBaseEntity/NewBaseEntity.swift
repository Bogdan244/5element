//
//  NewBaseEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/25/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

class NewBaseEntity: Decodable {
    let message: String
    let errors: [NewError]
    
    private enum CodingKeys: String, CodingKey {
        case errors
        case message
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.errors = try container.decodeIfPresent([NewError].self, forKey: .errors) ?? [NewError]()
    }
}

class NewError: Decodable, LocalizedError {
    let code: Int
    let message: String
    let data: String
    
    private enum CodingKeys: String, CodingKey {
        case code
        case message
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(Int.self, forKey: .code)
        message = try container.decode(String.self, forKey: .message)
        data = try container.decode(String.self, forKey: .data)
    }
    
    var errorDescription: String? {
        return data
    }
}

extension NewError {
    
    enum Errors<T>: LocalizedError {
        case cantParsingModel(type: T.Type)
        
        var errorDescription: String? {
            switch self {
            case .cantParsingModel(let type):
                return "Неудалось получить \(type)"
            }
        }
    }
    
}

