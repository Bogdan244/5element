//
//  MartletCodeDependencyEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 29.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class MartletCodeDependencyEntity: BaseEntity {
    
    dynamic var licence = ""
    dynamic var product = ""
    dynamic var id = 0
    dynamic var createdAt = ""
    dynamic var codeID = 0
    dynamic var type = ""
    dynamic var about = ""
    dynamic var updateAt = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        licence         <- map[MapperKey.licence]
        product         <- map[MapperKey.name]
        id              <- map[MapperKey.id]
        createdAt       <- map[MapperKey.createdAt]
        codeID          <- map[MapperKey.codeID]
        type            <- map[MapperKey.type]
        about           <- map[MapperKey.about]
        updateAt        <- map[MapperKey.updateAt]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return MartletCodeDependencyEntity()
}

}
