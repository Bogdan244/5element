//
//  RequestServiceInfoEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/24/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class RequestServiceInfoEntity: NewBaseEntity {
    
    let technicInstallCategories: [String]
    let requestReason: [String]
    let services: [MyServiceEntity]
    let technicCategories: [String]
    let requestsCount: Int
    
    enum CodingKeys: String, CodingKey {
        case technicInstallCategories = "technic_install_categories"
        case requestReason = "request_reason"
        case services
        case technicCategories = "technic_categories"
        case requestsCount = "requests_count"
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.technicInstallCategories = try container.decodeIfPresent([String].self, forKey: .technicInstallCategories) ?? [String]()
        self.requestReason = try container.decodeIfPresent([String].self, forKey: .requestReason) ?? [String]()
        self.services = try container.decodeIfPresent([MyServiceEntity].self, forKey: .services) ?? [MyServiceEntity]()
        self.technicCategories = try container.decodeIfPresent([String].self, forKey: .technicCategories) ?? [String]()
        self.requestsCount = try container.decodeIfPresent(Int.self, forKey: .requestsCount) ?? 0
        
        try super.init(from: decoder)
    }
}
