//
//  BaseEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

@objcMembers class BaseEntity: Object, StaticMappable {
    
    dynamic var uid = ""
    
    func mapping(map: Map) {
        uid <- map[MapperKey.uid]
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? BaseEntity else {
            return false;
        }
        return self.uid == object.uid;
    }
    
    class func objectForMapping(map: Map) -> BaseMappable? {
        return BaseEntity()
    }
    
    override class func primaryKey() -> String? {
        return "uid"
    }
}
