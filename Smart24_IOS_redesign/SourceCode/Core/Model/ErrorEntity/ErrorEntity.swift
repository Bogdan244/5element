//
//  ErrorEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/29/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorEntity: StaticMappable, Error {
    
    var code        = 0
    var message     = ""
    var data        = ""
    
    func mapping(map: Map) {
        code      <- map[MapperKey.code]
        message   <- map[MapperKey.message]
        data      <- map[MapperKey.data]
    }
    
    class func objectForMapping(map: Map) -> BaseMappable? {
        return ErrorEntity()
    }
    
    convenience init(text: String) {
        self.init()
        data = text
        message = text
    }
    
    convenience init(code: Int, message: String, data: String) {
        self.init()
        self.code = code
        self.message = message
        self.data = data
    }
}


