//
//  NewNotificationEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class NewNotificationsEntity: NewBaseEntity {
    
    let notifications: [NewNotificationEntity]
    
    enum CodingKeys: String, CodingKey {
        case notifications
        case data
    }
    
    required init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        notifications = try nestedContainer.decodeIfPresent([NewNotificationEntity].self, forKey: .notifications) ?? [NewNotificationEntity]()
        
        try super.init(from: decoder)
        } catch {
            print(error)
            throw error
        }
    }
}

class NewNotificationEntity: Decodable {
    
    enum NotificationType: String, Decodable {
        case insurance
        case gift
        case unknown
        
        typealias RawValue = String
        
        init?(rawValue: RawValue) {
            switch rawValue {
                case NotificationType.insurance.rawValue: self = .insurance
                case NotificationType.gift.rawValue: self = .gift
                default: self = .unknown
            }
        }
    }
    
    let publishDateUnix: Int
    let id: Int
    let title: String
    let description: String
    var read: Int
    let publishDate: String
    let imageActiveURL: String
    let imageDefaultURL: String
    let type: NotificationType
    var insurance: InsuranceEntity?
    
    enum CodingKeys: String, CodingKey {
        case publishDateUnix = "publish_date_unix"
        case id
        case title
        case description
        case read
        case publishDate = "publish_date"
        case imageActiveURL = "image_active"
        case imageDefaultURL = "image_default"
        case type
        case extraData = "extra_data"
    }
    
    enum ExtraDataCodingKeys: String, CodingKey {
        case insurance
    }
    
    required init(from decoder: Decoder) throws {
        do {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        publishDateUnix = try container.decodeIfPresent(Int.self, forKey: .publishDateUnix) ?? 0
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        read = try container.decodeIfPresent(Int.self, forKey: .read) ?? 0
        publishDate = try container.decodeIfPresent(String.self, forKey: .publishDate) ?? ""
        imageActiveURL = try container.decode(String.self, forKey: .imageActiveURL)
        imageDefaultURL = try container.decode(String.self, forKey: .imageDefaultURL)
        type = try container.decode(NotificationType.self, forKey: .type)
        if let nestedContainer = try? container.nestedContainer(keyedBy: ExtraDataCodingKeys.self, forKey: .extraData) {
            insurance = try nestedContainer.decodeIfPresent(InsuranceEntity.self, forKey: .insurance)
        }
        } catch {
            print(error)
            throw error 
        }
        
    }
}

