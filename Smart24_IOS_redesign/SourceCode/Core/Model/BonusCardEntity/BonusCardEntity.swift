//
//  BonusCardEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/3/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class BonusCardEntity: NewBaseEntity {
    
    let succes: Bool?
    let cards: [BonusCards]
    
    private enum CodingKeys: String, CodingKey {
        case succes
        case cards
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.succes = try container.decodeIfPresent(Bool.self, forKey: .succes)
        self.cards = try container.decodeIfPresent([BonusCards].self, forKey: .cards) ?? [BonusCards]()
        
        try super.init(from: decoder)
    }
}

class BonusCards: Decodable {
    
    let id: Int
    let phone: String
    let nonActiveBonuses: Int?
    let activeBonuses: Int?
    let nextActivationDate: String?
    let nextActivationSum: Int?
    let nextCancelingDate: String?
    let nextCancelingSum: Int?
    let card: String
    let available: Int
    
    private enum CodingKeys: String, CodingKey {
        case id
        case phone
        case nonActiveBonuses
        case activeBonuses
        case nextActivationDate
        case nextActivationSum
        case nextCancelingDate
        case nextCancelingSum
        case card
        case available
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.phone = try container.decode(String.self, forKey: .phone)
        self.card = try container.decode(String.self, forKey: .card)
        self.available = try container.decode(Int.self, forKey: .available)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.nonActiveBonuses = try nestedContainer.decodeIfPresent(Int.self, forKey: .nonActiveBonuses)
        self.activeBonuses = try nestedContainer.decodeIfPresent(Int.self, forKey: .activeBonuses)
        self.nextActivationDate = try nestedContainer.decodeIfPresent(String.self, forKey: .nextActivationDate)
        self.nextActivationSum = try nestedContainer.decodeIfPresent(Int.self, forKey: .nextActivationSum)
        self.nextCancelingSum = try nestedContainer.decodeIfPresent(Int.self, forKey: .nextCancelingSum)
        self.nextCancelingDate = try nestedContainer.decodeIfPresent(String.self, forKey: .nextCancelingDate)
    }
}
