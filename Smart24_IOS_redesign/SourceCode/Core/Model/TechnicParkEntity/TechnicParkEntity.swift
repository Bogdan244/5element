//
//  TechnicParkEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class TechnicParkEntityList: NewBaseEntity {
    
    var techicParkList = [TechnicParkEntity]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.techicParkList = try container.decodeIfPresent([TechnicParkEntity].self, forKey: .data) ?? [TechnicParkEntity]()
        try super.init(from: decoder)
    }
    
}

class TechnicParkEntity: Decodable {
    
    let technicCategoryID: Int
    let id: Int
    let repaired: Int
    let model: String
    let technicCategory: TechnicCategory
    let brand: String
    let exploitationTime: String
    let technicParkPRSEntity: TechnicParkPRSEntity?
    
    enum CodingKeys: String, CodingKey {
        case technicCategoryID = "technic_category_id"
        case id
        case repaired
        case model
        case technicCategory = "technic_category"
        case brand
        case exploitationTime = "exploitation_time"
        case technicParkPRSEntity = "prs_service"
    }
    
}

class TechnicCategory: Decodable {
    
    enum InstallStatus: Int, Decodable {
        case notAvailable = 0
        case available = 1
        case undefined
    }
    
    let active: Int
    let id: Int
    let imageBlack: String?
    let image: String?
    let imageBig: String?
    let parentID: Int
    let name: String
    let installAvailable: InstallStatus

    enum CodingKeys: String, CodingKey {
        case active
        case id
        case imageBlack = "image_black"
        case image
        case imageBig = "image_big"
        case parentID = "parent_id"
        case name
        case installAvailable = "install_available"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.active = try container.decodeIfPresent(Int.self, forKey: .active) ?? 0
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.imageBlack = try container.decodeIfPresent(String.self, forKey: .imageBlack)
        self.image = try container.decodeIfPresent(String.self, forKey: .image)
        self.imageBig = try container.decodeIfPresent(String.self, forKey: .imageBig)
        self.parentID = try container.decodeIfPresent(Int.self, forKey: .parentID) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.installAvailable = try container.decodeIfPresent(InstallStatus.self, forKey: .installAvailable) ?? .undefined
    }
    
}

class TechnicParkPRSEntity: Decodable {
    
    let id: Int
    let model: String
    let brand: String
    let name: String
    let category: String
    let expired: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case model
        case brand
        case name
        case category
        case expired
    }
    
}
