//
//  ServiceEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/28/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class ServicesArrayEntity: NewBaseEntity {
    
    var services = [ServiceEntity]()
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.services = try container.decodeIfPresent([ServiceEntity].self, forKey: .data) ?? [ServiceEntity]()
        
        try super.init(from: decoder)
    }
}

enum ServiceType: String, Decodable {
    case prsRequest = "prs_request"
    case technicStatus = "technic_status"
    case undefined
}

class ServiceEntity: Decodable {
    
    let id: Int?
    let createdAtSorting: String
    let type: ServiceType
    let productName: String
    let name: String
    let actNumber: String
    let technic: String
    let status: String
    let statusShort: String
    let actTime: String
    let createdAt: String
    let code: String
    let productCategory: String
    let productModule: String
    let shoppingTime: String
    let expired: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAtSorting
        case type
        case productName = "product_name"
        case name
        case actNumber = "act_number"
        case technic
        case data = "data"
        case status
        case statusShort = "status_short"	
        case actTime = "act_time"
        case createdAt = "created_at"
        case code
        case productCategory = "product_category"
        case productModule = "product_model"
        case shoppingTime = "shopping_time"
        case expired = "expired"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.createdAtSorting = try container.decodeIfPresent(String.self, forKey: .createdAtSorting) ?? ""
        self.type = try container.decodeIfPresent(ServiceType.self, forKey: .type) ?? .undefined
        self.productName = try container.decodeIfPresent(String.self, forKey: .productName) ?? ""
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.actNumber = try container.decodeIfPresent(String.self, forKey: .actNumber) ?? ""
        self.technic = try container.decodeIfPresent(String.self, forKey: .technic) ?? ""
        self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.statusShort = try container.decodeIfPresent(String.self, forKey: .statusShort) ?? ""
        self.actTime = try container.decodeIfPresent(String.self, forKey: .actTime) ?? ""
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        self.code = try container.decodeIfPresent(String.self, forKey: .code) ?? ""
        self.productCategory = try container.decodeIfPresent(String.self, forKey: .productCategory) ?? ""
        self.productModule = try container.decodeIfPresent(String.self, forKey: .productModule) ?? ""
        self.shoppingTime = try container.decodeIfPresent(String.self, forKey: .shoppingTime) ?? ""
        self.expired = try container.decodeIfPresent(String.self, forKey: .expired) ?? ""
    }
    
}
