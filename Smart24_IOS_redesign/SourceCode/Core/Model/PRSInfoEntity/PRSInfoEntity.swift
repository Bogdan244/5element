//
//  PRSInfoEntity.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/11/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

class PRSInfoEntity: Decodable {
    
    let requestReason: [String]
    let requestCount: Int
    let technicCategories: [String]
    
    init(requestReason: [String], requestCount: Int, technicCategories: [String]) {
        self.requestReason = requestReason
        self.requestCount = requestCount
        self.technicCategories = technicCategories
    }
    
    enum CodingKeys: String, CodingKey {
        case requestReason = "request_reason"
        case requestCount = "requests_count"
        case technicCategories = "technic_categories" 
    }
    
    enum DataKey: CodingKey {
        case data
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DataKey.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let requestReason = try container.decodeIfPresent([String].self, forKey: .requestReason) ?? [String]()
        let requestCount = try container.decodeIfPresent(Int.self, forKey: .requestCount) ?? 0
        let technicCategories = try container.decodeIfPresent([String].self, forKey: .technicCategories) ?? [String]()
        self.init(requestReason: requestReason, requestCount: requestCount, technicCategories: technicCategories)
    }
    
}
