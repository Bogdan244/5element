//
//  NotificationViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/29/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var message: NotificationEntity?
    
    private let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureSignals()
        setMessageAsReaded()
    }
    
    private func setMessageAsReaded() {
        if let message = message {
            profileViewModel.readNotification(message: message)
        }
    }
    
    private func configureViews() {
        if let message = message {
            titleLabel.text      = message.title
            messageTextView.text = message.descrip
            
            if let dateAsDouble = Double(message.publish_date) {
                dateLabel.text = dateAsDouble.getStringDate(dateFormat: Constants.backupDateFormat)
            }
        }
    }
    
    private func configureSignals() {
        guard let message = message, let messageId = Int(message.uid) else {
            return
        }
        
        profileViewModel.syncNotifications(userId: profileViewModel.currentId, notificationId: messageId, action: ApiManagerKey.read)
            .observe(on: UIScheduler())
            .start()
    }
}
