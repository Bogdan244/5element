//
//  UtilityViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/25/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class UtilityViewController: UIViewController {
    
    @IBOutlet weak var downloadButton: LoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.downloadButton.layer.borderWidth = 1.0
        self.downloadButton.layer.cornerRadius = 4.0
        self.downloadButton.backgroundColor = .white
        self.downloadButton.layer.borderColor = UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor//.setClearBackgrounStyle()
        self.downloadButton.setTitleColor(UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0), for: .normal)
        downloadButton.addTarget(self, action: #selector(openWebView), for: .touchUpInside)
    }
    
    @objc func openWebView() {

        if let url = URL(string: Constants.teamViewerQS) {
            UIApplication.shared.openURL(url)
        }
    }
}
