//
//  MyGiftsViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 22.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class MyGiftsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textLB: UILabel!
    @IBOutlet weak var badgetView: UIView!
    
}

class MyGiftsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [GiftCodeUserEntity]()
    
    fileprivate var profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorInset = .zero
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "Moи подарки"
        tableView.tableFooterView = UIView()
        profileViewModel.allUserGifts().start { [weak self] (evant) in
            guard let strongSelf = self else { return }
            
            switch evant {
            case .value(let value):
                strongSelf.dataSource = value
                strongSelf.tableView.reloadData()
            case .failed(let error):
                strongSelf.showSmart24Error(error: error)
            default:
                break
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        title = ""
    }
}

extension MyGiftsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyGiftsTableViewCell") as! MyGiftsTableViewCell
        cell.textLB?.text = dataSource[indexPath.row].title
        if dataSource[indexPath.row].viewed {
            cell.badgetView.backgroundColor = .clear
        } else {
            cell.badgetView.backgroundColor = .red
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
    }

}

extension MyGiftsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        profileViewModel.openMyGiftDetailVC(gift: dataSource[indexPath.row])
    }
    
}
