//
//  NewsViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import MBProgressHUD

class NewsViewController: UITableViewController {
    
    private let newsViewModel = NewsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureSignals()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        newsViewModel.closeRequest?.cancel()
    }
    
    private func configureView() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView?.register(NewsCell.self)
        title = Constants.newsVCTitle
    }
    
    private func configureSignals() {
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        newsViewModel.getNews(userId: String(newsViewModel.currentId))
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                if events.isCompleted {
                    strongSelf.tableView.reloadData()
                }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsViewModel.news.value?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NewsCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        if let news = newsViewModel.news.value?[indexPath.row] {
            cell.updateViewWithModel(model: news)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let news = newsViewModel.news.value?[indexPath.row] {
            newsViewModel.openNewsDescriptionVC(news: news)
        }
    }
}
