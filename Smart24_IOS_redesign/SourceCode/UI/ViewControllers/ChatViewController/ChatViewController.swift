//
//  ChatViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MBProgressHUD
import ReactiveSwift
import Kingfisher
import Reachability

class ChatViewController: JSQMessagesViewController {
    
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    private let chatViewModel = ChatViewModel()
    
    private let timerInterval: Double = 3
    private let reach = Reachability.forInternetConnection()
    private let refresher           = UIRefreshControl()
    private let closeChatAlertView  = CloseChatAlertView()
    private var closeChatButton     = UIBarButtonItem()
    private var openVideoButton     = UIBarButtonItem()
    private var openCallButton      = UIBarButtonItem()
    
    var downloadUtilButton : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.downloadUtilButton = UIButton(frame: CGRect(x:12.0, y:UIScreen.main.bounds.size.height - 100.0, width: UIScreen.main.bounds.size.width - 24.0, height: 45.0))
//         self.downloadUtilButton!.layer.borderWidth = 1.0
//         self.downloadUtilButton!.layer.cornerRadius = 4.0
//        self.downloadUtilButton!.backgroundColor = .white
//        self.downloadUtilButton!.layer.borderColor = UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor//.setClearBackgrounStyle()
//        self.downloadUtilButton!.setTitleColor(UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0), for: .normal)
//        self.downloadUtilButton!.addTarget(self, action: #selector(openWebView), for: .touchUpInside)
//        self.downloadUtilButton?.setTitle("Запустить утилиту", for: .normal)
//        NotificationCenter.default.addObserver(self, selector:#selector(changeKeyboardFrame(_:) , name: JSQMessagesKeyboardControllerNotificationKeyboardDidChangeFrame, object: nil)
        configureMessagesView()
        configureNavigationViewController()
        configureSignals()
//        self.view.addSubview(self.downloadUtilButton!)
//        self.downloadUtilButton!.isHidden = false
    }

    func changeKeyboardFrame(notification: NSNotification) {
//        guard let object = notification.object as? [AnyHashable: Any] else { return }
//        guard let frame = object
    }

//    override func jsq_setCollectionViewInsetsTopValue(_ top: CGFloat, bottomValue bottom: CGFloat) {
//        
////        let insets = UIEdgeInsetsMake(top, 0.0,bottom > 50 ? bottom : bottom + 64.0, 0.0)
////        self.collectionView.contentInset = insets
////        self.collectionView.scrollIndicatorInsets = insets
//    }

    func openWebView() {
    
    if let url = URL(string: Constants.teamViewerQS) {
        UIApplication.shared.openURL(url)
    }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        loadMessages()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        chatViewModel.didMessageRecive {
            if let containerVC = UIApplication.containerViewController() as? ContainerViewController {
              //  containerVC.updateChat()
            }
        }
//        if let containerVC = UIApplication.containerViewController() as? ContainerViewController {
//            chatViewModel.didMessageRecive {
//                
//                print("messageRecive")
//            }
//            containerVC.configureChatTimer(interval: timerInterval)
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        if let containerVC = UIApplication.containerViewController() as? ContainerViewController {
//            containerVC.configureChatTimer(interval: nil)
//        }
    }
    
    private func configureMessagesView() {
        hideKeyboardWhenTappedAround()
        title = Constants.chatVCTitle
        senderId = String(chatViewModel.currentId)
        senderDisplayName = ""
        
        refresher.attributedTitle = NSAttributedString(string: Constants.dataUpdating)
        refresher.addTarget(self, action: #selector(loadMessages), for: .valueChanged)
        collectionView.addSubview(refresher);
        
        inputToolbar.contentView.textView.placeHolder = "Новое сообщение"
        inputToolbar.contentView.rightBarButtonItem.setTitle(Constants.send, for: .normal)
        inputToolbar.contentView.rightBarButtonItem.setTitle(Constants.send, for: .disabled)
        inputToolbar.contentView.textView.autocorrectionType = .no
        inputToolbar.contentView.rightBarButtonItemWidth = 90
        inputToolbar.contentView.leftBarButtonItem = nil
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        
        setupBubbles()
        
        closeChatButton = UIBarButtonItem(image: R.image.close_menu_icon(), style: .plain , target: self, action: #selector(closeButtonTapped))
        openVideoButton = UIBarButtonItem(image: R.image.white_camera_icon(), style: .plain , target: self, action: #selector(videoButtonTapped))
        openCallButton = UIBarButtonItem(image: R.image.white_phone_icon(), style: .plain , target: self, action: #selector(callButtonTapped))
    }
    
    private func configureSignals() {
        chatViewModel.messages
            .signal
            .observe(on: UIScheduler())
            .observeValues { [weak self] event in
                self?.collectionView.reloadData()
        }
    }
    
    private func configureNavigationViewController() {
        if let dialogId = chatViewModel.dialogId, dialogId != Constants.emptyDialogId {
            navigationItem.rightBarButtonItems = [openVideoButton,openCallButton, closeChatButton]
        } else {
            navigationItem.rightBarButtonItems = [openVideoButton, openCallButton]
        }
    }
    
    @objc func closeButtonTapped() {
        guard let dialogId = chatViewModel.dialogId else {
            showAlertMessage(title: nil, message: Constants.notExixstChatMessage)
            return
        }
        
        closeChatAlertView.show(message: Constants.closeChatQuestion)
        view.endEditing(true)
        closeChatAlertView.dialogId = dialogId
        closeChatAlertView.sendButton.addTarget(self, action: #selector(sendCloseDialog), for: .touchUpInside)
    }
    
    @objc func videoButtonTapped() {
        if let isReach = reach?.isReachable(), isReach {
            chatViewModel.openVideoVC(call: nil)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    
    @objc func callButtonTapped() {
        guard let isReach = reach?.isReachable(), isReach else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
            return
        }
        
        if let expertCallId = chatViewModel.expertCallId, expertCallId != "" {
            chatViewModel.openMakeCallVC(callId: expertCallId)
        } else {
            chatViewModel.openMakeCallVC(callId: nil)
        }
    }
    
    @objc func sendCloseDialog() {
        closeChatAlertView.closeButtonAction(self)

        let chatGrade = closeChatAlertView.currentGrade.rawValue
        let opinion = closeChatAlertView.getMessage()
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        chatViewModel.closeDialog(dialog_id: closeChatAlertView.dialogId, grade: chatGrade, opinion: opinion)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.chatViewModel.isStartClosing = false
                
                switch event {
                case .completed:
                    strongSelf.configureNavigationViewController()
                    let alert = UIAlertController(title: nil, message: Constants.dialogClosedSuccess, preferredStyle: .alert)
                    let action = UIAlertAction(title: Constants.ok, style: .default, handler: { (action) in
                        strongSelf.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(action)
                    strongSelf.present(alert, animated: true, completion: nil)

                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                default:
                    break
                }
        }
    }
    
    @objc func loadMessages(isVideo: Int = IsVideoStart.not.rawValue, getDataFromDB: Bool = true) {
        
        // if chat start closing then not restart it
        if chatViewModel.isStartClosing {
            return
        }
        
        if getDataFromDB {
            chatViewModel.showMessagesFromDB()
        }
        
        guard let dialogId = chatViewModel.dialogId, dialogId != Constants.emptyDialogId else {
            return
        }
        
        chatViewModel.getAnswer(dialog_id: dialogId, timestamp: chatViewModel.dialogTimestamp, isVideo: isVideo, getDataFromDB: getDataFromDB)
            .producer
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                if let errorCode = events.error?.code,
                    errorCode == Constants.nodialogError,
                    dialogId != Constants.emptyDialogId {
                    
                    strongSelf.configureNavigationViewController()
                    
                    strongSelf.closeChatAlertView.show(message: Constants.evaluateDialog)
                    strongSelf.closeChatAlertView.dialogId = dialogId
                    strongSelf.view.endEditing(true)
                    strongSelf.closeChatAlertView.sendButton.addTarget(strongSelf, action: #selector(strongSelf.sendCloseDialog), for: .touchUpInside)
                }
                
                if let messageCount = strongSelf.chatViewModel.messages.value?.count,
                    events.value != nil {
                    let indexPath = IndexPath(row: messageCount - 1, section: 0)
                    strongSelf.collectionView.scrollToItem(at: indexPath, at: .bottom, animated: false)
                    strongSelf.collectionView.collectionViewLayout.invalidateLayout()
                }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.refresher.endRefreshing()
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return chatViewModel.messages.value?[indexPath.row]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let value = chatViewModel.messages.value, value.count > 0 {
            self.collectionView.backgroundView = nil
            //self.downloadUtilButton!.isHidden = false
        } else {
            let label = UILabel(frame: self.collectionView.bounds)
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 16.0)
            label.text = "Нет сообщений"
            label.textColor = .lightGray
            self.collectionView.backgroundView = label
            //self.downloadUtilButton!.isHidden = false
        }
        return chatViewModel.messages.value?.count ?? 0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = chatViewModel.messages.value?[indexPath.row]
        if message?.senderId == String(chatViewModel.currentId) {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        return JSQMessagesAvatarImageFactory.avatarImage(with: R.image.white_user_icon(), diameter: 15)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let jsqmessage = chatViewModel.messages.value?[indexPath.row]
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: jsqmessage?.date)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.textView.textColor = UIColor.black
        
        if let expertAvatarUrl = chatViewModel.messages.value?[indexPath.row].expert_avatar,
            let userAvatarUrl = URL(string: expertAvatarUrl) {
            
            cell.avatarImageView.sizeThatFits(CGSize(width: 25, height: 25))
            cell.avatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
            cell.avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                             placeholder: nil,
                                             options: nil,
                                             progressBlock: nil,
                                             completionHandler: nil)
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        view.endEditing(true)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!,
                                 heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return indexPath.row % 3 == 0 ? 20 : 0
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        if text.isEmpty {
            return
        }
        
        button.isEnabled = false
        
        //MARK:- doesn't send dialogId if it equalse 0
        var dialogId = chatViewModel.dialogId
        if chatViewModel.dialogId == Constants.emptyDialogId {
            dialogId = nil
        }
        
        chatViewModel.sendMessage(message: text, dialog_id: dialogId, isVideo: nil)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                switch events {
                case .completed:
                    button.isEnabled = true
                    self?.configureNavigationViewController()
                    self?.collectionView.reloadData()
                    self?.finishSendingMessage(animated: true)
                case .value(_):
                    break
                case .failed(let error):
                    button.isEnabled = true
                    self?.showSmart24Error(error: error)
                case .interrupted:
                    button.isEnabled = true
                    self?.showAlertMessage(title: nil, message: Constants.cantSendMessage)
                }
        }
    }
    
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: UIColor(named: .chatMessageColor))
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
}
