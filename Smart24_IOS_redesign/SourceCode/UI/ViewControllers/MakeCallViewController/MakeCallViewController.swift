//
//  MakeCallViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/22/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class MakeCallViewController: UIViewController {
    
    @IBOutlet weak var callTimerLabel: UILabel!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var microphoneButton: UIButton!
    
    var sipCallNumber = Keys.chatNotExistCallNumber
    
    private let viewModel = GeneralViewModel()
    
    private var account: VSLAccount?
    private var callManager: VSLCallManager?
    private var activeCall: VSLCall?
    private var connectDurationTimer: Timer?
    private var timerCount = 0
    private var sipObservingContext = 0
    
    @IBAction func closeButtonAction(_ sender: Any) {
        closeCall()
    }
    @IBAction func muteButtonAction(_ sender: Any) {
        muteSIP()
    }
    @IBAction func speakerButtonAction(_ sender: Any) {
        changeSpeaker()
    }
    @IBAction func videoButtonAction(_ sender: Any) {
        viewModel.openVideoVC(call: activeCall)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        callManager = VialerSIPLib.sharedInstance().callManager
        account     = VialerSIPLib.sharedInstance().accounts()?.first as! VSLAccount!
        
        setupCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        activeCall?.addObserver(self, forKeyPath: Constants.sipCallStateKey, options: .new, context: &sipObservingContext)
        UIDevice.current.isProximityMonitoringEnabled = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        connectDurationTimer?.invalidate()
        activeCall?.removeObserver(self, forKeyPath: Constants.sipCallStateKey)
        UIDevice.current.isProximityMonitoringEnabled = false
    }
    
    // MARK: - KVO
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &sipObservingContext {
            
            if let call = object as? VSLCall, call.callState == .disconnected {
                connectDurationTimer?.invalidate()
                timerCount = 0
                
                if (call.connected && !call.userDidHangUp) {
                    showCloseMessage()
                }
            }
            
            if let call = object as? VSLCall, call.callState == .confirmed {
                startConnectDurationTimer()
            }
            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    private func closeCall() {
        callManager?.endAllCalls()
        
        viewModel.backToPreviousVC()
    }
    
    private func changeSpeaker() {
        guard let call = activeCall else { return }
        
        if call.speaker {
            speakerButton.setImage(R.image.speaker_icon(), for: .normal)
        } else {
            speakerButton.setImage(R.image.tupped_speaker_icon(), for: .normal)
        }
        call.toggleSpeaker()
    }
    
    private func muteSIP() {
        guard let call = activeCall, call.callState != .disconnected else { return }
        
        if call.muted {
            microphoneButton.setImage(R.image.video_mic_icon(), for: .normal)
        } else {
            microphoneButton.setImage(R.image.video_unmute_mic_icon(), for: .normal)
        }
        
        callManager?.toggleMute(for: call) { error in
            if let error = error {
                debugPrint(error)
            }
        }
    }
    
    private func setupCall() {
        guard let account = account else { return }
        
        callManager?.startCall(toNumber: sipCallNumber, for: account ) { [weak self] call, error in
            if let error = error {
                debugPrint(error)
            } else {
                self?.activeCall = call
            }
        }
    }
    
    private func showCloseMessage() {
        let alertController = UIAlertController(title: nil, message: Constants.dialogClosed, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
            self?.viewModel.openRootVC()
        })
        
        present(alertController, animated: true, completion: nil)
    }
    
    func startConnectDurationTimer() {
        if connectDurationTimer == nil || !connectDurationTimer!.isValid {
            
            DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.connectDurationTimer = Timer.scheduledTimer(timeInterval: Constants.connectDurationInterval,
                                                                       target: strongSelf,
                                                                       selector: #selector(strongSelf.updateTimer),
                                                                       userInfo: nil,
                                                                       repeats: true)
            }
        }
    }
    
    @objc func updateTimer() {
        timerCount += 1
        callTimerLabel.text = timerCount.timeFormatted()
    }
}
