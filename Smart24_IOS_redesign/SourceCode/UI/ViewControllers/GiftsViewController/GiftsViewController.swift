//
//  GiftsViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 18.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import Kingfisher


class GiftTableViewCell: UITableViewCell {
    
    @IBOutlet weak var giftImage: UIImageView!
    @IBOutlet weak var giftTitle: UILabel!
    @IBOutlet weak var giftDescription: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        giftImage.image = nil 
    }
    
    func configurateCell(gift: Gift) {
        giftTitle.text = gift.title
        giftDescription.text = gift.description
        
        if let imageURL = URL(string: gift.imageLink) {
            giftImage.kf.setImage(with: ImageResource(downloadURL: imageURL, cacheKey: nil),
                                        placeholder: nil,
                                        options: nil,
                                        progressBlock: nil,
                                        completionHandler: nil)
        }
    }
}

class GiftsViewController: UIViewController {

    @IBOutlet weak var remindLaterButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    typealias selectGift = ((_ id: Int) -> Void)
    
    var didSelectGift: selectGift?
    var gifts = [Gift]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateViews()
    }
    
    private func configurateViews() {
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        tableView.rowHeight = UITableView.automaticDimension
        contentView.layer.cornerRadius = 5
        contentView.clipsToBounds = true
    }
    
    @IBAction func remindLaterAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
}

extension GiftsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gifts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiftTableViewCell") as! GiftTableViewCell
        cell.configurateCell(gift: gifts[indexPath.row])
        
        return cell
    }
    
}

extension GiftsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _ = didSelectGift else { return }
        dismiss(animated: false) { [weak self] in
            self?.didSelectGift!((self?.gifts[indexPath.row].id)!)
        }
    }
}


