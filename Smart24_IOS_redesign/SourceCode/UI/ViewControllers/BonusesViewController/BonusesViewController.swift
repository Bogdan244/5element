//
//  BonusesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 14.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class BonusesViewController: UIViewController {
    
    @IBOutlet weak var activeBonuseLabel: UILabel!
    @IBOutlet weak var nonActiveBonuseLabel: UILabel!
    
    @IBOutlet weak var nextCancelingSumLabel: UILabel!
    @IBOutlet weak var nextCancelingDateLabel: UILabel!
    
    @IBOutlet weak var nextActivationSumLabel: UILabel!
    @IBOutlet weak var nextActivationDateLabel: UILabel!
    
    @IBOutlet weak var deactivateButton: UIButton!
    
    var bonuses = BonusesEntity()
    private var bonusesViewModel = BonusesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateViews()
    }

    private func configurateViews() {
        title = "Мои бонусы"
        deactivateButton.layer.cornerRadius = 5
        
        activeBonuseLabel.text = "\(bonuses.activeBonuses / 100)"
        nonActiveBonuseLabel.text = "\(bonuses.nonActiveBonuses / 100)"
        nextCancelingSumLabel.text = "\(bonuses.nextCancelingSum / 100)"
        nextCancelingDateLabel.text = convertDateString(dateString: bonuses.nextCancelingDate)
        nextActivationSumLabel.text = "\(bonuses.nextActivationSum / 100)"
        nextActivationDateLabel.text = convertDateString(dateString: bonuses.nextActivationDate)
    }
    
    private func convertDateString(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let subString = dateString.prefix(10)
        if let date = dateFormatter.date(from: String(subString)) {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter.string(from: date)
            
        }
        return String(subString)
    }
    
    @IBAction func deactivateButtonAction(_ sender: Any) {
        bonusesViewModel.deactivateBonuseSignal(id: bonuses.id).observe(on: UIScheduler()).start { [weak self] (event) in
            guard let strongSelf = self else { return }
            
            switch event {
            case .completed:
                strongSelf.bonusesViewModel.backToPreviousVC()
            case let .failed(error):
                strongSelf.showAlertMessage(title: "Ошибка", message: error.data)
            default:
                break
            }
            
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        bonusesViewModel.openRootVC()
    }
    
}
