//
//  MainViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Reachability
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD
import RealmSwift

class MainViewController: UIViewController {
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var openMenuButton: UIButton!
    @IBOutlet weak var buttonToChange: UIButton!
    @IBOutlet weak var repairsStatusButton: UIButton!
    
    var rateId: Int?
    var isStart = true
    var productName = ""
    
    private let viewModel           = GeneralViewModel()
    private let reach               = Reachability.forInternetConnection()
    private let profileViewModel    = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        if let entity = profileViewModel.getSelfEntity(), entity.gift == true {
            profileViewModel.getGifts().start { [weak self] (event) in
                switch event {
//                case let .value(gifts):
//                    self?.profileViewModel.presentGiftsVC(gifts: gifts, didSelectGift: { (index) in
//                        self?.profileViewModel.activateGift(id: index).start({ (event) in
//                            switch event {
//                            case let .value(giftCodeEntity):
//                                do {
//                                    let realm = try Realm()
//                                    guard let user = self?.profileViewModel.getSelfEntity() else { return }
//                                    try realm.write {
//                                        user.setValue(user.codesNotViewed + 1, forKey: "codesNotViewed")
//                                    }
//                                } catch {
//                                    debugPrint("can't update data from DB")
//                                }
//                                self?.profileViewModel.presentGiftDetailVC(giftCodeEntity: giftCodeEntity)
//                            default:
//                                break
//                            }
//                        })
//                    })
                default:
                    break
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let entity = profileViewModel.getSelfEntity() {
            profileViewModel.getStatuses(userId: entity.uid)
                .observe(on: UIScheduler())
                .start { [weak self] events in
                    if self?.profileViewModel.statuses!["status"] as? String == "start" {
                        self?.buttonToChange.setImage(UIImage(named:"ic_prs_start"), for: .normal)
                        self?.rateId = nil
                        self?.isStart = true
                    } else if self?.profileViewModel.statuses!["status"] as? String == "working" {
                        self?.buttonToChange.setImage(UIImage(named:"ic_prs_working"), for: .normal)
                        self?.rateId = nil
                        self?.isStart = false
                    } else if self?.profileViewModel.statuses!["status"] as? String == "done" {
                        self?.buttonToChange.setImage(UIImage(named:"ic_prs_done"), for: .normal)
                        self?.rateId =  self?.profileViewModel.statuses!["request_id"] as? Int
                        self?.productName =  self?.profileViewModel.statuses!["product_name"] as! String
                        self?.isStart = false
                    }
            }
        }
        openMenuButton.setImage(R.image.menu_icon(), for: .normal)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.barTintColor = UIColor(red:185.0/255.0, green: 10.0/255.0, blue:25.0/255.0, alpha: 1.0) //ColorName.topBarColor.color
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        revealViewController().panGestureRecognizer().isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func configureViews() {
        openMenuButton.addTarget(revealViewController(),
                                 action: #selector(revealViewController().revealToggle(_:)),
                                 for: .touchUpInside)
        repairsStatusButton.layer.cornerRadius = 5
        repairsStatusButton.layer.borderColor = repairsStatusButton.titleLabel?.textColor.cgColor
        repairsStatusButton.layer.borderWidth = 1
    }
    
    private func closeMenu(isEnabled: Bool) {
        if revealViewController().frontViewPosition == .right {
            revealViewController().revealToggle(animated: false)
        }
        
        revealViewController().panGestureRecognizer().isEnabled = isEnabled
    }
    
    @IBAction func openChatButtonAction(_ sender: Any) {
        viewModel.openChatVC()
        closeMenu(isEnabled: false)
    }
    
    @IBAction func makeCallButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Позвонить на единый мобильный номер: 275", preferredStyle: .alert)
        let a = UIAlertAction(title: "Ок", style: .default) { (action) in
            if let urlToCall = URL(string: "tel://275"), UIApplication.shared.canOpenURL(urlToCall) {
                UIApplication.shared.openURL(urlToCall)
            }
        }
        
        let b = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
        }
        alert.addAction(a)
        alert.addAction(b)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func makePhoneCallButtonAction(_ sender: Any) {
        profileViewModel.openOrderCallVC()
    }
    
    @IBAction func openVideoButtonAction(_ sender: Any) {
        
        profileViewModel.getActiveServices(userId: profileViewModel.getSelfEntity()!.uid)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                //if events.isCompleted {
                guard let strongSelf = self else { return }
                if let _ = strongSelf.profileViewModel.activeServices.value {
                    strongSelf.isStart = false
                } else {
                    strongSelf.isStart = true
                }
                if (strongSelf.isStart == true) {
                    let stroybarod = UIStoryboard(name: "Menu", bundle: nil)
                    let viewcontroller = stroybarod.instantiateViewController(withIdentifier: "CreateRequestViewController") as! CreateRequestViewController
                    if let rateId = strongSelf.rateId {
                        viewcontroller.isRate = true
                        viewcontroller.rateId = rateId
                        viewcontroller.productName = strongSelf.productName
                    }
                    strongSelf.navigationController?.pushViewController(viewcontroller, animated: true)
                } else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let controller = storyboard.instantiateViewController(withIdentifier: "ActiveServicesViewController") as! ActiveServicesViewController
                    controller.isFromCreate = false
                    if let rateId = strongSelf.rateId {

                        controller.isRate = true
                        controller.rateId = rateId
                        controller.productName = strongSelf.productName
                    }
                    strongSelf.navigationController?.pushViewController(controller, animated: true)
                }
        }
    }
    
    @IBAction func repairsStatusAction(_ sender: Any) {
        profileViewModel.openRepairsStatusVC()
    }
    
    
}
