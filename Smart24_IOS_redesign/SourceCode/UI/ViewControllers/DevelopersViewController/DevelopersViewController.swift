//
//  DevelopersViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/10/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class DevelopersViewController: BaseViewController {

    @IBOutlet weak var firstOperatorView: UIView!
    @IBOutlet weak var secondOperatorView: UIView!
    @IBOutlet weak var thirdOperatorView: UIView!
    
    @IBOutlet weak var firstOperatorPhoneLabel: UILabel!
    @IBOutlet weak var secondOperatorPhoneView: UILabel!
    @IBOutlet weak var thirdOperatorPhoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstOperatorPhoneLabel.underline()
        secondOperatorPhoneView.underline()
        thirdOperatorPhoneLabel.underline()
        
        firstOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callFirstOperator)))
        secondOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callSecondOperator)))
        thirdOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callThirdOperator)))
    }
    
    @objc func callFirstOperator() {
        makeColl(firstOperatorPhoneLabel.text ?? "")
    }
    
    @objc func callSecondOperator() {
        makeColl(secondOperatorPhoneView.text ?? "")
    }
    @objc func callThirdOperator() {
        makeColl(thirdOperatorPhoneLabel.text ?? "")
    }
    
    func makeColl(_ phone: String) {
        let phoneStr = phone.replacingOccurrences(of: " ", with: "")
        guard let url = URL(string: "tel://\(phoneStr)"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
extension UILabel {
    
    func underline() {
        attributedText = NSAttributedString(string: text ?? "", attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    
}
