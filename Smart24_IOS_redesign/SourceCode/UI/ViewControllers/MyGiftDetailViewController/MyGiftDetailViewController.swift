//
//  MyGiftDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 23.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class MyGiftDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var copyButton: CopyTextButton!
    
    var gift = GiftCodeUserEntity()
    private var profileViewMode = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Мой подарок"
        titleLabel.text = gift.title
        codeLabel.text = gift.code
        copyButton.setCopyText(text: gift.code, for: UIControl.Event.touchUpInside)
        descriptionTextView.text = gift.descriptionGift
        if let imageURL = URL(string: gift.imageLink) {
            imageView.kf.setImage(with: ImageResource(downloadURL: imageURL))
        }
        
        profileViewMode.viewGiftCode(code: gift.id).start { [weak self] (event) in
            switch event {
            case .completed:
                do {
                    let realm = try Realm()
                    guard let user = self?.profileViewMode.getSelfEntity() else { return }
                    try realm.write {
                        user.setValue(user.codesNotViewed - 1, forKey: "codesNotViewed")
                    }
                } catch {
                    debugPrint("can't update data from DB")
                }

            default:
                break
            }
        }
    }
}
