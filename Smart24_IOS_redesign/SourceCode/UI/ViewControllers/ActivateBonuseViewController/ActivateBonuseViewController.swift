//
//  ActivateBonuseViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 15.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD

class ActivateBonuseViewController: UIViewController {
   
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var infoView: UIView!
    
    @IBOutlet weak var cardNumberTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    
    let bonusesViewModel = BonusesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurateViews()
        configurateSignals()
        
        cardNumberTF.delegate = self
        phoneTF.delegate = self
    }

    private func configurateViews() {
        title = "Мои бонусы"
        acceptButton.layer.cornerRadius = 5
        acceptButton.layer.borderWidth = 1.5
        acceptButton.layer.borderColor = UIColor(red: 80.0/255.0, green: 190.0/255.0, blue: 132.0/255.0, alpha: 1).cgColor
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 1.5
        cancelButton.layer.borderColor = UIColor.red.cgColor
        addCardButton.layer.cornerRadius = 5
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        infoView.isHidden = false
    }
    
    @IBAction func acceptButonAction(_ sender: Any) {
        
    }
    @IBAction func addCardButtonAction(_ sender: Any) {
        infoView.isHidden = true
    }
    
    private func configurateSignals() {
        
        let cardNumberProcedure = bonusesViewModel.racTextProducer(textField: cardNumberTF)
        let phoneProducer = bonusesViewModel.racTextProducer(textField: phoneTF)
        
        bonusesViewModel.cardNumberProperty     <~ cardNumberProcedure
        bonusesViewModel.phoneProperty          <~ phoneProducer
        
        acceptButton.addTarget(bonusesViewModel.cocoaActionActivate, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        acceptButton.reactive
            .controlEvents(.touchUpInside)
            .observe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.activateBonuse()
                strongSelf.view.endEditing(true)
        }
    }
    
    func activateBonuse() {
        MBProgressHUD.showAdded(to: view, animated: true)
        bonusesViewModel.activateBonuseSignal().observe(on: UIScheduler()).start { [weak self] (event) in
            guard let strongSelf = self else { return }
          
            switch event {
            case let .value(value):
                if let bonuse = value {
                    strongSelf.bonusesViewModel.openBonusesVC(bonuses: bonuse)
                }
            case let .failed(error):
                    strongSelf.showAlertMessage(title: "Ошибка", message: error.data)
            default:
                break
            }
            MBProgressHUD.hide(for: strongSelf.view, animated: false)
        }
    }
    
}

extension ActivateBonuseViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        if textField == phoneTF {
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            if range.location < 13 {
                return true
            }
            if range.location + range.length >= 13 {
                return false
            }
        }
        if textField == cardNumberTF {
            if range.location < 12 {
                return true
            }
            if range.location + range.length >= 12 {
                return false
            }
        }
      
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == phoneTF {
            phoneTF.text = "+375"
            bonusesViewModel.phoneProperty.value = "375"
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneTF {
            if phoneTF.text == "+375" {
                textField.text = ""
                bonusesViewModel.phoneProperty.value = ""
            }
        }
    }
}


