//
//  ForgotPasswordViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import MBProgressHUD

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var sendButton: LoginButton!
    
    fileprivate let forgotPasswordViewModel = ForgotPasswordViewModel()
    
    @IBAction func rememberedButtonAction(_ sender: Any) {
        forgotPasswordViewModel.backToPreviousVC()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        forgotPasswordViewModel.openRegistrationVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureSignals()
        
        sendButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendPassword)))
    }
    
   @objc func sendPassword() {
        getPassword(phone: phoneTextField.text!.replacingOccurrences(of: "+", with: ""), otp: nil)
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        title = Constants.forgotPasswordVC
        
        let phoneImageView          = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        phoneImageView.image        = R.image.gray_phone_icon()
        phoneImageView.contentMode  = .scaleAspectFit
        phoneTextField.leftView     = phoneImageView
        phoneTextField.leftViewMode = .always
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate     = self
        phoneTextField.clearButtonMode = .never
    }
    
    private func configureSignals() {
        sendButton.addTarget(forgotPasswordViewModel.cocoaActionForgotPassword, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        let phoneProducer = forgotPasswordViewModel.racTextProducer(textField: phoneTextField)
        
        forgotPasswordViewModel.phoneProperty   <~ phoneProducer
        sendButton.reactive.isEnabled           <~ forgotPasswordViewModel.phoneSignalProducer
        
        forgotPasswordViewModel.forgotPasswordSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues{ [weak self] (event) in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .completed:
                    strongSelf.showCodeMessage()
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                default:
                    break
                }
        }
    }
    
    private func showCodeMessage() {
        
        var codeTextField: UITextField?
        let smsAlert = UIAlertController(title: Constants.enterSMSCode, message: nil, preferredStyle: .alert)
        smsAlert.addAction(UIAlertAction(title: Constants.close, style: .cancel))
        smsAlert.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
            self?.view.endEditing(true)
            self?.confirmSMSCode(code: codeTextField?.text)
        })
        
        smsAlert.addTextField { textField in
            codeTextField = textField
        }
        
        present(smsAlert, animated: true, completion: nil)
    }
    
    private func confirmSMSCode(code: String?) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        forgotPasswordViewModel.forgotPassword(code: code)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                switch event {
                case .completed:
                    strongSelf.loginUser(code: code)
                    
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                case .interrupted:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showAlertMessage(title: nil, message: Constants.errorGettingData)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    
    private func loginUser(code: String?) {
        guard let code = code else {
            return
        }
        
        forgotPasswordViewModel.loginUser(password: code, otpCode: nil)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.closeCallMessage)
                default:
                    break
                }
        }
    }
    
    private func getPassword(phone: String, otp: String?) {
        MBProgressHUD.showAdded(to: view, animated: true)
        networkProvider.request(ApiManager.forgotPasswordRes(phone: phone, code: otp)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let json = try response.mapJSON() as? [String: AnyObject]
                    if response.statusCode == 200 {
                        if let errors = json?["errors"] as? [[String : AnyObject]], errors.count > 0 {
                            if let _ = errors.first(where: {$0["code"] as? Int == 1015 }) {
                                var message = ""
                                if let _ = errors.first(where: {$0["code"] as? Int == 1011 }) {
                                    message = "Неверный код"
                                }
                                let alert = UIAlertController(title: "Введите пароль из смс", message: message, preferredStyle: .alert)
                                alert.addTextField { (textField) in
                                    textField.keyboardType = .numberPad
                                    if #available(iOS 12.0, *) {
                                        textField.textContentType = UITextContentType.oneTimeCode
                                    }
                                }
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                                    self.getPassword(phone: phone, otp: alert?.textFields?.first?.text)
                                }))
                                alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            } else {
                                guard let message = errors.first?["message"] as? String else { return }
                                self.showAlertMessage(title: nil, message: message)
                            }
                        } else {
                            guard let message = json?["message"] as? String else { return }
                            self.showCustomAlertMessage(title: nil, message: message, firstBTHandler: {
                                if let user = self.forgotPasswordViewModel.getSelfEntity() {
                                    
                                }
                                self.dismiss(animated: true, completion: nil)
                            }, secondBTHandler: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                } catch {
                    print("error")
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}

//MARK:- UITextFieldDelegate delegate
extension ForgotPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let protectedRange = NSMakeRange(0, 1)
        let intersection = NSIntersectionRange(protectedRange, range)
        if intersection.length > 0 {
            return false
        }
        if range.location == 12 {
            return true
        }
        if range.location + range.length > 12 {
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            textField.text = "+3"
            forgotPasswordViewModel.phoneProperty.value = "3"
        }
        
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "+3" {
            textField.text = ""
        }
    }
}
