//
//  ChangeDataViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/30/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD
import Kingfisher

class ChangeDataViewController: UIViewController {
    
    @IBOutlet weak var topBackgroundView: UIView!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var saveButton: LoginButton!
    
    @IBOutlet weak var surnameTextField: LoginTextField!
    @IBOutlet weak var nameTextField: LoginTextField!
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var confirmPasswordTextField: LoginTextField!
    
    @IBAction func changeAvatarButtonAction(_ sender: Any) {
        openAvatarMenuDialog()
    }
    
    let changeDataViewModel = ChangeUserDataViewModel()
    
    //MARK:- private properties
    private var user: UserEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureNavigationBarButton()
        confgiureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        user = changeDataViewModel.getSelfEntity()
        title = Constants.editingTitle
        
        topBackgroundView.backgroundColor = ColorName.topBarColor.color
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate = self
        
        surnameTextField.bottomInfoLayer.backgroundColor            = UIColor.lightGray.cgColor
        nameTextField.bottomInfoLayer.backgroundColor               = UIColor.lightGray.cgColor
        phoneTextField.bottomInfoLayer.backgroundColor              = UIColor.lightGray.cgColor
        passwordTextField.bottomInfoLayer.backgroundColor           = UIColor.lightGray.cgColor
        confirmPasswordTextField.bottomInfoLayer.backgroundColor    = UIColor.lightGray.cgColor
        
        avatarButton.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        avatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        
        let backgroundColor = ColorName.topBarColor.color
        navigationController?.navigationBar.setBackgroundImage(backgroundColor.toImage(), for: .default)
        navigationController?.navigationBar.shadowImage = backgroundColor.toImage()
        
        if let user = user {
            nameTextField.text    = user.firstname
            surnameTextField.text = user.surname
            phoneTextField.text   = user.phone
            
            changeDataViewModel.nameProperty.value    = user.firstname
            changeDataViewModel.surnameProperty.value = user.surname
            changeDataViewModel.phoneProperty.value   = user.phone
            
            if let userAvatarUrl = URL(string: user.userPhoto) {
                avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                            placeholder: nil,
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
            }
        }
    }
    
    @IBAction func saveButton(sender: UIButton) {
        view.endEditing(true)
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    private func configureNavigationBarButton() {
        let barButton = UIBarButtonItem(image: R.image.white_save_icon(),
                                        style: .plain,
                                        target: self,
                                        action: #selector(saveImageTapped))
        navigationItem.rightBarButtonItem = barButton
        barButton.reactive.isEnabled <~ changeDataViewModel.inputValidData
    }
    
    private func confgiureSignals() {
        saveButton.addTarget(changeDataViewModel.cocoaActionChangeData, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        // MARK:- button enable when data are valid
        // saveButton.reactive.isEnabled <~ changeDataViewModel.inputValidData
        
        let nameProducer            = changeDataViewModel.racTextProducer(textField: nameTextField)
        let surnameProducer         = changeDataViewModel.racTextProducer(textField: surnameTextField)
        let phoneProducer           = changeDataViewModel.racTextProducer(textField: phoneTextField)
        let passwordProducer        = changeDataViewModel.racTextProducer(textField: passwordTextField)
        let confirmPasswordProduser = changeDataViewModel.racTextProducer(textField: confirmPasswordTextField)
        
        changeDataViewModel.nameProperty              <~ nameProducer
        changeDataViewModel.surnameProperty           <~ surnameProducer
        changeDataViewModel.phoneProperty             <~ phoneProducer
        changeDataViewModel.passwordProperty          <~ passwordProducer
        changeDataViewModel.confirmPasswordProperty   <~ confirmPasswordProduser
        
        changeDataViewModel.changeDataSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues { [weak self] (event) in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.changeDataActionHandler(event: event)
        }
    }
    
    private func openImageSource(imagePicker: UIImagePickerController) {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false;
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func showSuccessMessage(title: String?, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
            self?.changeDataViewModel.backToPreviousVC()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func openAvatarMenuDialog() {
        
        let alertController = UIAlertController(title: Constants.changePhoto, message: nil, preferredStyle: .actionSheet)
        alertController.view.tintColor = UIColor(named: .loginButtonNormal)
        alertController.popoverPresentationController?.sourceView = avatarButton
        alertController.popoverPresentationController?.sourceRect = .zero
        let cameraAction = UIAlertAction(title: Constants.camera, style: .default) { [weak self] (action: UIAlertAction!) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            self?.openImageSource(imagePicker: imagePicker)
        }
        
        let galleryAction = UIAlertAction(title: Constants.galery, style: .default) { [weak self]  (action: UIAlertAction!) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            self?.openImageSource(imagePicker: imagePicker)
        }
        
        let cancelAction = UIAlertAction(title: Constants.close, style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        
        changeDataViewModel.showAlertController(alertViewController: alertController)
    }
    
    @objc func saveImageTapped() {
        view.endEditing(true)
        MBProgressHUD.showAdded(to: view, animated: true)
        
        changeDataViewModel.changeUserData()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.changeDataActionHandler(event: event)
        }
    }
    
    private func changeDataActionHandler(event: (Signal<Any, ErrorEntity>.Event)) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            MBProgressHUD.hide(for: self.view, animated: true)
        }
//        switch event {
//        case .value(let action as BPLoginAction):
//            if action == BPLoginAction.needSMSConfirm {
//                var message = ""
//                if action == BPLoginAction.wrongOTP {
//                    message = "Неверный код"
//                }
//                let alert = UIAlertController(title: "Введите код из смс", message: message, preferredStyle: .alert)
//                alert.addTextField { (textField) in
//                    textField.keyboardType = .numberPad
//                    if #available(iOS 12.0, *) {
//                        textField.textContentType = UITextContentType.oneTimeCode
//                    }
//                    self.changeDataViewModel.otpProperty <~ self.changeDataViewModel.racTextProducer(textField: textField)
//                }
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
//                    self.changeDataViewModel.changeUserData().start({ [weak self] (event) in
//                        self?.changeDataActionHandler(event: event)
//                    })
//                }))
//                alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
//        case .completed:
//            self.showSuccessMessage(title: nil, message: Constants.savedMessage)
//        case .failed(let error):
//            self.showSmart24Error(error: error)
//        default:
//            break
//        }

    }
}

extension ChangeDataViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK - UIImagePickerControllerDelegate
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        avatarImageView.image = pickedImage.resizeImage(newWidth: 320)
        changeDataViewModel.userAvatarImage = pickedImage.resizeImage(newWidth: 320)
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if navigationController.navigationBar.topItem?.rightBarButtonItem == nil {
            print("backNil")
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if navigationController.navigationBar.topItem?.rightBarButtonItem == nil {
            print("backNil")
        }
    }
}

//MARK:- UITextFieldDelegate delegate
extension ChangeDataViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        if textField == phoneTextField {
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            if range.location == 12 {
                return true
            }
            if range.location + range.length > 12 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == phoneTextField ,textField.text?.isEmpty == true {
            textField.text = "+375"
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "+375" {
            textField.text = ""
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == phoneTextField {
            textField.text = "+375"
            return false
        }
        return true
    }
}
