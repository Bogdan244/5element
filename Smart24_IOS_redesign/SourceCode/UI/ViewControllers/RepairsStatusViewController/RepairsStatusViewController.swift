//
//  RepairsStatusViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/15/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class RepairsStatusViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addAct: UIButton!
    
    var dataSource = [Status]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurateViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadTechnicList()
    }
    
    private func configurateViews() {
        addAct.layer.cornerRadius = 5
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    private func loadTechnicList() {
        networkProvider.request(ApiManager.technicStatusList(page: nil, count: nil)) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let response):
                if let json = try? response.mapJSON() as? [String: AnyObject], let error = ErrorFactory.createEntity(dictionary: json!) {
                    strongSelf.showSmart24Error(error: error)
                } else if let repairStatuses = try? response.map(RepairStatusEntity.self) {
                    if repairStatuses.data.statuses.count > 0 {
                        strongSelf.tableView.backgroundView = nil
                        strongSelf.tableView.separatorStyle = .singleLine
                    } else {
                        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: strongSelf.view.bounds.size.width, height: strongSelf.view.bounds.size.height))
                        let messageLabel = UILabel(frame: rect)
                        messageLabel.text = "Для просмотра статуса ремонта, добавьте номер Акта/Сервиса"
                        messageLabel.textColor = UIColor.black
                        messageLabel.numberOfLines = 0;
                        messageLabel.textAlignment = .center
                        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
                        messageLabel.sizeToFit()
                        strongSelf.tableView.backgroundView = messageLabel
                        strongSelf.tableView.separatorStyle = .none
                    }
                    strongSelf.dataSource = repairStatuses.data.statuses
                    strongSelf.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func addActAction(_ sender: Any) {
        let alert = UIAlertController(title: "Добавить Акт/Номер сервиса", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.keyboardType = UIKeyboardType.numberPad
        }
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { (_) in
            networkProvider.request(ApiManager.technicStatusAdd(code: (alert.textFields?.first?.text)!), completion: { [weak self] (result) in
                switch result {
                case .success(let response):
                    if let json = try? response.mapJSON() as? [String: AnyObject], let error = ErrorFactory.createEntity(dictionary: json!) {
                        self?.showSmart24Error(error: error)
                    } else {
                        self?.loadTechnicList()
                    }
                case .failure(let error):
                    print(error)
                }
            })
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension RepairsStatusViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepairsStatusCell") as! RepairsStatusCell
        cell.configurateCell(with: dataSource[indexPath.row])
        return cell
    }
    
    
}
