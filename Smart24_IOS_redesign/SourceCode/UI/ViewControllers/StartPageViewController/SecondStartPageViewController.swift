//
//  SecondStartPageViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class SecondStartPageViewController: UIViewController {
    
    var pageContainer: PageContent?
    
    @IBAction func previousButtonAction(_ sender: Any) {
        if let startVC = pageContainer {
            startVC.openPreviousPage()
        }
    }

    @IBAction func nextButtonAction(_ sender: Any) {
        if let startVC = pageContainer {
            startVC.openNextPage()
        }
    }
}
