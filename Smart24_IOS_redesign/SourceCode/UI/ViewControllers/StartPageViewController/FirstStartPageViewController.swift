//
//  FirstStartPageViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class FirstStartPageViewController: UIViewController {
    
    var pageContainer: PageContent?
 
    @IBAction func nextButtonAction(_ sender: Any) {
        if let startVC = pageContainer {
            startVC.openNextPage()
        }
    }
}
