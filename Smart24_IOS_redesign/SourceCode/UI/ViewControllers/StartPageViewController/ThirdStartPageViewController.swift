//
//  ThirdStartPageViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ThirdStartPageViewController: UIViewController {
    
    var pageContainer: PageContent?
    
    private lazy var presenter: Presenter = PresenterImpl();
    
    @IBAction func previousButtonAction(_ sender: Any) {
        if let startVC = pageContainer {
            startVC.openPreviousPage()
        }
    }
    
    @IBAction func startButtonAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: Constants.firstRun)
        UserDefaults.standard.synchronize()
        
        presenter.openLoginVC()
    }
}
