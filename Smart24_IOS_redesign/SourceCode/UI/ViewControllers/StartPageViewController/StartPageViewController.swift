//
//  StartViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class StartPageViewController: UIViewController, PageContent {
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var previousIndex: Int?
    var currentIndex = 0
    var pendingIndex = 1
    var tempPendingIndex = 1
    var pagesArray  = [UIViewController]()
    var pageContainer: UIPageViewController!
    
    private let firstPage   = R.storyboard.startScrean.firstStartPageViewController()
    private let secondPage  = R.storyboard.startScrean.secondStartPageViewController()
    private let thirdPage   = R.storyboard.startScrean.thirdStartPageViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.isHidden = true
//
//        pagesArray.append(firstPage!)
//        pagesArray.append(secondPage!)
        pagesArray.append(thirdPage!)
//
//        firstPage?.pageContainer = self
//        secondPage?.pageContainer = self
        thirdPage?.pageContainer = self
        
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pagesArray.first!], direction: .forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
        
        view.bringSubviewToFront(pageControl)
        pageControl.numberOfPages = pagesArray.count
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func openNextPage() {
        if pendingIndex >= pagesArray.count {
            return
        }
        
        pageContainer.setViewControllers([pagesArray[pendingIndex]],
                                         direction: .forward,
                                         animated: true,
                                         completion: nil )
        setIndex(index: pendingIndex)
    }
    
    func openPreviousPage() {
        guard let previousIndex = previousIndex else { return }
        
        pageContainer.setViewControllers([pagesArray[previousIndex]],
                                         direction: .reverse,
                                         animated: true,
                                         completion: nil )
        setIndex(index: previousIndex)
    }
    
    func setIndex(index: Int) {
        currentIndex = index
        pendingIndex = index + 1
        previousIndex = currentIndex == 0 ? nil : currentIndex - 1
        pageControl.currentPage = index
    }
}

// MARK: - UIPageViewController delegates
extension StartPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let index = pagesArray.index(of: pendingViewControllers.first!) {
            tempPendingIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            setIndex(index: tempPendingIndex)
        }
    }
}

// MARK: UIPageViewControllerDataSource
extension StartPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pagesArray.index(of: viewController),
            currentIndex > 0 else {
                return nil
        }
        
        return pagesArray[currentIndex - 1]
    }
    
    func pageViewController(_ viewControllerBeforepageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pagesArray.index(of: viewController),
            currentIndex != pagesArray.count - 1 else {
                return nil
        }
        
        return pagesArray[currentIndex + 1]
    }
}
