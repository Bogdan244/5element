//
//  HowItWorkController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/8/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class HowItWorkController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
    }
    
    private func configureView() {
        title = Constants.howItWorkVCTitle
        tabBar.tintColor = UIColor.white
        tabBar.barTintColor = UIColor(red:185.0/255.0, green: 10.0/255.0, blue:25.0/255.0, alpha: 1.0)
        
        let backgroundColor = UIColor(red:185.0/255.0, green: 10.0/255.0, blue:25.0/255.0, alpha: 1.0)
        //navigationController?.navigationBar.setBackgroundImage(backgroundColor.toImage(), for: .default)
        //navigationController?.navigationBar.shadowImage = backgroundColor.toImage()
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = UIColor(red:185.0/255.0, green: 10.0/255.0, blue:25.0/255.0, alpha: 1.0)
//        let shieldButton = UIBarButtonItem(image: R.image.white_shield_icon(), style: .plain , target: self, action: #selector(shieldButtonTapped))
//        navigationItem.rightBarButtonItem = shieldButton
        
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.white,
                                                                            size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count),
                                                                                         height: tabBar.frame.height),
                                                                            lineWidth: 2.0)
    }
    
    func shieldButtonTapped() {
        showAlertMessage(title: nil, message: Constants.shialdMessage)
    }
}

