//
//  IPadTabBarController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/9/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class IPadTabBarController: GeneralContentViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPage?.controllerType = .ipadRegistration
        secondPage?.controllerType = .ipadCall
        thirdPage?.controllerType = .ipadSettings
        
        firstPage?.checkType(type: .ipadRegistration)
        
        pageControl.numberOfPages = pagesArray.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    override func setIndex(index: Int) {
        super.setIndex(index: index)
        pageControl.currentPage = index
    }
}

