//
//  ComputerTabBarController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/8/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ComputerTabBarController: GeneralContentViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPage?.controllerType = .computerRegistration
        secondPage?.controllerType = .computerCall
        thirdPage?.controllerType = .computerSettings
        
        firstPage?.checkType(type: .computerRegistration)
        
        pageControl.numberOfPages = pagesArray.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    override func setIndex(index: Int) {
        super.setIndex(index: index)
        pageControl.currentPage = index
    }
}
