//
//  PhoneTabBarController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/9/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class PhoneTabBarController: GeneralContentViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstPage?.controllerType = .phoneRegistration
        secondPage?.controllerType = .phoneCall
        thirdPage?.controllerType = .phoneSettings
        
        firstPage?.checkType(type: .phoneRegistration)
        
        pageControl.numberOfPages = pagesArray.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    override func setIndex(index: Int) {
        super.setIndex(index: index)
        pageControl.currentPage = index
    }
}

