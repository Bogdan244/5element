//
//  GeneralContentViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 1/24/17.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit


protocol PageContent {
    var previousIndex: Int? { get set }
    var pendingIndex: Int { get set }
    var currentIndex: Int { get set }
    var tempPendingIndex: Int { get set }
    var pagesArray: [UIViewController] { get set }
    var pageContainer: UIPageViewController! { get set}
    
    func openNextPage()
    func openPreviousPage()
    func setIndex(index: Int)
}

class GeneralContentViewController: UIViewController, PageContent {
    
    var previousIndex: Int?
    var currentIndex = 0
    var pendingIndex = 1
    var tempPendingIndex = 1
    var pagesArray  = [UIViewController]()
    var pageContainer: UIPageViewController!
    
    let firstPage   = R.storyboard.howItWork.howItWorkContentPageController()
    let secondPage  = R.storyboard.howItWork.howItWorkContentPageController()
    let thirdPage   = R.storyboard.howItWork.howItWorkContentPageController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagesArray.append(firstPage!)
        pagesArray.append(secondPage!)
        pagesArray.append(thirdPage!)
        
        firstPage?.howItWorkContent = self
        secondPage?.howItWorkContent = self
        thirdPage?.howItWorkContent = self
        
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pagesArray.first!], direction: .forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
    }
    
    func openNextPage() {
        if pendingIndex >= pagesArray.count {
            return
        }
        
        pageContainer.setViewControllers([pagesArray[pendingIndex]],
                                         direction: .forward,
                                         animated: true,
                                         completion: nil )
        setIndex(index: pendingIndex)
    }
    
    func openPreviousPage() {
        guard let previousIndex = previousIndex else { return }
        
        pageContainer.setViewControllers([pagesArray[previousIndex]],
                                         direction: .reverse,
                                         animated: true,
                                         completion: nil )
        setIndex(index: previousIndex)
    }
    
    func setIndex(index: Int) {
        currentIndex = index
        pendingIndex = index + 1
        previousIndex = currentIndex == 0 ? nil : currentIndex - 1
    }
}

// MARK: - UIPageViewController delegates
extension GeneralContentViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let index = pagesArray.index(of: pendingViewControllers.first!) {
            tempPendingIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            setIndex(index: tempPendingIndex)
        }
    }
}

// MARK: UIPageViewControllerDataSource
extension GeneralContentViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pagesArray.index(of: viewController),
            currentIndex > 0 else {
                return nil
        }
        
        return pagesArray[currentIndex - 1]
    }
    
    func pageViewController(_ viewControllerBeforepageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pagesArray.index(of: viewController),
            currentIndex != pagesArray.count - 1 else {
                return nil
        }
        
        return pagesArray[currentIndex + 1]
    }
}
