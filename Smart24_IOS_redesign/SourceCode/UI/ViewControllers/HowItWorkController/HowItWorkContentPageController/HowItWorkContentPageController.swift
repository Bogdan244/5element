//
//  HowItWorkContentPageController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/8/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

enum PageTypes: Int {
    case computerRegistration
    case computerCall
    case computerSettings
    
    case phoneRegistration
    case phoneCall
    case phoneSettings
    
    case ipadRegistration
    case ipadCall
    case ipadSettings
}

class HowItWorkContentPageController: UIViewController {
    
    @IBOutlet weak var topLogoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    
    @IBAction func nextButtonAction(_ sender: Any) {
        howItWorkContent?.openNextPage()
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        howItWorkContent?.openPreviousPage()
    }
    
    var howItWorkContent: PageContent?
    var controllerType = PageTypes.computerRegistration
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkType(type: controllerType)
    }
    
    func checkType(type: PageTypes) {
        
        switch type {
        case .computerRegistration:
            previousButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "computer_login_icon")
            titleLabel.text     = "Зарегистрируйтесь и активируйте услугу"
            messageLabel.text   = "На нашем сайте введите свои контактные данные и активируйте услугу или позвоните по телефону 0-800-600-001"
        case .computerCall:
            topLogoImage.image  = #imageLiteral(resourceName: "computer_call_icon")
            titleLabel.text     = "Задайте вопрос нашему ИТ-эксперту"
            messageLabel.text   = "Войдите в центр поддержки или нажмите на кнопку Задать вопрос эксперту, внизу экрана, и опишите Вашу проблему"
        case .computerSettings:
            nextButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "computer_settings_icon")
            titleLabel.text     = "Мы настраиваем Ваш компьютер"
            messageLabel.text   = "Предоставте удаленный доступ к Вашему компьютеру, устройтесь поудобнее и наблюдайте, как эксперты Мастер Сервис производят необходимые настройки"
            
        case .phoneRegistration:
            previousButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "phone_login_icon")
            titleLabel.text     = "Зарегистрируйтесь и активируйте услугу"
            messageLabel.text   = "Введите свои контактные данные и активируйте услугу, или позвоните по телефону 0 800 600 001"
        case .phoneCall:
            topLogoImage.image  = #imageLiteral(resourceName: "phone_call_icon")
            titleLabel.text     = "Задайте вопрос нашем ИТ-эксперту"
            messageLabel.text   = "Кликните на кнопку для получения помощи нашего ИТ-эксперта и опишите проблему"
        case .phoneSettings:
            nextButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "phone_settings_icon")
            titleLabel.text     = "Мы настраиваем Ваш смартфон"
            messageLabel.text   = "Предоставьте удаленный доступ к Вашему смартфону, устройтесь поудобнее и наблюдайте, как эксперты Мастер Сервис производят необходимые настройки"
            
        case .ipadRegistration:
            previousButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "ipad_login_icon")
            titleLabel.text     = "Зарегистрируйтесь и активируйте услугу"
            messageLabel.text   = "Введите свои контактные данные и активируйте услугу, или позвоните по телефону 0 800 600 001"
        case .ipadCall:
            topLogoImage.image  = #imageLiteral(resourceName: "ipad_call_icon")
            titleLabel.text     = "Задайте вопрос нашем ИТ-эксперту"
            messageLabel.text   = "Предоставьте удаленный доступ к Вашему планшету, устройтесь поудобнее и наблюдайте, как эксперты Мастер Сервис производят необходимые настройки"
        case .ipadSettings:
            nextButton.isHidden = true
            topLogoImage.image  = #imageLiteral(resourceName: "ipad_settings_icon")
            titleLabel.text     = "Мы настраиваем Ваш компьютер"
            messageLabel.text   = "Эксперты Мастер Сервис помогут решить любые вопросы, касающихся работы Вашего устройства"
        }
    }
    
}
