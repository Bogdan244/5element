//
//  NewsDescriptionViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD

class NewsDescriptionViewController: UIViewController {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsWebView: UIWebView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var webviewHeightConstraint: NSLayoutConstraint!
    
    var news: NewsEntity?
    
    fileprivate var observing = false
    private var newsObservingContext = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationViewController()
        configureView()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &newsObservingContext {
            webviewHeightConstraint.constant = newsWebView.scrollView.contentSize.height
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    private func configureNavigationViewController() {
        let sharing = UIBarButtonItem(image: R.image.sharing_icon(), style: .plain , target: self, action: #selector(sheringButtonTapped))
        navigationItem.rightBarButtonItems = [sharing]
        
        newsWebView.scrollView.isScrollEnabled = false
        newsWebView.delegate = self
    }
    
    private func configureView() {
        title = Constants.newsVCTitle
        
        if let news = news {
            titleLabel.text = news.title
            dateLabel.text = news.publish_date.getDate(dateFormat: "dd MMM YYYY")
            
            newsWebView.loadHTMLString(news.newsdesc, baseURL: nil)
            
            if let newsUrl = URL(string: news.header_image) {
                newsImageView.kf.setImage(with: ImageResource(downloadURL: newsUrl, cacheKey: nil),
                                          placeholder: nil,
                                          options: nil,
                                          progressBlock: nil,
                                          completionHandler: nil)
            }
        }
    }
    
    @objc func sheringButtonTapped() {
        if let news = news {
            let vc = UIActivityViewController(activityItems: [news.title], applicationActivities: [])
            present(vc, animated: true)
        }
    }
    
    func startObservingHeight() {
        newsWebView.scrollView.addObserver(self, forKeyPath: Constants.scrollObserverKey, options: [.new], context: &newsObservingContext)
        observing = true
    }
    
    func stopObservingHeight() {
        if observing {
            newsWebView.scrollView.removeObserver(self, forKeyPath: Constants.scrollObserverKey, context: &newsObservingContext)
            observing = false
        }
    }
    
    deinit {
        stopObservingHeight()
    }
}

extension NewsDescriptionViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webviewHeightConstraint.constant = newsWebView.scrollView.contentSize.height
        if !observing {
            startObservingHeight()
        }
    }
}
