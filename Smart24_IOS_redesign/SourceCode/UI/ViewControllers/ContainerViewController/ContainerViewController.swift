//
//  ContainerViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import SWRevealViewController
import ReactiveSwift
import Reachability

class ContainerViewController: SWRevealViewController {
    
    var chatTimer: Timer?
    var chatViewModel = ChatViewModel()
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    var reach: Reachability?
    
    private let timerInterval: Double = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.rearViewRevealWidth = UIScreen.main.bounds.size.width - 60.0
        //configureChatTimer(interval: nil)
        
//        chatViewModel.didMessageRecive {
//            self.updateChat()
//        }
        
        //if let controller = R.storyboard.main.mainViewController() {
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BaseMenuViewController")
        let navigationController = Smart24NavigationController(rootViewController: controller)
            frontViewController = navigationController
        //}
        
        // Add left menu
        if let controller = R.storyboard.menu.menuTableViewController() {
            frontViewController = controller
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reach = Reachability.forInternetConnection()
        reach?.reachableOnWWAN = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: NSNotification.Name.reachabilityChanged,
                                               object: nil)
        reach?.startNotifier()
    }
    
    @objc func reachabilityChanged(notification: NSNotification) {
        if self.reach!.isReachableViaWiFi() || self.reach!.isReachableViaWWAN() {
            debugPrint("reachable connect")
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableMessage)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        
        if chatTimer != nil {
            chatTimer?.invalidate()
            chatTimer = nil
        }
        
        reach?.stopNotifier()
    }
    
//    func updateChat() {
//
//        guard let dialogId = chatViewModel.dialogId,
//            dialogId != Constants.emptyDialogId else {
//                return
//        }
//
//        //MARK:- if ChatVC is current VC then update chat view
//        if let chatVC = frontViewController.children.last as? ChatViewController {
//            chatVC.loadMessages(getDataFromDB: false)
//            return
//        }
//
//        //MARK:- if VideoVC is current VC then update chat view
//        if let videoVC = frontViewController.children.last as? VideoViewController,
//            let chatVC = videoVC.children.first as? ChatViewController {
//            chatVC.loadMessages(isVideo: IsVideoStart.start.rawValue, getDataFromDB: false)
//            return
//        }
//
//        //MARK:- if get new message then show local notification
//        chatViewModel.getAnswer(dialog_id: dialogId, timestamp: chatViewModel.dialogTimestamp, isVideo: IsVideoStart.not.rawValue, getDataFromDB: false)
//            .producer
//            .start { [weak self] event in
//                switch event {
//                case .value(let value):
//                    self?.createNotification(message: value)
//                case .failed(let error):
//                    DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
//                        self?.showErrorMessage(error: error)
//                    }
//                default:
//                    debugPrint("message observing interrupted")
//                }
//        }
//    }
    
    private func createNotification(message: JSQMessageEntity) {
        let localNotification = UILocalNotification()
        localNotification.alertBody = message.text
        localNotification.alertTitle = message.senderDisplayName
        localNotification.timeZone = TimeZone.current
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    private func showErrorMessage(error: ErrorEntity) {
        if error.code == Constants.notAuthError {
            chatTimer?.invalidate()
            let alertController = UIAlertController(title: nil, message: error.data, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
                self?.chatViewModel.presentLoginVC()
            }
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
    }
}
