 //
 //  BackupViewController.swift
 //  Smart24_IOS_redesign
 //
 //  Created by Vitya on 12/1/16.
 //  Copyright © 2016 Vitya. All rights reserved.
 //
 
 import UIKit
 import MBProgressHUD
 import ReactiveSwift
 import SwiftKeychainWrapper
 import Reachability
 
 enum CloudSyncType: Error {
    case success
    case error
    case unavailable
    case noContacts
    case restoreSuccess
    case restoreFailure
    case backgroundBackupAccessDenied
    case backgroundBackupAccessLimited
 }
 
 enum BackupState {
    case createBackup
    case getBackup
 }
 
 class BackupViewController: UIViewController {
    
    @IBOutlet weak var restoreLastDataButton: LoginButton!
    @IBOutlet weak var saveDataButton: LoginButton!
    
    @IBOutlet weak var lastBackupDate: UILabel!
    @IBOutlet weak var nextBackupDate: UILabel!
    @IBOutlet weak var selectedAdressBookView: UIView!
    @IBOutlet weak var bookCheckImageView: UIImageView!
    
    let viewModel = BackupViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureSignals()
        
        if let backupButtonState = UserDefaults.standard.value(forKey: Constants.backupButtonState) as? Bool, backupButtonState == false {
            // set default if first start
            UserDefaults.standard.set(false, forKey: Constants.backupButtonState)
            UserDefaults.standard.set(0, forKey: Constants.backupButtonDayIndex)
            UserDefaults.standard.synchronize()
        }
    }
    
    private func configureView() {
        saveDataButton.addTarget(self, action: #selector(saveData), for: .touchUpInside)
        saveDataButton.layer.cornerRadius = 4.0
        saveDataButton.layer.borderWidth = 2.0
        saveDataButton.layer.borderColor = UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor
        restoreLastDataButton.layer.cornerRadius = 4.0
        restoreLastDataButton.layer.borderWidth = 2.0
        restoreLastDataButton.layer.borderColor = UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor
        restoreLastDataButton.addTarget(self, action: #selector(toggleRestoreLastCopyButton), for: .touchUpInside)
        restoreLastDataButton.isEnabled = false
        
        let adressBookGesture = UITapGestureRecognizer(target: self, action: #selector(backupAdressBookTap))
        selectedAdressBookView.addGestureRecognizer(adressBookGesture)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        nextBackupDate.isHidden = true
        setNextDateBuckupInfo()
    }
    
    func setNextDateBuckupInfo() {
        if let backupDateToString = UserDefaults.standard.value(forKey: Constants.backupDate) as? Double {
            nextBackupDate.text = secondsToDate(seconds: Int64(backupDateToString))
        }
    }
    
    @objc func saveData() {
        let reach = Reachability.forInternetConnection()
        
        if let isReach = reach?.isReachable(), isReach {
            let alert = UIAlertController(title: title, message: Constants.confirmBackupMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
                self?.viewModel.openBackupProgressVC(status: .createBackup)
            }
            alert.addAction(okAction)
            alert.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    
    @objc func toggleRestoreLastCopyButton() {
        let reach = Reachability.forInternetConnection()
        
        if let isReach = reach?.isReachable(), isReach {
            
            let alert = UIAlertController(title: title, message: Constants.confirmRestoreBackup, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
                self?.viewModel.openBackupProgressVC(status: .getBackup)
            }
            alert.addAction(okAction)
            alert.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableRestoreBackup)
        }
    }
    
    private func secondsToDate(seconds: Int64) -> String {
        let date = Date(timeIntervalSince1970: Double(seconds))
        let formatter = DateFormatter()
        
        formatter.calendar = Calendar.current
        formatter.locale = NSLocale.current
        formatter.dateFormat = Constants.backupDateFormat
        
        return formatter.string(from: date)
    }
    
    private func configureSignals() {
        
        viewModel.getInfoBackup()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else {return}
                
                switch event {
                case .value(let date):
                    let newDate = strongSelf.secondsToDate(seconds: Int64(date))
                    strongSelf.lastBackupDate.text = "\(Constants.backupLastActivation) \(newDate)"
                    strongSelf.restoreLastDataButton.isEnabled = true
                case .interrupted:
                    strongSelf.lastBackupDate.text = Constants.backupLastActivation
                default:
                    break
                }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }
    }
    
    @objc func backupAdressBookTap() {
        if bookCheckImageView.isHidden {
            bookCheckImageView.isHidden = false
            restoreLastDataButton.isEnabled = true
            saveDataButton.isEnabled = true
        } else {
            bookCheckImageView.isHidden = true
            restoreLastDataButton.isEnabled = false
            saveDataButton.isEnabled = false
        }
    }
 }
