//
//  MyServicesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 25.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD

class MyServiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var copyButton: CopyTextButton!
}

class MyServicesViewController: UIViewController {

    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSorce = [MartletCodeDependencyEntity]()
    
    var dataSource2 = [ActiveServicesEntity]()
    var dataSource3 = [ActiveServicesEntity]()
    fileprivate var scannedString: String?
    fileprivate let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateViews()
        configurateTableView()
        getMartletCodes()
        configurateAndExecuteSignals()
    }

    private func configurateViews() {
        title = Constants.services
        activateButton.layer.cornerRadius = 5
        activateButton.layer.borderWidth = 2
        activateButton.layer.borderColor = activateButton.titleLabel?.textColor.cgColor
    }
    
    private func configurateTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(ProfileOptionCell.self)
        tableView.register(ProfileNotificationCell.self)
    }
    
    @IBAction func activeButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: Constants.enterActivationCode, message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            if let string = self.scannedString {
                textField.text = string
            } else {
                textField.placeholder = Constants.enterActivationCodePlaceholder
            }
        }
        
        alert.addAction(UIAlertAction(title: Constants.scanCodeAction, style: .default) { [weak self] _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let barcodeScannerVC = storyboard.instantiateViewController(withIdentifier: "BarcodeScannerViewController") as! BarcodeScannerViewController
            barcodeScannerVC.delegate = self
            self?.present(barcodeScannerVC, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] _ in
            if alert.textFields?[0].text?.count == 16 {
                self?.activateMartletServices(code:(alert.textFields?[0].text)!)
            } else {
                self?.activateServices(userId: (self?.profileViewModel.getSelfEntity()?.uid)!, activateServices: (alert.textFields?[0].text)!)
            }
        })
        alert.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
        present(alert, animated: self.scannedString != nil ? false :true, completion: nil)
    }
    
    private func configurateAndExecuteSignals() {
        guard let userId = profileViewModel.getSelfEntity()?.uid else { return }
    
        profileViewModel.getActiveServicesProfile(userId: userId)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                if let services = self?.profileViewModel.activeServicesProfile.value, services.count != 0 {
                    self?.dataSource2 = services
                    self?.tableView.reloadData()
                }
        }
        
        profileViewModel.getActiveServices(userId: userId)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                if let services = self?.profileViewModel.activeServices.value, services.count != 0 {
                    self?.dataSource3 = services
                    self?.tableView.reloadData()
                }
        }
    }
    
    private func getMartletCodes() {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.showMartletCodes().start { [weak self] (event) in
            guard let strongSelf = self else { return }
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
            
            switch(event) {
            case .value(let item):
                strongSelf.dataSorce = item.flatMap{ $0.dependencies }.flatMap{ $0 }
                strongSelf.tableView.reloadData()
            case .failed(let error):
                strongSelf.showSmart24Error(error: error)
            default:
                break
            }
        }
    }
    
    ///Activation Martlet Codes
    private func activateMartletServices(code: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.activateMartletCode(code: code).observe(on: UIScheduler()).start { [weak self] (event) in
            guard let strongSelf = self else { return }
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
            
            switch event {
            case .completed:
                strongSelf.showAlertMessage(title: nil, message: Constants.activationSuccess)
                strongSelf.getMartletCodes()
            case .failed(let error):
                strongSelf.showSmart24Error(error: error)
            case .interrupted:
                strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
            default:
                break
            }
        }
    }
    
    ///Activation Services Codes PRS
    private func activateServices(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.activateService(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .completed:
                    strongSelf.profileViewModel.getActiveServices(userId: (self?.profileViewModel.getSelfEntity()!.uid)!)
                        .observe(on: UIScheduler())
                        .start { events in
                            strongSelf.tableView.reloadData()
                    }
                case .value(let gift): 
                    if gift == true {
                        
                        strongSelf.profileViewModel.getGifts().start({ (event) in
                            switch event {
//                            case .value(let gifts):
//                                strongSelf.profileViewModel.presentGiftsVC(gifts: gifts, didSelectGift: { (id) in
//                                    
//                                    strongSelf.profileViewModel.activateGift(id: id).start({ (event) in
//                                        self?.configurateAndExecuteSignals()
//                                        switch event {
//                                        case .value(let gift):
//                                            strongSelf.profileViewModel.presentGiftDetailVC(giftCodeEntity: gift)
//                                        case .failed(let error):
//                                            strongSelf.showSmart24Error(error: error)
//                                        default:
//                                            break
//                                        }
//                                    })
//                                })
                            case .failed(let error):
                                strongSelf.showSmart24Error(error: error)
                            default:
                                break
                            }
                        })
                    }
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
                }
        }
    }
    
    ///Activate Services Profile
    private func activateServicesProfile(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.activateServiceProfile(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                switch events {
                case .completed:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationSuccess)
                    strongSelf.tableView.reloadData()
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
                default:
                    break
                }
        }
    }
    
    @IBAction func createRequestCellAction(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        guard let values = self.profileViewModel.activeServices.value else { return  }
        let activeService = values[indexPath!.row]
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateZayavkaViewController") as! CreateZayavkaViewController
        vc.service = activeService
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func historyCellAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateZayavkaViewController1") as! CreateZayavkaViewController
        vc.isHistory = true
        guard let values = self.profileViewModel.activeServices.value else { return  }
        let activeService = values[indexPath!.row]
        vc.service = activeService
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyServicesViewController: BarcodeScannerViewControllerDelegate {
    
    func didFinishWith(string : String) {
        self.scannedString = string
        self.activeButtonAction(UIButton())
    }
}

extension MyServicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return dataSource3.count
        } else if section == 1 {
            return dataSource2.count
        } else if section == 2 {
            return dataSorce.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 150
        } else {
            return 75
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceTableViewCell") as! MyServiceTableViewCell
            cell.titleLabel.text = dataSorce[indexPath.row].product
            cell.codeLabel.text = dataSorce[indexPath.row].licence
            cell.copyButton.setCopyText(text: dataSorce[indexPath.row].licence, for: UIControl.Event.touchUpInside)
            return cell
        } else if indexPath.section == 1 {
            let cell: ProfileOptionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            cell.updateViewWithModel(model: dataSource2[indexPath.row])
            cell.titleLabel.font = cell.titleLabel.font.withSize(17)
            cell.descriptionLabel.font = cell.descriptionLabel.font.withSize(14)
            return cell
        } else if indexPath.section == 0 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ActiveServicesTableViewCell", for: indexPath) as! ActiveServicesTableViewCell
            let activeService = dataSource3[indexPath.row]
            cell.selectionStyle = .none
            cell.configrateCell(activeService: activeService)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 2 {
            profileViewModel.openMyServiceDetailVC(martletCodeEntity:(dataSorce[indexPath.row]))
        }
    }
	
}




