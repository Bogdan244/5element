//
//  BackupPerionViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

enum BackupPeriod: Int {
    case day
    case week
    case month
    case year
}

class BackupPerionViewController: UIViewController {
    
    @IBOutlet weak var dayCheckImage: UIImageView!
    @IBOutlet weak var weekCheckImage: UIImageView!
    @IBOutlet weak var monthChackImage: UIImageView!
    @IBOutlet weak var yearCheckImage: UIImageView!
    
    @IBAction func dayButtonAction(_ sender: UIButton) {
        showCheckIcon(icon: dayCheckImage)
        KeychainWrapper.standard.set(BackupPeriod.day.rawValue, forKey: Constants.backupPeriod)
    }
    @IBAction func weekButtonAction(_ sender: UIButton) {
        showCheckIcon(icon: weekCheckImage)
        KeychainWrapper.standard.set(BackupPeriod.week.rawValue, forKey: Constants.backupPeriod)
    }
    @IBAction func monthButtonAction(_ sender: UIButton) {
        showCheckIcon(icon: monthChackImage)
        KeychainWrapper.standard.set(BackupPeriod.month.rawValue, forKey: Constants.backupPeriod)
    }
    @IBAction func yearButtonAction(_ sender: UIButton) {
        showCheckIcon(icon: yearCheckImage)
        KeychainWrapper.standard.set(BackupPeriod.year.rawValue, forKey: Constants.backupPeriod)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBackupPeriod()
        configureNavigationViewController()
    }
    
    private func configureBackupPeriod() {
        guard let value = KeychainWrapper.standard.integer(forKey: Constants.backupPeriod),
            let period = BackupPeriod(rawValue: value) else {
                showCheckIcon(icon: nil)
                return
        }
     
        switch period {
        case .day:
            showCheckIcon(icon: dayCheckImage)
        case .week:
            showCheckIcon(icon: weekCheckImage)
        case .month:
            showCheckIcon(icon: monthChackImage)
        case .year:
            showCheckIcon(icon: yearCheckImage)
        }
    }
    
    private func configureNavigationViewController() {
        title = Constants.backupPeriodTitle
//        let shield = UIBarButtonItem(image: R.image.white_shield_icon(), style: .plain , target: self, action: #selector(shieldButtonTapped))
//        navigationItem.rightBarButtonItems = [shield]
    }
    
    private func showCheckIcon(icon: UIImageView?) {
        dayCheckImage.isHidden = true
        weekCheckImage.isHidden = true
        monthChackImage.isHidden = true
        yearCheckImage.isHidden = true
        
        icon?.isHidden = false
    }
    
    func shieldButtonTapped() {
        showAlertMessage(title: nil, message: Constants.shialdMessage)
    }
    
}
