//
//  RegistrationViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import MBProgressHUD

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: LoginTextField!
    @IBOutlet weak var surnameTextField: LoginTextField!
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var confirmPasswordTextField: LoginTextField!
    
    @IBOutlet weak var registrationButton: LoginButton!
    
    @IBAction func howItWorkButtonAction(_ sender: Any) {
        registrationViewModel.openHowItWorkTBC()
    }
    
    fileprivate let registrationViewModel = RegistrationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        confgiureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationController()
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        title = Constants.registrationVCTitle
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate = self
        phoneTextField.clearButtonMode = .never
        registrationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(registrationAction)))
    }
    
    private func configureNavigationController() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func confgiureSignals() {
     //   registrationButton.addTarget(registrationViewModel.cocoaActionRegistration, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        // MARK button enable when email is valid
        registrationButton.reactive.isEnabled <~ registrationViewModel.inputValidData
        
        let nameProducer            = registrationViewModel.racTextProducer(textField: nameTextField)
        let surnameProducer         = registrationViewModel.racTextProducer(textField: surnameTextField)
        let phoneProducer           = registrationViewModel.racTextProducer(textField: phoneTextField)
        let passwordProducer        = registrationViewModel.racTextProducer(textField: passwordTextField)
        let confirmPasswordProduser = registrationViewModel.racTextProducer(textField: confirmPasswordTextField)
        
        registrationViewModel.nameProperty              <~ nameProducer
        registrationViewModel.surnameProperty           <~ surnameProducer
        registrationViewModel.phoneProperty             <~ phoneProducer
        registrationViewModel.passwordProperty          <~ passwordProducer
        registrationViewModel.confirmPasswordProperty   <~ confirmPasswordProduser
        
//        registrationViewModel.registrationSignalProducer.events.observe { [weak self] value in
//            guard let strongSelf = self else { return }
//
//            strongSelf.view.endEditing(true)
//            MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
//        }
//
//        registrationViewModel.registrationSignalProducer
//            .events
//            .observe(on: UIScheduler())
//            .observeValues{ [weak self] (event) in
//                guard let strongSelf = self else { return }
//                MBProgressHUD.hide(for: strongSelf.view, animated: true)
//
//                switch event {
//                case .completed:
//                    strongSelf.showConfirmRegistration()
//                case .failed(let error):
//                    strongSelf.showSmart24Error(error: error)
//                default:
//                    break
//                }
//        }
    }
    
    @objc func registrationAction() {
        registrationViewModel.registerUser(otp: nil).start { [weak self] (event) in
            guard let strongSelf = self else { return }
            strongSelf.registrationActionHandler(event: event)
        }
    }
    
    func registrationActionHandler(event: (Signal<Any, ErrorEntity>.Event)) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        switch event {
        case .completed:
            self.showConfirmRegistration()
        case .failed(let error):
            self.showAlertMessage(title: "", message: error.data )
        case .value(let action as BPRegistrationAction):
            var message = ""
            if action == BPRegistrationAction.wrongOTP {
                message = "Неверный код"
            }
            let alert = UIAlertController(title: "Введите код из смс", message: message, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.keyboardType = .numberPad
                if #available(iOS 12.0, *) {
                    textField.textContentType = UITextContentType.oneTimeCode
                }
                self.registrationViewModel.otp <~ self.registrationViewModel.racTextProducer(textField: textField)
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                self.registrationViewModel.registerUser(otp: self.registrationViewModel.otp.value).start({ [weak self] (event) in
                    self?.registrationActionHandler(event: event)
                })
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        default:
            break
        }
    }

    
    private func showConfirmRegistration() {
        
        let smsAlert = UIAlertController(title: nil, message: Constants.registrationMessage, preferredStyle: .alert)
        smsAlert.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] action in
            self?.confirmRegistration()
        })
        
        present(smsAlert, animated: true, completion: nil)
    }
    
    private func confirmRegistration() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        registrationViewModel.loginUser(otpCode: nil)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.errorGettingData)
                default:
                    break
                }
        }
    }
}

//MARK:- UITextFieldDelegate delegate
extension RegistrationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let protectedRange = NSMakeRange(0, 4)
        let intersection = NSIntersectionRange(protectedRange, range)
        if intersection.length > 0 {
            return false
        }
        if range.location == 12 {
            return true
        }
        if range.location + range.length > 12 {
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            registrationViewModel.phoneProperty.value = "3"
            textField.text = "+3"
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "+3" {
            textField.text = ""
        }
    }
}
