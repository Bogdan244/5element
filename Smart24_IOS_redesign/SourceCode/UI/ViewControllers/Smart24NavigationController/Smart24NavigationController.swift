//
//  Smart24NavigationController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class Smart24NavigationController: UINavigationController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        interactivePopGestureRecognizer?.isEnabled = true
        
//        if revealViewController() != nil {
//            view.addGestureRecognizer(revealViewController().panGestureRecognizer())
//        }
    }
}
