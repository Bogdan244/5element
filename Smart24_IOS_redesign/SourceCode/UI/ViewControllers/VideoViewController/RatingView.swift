//
//  RatingView.swift
//  Smart24_IOS_redesign
//
//  Created by Paul Minakov on 28.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import FloatRatingView

protocol Delegate1 : class {
    func ok(rating : Float, text : String)
    func cancel()
}

class RatingView: UIView {
    @IBOutlet weak var ratingView: FloatRatingView!
    weak var delegate : Delegate1?
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var textField: UITextField!
    @IBAction func cancelAction(_ sender: Any) {
        self.delegate?.cancel()
    }
    @IBOutlet weak var cancel: UIButton!
    @IBAction func ok(_ sender: Any) {
        self.delegate?.ok(rating: Float(self.ratingView?.rating ?? 0), text: self.textField.text ?? "")
    }
}
