//
//  ActiveServicesTableViewCell.swift
//  Smart24_IOS_redesign
//
//  Created by Paul Minakov on 28.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit

class ActiveServicesTableViewCell: UITableViewCell {

    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var createRequestButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activateButton?.layer.borderColor = self.activateButton.titleLabel?.textColor.cgColor
        self.activateButton?.layer.borderWidth = 1.0
        self.historyButton?.layer.borderWidth = 1.0
        self.historyButton?.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.createRequestButton?.layer.borderWidth = 1.0
        self.createRequestButton?.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        // Initialization code
    }
  
    func configrateCell(activeService: ActiveServicesEntity ){
        firstLabel.text = activeService.name
        let dateString = activeService.expired_time.getStringDate(dateFormat: "dd.MM.yyyy")
        secondLabel.text = activeService.product_name
        thirdLabel.text   = "Сервис действительный до \(dateString)"
        
        
        if activeService.expired_time == 0 {
            thirdLabel.isHidden = true
        } else {
            thirdLabel.isHidden = false
        }
        if activeService.status == "working" {
            statusLabel.text = "В процессе"
            statusLabel.textColor = UIColor.red
        } else if activeService.status == "done" {
            statusLabel.text = "Решено"
            statusLabel.textColor = UIColor.green
        } else {
            statusLabel.text = ""
        }
    }
}
