//
//  CreateZayavkaViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Павел Останин on 06.11.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD

class CreateZayavkaViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var commentHistory: UILabel!
    @IBOutlet weak var viewForHistory: UIView!
    @IBOutlet weak var titleHistory: UILabel!
    @IBOutlet weak var statusHistory: UILabel!
    @IBOutlet weak var h1Label: UILabel!
    @IBOutlet weak var h2Label: UILabel!
    @IBOutlet weak var h3Label: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var commentTextView: UITextView!
    
    let profileViewModel = ProfileViewModel()
    var serviceId = 0
    var isHistory = false
    var service = ActiveServicesEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isHistory {
            self.getHistory()
        } else {
            let user = profileViewModel.getSelfEntity()
            self.nameTextField.text = user!.firstname
            self.phoneTextField.text = user!.phone
            self.numberTextField.text = service.code
            self.commentTextView.delegate = self
            self.nameTextField.delegate = self
            self.phoneTextField.delegate = self
            self.numberTextField.delegate = self
        }
        
        self.h1Label.text = service.name
        let dateString    = service.expired_time.getStringDate(dateFormat: "dd.MM.yyyy")
        self.h2Label.text = service.product_name
        self.h3Label.text = "Сервис действительный до \(dateString)"
        
        if service.expired_time <= 0 {
            h3Label.isHidden = true
        }
        
    }
    
    func getHistory() {
        self.profileViewModel.getHistory(uid: profileViewModel.getSelfEntity()!.uid, serviceId: Int(service.service_id)!).start { [weak self] events in
            if let history = self?.profileViewModel.history {
                let id = history["id"] as! String
                let date = Double(history["created_time"] as! String)!.getStringDate(dateFormat: "dd.MM.yyyy")
                let status = history["status"] as? String == "done"
                self?.titleHistory.text = "Заявка №\(id) от \(date)"
                self?.commentHistory.text = history["comment"] as? String
                
                if status {
                    self?.statusHistory.text = "Решено"
                    self?.statusHistory.textColor = .green
                } else {
                    self?.statusHistory.text = "В процессе"
                    self?.statusHistory.textColor = .red
                }
            } else {
                self?.viewForHistory.isHidden = true
            }
        }
    }

    
    @IBAction func sendAction(_ sender: Any) {
        guard let serviceID = Int(service.service_id) else { return }
        self.profileViewModel.postHistory(uid: profileViewModel.getSelfEntity()!.uid,
                                          username: nameTextField.text!,
                                          phone: phoneTextField.text!,
                                          comment: commentTextView.text,
                                          serviceId: serviceID).observe(on: UIScheduler())
            .start { [weak self] events in
                let refreshAlert = UIAlertController(title: "Заявка принята",
                                                     message: "Ваша заявка принята. Консультант свяжется с Вами в течении 30 минут.",
                                                     preferredStyle: .actionSheet)
                
                refreshAlert.addAction(UIAlertAction(title: "ОК",
                                                     style: .default,
                                                     handler: { (action: UIAlertAction!) in
                                                        self?.profileViewModel.presenter.backToPreviousVC()
                }))
                UserDefaults.standard.set(true, forKey: "bool")
                self?.present(refreshAlert, animated: true) {
                    //self.revealViewController().revealToggle(animated: false)
                }
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


