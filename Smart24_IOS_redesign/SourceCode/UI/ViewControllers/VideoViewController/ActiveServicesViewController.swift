//
//  ActiveServicesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Paul Minakov on 28.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD

class ActiveServicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    let ACCEPTABLE_CHARACTERS = "0123456789-"

    var ratingView : RatingView?
    var blurview : UIView?
    var isRate = false
    var rateId = 0
    var productName = ""

    var isFromCreate = true
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createRequestAction: UIButton!
    @IBOutlet weak var historyAction: UIButton!
    @IBOutlet weak var activeButton: UIButton!
    
    fileprivate var scannedString: String?
    fileprivate let profileViewModel = ProfileViewModel()

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")

        return (string == filtered)
    }

    @IBAction func activateAction(_ sender: Any) {
        let alert = UIAlertController(title: Constants.enterActivationCode, message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.delegate = self
            if let string = self.scannedString {
                textField.text = string
            } else {
                textField.placeholder = Constants.enterActivationCodePlaceholder
            }
        }
        
        alert.addAction(UIAlertAction(title: Constants.scanCodeAction, style: .default) { [weak self] _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let barcodeScannerVC = storyboard.instantiateViewController(withIdentifier: "BarcodeScannerViewController") as! BarcodeScannerViewController
            barcodeScannerVC.delegate = self
            self?.present(barcodeScannerVC, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] _ in
            if let activationCode = alert.textFields?[0].text, let userId = self?.profileViewModel.getSelfEntity()?.uid {
                self?.activateServices(userId: userId, activateServices: activationCode)
            }
        })
        alert.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
        present(alert, animated: self.scannedString != nil ? false :true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        
        self.activeButton.layer.cornerRadius = 4.0
        self.activeButton.layer.borderColor = self.activeButton.titleLabel?.textColor.cgColor
        self.activeButton.layer.borderWidth = 1.0
        if (self.isFromCreate) {
            self.showAlertMessage(title: nil, message: Constants.activationSuccess)
        }
        if self.isRate {
            let nibs = Bundle.main.loadNibNamed("RatingView", owner: self, options: nil)
            guard let view = nibs?.first as? RatingView  else {
                return
            }
            view.delegate = self
            self.blurview = UIView(frame:self.view.bounds)
            self.blurview!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            self.view.addSubview(self.blurview!)
            view.frame = CGRect(x: 30, y: 30, width: UIScreen.main.bounds.size.width - 60.0, height: 260.0)
            view.center = self.view.center
            self.ratingView = view
            self.ratingView?.topLabel.text = productName
            self.view.addSubview(view)
        }
        profileViewModel.getActiveServices(userId: profileViewModel.getSelfEntity()!.uid)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                    self?.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let value = self.profileViewModel.activeServices.value {
            return value.count
        } else {
            return 0
        }
        //return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    @IBAction func createRequestCellAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        guard let values = self.profileViewModel.activeServices.value else { return  }
        let activeService = values[indexPath!.row]
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateZayavkaViewController") as! CreateZayavkaViewController
        vc.service = activeService
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func historyCellAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "Menu", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateZayavkaViewController1") as! CreateZayavkaViewController
        vc.isHistory = true
        guard let values = self.profileViewModel.activeServices.value else { return  }
        let activeService = values[indexPath!.row]
        vc.service = activeService
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let values = self.profileViewModel.activeServices.value else { return UITableViewCell() }
        if values.count == indexPath.row {
            return self.tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
        }
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActiveServicesTableViewCell
        cell.configrateCell(activeService: values[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    private func activateServices(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)

        profileViewModel.activateService(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }

                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch events {
                case .completed:
                    strongSelf.profileViewModel.getActiveServices(userId: (self?.profileViewModel.getSelfEntity()!.uid)!)
                        .observe(on: UIScheduler())
                        .start { events in
                            strongSelf.tableView.reloadData()
                    }
                case .value(let gift):
                    if gift == true {
                        
                        strongSelf.profileViewModel.getGifts().start({ (event) in
                            switch event {
//                            case .value(let gifts):
//                                strongSelf.profileViewModel.presentGiftsVC(gifts: gifts, didSelectGift: { (id) in
//                                    strongSelf.profileViewModel.activateGift(id: id).start({ (event) in
//                                        switch event {
//                                        case .value(let gift):
//                                            strongSelf.profileViewModel.presentGiftDetailVC(giftCodeEntity: gift)
//                                        case .failed(let error):
//                                            strongSelf.showSmart24Error(error: error)
//                                        default:
//                                            break
//                                        }
//                                    })
//                                })
                            case .failed(let error):
                                strongSelf.showSmart24Error(error: error)
                            default:
                                break
                            }
                        })
                    }
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
                }
        }
    }
    
    @IBAction func contractButtonAction(_ sender: Any) {
        guard let url = URL(string: "http://5element.so24.net/assets/files/contract/ru/oferta-zaschita.pdf") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}

extension ActiveServicesViewController : BarcodeScannerViewControllerDelegate {
    func didFinishWith(string : String) {
        self.scannedString = string
        self.activateAction(UIButton())
    }
}

extension ActiveServicesViewController : Delegate1 {
    func ok(rating : Float, text: String) {
        self.profileViewModel.rate(uid: self.profileViewModel.getSelfEntity()!.uid, requestId: self.rateId, rating: self.ratingView!.ratingView.rating as! Int, comment: self.ratingView!.textField.text ?? "")
        self.blurview?.removeFromSuperview()
        self.ratingView?.removeFromSuperview()
        
    }
    
    func cancel() {
        self.blurview?.removeFromSuperview()
        self.ratingView?.removeFromSuperview()
    }
}
