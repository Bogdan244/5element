//
//  VideoViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 1/5/17.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import VideoCore
import ReactiveSwift
import MBProgressHUD

class VideoViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var changeCameraButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    
    var streamingVideoSession: VCSimpleSession!
    var activeCall: VSLCall?
    
    private let chatViewModel = ChatViewModel()
    private var startCall = false
    private var account: VSLAccount?
    private var callManager: VSLCallManager?
    private let closeChatAlertView  = CloseChatAlertView()
    private let timerInterval: Double = 3
    
    @IBAction func makeCallButtonAction(_ sender: Any) {
        if startCall {
            callButton.setImage(R.image.make_call_icon(), for: .normal)
            startCall = false
            callManager?.endAllCalls()
        } else {
            callButton.setImage(R.image.close_video_icon(), for: .normal)
            startCall = true
            makeCall()
        }
    }
    
    @IBAction func changeCameraButtonAction(_ sender: Any) {
        if streamingVideoSession.cameraState == .front {
            streamingVideoSession.cameraState = .back
        } else {
            streamingVideoSession.cameraState = .front
        }
    }
    
    @IBAction func muteButtonAction(_ sender: Any) {
        if streamingVideoSession.micGain == Constants.videoMicrophoneOff {
            streamingVideoSession.micGain = Constants.videoMicrophoneOn
            muteButton.setImage(R.image.video_mic_icon(), for: .normal)
        } else {
            streamingVideoSession.micGain = Constants.videoMicrophoneOff
            muteButton.setImage(R.image.video_unmute_mic_icon(), for: .normal)
        }
    }
    
    @IBAction func videoButtonAction(_ sender: Any) {
        showCloseChatAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureSIP()
        configureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        chatViewModel.didMessageRecive {
            if let containerVC = UIApplication.containerViewController() as? ContainerViewController {
              //  containerVC.updateChat()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = false
        
    }
    
    private func configureView() {
        hideKeyboardWhenTappedAround()
        revealViewController().panGestureRecognizer().isEnabled = false
        
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        if let chatVC = children.first as? ChatViewController {
            chatVC.collectionView.backgroundColor = .clear
            chatVC.view.backgroundColor = .clear
            containerView.backgroundColor = .clear
        }
    }
    
    private func configureSIP() {
        callManager = VialerSIPLib.sharedInstance().callManager
        if let account = VialerSIPLib.sharedInstance().accounts()?.first as? VSLAccount {
            self.account = account
        }
        
        if let _ = activeCall {
            callButton.setImage(R.image.close_video_icon(), for: .normal)
            startCall = true
        }
    }
    
    private func configureSignals() {
        //MARK:- doesn't send dialogId if it equalse 0
        var dialogId = chatViewModel.dialogId
        if chatViewModel.dialogId == Constants.emptyDialogId {
            dialogId = nil
        }
        
        chatViewModel.sendMessage(message: Constants.startVideoMessage, dialog_id: dialogId, isVideo: IsVideoStart.start.rawValue)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                switch events {
                case .completed:
                    self?.configureRTMP()
                case .value(_):
                    break
                default:
                    let alert = UIAlertController(title: nil, message: Constants.cantStartVideoMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: Constants.ok, style: .default, handler: { (action) in
                        self?.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(action)
                    self?.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    private func configureRTMP() {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            guard let strongSelf = self else { return }
            
            let videoCaptureSize = CGSize(width: 640, height: 360)
            strongSelf.streamingVideoSession = VCSimpleSession(videoSize: videoCaptureSize,
                                                               frameRate: 30, bitrate: 500000,
                                                               useInterfaceOrientation: true,
                                                               cameraState: .back,
                                                               aspectMode: .aspectModeFit)
            strongSelf.streamingVideoSession.orientationLocked = false
            strongSelf.streamingVideoSession.useAdaptiveBitrate = false
            strongSelf.streamingVideoSession.delegate = strongSelf
            strongSelf.view.addSubview(strongSelf.streamingVideoSession.previewView)
            strongSelf.view.sendSubviewToBack(strongSelf.streamingVideoSession.previewView)
            strongSelf.streamingVideoSession.previewView.frame = strongSelf.view.bounds
            
            let stringUserId = String(strongSelf.chatViewModel.currentId)
            strongSelf.streamingVideoSession.startRtmpSession(withURL: Constants.rtmpUrl, andStreamKey: stringUserId)
            
        }
    }
    
    private func showCloseChatAlert() {
        let alert = UIAlertController(title: title, message: Constants.closeChatMessage, preferredStyle: .alert)
        let closeVideo = UIAlertAction(title: Constants.continueChat, style: .default) { [weak self] action in
            self?.chatViewModel.presentChatVC()
        }
        let closeAll = UIAlertAction(title: Constants.closeChat, style: .default) { [weak self] action in
            self?.closeButtonTapped()
        }
        alert.addAction(closeVideo)
        alert.addAction(closeAll)
        alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(alertClose)))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func alertClose() {
        dismiss(animated: true, completion: nil)
    }
    
    func closeButtonTapped() {
        guard let dialogId = chatViewModel.dialogId else {
            showAlertMessage(title: nil, message: Constants.notExixstChatMessage)
            return
        }
        
        closeChatAlertView.show(message: Constants.closeChatQuestion)
        view.endEditing(true)
        closeChatAlertView.dialogId = dialogId
        closeChatAlertView.sendButton.addTarget(self, action: #selector(sendCloseDialog), for: .touchUpInside)
    }
    
    @objc func sendCloseDialog() {
        closeChatAlertView.closeButtonAction(self)
        
        guard let containerVC = UIApplication.containerViewController() as? ContainerViewController else {
            return
        }
        
        let chatGrade = closeChatAlertView.currentGrade.rawValue
        let opinion = closeChatAlertView.getMessage()
        containerVC.chatTimer?.invalidate()
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        chatViewModel.closeDialog(dialog_id: closeChatAlertView.dialogId, grade: chatGrade, opinion: opinion)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                strongSelf.chatViewModel.openRootVC()
        }
    }
    
    private func makeCall() {
        guard let account = account, let callNumber = chatViewModel.expertCallId else { return }
        
        callManager?.startCall(toNumber: callNumber, for: account ) { [weak self] call, error in
            if let error = error {
                debugPrint(error)
            } else {
                self?.activeCall = call
            }
        }
    }
    
    private func muteSIP() {
        guard let call = activeCall, call.callState != .disconnected else { return }
        
        if call.muted {
            muteButton.setImage(R.image.video_mic_icon(), for: .normal)
        } else {
            muteButton.setImage(R.image.video_unmute_mic_icon(), for: .normal)
        }
        
        callManager?.toggleMute(for: call) { error in
            if let error = error {
                debugPrint(error)
            }
        }
    }
    
    deinit {
        if streamingVideoSession != nil {
            streamingVideoSession.endRtmpSession()
        }
        
        callManager?.endAllCalls()
    }
}

extension VideoViewController: VCSessionDelegate {
    //MARK - VCSessionDelegate
    func connectionStatusChanged(_ sessionState: VCSessionState) {
        switch sessionState {
        case .none:
            break;
        case .previewStarted:
            break;
        case .starting:
            break
        case .started:
           break
        case .ended:
            break
        case .error:
            if streamingVideoSession != nil {
                streamingVideoSession.endRtmpSession()
            }
        }
    }
}
