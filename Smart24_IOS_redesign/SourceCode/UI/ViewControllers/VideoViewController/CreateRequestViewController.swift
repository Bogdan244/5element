//
//  CreateRequestViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Paul Minakov on 28.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import MBProgressHUD


class CreateRequestViewController: UIViewController, UITextFieldDelegate {

    let ACCEPTABLE_CHARACTERS = "0123456789-"

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")

        return (string == filtered)
    }

    var ratingView : RatingView?
    var blurview : UIView?
    var isRate = false
    var rateId = 0
    var productName = ""
    fileprivate let profileViewModel = ProfileViewModel()
    fileprivate var scannedString: String?
    @IBAction func activateAction(_ sender: UIButton) {
        let alert = UIAlertController(title: Constants.enterActivationCode, message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.delegate = self
            if let string = self.scannedString {
                textField.text = string
            } else {
               textField.placeholder = Constants.enterActivationCodePlaceholder
            }
            
        }
        
        
        alert.addAction(UIAlertAction(title: Constants.scanCodeAction, style: .default) { [weak self] _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let barcodeScannerVC = storyboard.instantiateViewController(withIdentifier: "BarcodeScannerViewController") as! BarcodeScannerViewController
            barcodeScannerVC.delegate = self
            self?.present(barcodeScannerVC, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: Constants.ok, style: .default) { [weak self] _ in
            if let activationCode = alert.textFields?[0].text, let userId = self?.profileViewModel.getSelfEntity()?.uid {
                self?.activateServices(userId: userId, activateServices: activationCode)
            }
        })
        alert.addAction(UIAlertAction(title: Constants.close, style: .cancel, handler: nil))
        present(alert, animated: self.scannedString != nil ? false :true, completion: nil)
    }
    @IBOutlet weak var activateButton: UIButton!
    
    override func viewDidLoad() {
        self.activateButton.layer.cornerRadius = 4.0
        self.activateButton.layer.borderWidth = 1.0
        self.activateButton.layer.borderColor = UIColor(red: 80.0/255.0, green: 190.0/255.0, blue: 132.0/255.0, alpha: 1).cgColor
        if self.isRate {
            let nibs = Bundle.main.loadNibNamed("RatingView", owner: self, options: nil)
            guard let view = nibs?.first as? RatingView  else {
                return
            }
            view.delegate = self
            self.blurview = UIView(frame:self.view.bounds)
            self.blurview!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            self.view.addSubview(self.blurview!)
            view.frame = CGRect(x: 30, y: 30, width: UIScreen.main.bounds.size.width - 60.0, height: 260.0)
            view.center = self.view.center
            self.ratingView = view
            self.ratingView?.topLabel.text = productName
            self.view.addSubview(view)
        }
        
        
    }
    
    private func activateServices(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)

        profileViewModel.activateService(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }

                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                switch events {
                case .completed:
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ActiveServicesViewController")
                    self?.navigationController?.pushViewController(controller, animated: true)
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
                default:
                    break
                }
        }
    }
}

extension CreateRequestViewController : BarcodeScannerViewControllerDelegate {
    func didFinishWith(string : String) {
        self.scannedString = string
        self.activateAction(UIButton())
    }
}

extension CreateRequestViewController : Delegate1 {
    func ok(rating : Float, text: String) {
        self.profileViewModel.rate(uid: self.profileViewModel.getSelfEntity()!.uid, requestId: self.rateId, rating: self.ratingView!.ratingView.rating as! Int, comment: self.ratingView!.textField.text ?? "")
        self.blurview?.removeFromSuperview()
        self.ratingView?.removeFromSuperview()
        
    }
    
    func cancel() {
        self.blurview?.removeFromSuperview()
        self.ratingView?.removeFromSuperview()
    }
}

