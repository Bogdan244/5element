//
//  BarcodeScannerViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Paul Minakov on 25.10.2017.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit

protocol BarcodeScannerViewControllerDelegate : class {
    func didFinishWith(string : String)
}

class BarcodeScannerViewController: UIViewController {
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var closeButton: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var scanner: MTBBarcodeScanner?
    
    weak var delegate: BarcodeScannerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.bringSubviewToFront(self.closeButton)
        scanner = MTBBarcodeScanner(previewView: self.view)
        let overlayPath = UIBezierPath(rect: self.view.bounds)
        let transparentPath = UIBezierPath(rect: CGRect(x: 60,y: UIScreen.main.bounds.size.height / 2.0 - 80.0,width: UIScreen.main.bounds.size.width - 120.0,height: 160.0))
        overlayPath.append(transparentPath)
        overlayPath.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = CAShapeLayerFillRule.evenOdd
        fillLayer.fillColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.view.layer.addSublayer(fillLayer)
        /*UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
        // 2
        UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRect:CGRectMake(60, 120, 200, 200)];
        [overlayPath appendPath:transparentPath];
        [overlayPath setUsesEvenOddFillRule:YES];
        // 3
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = overlayPath.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor colorWithRed:255/255.0 green:20/255.0 blue:147/255.0 alpha:1].CGColor;
        // 4
        [self.view.layer addSublayer:fillLayer];*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        self.scanner?.stopScanning()
                        if let codes = codes {
                            if let code = codes.first {
                                let stringValue = code.stringValue!
                                
                                //DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                                self.dismiss(animated: false, completion: {
                                    self.delegate?.didFinishWith(string: stringValue)
                                })
                                //})
                            }
                        }
                    })
                } catch {
                    NSLog("Unable to start scanning")
                }
            } else {
                UIAlertView(title: "Scanning Unavailable", message: "This app does not have permission to access the camera", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "Ok").show()
            }
            
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopScanning()
        
        super.viewWillDisappear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

