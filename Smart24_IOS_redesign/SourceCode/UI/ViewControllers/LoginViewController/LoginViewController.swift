//
//  LoginViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD
import GoogleSignIn
import ok_ios_sdk
import VK_ios_sdk
import FacebookCore
import FacebookLogin

class LoginViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var enterButton: LoginButton!
    
    fileprivate let loginViewModel = LoginViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        confgiureSignals()
        if let _ = loginViewModel.userPhoneProperty.value, let _ = loginViewModel.userPasswordProperty.value {
            loginViewModel.loginUser(otpCode: nil).start { [weak self] (event) in
                self?.applicationLoginHandler(event: event)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        phoneTextField.delegate = self
        phoneTextField.keyboardType = .numberPad
        phoneTextField.clearButtonMode = .never
        passwordTextField.clearButtonMode = .never
        GIDSignIn.sharedInstance().delegate = self
    
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        phoneTextField.setLeftImage(image: R.image.gray_phone_icon())
        passwordTextField.setLeftImage(image: R.image.gray_lock_icon())
        
        if let phone = UserDefaults.standard.string(forKey: Constants.savedPhone),
            let password = UserDefaults.standard.string(forKey: Constants.savedPassword) {
            var phoneStr = phone
            if phoneStr.first != "+" {
                phoneStr = "+" + phoneStr
            }
            
            phoneTextField.text      = phoneStr
            passwordTextField.text   = password
            
            loginViewModel.userPhoneProperty.value = phone
            loginViewModel.userPasswordProperty.value = password
        }
    }
    
    private func confgiureSignals() {
        let userNameProducer        = loginViewModel.racTextProducer(textField: phoneTextField)
        let userPasswordProducer    = loginViewModel.racTextProducer(textField: passwordTextField)
        
        loginViewModel.userPhoneProperty    <~ userNameProducer
        loginViewModel.userPasswordProperty <~ userPasswordProducer
        
        enterButton.reactive.isEnabled      <~ loginViewModel.loginInputValid
        
        
        enterButton.addTarget(loginViewModel.cocoaActionLogin, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        enterButton.reactive
            .controlEvents(.touchUpInside)
            .observe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
                MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
        }
        
        loginViewModel.loginSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.applicationLoginHandler(event: event)
        }
        
        loginViewModel.loginSignal
            .observe(on: UIScheduler())
            .observe { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.applicationLoginHandler(event: event)
        }
        
        if let _ = UserDefaults.standard.string(forKey: Constants.savedPhone),
            let _ = UserDefaults.standard.string(forKey: Constants.savedPassword) {
            loginViewModel.cocoaActionLogin.execute(enterButton)
        }
    }
    
    private func loginActionHandler(event: (Signal<Any, ErrorEntity>.Event), otpHandler: @escaping (() -> Void)) {
//        DispatchQueue.main.async {
//            self.view.endEditing(true)
//            MBProgressHUD.hide(for: self.view, animated: true)
//        }
//        switch event {
////        case .completed:
////            self.showConfirmRegistration()
//        case .failed(let error):
//            self.showAlertMessage(title: "", message: error.data )
//        case .value(let action as BPLoginAction):
//            if action == BPLoginAction.needPhoneNumber {
//                let alert = UIAlertController(title: "Введите свой номер телефона", message: "", preferredStyle: .alert)
//                alert.addTextField { (textField) in
//                    textField.keyboardType = .numberPad
//                    self.loginViewModel.phoneSocial <~ self.loginViewModel.racTextProducer(textField: textField)
//                }
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
//                    otpHandler()
//                }))
//                alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            } else {
//                var message = ""
//                if action == BPLoginAction.wrongOTP {
//                    message = "Неверный код"
//                }
//                let alert = UIAlertController(title: "Введите код из смс", message: message, preferredStyle: .alert)
//                alert.addTextField { (textField) in
//                    textField.keyboardType = .numberPad
//                    if #available(iOS 12.0, *) {
//                        textField.textContentType = UITextContentType.oneTimeCode
//                    }
//                    self.loginViewModel.otpCode <~ self.loginViewModel.racTextProducer(textField: textField)
//                }
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
//                    otpHandler()
//                }))
//                alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
//        default:
//            break
//        }
    }
    
    func applicationLoginHandler(event: (Signal<Any, ErrorEntity>.Event)) {
        loginActionHandler(event: event) {
            self.loginViewModel.loginUser(otpCode: self.loginViewModel.otpCode.value).start({ [weak self] (event) in
                self?.applicationLoginHandler(event: event)
            })
        }
    }
    
    func socialLoginHandler(event: (Signal<Any, ErrorEntity>.Event), social: SocialName, token: String?) {
        loginActionHandler(event: event) { [weak self] in
            self?.loginViewModel.authWithSocial(socialName: social,
                                                accessToken: token,
                                                phone: self?.loginViewModel.phoneSocial.value,
                                                otp: self?.loginViewModel.otpCode.value)
                .start({ [weak self] (event) in
                    self?.socialLoginHandler(event: event, social: social, token: token)
            })
        }
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        loginViewModel.openForgotPasswordVC()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        loginViewModel.openRegistrationVC()
    }
    
    @IBAction func vkButtonAction(_ sender: Any) {
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        VKSdk.forceLogout()
        VKSdk.authorize(["wall"])
    }
    
    @IBAction func faceBookButtonAction(_ sender: Any) {
        loginViewModel.loginWithFacebook(viewController: self) { [weak self] (loginResult, error)  in
            guard let token = loginResult?.token else { return }
            self?.loginViewModel
                .authWithSocial(socialName: SocialName.facebook, accessToken: token.tokenString)
                .start({ [weak self] (event) in
                    self?.socialLoginHandler(event: event, social: SocialName.facebook, token: token.tokenString)
                })
//            switch loginResult {
//            case .failed(let error):
//                print(error)
//            case .cancelled:
//                debugPrint("User cancelled login.")
//            case .success( _, _, let token):
//                self?.loginViewModel.authWithSocial(socialName: SocialName.facebook, accessToken: token.authenticationToken).start({ [weak self] (event) in
//                    self?.socialLoginHandler(event: event, social: SocialName.facebook, token: token.authenticationToken)
//                })
//            }
        }
    }
    
    @IBAction func googlePlusButtonAction(_ sender: Any) {
        GIDSignIn.sharedInstance().clientID = Constants.gClientID
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        OKSDK.clearAuth()
        OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS","LONG_ACCESS_TOKEN"], success: { [weak self] (data) in
            guard let dataArray = data as? Array<String> else { return }
            print("\(dataArray)")
            self?.loginViewModel.authWithSocial(socialName: SocialName.odnoklassniki, accessToken: dataArray.first ?? "").start({ [weak self] (event) in
                self?.socialLoginHandler(event: event, social: SocialName.odnoklassniki, token: dataArray.last ?? "")
            })
//            OKSDK.invokeMethod("users.getCurrentUser", arguments: [:], success: { (data) in
//                guard let dataDict = data as? [String : Any] ,let uid = dataDict["uid"] as? String else { return }
//
//
//            }, error: { (error) in
//                if let e = error {
//                    self.showSmart24Error(error: e)
//                }
//
//            })
        }) { (error) in
            print("\(String(describing: error))")
        }
        
    }
    
//    func loginWithSocial(type: Int, id: String) {
//        loginViewModel.loginWithSocial(type: type, id: id)
//            .observe(on: UIScheduler())
//            .start { [weak self] event in
//                guard let strongSelf = self else { return }
//                MBProgressHUD.hide(for: strongSelf.view, animated: true)
//                
//                if let error = event.error {
//                    strongSelf.showSmart24Error(error: error)
//                }
//        }
//    }
}

//MARK:- UITextFieldDelegate delegate
extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let protectedRange = NSMakeRange(0, 2)
        let intersection = NSIntersectionRange(protectedRange, range)
        if intersection.length > 0 {
            return false
        }
        if range.location == 12 {
            return true
        }
        if range.location + range.length > 12 {
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let isEmpty = textField.text?.isEmpty, isEmpty == true {
            textField.text = "+3"
            loginViewModel.userPhoneProperty.value = "+3"
            enterButton.isEnabled = false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "+3" {
            textField.text = ""
        }
    }
}

extension LoginViewController: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        VKCaptchaViewController.captchaControllerWithError(captchaError).present(in: self)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        loginViewModel.authWithSocial(socialName: SocialName.vkontakte, accessToken: result.token.accessToken).start({ [weak self] (event) in
            self?.socialLoginHandler(event: event, social: SocialName.vkontakte, token: result.token.accessToken)
        })
    }
    
    func vkSdkUserAuthorizationFailed() {
        
    }
}

//MARK:- google+ delegate
extension LoginViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user == nil {
            MBProgressHUD.hide(for: view, animated: true)
            return
        }

        MBProgressHUD.showAdded(to: view, animated: true)
        
        loginViewModel.authWithSocial(socialName: SocialName.google, accessToken: user.authentication.accessToken).start({ [weak self] (event) in
            self?.socialLoginHandler(event: event, social: SocialName.google, token: user.authentication.accessToken)
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

//extension LoginViewController: GIDSignInUIDelegate {
//    
//    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//    
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        present(viewController, animated: true, completion: nil)
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//    
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
//        dismiss(animated: true, completion: nil)
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//}
