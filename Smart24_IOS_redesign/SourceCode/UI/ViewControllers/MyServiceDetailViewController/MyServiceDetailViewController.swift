//
//  MyServiceDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 29.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class MyServiceDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var copyButton: CopyTextButton!
    
    var martletCodeEntity = MartletCodeDependencyEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
    }
    
    private func configureViews() {
    
        titleLabel.text = martletCodeEntity.product
        codeLabel.text = martletCodeEntity.licence
        descriptionTextView.text = martletCodeEntity.about
        
        copyButton.setCopyText(text: martletCodeEntity.licence,
                               for: UIControl.Event.touchUpInside)
    }
    
}
