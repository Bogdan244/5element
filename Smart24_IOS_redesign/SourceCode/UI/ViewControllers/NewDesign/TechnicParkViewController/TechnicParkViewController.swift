//
//  TechnicParkViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class TechnicParkViewController: BaseMenuViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var floatingActionButton: UIButton!
    
    var dataSource = [TechnicParkEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 80, right: 0)
        floatingActionButton.makeCircle()
        floatingActionButton.layer.shadowColor = UIColor.black.cgColor
        floatingActionButton.layer.shadowOffset = CGSize(width: 0, height: 14)
        floatingActionButton.layer.shadowOpacity = 0.1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTechnikPark()
    }
    
    private func updateTechnikPark() {
        TechnicParkService().getProductListFromTechnicPark(complition: { [weak self] (technickList) in
            guard let self = self else { return }
            self.dataSource = technickList.techicParkList
            self.tableView.reloadData()
        }) {
            
        }
    }
    
    @IBAction func floatingButtomAction(_ sender: Any) {
        PresenterImpl().openAddTechnicToParkViewController()
    }
    
}

extension TechnicParkViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TechnicParkCell") as! TechnicParkCell
        cell.setTechnic(technic: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        PresenterImpl().openTechnicParkDetailVC(technicParkList: dataSource, delegate: self, scrollToIndex: indexPath)
    }
}

extension TechnicParkViewController: TechnicParkDetailViewControllerDelegate {
    func viewController(_ viewController: TechnicParkDetailViewController, removeProductAt index: Int) {
        viewController.navigationController?.popViewController(animated: true)
        MBProgressHUD.showAdded(to: view, animated: true)
        TechnicParkService().getProductListFromTechnicPark(complition: { [weak self] (technickList) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.dataSource = technickList.techicParkList
            self.tableView.reloadData()
        }) {
           MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func viewController(_ viewController: TechnicParkDetailViewController, didEditAt index: Int) {
        TechnicParkService().getProductListFromTechnicPark(complition: { [weak self] (technickList) in
            guard let self = self else { return }
            self.dataSource = technickList.techicParkList
            self.tableView.reloadData()
            guard technickList.techicParkList.count > 0 else { return }
            viewController.dataSource = technickList.techicParkList
            viewController.collectionView.reloadData()
            viewController.setupUIforDataSource(index: index)
            
        }) {
            
        }
    }
}
