//
//  NewNotificationsViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewNotificationsViewController: BaseMenuViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [NewNotificationEntity]()
    private var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationsService.shared.getNotification(complition: { [weak self] (notifications) in
            guard let self = self else { return }
            self.isFirstLoad = false
            self.dataSource = notifications.notifications
            self.tableView.reloadData()
        }) {
            
        }
    }

}

extension NewNotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0, !isFirstLoad {
            let label = UILabel(frame: tableView.frame)
            label.text = "Сообщения и уведомления отсутствуют"
            label.numberOfLines = 0
            label.font = UIFont(name: "Roboto-Regular", size: 18)
            label.textAlignment = .center
            label.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            tableView.backgroundView = label
        } else {
            tableView.backgroundView = nil
        }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.configurate(notification: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        MBProgressHUD.showAdded(to: view, animated: true)
        NotificationsService.shared.readNotification(id: dataSource[indexPath.row].id, complition: { [weak self] notification in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.dataSource[indexPath.row].read = 1
            self.tableView.reloadData()
            PresenterImpl().openNotificationDetailVC(notification: notification)
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
}
