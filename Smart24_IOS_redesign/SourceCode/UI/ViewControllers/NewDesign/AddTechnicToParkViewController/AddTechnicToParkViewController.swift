//
//  AddTechnicToParkViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/13/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class AddTechnicToParkViewController: BaseMenuViewController {

    var openSection: Int?
    var openView: AddTechnicSectionHeaderView?
    @IBOutlet weak var tableView: UITableView!
    
    var technicParkInfo: TechnikParkInfoEntity?
    var technicCategoriesArray: [TechnicCategories] {
        guard let technicPark = technicParkInfo else {
            return [TechnicCategories]()
        }
        return technicPark.technicCategories
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AddTechnicSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "AddTechnicSectionHeaderView")
        TechnicParkService().getTechnicParkInfo(complition: { [unowned self] (technicParkInfo) in
            self.technicParkInfo = technicParkInfo
            self.tableView.reloadData()
        }) { [weak self] in
            self?.showAlertMessage(title: nil, message: "Ошибка сервера")
        }
    }
    
    func openDetailViewFor(section: Int, row: Int, state: AddTechnicToParkDetailViewController.State) {
        guard let technikPark = technicParkInfo,
            let technicCategories = technicParkInfo?.technicCategories[section].childrens?[row] else {
                showAlertMessage(title: "Ошибка", message: "Отсутвуют данные техники. Попробуйте позже.")
                return
        }
        PresenterImpl().openAddTechnicToParkDetailVC(technikParkInfo: technikPark, technicCategories: technicCategories, state: state)
    }
}

extension AddTechnicToParkViewController: UITableViewDelegate, UITableViewDataSource, AddTechnicSectionHeaderViewDelegate {
    func addTechnicSectionHeaderView(_ view: AddTechnicSectionHeaderView, didSelect section: Int) {
        if technicCategoriesArray[section].name == "Прочие устройства" {
            openDetailViewFor(section: section, row: 0, state: .otherDevices)
        } else if openSection == section {
            openSection = nil
            tableView.beginUpdates()
            tableView.reloadSections([section], with: .automatic)
            tableView.scrollToRow(at: IndexPath(row: 0, section: section), at: .none, animated: true)
            tableView.endUpdates()
        } else {
            var indexSet = IndexSet()
            if openSection != nil {
                indexSet.insert(openSection!)
            }
            openSection = section
            indexSet.insert(section)
            tableView.beginUpdates()
            tableView.reloadSections(indexSet, with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openDetailViewFor(section: indexPath.section, row: indexPath.row, state: .defaultState)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AddTechnicSectionHeaderView") as! AddTechnicSectionHeaderView
        view.configurate(with: technicCategoriesArray[section], section: section)
        view.delegate = self
        if openSection == section {
            view.headerContentView.backgroundColor = UIColor(red: 207/255, green: 43/255, blue: 43/255, alpha: 1)
            view.titleLB.textColor = .white
        } else {
            view.headerContentView.backgroundColor = .white
            view.titleLB.textColor = .black
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return technicCategoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if openSection == section, let childrens = technicCategoriesArray[section].childrens {
            return childrens.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTechnicCell") as! AddTechnicCell
        if let childrens = technicCategoriesArray[indexPath.section].childrens {
            cell.titleLabel.text = childrens[indexPath.row].name
        }
        return cell
    }
    
    
    
    
}
