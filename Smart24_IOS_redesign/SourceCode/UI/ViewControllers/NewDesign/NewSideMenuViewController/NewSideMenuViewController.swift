//
//  NewSideMenuViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/15/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD
import SideMenu

class NewSideMenuViewController: UITableViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var settingsImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var notificationAlertLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(settingsAction)))
        notificationAlertLabel.makeCircle()
        notificationAlertLabel.clipsToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width / 2
        avatarImageView.clipsToBounds = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setup()
    }
    
    private func setup() {
        notificationAlertLabel.isHidden = NotificationsService.shared.notViewed <= 0
        notificationAlertLabel.text = "\(NotificationsService.shared.notViewed)"
        
        if let user = UserEntity.current {
            nameLabel.text = "\(user.firstname)\n\(user.surname)"
            
            if let userAvatarURL = URL(string: user.userPhoto)  {
                avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarURL, cacheKey: nil),
                                            placeholder: nil,
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
            }
        }
    }
    
    @objc func settingsAction() {
        PresenterImpl().openNewProfileVC()
    }
    
    static func create() -> NewSideMenuViewController {
        let storyboard = UIStoryboard(name: "Menu", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewSideMenuViewController") as! NewSideMenuViewController
        return vc
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            //dismiss(animated: false, completion: nil)
            PresenterImpl().openNotificationsVC()
        case 2:
            RSService.shared.getRequestServices(complition: { (serviceArray) in
                PresenterImpl().openRequestServicesListVC(servicesList: serviceArray.services)
            }) {
                
            }
        case 3:
            MBProgressHUD.showAdded(to: view, animated: true)
            PRSService().getUserBonusCard(complition: { [weak self] (bonusCard) in
                guard let self = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                if bonusCard.cards.count > 0 {
                    GeneralViewModel().openBonussesVC(bonuses: bonusCard)
                } else {
                    GeneralViewModel().openActivateBonussesVC()
                }
            }) {
                 MBProgressHUD.hide(for: self.view, animated: true)
            }
        case 4:
            UIApplication.shared.openURL(URL(string:"https://5element.by/actions")!)
        case 5:
            UIApplication.shared.openURL(URL(string:"http://5element.by/shops/")!)
        case 6:
            SideMenuManager.default.menuLeftNavigationController!.dismiss(animated: true) {
                UIViewController.showAlert(with: "В данный момент опрос проводится на сайте 5element.by", duration: 2)
            }
            SideMenuManager.default.menuLeftNavigationController!.dismiss(animated: true, completion: nil)
        case 7:
            UIApplication.shared.openURL(URL(string:"https://5element.by/services/997-programma-zaschita")!)
        case 8:
            PresenterImpl().openDevelopersVC()
        case 9:
            MBProgressHUD.showAdded(to: view, animated: true)
            AuthorizationServices().logOut(complition: { [weak self] (entity) in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.dismiss(animated: true, completion: {
                    PresenterImpl().presentGreetingVC()
                })
            }) { [weak self] in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
            }
            
        default:
            break
        }
    }
    
}
