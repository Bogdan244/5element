//
//  NewMyServicesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/31/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewMyServicesViewController: BaseMenuViewController, GiftUIProtocol {

    @IBOutlet weak var floatingActionButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [MyBaseService]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        floatingActionButton.makeCircle()
        floatingActionButton.layer.shadowColor = UIColor.black.cgColor
        floatingActionButton.layer.shadowOffset = CGSize(width: 0, height: 14)
        floatingActionButton.layer.shadowOpacity = 0.1
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 80, right: 0)
        updateDataSource()
    }
    
    @IBAction func tapFloatingActionButtonAction(_ sender: Any) {
        PresenterImpl().openNewActivateMyServiceVC { [weak self] (isGift) in
            if isGift {
                self?.activateGifts(didActivateGift: nil)
            }
            self?.updateDataSource()
        }
    }
    
    override func leftButtonAction() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    fileprivate func updateDataSource() {
        RSService.shared.getServices(complition: { [weak self] (myServicesArrayEntity) in
            guard let self = self else { return }
            self.dataSource = myServicesArrayEntity.services
            self.tableView.reloadData()
        }) { [weak self] in
            guard let self = self else { return }
            self.showAlertMessage(title: nil, message: "Ошибка загрузки с сервера")
        }
    }
}

extension NewMyServicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let service = dataSource[indexPath.row]
        switch service {
        case .prs(let service):
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceCell") as! MyServiceCell
            cell.configurate(with: service)
            return cell
        case .martlet(let service):
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceMartletCell") as! MyServiceMartletCell
            cell.configurate(with: service)
            return cell
        case .insurance(let service):
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyInsuranceServiceCell") as! MyInsuranceServiceCell
            cell.configurate(with: service)
            return cell
        case .undefined:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceCell") as! MyServiceCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let service = dataSource[indexPath.row]
        switch service {
        case .prs(let service):
            PresenterImpl().openGiftVC(gift: service, giftID: service.id) { [weak self] in
                self?.updateDataSource()
            }
        case .martlet(let service):
            PresenterImpl().openGiftVC(gift: service, giftID: service.id) { [weak self] in
                self?.updateDataSource()
            }
        case .insurance(let service):
            PresenterImpl().openInsuranceVC(insurance: service) { [weak self] in
                self?.updateDataSource()
            }
        case .undefined:
            break
        }
    }
}
