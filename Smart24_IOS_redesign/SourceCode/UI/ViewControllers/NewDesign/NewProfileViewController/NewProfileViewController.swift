//
//  NewProfileViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/17/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import Kingfisher

class AvatarImageView: UIView {
    
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak var cameraImageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            imageView.image = image
            cameraImageView.isHidden = image != nil
        }
    }
    
    func setImage(stringURL: String?) {
        if let userAvatarURL = URL(string: stringURL ?? "")  {
            cameraImageView.isHidden = true
            imageView.kf.setImage(with: ImageResource(downloadURL: userAvatarURL, cacheKey: nil),
                                  placeholder: nil,
                                  options: nil,
                                  progressBlock: nil,
                                  completionHandler: nil)
        }
    }
    
    override func awakeFromNib() {
        makeCircle()
        layer.borderWidth = 1
        layer.borderColor = UIColor.init(hexString: "#DD092E")?.cgColor ?? UIColor.red.cgColor
        imageView.makeCircle()
        imageView.clipsToBounds = true
    }
}

class NewProfileViewController: UITableViewController, AuthorizationUIActionsProtocol {
    
    var authService = AuthorizationServices()
    
    @IBOutlet weak var avatarView: AvatarImageView!
    @IBOutlet weak var nameTF: Smart24TextFieldView!
    @IBOutlet weak var surnameTF: Smart24TextFieldView!
    @IBOutlet weak var phoneTF: Smart24TextFieldView!
    @IBOutlet weak var bonusCardTF: Smart24TextFieldView!
    @IBOutlet weak var passwordTF: Smart24TextFieldView!
    @IBOutlet weak var confirmPasswordTF: Smart24TextFieldView!
    @IBOutlet weak var genderTF: Smart24TextFieldView!
    @IBOutlet weak var cityTF: Smart24TextFieldView!
    @IBOutlet weak var birthdayTF: Smart24TextFieldView!
    
    var city: CityEntity?
    var date: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTF.state = .standart
        surnameTF.state = .standart
        phoneTF.state = .phone(code: "375")
        bonusCardTF.state = .bonusCard
        passwordTF.state = .password
        confirmPasswordTF.state = .password
        genderTF.state = .list(list: Gender.allCases.map { $0.rawValue })
        cityTF.state = .searchCities
        birthdayTF.state = .birthday(date: nil)
        cityTF.textField.didPickCity = { [weak self] city in
            self?.city = city
        }
        birthdayTF.textField.didChangeDate = { [weak self] date in
            self?.date = date
        }
        tableView.separatorStyle = .none
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton")?.withRenderingMode(.alwaysOriginal),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(backAction))
        avatarView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnAvatar)))
        
        avatarView.setImage(stringURL: UserEntity.current?.userPhoto ?? "")
        
        setupUser()
    }
    
    func setupUser() {
        if let user = UserEntity.current {
            nameTF.textField.text = user.firstname
            surnameTF.textField.text = user.surname
            phoneTF.textField.text = user.phone.replacingOccurrences(of: "375", with: "").group(by: [2,3,2,2])
            bonusCardTF.textField.text = user.bonusCard
            cityTF.textField.text = user.city?.cityName
            city = user.city
            birthdayTF.state = .birthday(date: user.birthday)
            date = user.birthday
            genderTF.textField.text = Gender(string: user.gender)?.rawValue
        }
    }
    
    @objc func tapOnAvatar() {
        PresenterImpl().openImagePickerVC(makePhotoHandler: { [weak self] in
            self?.presentImagePickerController(sourceType: .camera)
        }) { [weak self] in
            self?.presentImagePickerController(sourceType: .photoLibrary)
        }
    }
    
    private func presentImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.allowsEditing = true
        
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor(rgb: 0x2E2E2D)
        imagePicker.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        imagePicker.navigationBar.tintColor = .white
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                              NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 20) as Any]
        imagePicker.navigationBar.titleTextAttributes = textAttributes
        present(imagePicker, animated: true, completion: nil)
        
    }

    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if passwordTF.textField.text == confirmPasswordTF.textField.text {
            var base64ImageData: String = ""
            if let userImage = avatarView.image, let imageData = userImage.pngData() {
                base64ImageData = imageData.base64EncodedString(options: .endLineWithCarriageReturn)
            }
            changeUserData(name: nameTF.textField.text!,
                           surname: surnameTF.textField.text!,
                           password: passwordTF.textField.text!,
                           photo: base64ImageData,
                           phone: phoneTF.textField.text,
                           gender: Gender(rawValue: genderTF.textField.text ?? ""),
                           cityID: city?.cityExternalID, 
                           birthday: (date.serverTimeInterval != nil) ? String(date.serverTimeInterval!) : nil,
                           joinAccount: nil)
        } else {
            showAlertMessage(title: "", message: "Пароли не совпадают")
        }
        
    }
}

extension NewProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[.editedImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        avatarView.image = pickedImage.resizeImage(newWidth: 320)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
