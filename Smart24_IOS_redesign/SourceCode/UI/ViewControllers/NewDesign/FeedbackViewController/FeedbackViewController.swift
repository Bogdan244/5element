//
//  FeedbackViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class FeedbackViewController: BaseMenuViewController {
    
    var state: FeedbackType = .Thanks
    
    @IBOutlet weak var emailTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = state.title
        emailTextFieldView.state = .email
        textView.keyboardAppearance = .dark
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        RSService.shared.addReview(type: state, email: emailTextFieldView.textField.text!, description: textView.text!, complition: { [weak self] (entity) in
            if entity.errors.count > 0 {
                self?.showAlertMessage(title: "Ошибка", message: entity.errors.first!.data)
            } else {
                self?.showAlertMessage(title: nil, message: "Спасибо за Ваш отзыв.", complition: {
                    self?.navigationController?.popToRootViewController(animated: true)
                }, cancel: nil)
                self?.showAlertMessage(title: nil, message: "Спасибо за Ваш отзыв.")
            }
        }) {
            self.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
        }
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height 
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}

extension FeedbackType {
    var title: String {
        switch self {
        case .Thanks:
            return "Благодарность"
        case .Remark:
            return "Замечание"
        case .Suggest:
            return "Предложение"
        }
    }
}
