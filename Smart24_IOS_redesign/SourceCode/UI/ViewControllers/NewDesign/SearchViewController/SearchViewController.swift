//
//  SearchViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 4/2/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate: class {
    func didFindString(searchViewController: SearchViewController, searchString: String)
}

class SearchViewController: BaseTableViewController {
    
    weak var delegate: SearchViewControllerDelegate?
    
    var inputSearchText = ""
    
    var dataSource: [String] {
        guard let text = searchBar.text, text.count > 0 else {
            return items
        }
        return inputSearchText.isEmpty ? searchItems : [inputSearchText] + searchItems
    }
    
    var items = [String]()
    var searchItems = [String]()
    lazy var searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        tableView.separatorInset = .zero
        searchBar.sizeToFit()
        searchBar.delegate = self
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = .white
        navigationItem.titleView = searchBar
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
    }
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        cell.textLabel?.text = dataSource[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didFindString(searchViewController: self, searchString: dataSource[indexPath.row])
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        inputSearchText = items.contains { $0.lowercased() == searchText.lowercased() } ? "" : searchText
        searchItems = items.filter { $0.lowercased().contains(searchText.lowercased()) }
        tableView.reloadData()
    }
}

