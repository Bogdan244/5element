//
//  InsuranceViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/4/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class InsuranceViewController: BaseMenuViewController {
    
    var insurance: MyInsuranceServiceEntity!
    var didRemoveInsurance: (() -> ())?
    
    
    @IBOutlet weak var statusContainer: UIView!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var imeiLabel: UILabel!
    
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoImageContainerView: UIView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoTextLabel: UILabel!
    
    @IBOutlet weak var amountInfoView: UIView!
    @IBOutlet weak var amountInfoTextView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var dateInfoView: UIView!
    @IBOutlet weak var activateTimeLabel: UILabel!
    @IBOutlet weak var expiredTimeLabel: UILabel!
    
    @IBOutlet weak var risksInfoView: UIView!
    @IBOutlet weak var risksInfoTextView: UIView!
    @IBOutlet weak var risksLabel: UILabel!
    
    lazy var clearButton: UIBarButtonItem = {
        return UIBarButtonItem(image: UIImage(named: "clearGiftImage"), style: .plain, target: self, action: #selector(clear))
    }()
    
    lazy var contractButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "contractImage"), style: .plain, target: self, action: #selector(contract))
        button.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: 1, right: 0)
        return button
    }()
    
    override var rightBarButtons: [UIBarButtonItem] {
        return [clearButton, contractButton]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusView.makeCircle()
        infoView.isHidden = true
        amountInfoTextView.isHidden = true
        risksInfoTextView.isHidden = true
        infoStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnInfoView)))
        infoImageContainerView.addShadow(height: 10, opacity: 0.2)
        
        amountInfoView.addShadow(height: 5)
        amountInfoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnAmountInfoView)))
        dateInfoView.addShadow(height: 5)
        risksInfoView.addShadow(height: 5)
        risksInfoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnRisksInfoView)))
        setupInsuranceInfo()
        setupAmountInfo()
        setupDateInfo()
        setupRisksInfo()
    }
    
    func setupRisksInfo() {
        risksLabel.attributedText = NSAttributedString(string: risksLabel.text ?? "",
                                                        attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    
    func setupDateInfo() {
        activateTimeLabel.text = "с " + insurance.activatedTime
        expiredTimeLabel.text = "до " + insurance.expiredAt
    }
    
    func setupAmountInfo() {
        amountLabel.attributedText = NSAttributedString(string: "\(insurance.insuranceAmount) руб.",
                                                        attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
    }
    
    func setupInsuranceInfo() {
        statusContainer.isHidden = false
        statusLabel.text = insurance.statusText
        statusView.backgroundColor = UIColor(hexString: insurance.statusColor) ?? .clear
        
        modelLabel.text = insurance.model
        imeiLabel.text = "imei: \(insurance.imei)"
        infoTitleLabel.text = insurance.processDescriptionTitle
        let text = "<span style=\"color: rgb(255, 255, 255); font-size: 12\">" + (insurance?.processDescriptionText ?? "") + "</span>"
        let text2 = "<span style=\"color: rgba(255, 255, 255, 0.6); font-size: 11\">" + (insurance?.processDescriptionDefinition ?? "") + "</span>"
        infoTextLabel.attributedText = (text + text2).attributedHtmlString
    }
    
    @objc func tapOnInfoView() {
        infoView.isHidden = !infoView.isHidden
        infoImageView.image = infoView.isHidden ? UIImage(named: "downArrow") : UIImage(named: "upArrow")
    }

    @objc func tapOnAmountInfoView() {
        amountInfoTextView.isHidden = !amountInfoTextView.isHidden
    }
    
    @objc func tapOnRisksInfoView() {
        risksInfoTextView.isHidden = !risksInfoTextView.isHidden
    }
    
    @objc func contract() {
        PresenterImpl().openContractWebVC(urlString: insurance.contractLink)
    }
    
    @objc func clear() {
        let alertStr = "Страховка активна до \(insurance.expiredAt). Вы действительно хотите удалить страховку?"
        showCustomAlertMessage(title: nil, message: alertStr, firstButtonTitle: "OK", secondButtonTitle: "ОТМЕНА",
                               firstBTHandler: { [weak self] in
            guard let self = self else { return }
            InsuranceService().removeInsurance(id: self.insurance.id, complition: {
                self.navigationController?.popViewController(animated: true)
                self.didRemoveInsurance?()
            }, failure: {
                
            })
        }, secondBTHandler: { }, isNeedScrollToBottom: false)

    }
        
}
