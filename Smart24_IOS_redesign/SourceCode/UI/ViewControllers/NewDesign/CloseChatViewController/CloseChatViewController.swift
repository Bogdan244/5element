//
//  CloseChatViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/26/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class MarkButton: UIButton {
    override open var isSelected: Bool {
        didSet {
            self.tintColor = isSelected ? UIColor(rgb: 0xCF2B2B) : UIColor.black
        }
    }
}

class CloseChatViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var markButtons: [MarkButton]!
    
    var handler: (() -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.cornerRadius = 6
        textView.keyboardAppearance = .dark
        textView.delegate = self
        textView.text = "Ваш отзыв"
        textView.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
//        for button in markButtons {
////            let image = button.image(for: .normal)
////            let image2 = image?.maskWithColor(color: UIColor(rgb: 0xCF2B2B))
////            button.setImage(image2, for: .selected)
//        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        scrollView.scrollRectToVisible(alertView.frame, animated: true)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }

    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okButton(_ sender: Any) {
        ChattingService.shared.closeDialog(grade: String(selectedButtonTag), opinion: textView.text, complition: { [weak self] (isClose) in
            guard let self = self else { return }
            self.dismiss(animated: true, completion: self.handler)
        }) { (error) in
            
        }
    }
    
    @IBAction func markButton(_ sender: UIButton) {
        if !sender.isSelected {
            for button in markButtons {
                button.isSelected = false
            }
        }
        sender.isSelected = !sender.isSelected
    }
    
    var selectedButtonTag: Int {
        return markButtons.first{ $0.isSelected }?.tag ?? 0
    }
    
    var isTagSelected: Bool {
        return markButtons.first{ $0.isSelected } != nil
    }
}

extension CloseChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor(red: 0, green: 0, blue: 0, alpha: 0.5) {
            textView.text = nil
            textView.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Ваш отзыв"
            textView.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        }
    }
}
