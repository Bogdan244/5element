//
//  CreateServiceRequestViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/24/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD

class CreateServiceRequestViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var requestToRepairView: UIView!
    @IBOutlet weak var requestForInstallEquipment: UIView!
    @IBOutlet weak var checkStatusRequestView: UIView!    
    
    @IBOutlet weak var requestToRepairLabel: UILabel!
    @IBOutlet weak var requestForInstallEquipmentLabel: UILabel!
    @IBOutlet weak var checkStatusRequestLabel: UILabel!
    @IBOutlet weak var codeEquipmentLabel: UITextField!
    @IBOutlet weak var sendCodeButton: UIButton!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var codeEquipmentPlaceholder: UILabel!
    
    @IBOutlet weak var codeConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var contentViewConstaintHeight: NSLayoutConstraint!
    
    var didEnterCode: ((String) -> ())?
    var didCreateRequestForInstall: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendCodeButton.makeCircle()
        contentViewConstaintHeight.constant -= codeConstraintHeight.constant
        codeConstraintHeight.constant = 0
        codeEquipmentPlaceholder.adjustsFontSizeToFitWidth = true

        codeEquipmentLabel.delegate = self
        
        codeView.layer.cornerRadius = 5
        
        requestToRepairView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestToRepair)))
        requestForInstallEquipment.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestInstallEquipment)))
        checkStatusRequestView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkStatusRequestAction)))
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        contentView.center = CGPoint(x: view.frame.width / 2, y: (view.frame.height - keyboardFrame.height) / 2)
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        contentView.center = view.center
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        requestForInstallEquipmentLabel.adjustWidthFont()
        requestToRepairLabel.font = requestForInstallEquipmentLabel.font
        checkStatusRequestLabel.font = requestForInstallEquipmentLabel.font
    }
    
    @objc func requestToRepair() {
        dismiss(animated: true) { [weak self] in
            PresenterImpl().openRequestRepairEquipmentVC(didCreateRequestForInstall: self?.didCreateRequestForInstall)
        }
    }
    
    @objc func requestInstallEquipment() {
        showCustomAlertMessage(message: "Приобретали ли Вы сервис «Установка техники»?", firstButtonTitle: "ДА", secondButtonTitle: "НЕТ", firstBTHandler: { [weak self] in
            self?.dismiss(animated: true, completion: {
                PresenterImpl().openRequestInstallEquipmentVC()
            })
            
            }, secondBTHandler: { [weak self] in
                self?.showCustomAlertMessage(message: "К сожалению, приобрести сервис “Установка” можно только в магазинах 5 ЭЛЕМЕНТ, просим Вас обратиться в ближайший магазин.", firstBTHandler: nil, secondBTHandler: nil)
        })

    }
    
    @objc func checkStatusRequestAction() {
        if codeConstraintHeight.constant == 50 {
            contentViewConstaintHeight.constant -= 50
            codeConstraintHeight.constant = 0
            view.endEditing(true)
        } else {
            codeConstraintHeight.constant = 50
            contentViewConstaintHeight.constant += 50
        }
    }
    
    @IBAction func crossAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkRequestStatus(_ sender: Any) {
        view.endEditing(true)
        didEnterCode?(codeEquipmentLabel.text!)
        dismiss(animated: true, completion: nil)
    }
}

extension CreateServiceRequestViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        codeEquipmentPlaceholder.isHidden = true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        codeEquipmentPlaceholder.isHidden = !(textField.text?.isEmpty)!
    }
    
}
