//
//  RequestRepairEquipmentViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class RequestRepairEquipmentViewController: BaseMenuViewController {

    @IBOutlet weak var nameTFView: Smart24TextFieldView!
    @IBOutlet weak var phoneTFView: Smart24TextFieldView!
    @IBOutlet weak var serviceNumberTFView: Smart24TextFieldView!
    @IBOutlet weak var equipmetTFView: Smart24TextFieldView!
    @IBOutlet weak var requestReasonTFView: Smart24TextFieldView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var services = [MyServiceEntity]()
    var didCreateRequestForInstall: (() -> ())?
    var technicParkPRSEntity: TechnicParkPRSEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTFView.state = .standart
        phoneTFView.state = .phone(code: "375")
        serviceNumberTFView.state = .list(list: [String]())
        equipmetTFView.state = .list(list: [String]())
        requestReasonTFView.state = .list(list: [String]())
        MBProgressHUD.showAdded(to: view, animated: true)
        RSService.shared.getRequestServicesInfo(complition: { [weak self] (requestServiceInfo) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.equipmetTFView.state = .list(list: requestServiceInfo.technicCategories)
            self.requestReasonTFView.state = .list(list: requestServiceInfo.requestReason)
            self.services = requestServiceInfo.services
            self.serviceNumberTFView.state = .list(list: ["Нет сервиса"] + requestServiceInfo.services.map { $0.name })
        }) {
            
        }
        
        if let user = UserEntity.current {
            nameTFView.textField.text = user.firstname + " " + user.surname
            phoneTFView.textField.text = user.phone.replacingOccurrences(of: "375", with: "")
        }
        
        if let technicValue = technicParkPRSEntity {
            serviceNumberTFView.textField.text = technicValue.name
            equipmetTFView.textField.text = technicValue.category
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let user = UserEntity.current {
            nameTFView.textField.text = user.firstname + " " + user.surname
            phoneTFView.textField.text = user.phone.replacingOccurrences(of: "375", with: "")
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }

    @IBAction func cancelButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        
        guard let name = nameTFView.textField.text, name.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Имя' обьязательное и должно содержать больше двух символов")
            return
        }
        guard let phone = phoneTFView.textField.text, phone.count == 12 else {
            showAlertMessage(title: nil, message: "Поле 'Ваш номер телефона' обьязательное")
            return
        }
        guard let technicCategory = equipmetTFView.textField.text, technicCategory.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Вид техники' обьязательное")
            return
        }
        guard let requestReason = requestReasonTFView.textField.text, requestReason.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Тип поломки' обьязательное")
            return
        }
        guard let requestComment = descriptionTextView.text, requestComment.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Краткое описание' обьязательное")
            return
        }
        
        var serviceID: Int?
        if serviceNumberTFView.textField.text! == "Нет сервиса" {
            serviceID = nil
        } else {
            serviceID = services.first(where: { $0.name == serviceNumberTFView.textField.text! })?.id
        }
        
        RSService.shared.createRequestToRepair(name: name,
                                               phone: phone,
                                               technicCategory: technicCategory,
                                               requestReason: requestReason,
                                               requestComment: requestComment,
                                               serviceID: serviceID,
                                               complition: { [unowned self] (baseEntity) in
                                                if baseEntity.errors.count > 0 {
                                                    self.showAlertMessage(title: nil, message: baseEntity.errors.first!.data)
                                                } else {
                                                    let attribute = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 14) as Any]
                                                    let attMutableString = NSMutableAttributedString(string: "Ваша заявка принята.\nВ течение 30 минут с вами свяжется эксперт по номеру, указанному в заявке.", attributes: attribute)
                                                    let attribute2 = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 14) as Any]
                                                    let attString2 = NSAttributedString(string: "\n\n5 элемент всегда на Вашей стороне!", attributes: attribute2)
                                                    attMutableString.append(attString2)
                                                    self.showAlertMessage(attributedString: attMutableString, complition: {
                                                        self.didCreateRequestForInstall?()
                                                        self.navigationController?.popViewController(animated: true)
                                                    }, cancel: nil)
                                                    
                                                }
        }) { [unowned self] in
            self.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
        }
        
    }
}
