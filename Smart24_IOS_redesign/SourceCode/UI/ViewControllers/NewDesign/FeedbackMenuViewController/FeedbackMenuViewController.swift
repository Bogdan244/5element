//
//  FeedbackMenuViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 7/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class TouchView: UIView {
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var beganTouch: ((TouchView) -> Void)?
    var endedTouch: ((TouchView) -> Void)?
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        beganTouch?(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        endedTouch?(self)
    }
}

class FeedbackMenuViewController: BaseMenuViewController {

    @IBOutlet weak var thanksView: TouchView!
    @IBOutlet weak var remarkView: TouchView!
    @IBOutlet weak var suggestView: TouchView!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Отзывы"
        setupThanksView()
        setupRemarkView()
        setupSuggestView()
    }
    
    func setupThanksView() {
        thanksView.addShadow(height: 2)
        thanksView.beganTouch = {
            $0.backgroundView.backgroundColor = UIColor(rgb: 0xCF2B2B)
            $0.imageView.image = UIImage(named: "ThanksWhiteImage")
        }
        thanksView.endedTouch = {
            $0.backgroundView.backgroundColor = .white
            $0.imageView.image = UIImage(named: "ThanksImage")
            PresenterImpl().openFeedbackVC(state: .Thanks)
        }
    }
    
    func setupRemarkView() {
        remarkView.addShadow(height: 2)
        remarkView.beganTouch = {
            $0.backgroundView.backgroundColor = UIColor(rgb: 0xCF2B2B)
            $0.imageView.image = UIImage(named: "RemarkWhiteImage")
        }
        remarkView.endedTouch = {
            $0.backgroundView.backgroundColor = .white
            $0.imageView.image = UIImage(named: "RemarkImage")
            PresenterImpl().openFeedbackVC(state: .Remark)
        }
    }
    
    func setupSuggestView() {
        suggestView.addShadow(height: 2)
        suggestView.beganTouch = {
            $0.backgroundView.backgroundColor = UIColor(rgb: 0xCF2B2B)
            $0.imageView.image = UIImage(named: "SuggestWhiteImage")
        }
        suggestView.endedTouch = {
            $0.backgroundView.backgroundColor = .white
            $0.imageView.image = UIImage(named: "SuggestImage")
            PresenterImpl().openFeedbackVC(state: .Suggest)
        }
    }
}
