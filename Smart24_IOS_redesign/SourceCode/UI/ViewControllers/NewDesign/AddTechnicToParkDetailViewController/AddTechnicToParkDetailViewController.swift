//
//  AddTechnicToParkDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/15/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class AddTechnicToParkDetailViewController: BaseMenuViewController {
    
    enum State {
        case defaultState
        case otherDevices
    }
    
    var state: State = .defaultState
    
    var technicCategories: TechnicCategories!
    var technikParkInfo: TechnikParkInfoEntity!
    var technicParkEntity: TechnicParkEntity?
    
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var exploitationLabel: UILabel!
    @IBOutlet weak var equipmentInRemainLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var brandTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var modelTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var exploitationTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var equipmentInRemainTextFieldView: Smart24TextFieldView!
    
    var didEdit: (() -> Void)?
    
    enum RemainState: String {
        case no = "НЕТ"
        case yes = "ДА"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = technicCategories.name
        brandTextFieldView.state = .searchList(list: technicCategories.brands)
        modelTextFieldView.state = .standart
        exploitationTextFieldView.state = .list(list: technikParkInfo.exploitationTime)
        equipmentInRemainTextFieldView.state = .list(list: [RemainState.no.rawValue,RemainState.yes.rawValue])
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupTechnicParkEntity()
        setupState()
    }
    
    func setupState() {
        switch state {
        case .defaultState:
            brandLabel.text = "Бренд"
            modelLabel.text = "Модель устройства"
            exploitationLabel.text = "Срок эксплуатации"
            equipmentInRemainLabel.text = "Техника была в ремонте?"
        case .otherDevices:
            brandLabel.text = "Бренд"
            modelLabel.text = "Наименование техники"
            exploitationLabel.text = "Срок эксплуатации"
            equipmentInRemainLabel.text = "Техника была в ремонте?"
        }
    }
    
    func setupTechnicParkEntity() {
        guard let technic = technicParkEntity else { return }
        brandTextFieldView.textField.text = technic.brand
        modelTextFieldView.textField.text = technic.model
        exploitationTextFieldView.textField.text = technic.exploitationTime
        equipmentInRemainTextFieldView.textField.text = technic.repaired > 0 ? RemainState.no.rawValue : RemainState.yes.rawValue
    }

    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @IBAction func cancelBTAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendBTAction(_ sender: Any) {
        
        guard let brandText = brandTextFieldView.textField.text, !brandText.isEmpty else {
            showAlertMessage(title: nil, message: "Выберите бренд")
            return
        }
        guard let modelText = modelTextFieldView.textField.text, !modelText.isEmpty else {
            showAlertMessage(title: nil, message: "Поле модель должно быть заполнено")
             return
        }
        guard let exploitationText = exploitationTextFieldView.textField.text, !exploitationText.isEmpty else {
            showAlertMessage(title: nil, message: "Выберите срок эксплуатации")
            return
        }
        
        guard let repairedText = equipmentInRemainTextFieldView.textField.text, let repairState = RemainState(rawValue: repairedText)?.hashValue else {
            showAlertMessage(title: nil, message: "Установите была ли техника в ремонте")
            return
        }
        
        if let technicValue = technicParkEntity {
            TechnicParkService().editProduct(id: technicValue.id, categoryID: technicValue.technicCategoryID, brand: brandText, model: modelText, exploatitionTime: exploitationText, repaired: repairState, complition: { [weak self] (baseEntity) in
                guard let self = self else { return }
                if let error = baseEntity.errors.first {
                    self.showAlertMessage(title: "Ошибка", message: error.data)
                } else {
                    self.didEdit?()
                    self.navigationController?.popViewController(animated: true)
                }
            }) { [weak self] in
                guard let self = self else { return }
                self.showAlertMessage(title: "Ошибка", message: "Ошибка связи с сервером")
            }
        } else {
            TechnicParkService().addProductToTechnicPark(id: technicCategories.id, brand: brandText, model: modelText, exploitationTime: exploitationText, repaired: repairState, complition: { [weak self] (baseEntity) in
                guard let self = self else { return }
                if let error = baseEntity.errors.first {
                    self.showAlertMessage(title: "Ошибка", message: error.data)
                } else {
                    PresenterImpl().addTechnikSuccessVC(imageUrlString: self.technicCategories.imageBig)
                }
            }) { [weak self] in
                guard let self = self else { return }
                self.showAlertMessage(title: "Ошибка", message: "Ошибка связи с сервером")
            }
        }
        
    }
}
