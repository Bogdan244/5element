//
//  ActivateBonuseAlertViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/3/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class ActivateBonuseAlertViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bonusCardTF: Smart24TextFieldView!
    @IBOutlet weak var phoneTF: Smart24TextFieldView!
    @IBOutlet weak var alignCenterYConstraint: NSLayoutConstraint!
    
    var handler: ((_ cardNumber: String, _ phone: String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bonusCardTF.state = .bonusCard
        phoneTF.state = .phone(code: "375")
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        alignCenterYConstraint.constant = (keyboardFrame.height / -2)
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        alignCenterYConstraint.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func activateAction(_ sender: Any) {
        dismiss(animated: true) {
            self.handler?(self.bonusCardTF.textField.text ?? "", self.phoneTF.textField.text ?? "")
        }
    }
    
    @objc func handleSwipe(_ sender: Any) {
        view.endEditing(true)
    }
}
