//
//  ChattingContainerViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/22/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD
import Reachability

class ChattingContainerViewController: BaseMenuViewController {

    weak var chattingVC: MainChattingViewController!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.selectedItem = tabBar.items![0]
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        chattingVC = segue.destination as? MainChattingViewController
        super.prepare(for: segue, sender: sender)
    }
    
    override var leftButton: UIBarButtonItem? {
        return UIBarButtonItem(image: UIImage(named: "backButton")?.withRenderingMode(.alwaysOriginal),
                               style: .plain,
                               target: self,
                               action: #selector(leftButtonAction))
    }
    
    override var rightBarButtons: [UIBarButtonItem] {
        return [UIBarButtonItem(image: UIImage(named: "phoneIcon")?.withRenderingMode(.alwaysOriginal),
                                style: .plain,
                                target: self,
                                action: #selector(phoneButton))]
    }
    
    @objc func phoneButton() {
        guard let isReach = Reachability.forInternetConnection()?.isReachable(), isReach else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
            return
        }
        
        if let expertCallId = ChattingService.shared.expertCallId, expertCallId != "" {
            GeneralViewModel().openMakeCallVC(callId: expertCallId)
        } else {
            GeneralViewModel().openMakeCallVC(callId: nil)
        }
    }
    
    @objc override func leftButtonAction() {
        if ChattingService.shared.isDialogExist {
            self.showCustomAlertMessage(message: "Закрыть чат с консультантом?", firstButtonTitle: "ДА", secondButtonTitle: "ПОЗЖЕ", firstBTHandler: {
                GeneralViewModel().openCloseChattingVC { [weak self] in
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            }, secondBTHandler: { [weak self] in
                self?.navigationController?.popToRootViewController(animated: true)
            })
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
