//
//  SearchCityViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bogdan on 26.05.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation

protocol SearchCityViewControllerDelegate: class {
    func searchCityViewController(searchViewController: SearchCityViewController, didSelect searchCity: CityEntity)
}

class SearchCityViewController: BaseTableViewController {
    
    weak var delegate: SearchCityViewControllerDelegate?
    var timer: Timer?
    var inputSearchText = ""
    
    var dataSource: [CityEntity] = []
    
    lazy var searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        tableView.separatorInset = .zero
        searchBar.sizeToFit()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
    }
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        }
        cell.textLabel?.text = dataSource[indexPath.row].cityName
        cell.detailTextLabel?.text = dataSource[indexPath.row].regionName + " область"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.searchCityViewController(searchViewController: self, didSelect: dataSource[indexPath.row])
    }
    
    @objc func performSearch() {
        AuthorizationServices().cities(query: inputSearchText, completion: { [weak self] (cities) in
            self?.dataSource = cities
            self?.tableView.reloadData()
        }) {
            print("fail load cities")
        }
    }
}

extension SearchCityViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        inputSearchText = searchText
        timer?.invalidate()
        if searchText.count >= 3 {
            timer = Timer.scheduledTimer(timeInterval: 0.5,
                                         target: self,
                                         selector: #selector(performSearch),
                                         userInfo: nil,
                                         repeats: false)
        }
    }
}
