//
//  AddTechnikSuccessViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/18/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class AddTechnikSuccessViewController: BaseMenuViewController {

    @IBOutlet weak var parkTechnikButton: UIButton!
    @IBOutlet weak var addTechnikButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var imageString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.kf.setImage(with: URL(string: imageString))
        parkTechnikButton.layer.cornerRadius = parkTechnikButton.frame.height / 2
        addTechnikButton.layer.cornerRadius = addTechnikButton.frame.height / 2
    }
    
    @IBAction func parkTechnicAction(_ sender: Any) {
        if !PresenterImpl().popTechnicParkVCIfInStack() {
            guard let navigationVC = navigationController else { return }
            PresenterImpl().pushTechnicParkVCToRoot(in: navigationVC)
        }
    }
    
    @IBAction func addTechnicAction(_ sender: Any) {
        PresenterImpl().popAddTechnicToParkVCIfInStack()
    }
    
}
