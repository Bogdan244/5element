//
//  NewMenuViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/7/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD
import SideMenu

class NewMenuViewController: BaseMenuViewController {

    @IBOutlet weak var serviceRequestView: UIView!
    @IBOutlet weak var technikParkView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var servicesView: UIView!
    @IBOutlet weak var technikParkLabel: UILabel!
    @IBOutlet weak var adsView: AdsView!
    
    let prsService = PRSService()
    let giftsService = GiftsService.shared
    
    var isFirstLoad = true
    
    override var leftButton: UIBarButtonItem? {
        return UIBarButtonItem(image: UIImage(named: "burgerMenu")?.withRenderingMode(.alwaysOriginal),
                               style: .plain,
                               target: self,
                               action: #selector(menuButtonAction))
    }
    
    @objc func menuButtonAction() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override var rightBarButtons: [UIBarButtonItem] {
        return [notificationButton, giftButton, bonusButton]
    }
    
    lazy var notificationButton: BarButtonItemBadge = {
        let bt = BarButtonItemBadge(image: (UIImage(named: "notificationImage")?.withRenderingMode(.alwaysOriginal))!,
                                    badgeText: NotificationsService.shared.notViewed > 0 ? "\(NotificationsService.shared.notViewed)" : nil,
                                    target: self,
                                    action: #selector(notificationAction))
        bt.width = 24
        return bt
    }()
    
    lazy var giftButton: BarButtonItemBadge = {
        let bt = BarButtonItemBadge(image: (UIImage(named: "giftsImage")?.withRenderingMode(.alwaysOriginal))!,
                                    badgeText: GiftsService.shared.notViewed > 0 ? "\(GiftsService.shared.notViewed)" : nil,
                                    target: self,
                                    action: #selector(giftsAction))
        bt.width = 34
        return bt
    }()
    
    lazy var bonusButton: BarButtonItemBadge = {
        let bt = BarButtonItemBadge(image: (UIImage(named: "bonusesImage")?.withRenderingMode(.alwaysOriginal))!,
                           badgeText: nil,
                           target: self,
                           action: #selector(bonussesAction))
        bt.width = 34
        return bt
    }()
    
    @objc func notificationAction() {
        PresenterImpl().openNotificationsVC()
    }
    
    @objc func giftsAction() {
        PresenterImpl().openGiftsListVC()
    }
    
    @objc func bonussesAction() {
        MBProgressHUD.showAdded(to: view, animated: true)
        prsService.getUserBonusCard(complition: { [weak self] (bonusCard) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            if bonusCard.cards.count > 0 {
                GeneralViewModel().openBonussesVC(bonuses: bonusCard)
            } else {
                GeneralViewModel().openActivateBonussesVC()
            }
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc = NewSideMenuViewController.create()
      
        SideMenuManager.default.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.menuWidth = view.frame.size.width * 0.85
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view)
        SideMenuManager.default.menuAnimationCompletionCurve = .easeInOut
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuShadowRadius = 4.0
        
        view.layoutIfNeeded()
        serviceRequestView.makeCircle()
        addShadow(to: serviceRequestView)
        serviceRequestView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceRequestViewAction)))
        
        technikParkView.makeCircle()
        addShadow(to: technikParkView)
        technikParkView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(technicParkAction)))
        
        reviewsView.makeCircle()
        addShadow(to: reviewsView)
        reviewsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reviewsAction)))
        
        servicesView.makeCircle()
        addShadow(to: servicesView)
        servicesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(myServices)))
        updateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFirstLoad {
            updateBadges()
        }
        isFirstLoad = false 
    }
    
    private func updateView() {
        AuthorizationServices().updateUserInfo(didUpdate: { [weak self] in
            guard let self = self else { return }
            self.notificationButton.badgeText = NotificationsService.shared.notViewed > 0 ? "\(NotificationsService.shared.notViewed)" : nil
            self.giftButton.badgeText = GiftsService.shared.notViewed > 0 ? "\(GiftsService.shared.notViewed)" : nil
        }) { [weak self] (ads) in
            guard let self = self else { return }
            self.adsView.setAds(ads: ads)
        }
    }
    
    private func updateBadges() {
        AuthorizationServices().updateUserInfo(didUpdate: { [weak self] in
            guard let self = self else { return }
            self.notificationButton.badgeText = NotificationsService.shared.notViewed > 0 ? "\(NotificationsService.shared.notViewed)" : nil
            self.giftButton.badgeText = GiftsService.shared.notViewed > 0 ? "\(GiftsService.shared.notViewed)" : nil
        }) { _ in
           
        }
    }
    
    @objc private func technicParkAction() {
        MBProgressHUD.showAdded(to: view, animated: true)
        TechnicParkService().getProductListFromTechnicPark(complition: { [weak self] (technickList) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            if technickList.techicParkList.count > 0 {
                PresenterImpl().openTechnicParkVC()
            } else {
                PresenterImpl().openAddTechnicToParkViewController()
            }
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    @objc private func serviceRequestViewAction() {
        MBProgressHUD.showAdded(to: view, animated: true)
        RSService.shared.getRequestServices(complition: { [weak self] (serviceArray) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            if serviceArray.services.count > 0 {
                PresenterImpl().openRequestServicesListVC(servicesList: serviceArray.services)
            } else {
                self.showServiceRequestAlert()
            }
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func showServiceRequestAlert() {
        PresenterImpl().openRequestService(didEnterEquipmentCode: { [weak self] (code) in
            guard let strongSelf = self else { return }
            MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
            RSService.shared.addTechnicToRepair(code: code, complition: { () in
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                self?.serviceRequestViewAction()
            }) { (text) in
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.showAlertMessage(title: nil, message: text)
            }
        }) {
            
        }
    }
    
    @objc private func myServices() {
        PresenterImpl().openNewMyServicesVC()
    }
    
    @objc private func reviewsAction() {
        PresenterImpl().openFeedbackMenuVC()
    }
    
    private func addShadow(to view: UIView) {
        view.layer.shadowColor = UIColor(rgb: 0xCF2B2B).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 5.0
    }
    
}
