//
//  BaseMainViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/6/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class BaseMenuViewController: BaseViewController {
   
    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.delegate = self
        tabBar.barTintColor = UIColor(rgb: 0x2E2D2D)
        tabBar.tintColor = .white
        tabBar.items?.first!.title = "Чат"
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        tabBar.frame = CGRect(x: 0, y: tabBar.frame.origin.y, width: view.frame.size.width, height: tabBar.frame.height + bottomSafeArea)
        
        let numberOfItems = CGFloat(self.tabBar.items!.count)
        let tabBarItemSize = CGSize(width: self.tabBar.frame.width/numberOfItems, height: self.tabBar.frame.height)
        let image = UIImage.imageWithColor(color: UIColor(rgb: 0xCF2B2B), size: tabBarItemSize)

        tabBar.selectionIndicatorImage = image
    }
    
    private func callTo275() {
        showCustomAlertMessage(message: "Позвонить на единый номер: 275?", firstButtonTitle: "ОТМЕНА", secondButtonTitle: "ДА", firstBTHandler: {
            
        }, secondBTHandler: {
            guard let number = URL(string: "tel://" + "275") else { return }
            if #available(iOS 10, *) {
                UIApplication.shared.open(number, options: [:]) { (_) in
                    self.tabBar.selectedItem = nil
                }
            } else {
                UIApplication.shared.openURL(number)
                self.tabBar.selectedItem = nil
            }
        })
    
    }
    
}

extension BaseMenuViewController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if tabBar.items?[0] == item, type(of: self) != ChattingContainerViewController.self {
            tabBar.selectedItem = nil
            if !PresenterImpl().popChattingVCIfInStack() {
                PresenterImpl().openChattingVC()
            }
        } else if tabBar.items?[1] == item, type(of: self) != CallPopupViewController.self {
            tabBar.selectedItem = nil
            PresenterImpl().presentCallPopup(didTapCallRequest: {
                PresenterImpl().openCallMeVC()
            }) {
                PresenterImpl().openChattingVC()
            }
        } else if tabBar.items?[2] == item, type(of: self) != CallMeViewController.self {
            tabBar.selectedItem = nil
            if !PresenterImpl().popCallMeVCIfInStack() {
                PresenterImpl().openCallMeVC()
            }
            
        }
    }
    
}

