//
//  NewForgotPasswordViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/28/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class NewForgotPasswordViewController: BaseViewController {

    var authService = AuthorizationServices()
    
    @IBOutlet weak var phoneTFV: Smart24TextFieldView!
    @IBOutlet weak var sendSMSButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTFV.state = .phone(code: "375")
        sendSMSButton.makeCorner()
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5),
                              NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 12) as Any,
                              NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let attrText = NSAttributedString(string: "Зарегистрируйтесь", attributes: textAttributes)
        registrationButton.setAttributedTitle(attrText, for: .normal)
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func sendSMSAction(_ sender: Any) {
        authService.forgotPassword(phone: phoneTFV.textField.text ?? "", otpCode: nil) { (action) in
            switch action {
            case .failureWithString(let errorStr):
                self.showAlertMessage(title: "", message: errorStr)
            case .failure(let error):
                self.showAlertMessage(title: "", message: error.data)
            case .needSMSConfirm:
                PresenterImpl().openSMSConfirmVC(authService: self.authService, phone: self.phoneTFV.textField.text!)
            default:
                break
            }
        }
    }
    
    @IBAction func registrationButtonAction(_ sender: Any) {
        GeneralViewModel().openNewRegistrationVC()
    }
}


