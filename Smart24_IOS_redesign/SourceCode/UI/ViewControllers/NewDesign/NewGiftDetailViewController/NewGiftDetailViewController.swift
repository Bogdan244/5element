//
//  NewGiftDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/9/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

enum GiftDetailUIType {
    case gift
    case myService(MyServiceType)
}

protocol GiftDetailUIProtocol {
    var title: String { get }
    var description: String { get }
    var codeUI: String { get }
    var appLink: String { get }
    var typeUI: GiftDetailUIType { get }
}

class NewGiftDetailViewController: BaseMenuViewController {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var copyCodeButton: CopyTextButton!
    @IBOutlet weak var installAppButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusContainer: UIView!
    
    var giftID: Int?
    var gift: GiftDetailUIProtocol!
    var didRemoveService: (() -> ())?
    
    lazy var clearButton: UIBarButtonItem = {
        var selector: String
        switch gift.typeUI {
        case .gift:
            selector = "removeGift"
        case .myService(let myService):
            switch myService {
            case .prs, .undefined:
                selector = "removeRequest"
            case .martlet:
                selector = "removeMartlet"
            case .insurance:
                selector = ""
            }
        }
        return UIBarButtonItem(image: UIImage(named: "clearGiftImage"), style: .plain, target: self, action: Selector(selector))
    }()
    
    lazy var contractButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: UIImage(named: "contractImage"), style: .plain, target: self, action: #selector(contract))
        button.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: 1, right: 0)
        return button
    }()
    
    override var rightBarButtons: [UIBarButtonItem] {
        var buttons = [UIBarButtonItem]()
        if giftID != nil {
            buttons.append(clearButton)
        }
        if let service = gift as? MyServiceEntity, service.type == .prs {
            buttons.append(contractButton)
        }
        return buttons
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeLabel.layer.cornerRadius = 5
        codeLabel.layer.masksToBounds = true
        headerView.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        headerView.layer.shadowOpacity = 0.4
        headerView.layer.shadowRadius = 5.0
        headerView.backgroundColor = navigationController?.navigationBar.barTintColor
        statusView.makeCircle()
        statusContainer.isHidden = true
        titleLable.text = gift.title
        codeLabel.text = gift.codeUI
        descriptionTextView.text = gift.description

        codeLabel.adjustsFontSizeToFitWidth = true
        titleLable.adjustsFontSizeToFitWidth = true
        
        installAppButton.isHidden = gift.appLink.isEmpty
        
        switch gift.typeUI {
        case .gift:
            imageView.image = UIImage(named: "giftBackground")
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
        case .myService(_):
            imageView.image = UIImage(named: "serviceBackgrounde")
            imageView.contentMode = .topRight
            imageView.clipsToBounds = true
        }
        
        if let service = gift as? MyServiceEntity {
            title = "Сервис"
            titleLable.textColor = UIColor(red: 236/255, green: 180/255, blue: 72/255, alpha: 1)
            if service.type == .prs {
                statusContainer.isHidden = false
                statusLabel.text = service.statusText
                statusView.backgroundColor = UIColor(hexString: service.statusColor) ?? .clear
                descriptionTextView.text += "\n\(service.productBrand)"
                descriptionTextView.text += "\n\(service.productModel)"
                descriptionTextView.text += "\nПродан: \(service.salePlace)"
                descriptionTextView.text += "\nДата продажи: \(service.shoppingTime)"
                descriptionTextView.text += "\nСрок действия до: \(service.expired)"
            }
        }
    }
    
    @objc func contract() {
        let service = gift as! MyServiceEntity
        PresenterImpl().openContractWebVC(urlString: service.contractLink)
    }
    
    @objc func removeMartlet() {
        showCustomAlertMessage(title: nil,
                               message: "Вы действительно хотите удалить данный сервис?",
                               firstButtonTitle: "ДА, ХОЧУ!",
                               secondButtonTitle: "ОТМЕНА",
                               firstBTHandler: { [unowned self] in
                                RSService.shared.removeMartletService(id: self.giftID!, complition: { [weak self] in
                                    self?.didRemoveService?()
                                    self?.navigationController?.popViewController(animated: true)
                                }, failure: {
                                    
                                })
            }, secondBTHandler: {
            
        })
    }
    
    @objc func removeRequest() {
        let service = gift as! MyServiceEntity
        let alertStr = "Сервис активный до \(service.expired). Вы действительно хотите удалить сервис и историю обращений по нему?"
        showCustomAlertMessage(message: alertStr, firstButtonTitle: "ОК", secondButtonTitle: "ОТМЕНА", firstBTHandler: { [unowned self] in
            RSService.shared.removeService(id: self.giftID!, complition: { [weak self] in
                self?.didRemoveService?()
                self?.navigationController?.popViewController(animated: true)
            }, failure: {
                
            })
            }, secondBTHandler: {
            
        })
    }
    
    @objc func removeGift() {
        showCustomAlertMessage(message: "Вы действительно хотите удалить подарок без возможности его восстановления?",
                               firstButtonTitle: "ДА, ХОЧУ!",
                               secondButtonTitle: "ОТМЕНА",
                               firstBTHandler: { [unowned self] in
                                PRSService().removeGift(id: self.giftID!)
                                self.navigationController?.popViewController(animated: true)
            }, secondBTHandler: {
            
        })
    }
    
    @IBAction func copyTextAction(_ sender: CopyTextButton) {
        sender.copyText(text: codeLabel.text)
    }
    
    @IBAction func installAppAction(_ sender: Any) {
        if let url = URL(string: gift.appLink),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
