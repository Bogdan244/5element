//
//  CustomAlertViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/27/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertText: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var okButtonTitle: String?
    var cancelButtonTitle: String?
    
    private var text: String?
    private var attributedText: NSAttributedString?
    private var isNeedScrollToBottom = false
    private var isLinksEnabled = false
    
    typealias Handler = (() -> ())?
    
    var cancelHandler: Handler = nil
    var okHandler: Handler = nil 
    
    static func alertVC(alertText: String, okHandler: Handler, cancelHandler: Handler, isNeedScrollToBottom: Bool, isLinksEnabled: Bool = false) -> CustomAlertViewController {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        vc.text = alertText
        vc.cancelHandler = cancelHandler
        vc.okHandler = okHandler
        vc.isNeedScrollToBottom = isNeedScrollToBottom
        vc.isLinksEnabled = isLinksEnabled
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        
        return vc
    }
    
    static func alertVC(alertText: NSAttributedString?, okHandler: Handler, cancelHandler: Handler, isNeedScrollToBottom: Bool, isLinksEnabled: Bool = false) -> CustomAlertViewController {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        vc.attributedText = alertText
        vc.cancelHandler = cancelHandler
        vc.okHandler = okHandler
        vc.isNeedScrollToBottom = isNeedScrollToBottom
        vc.isLinksEnabled = isLinksEnabled
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let str = text {
            alertText.text = str
        } else if let attStr = attributedText {
            alertText.attributedText = attStr
        }
        view.layer.cornerRadius = 4
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 5.0
        if okButtonTitle != nil {
            okButton.setTitle(okButtonTitle, for: .normal)
            okButton.setTitle(okButtonTitle, for: .disabled)
        }
        if cancelButtonTitle != nil {
            cancelButton.setTitle(cancelButtonTitle, for: .normal)
        }
        if cancelHandler == nil {
            cancelButton.isHidden = true
        }
        scrollView.delegate = self
        okButton.isEnabled = !isNeedScrollToBottom
        prepareLinkGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        enabledOkButtonIfNeeded()
    }
    
    lazy var gesture = UITapGestureRecognizer(target: self, action: #selector(tapOnLabel(tap:)))
    
    func prepareLinkGesture() {
        alertText.isUserInteractionEnabled = true
        alertText?.gestureRecognizers?.removeAll()
        if isLinksEnabled == true {
            alertText.addGestureRecognizer(gesture)
            guard let text = alertText.attributedText?.string else { return }
            guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return }
            let matches = detector.matches(in: text,
                                           options: [],
                                           range: NSRange(location: 0, length: text.count))
            let attributedStr = NSMutableAttributedString(attributedString: alertText.attributedText ?? NSAttributedString())
            matches.forEach {
                attributedStr.addAttributes([NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue,
                                             NSAttributedString.Key.foregroundColor : UIColor.blue], range: $0.range)
            }
            alertText.attributedText = attributedStr
        }
    }
    
    @objc func tapOnLabel(tap: UITapGestureRecognizer) {
        guard let text = alertText.attributedText?.string else { return }
        if isLinksEnabled == true {
            guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return }
            let matches = detector.matches(in: text,
                                           options: [],
                                           range: NSRange(location: 0, length: text.count))
            
            
            matches.forEach{
                if tap.didTapAttributedTextInLabel(label: alertText, inRange: $0.range) {
                    guard let range = Range($0.range, in: text) else { return }
                    let link = text[range]
                    guard let url = URL(string: String(link)) else { return }
                    if UIApplication.shared.canOpenURL(url) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }
        }
    }
    
    func enabledOkButtonIfNeeded() {
        if isNeedScrollToBottom {
            okButton.isEnabled = scrollView.isScrollToBottom
            isNeedScrollToBottom = !scrollView.isScrollToBottom
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: cancelHandler)
    }
    
    @IBAction func okButton(_ sender: Any) {
        dismiss(animated: true, completion: okHandler)
    }
}



extension CustomAlertViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        enabledOkButtonIfNeeded()
    }
    
}

extension UIScrollView {
    
    var isScrollToBottom: Bool {
        return contentOffset.y >= (contentSize.height - frame.size.height)
    }
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        
        // Configure NSTextContainer
        let textContainer = NSTextContainer(size: label.bounds.size)
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines

               // Configure NSLayoutManager and add the text container
        let layoutManager = NSLayoutManager()
        layoutManager.addTextContainer(textContainer)

               // Configure NSTextStorage and apply the layout manager
        let attributedText = NSMutableAttributedString(attributedString: label.attributedText!)
        let textStorage = NSTextStorage(attributedString: attributedText)
        textStorage.addAttribute(NSAttributedString.Key.font, value: label.font, range: NSMakeRange(0, attributedText.length))
        textStorage.addLayoutManager(layoutManager)

               // get the tapped character location
        let locationOfTouchInLabel = location(in: label)

               // account for text alignment and insets
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        var alignmentOffset: CGFloat!
        switch label.textAlignment {
            case .left, .natural, .justified:
                alignmentOffset = 0.0
            case .center:
                alignmentOffset = 0.5
            case .right:
                alignmentOffset = 1.0
            }
        let xOffset = ((label.bounds.size.width - textBoundingBox.size.width) * alignmentOffset) - textBoundingBox.origin.x
        let yOffset = ((label.bounds.size.height - textBoundingBox.size.height) * alignmentOffset) - textBoundingBox.origin.y
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - xOffset, y: locationOfTouchInLabel.y - yOffset)

        // figure out which character was tapped
        let characterTapped = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(characterTapped, targetRange)
    }
    
}
