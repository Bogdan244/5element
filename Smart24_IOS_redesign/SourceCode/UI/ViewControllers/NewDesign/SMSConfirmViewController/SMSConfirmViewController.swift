//
//  SMSConfirmViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/30/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class SMSConfirmViewController: BaseViewController, AuthorizationUIActionsProtocol {
    
    var authService: AuthorizationServices = AuthorizationServices()
    var phone: String?
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var codeTFV: Smart24TextFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.makeCorner()
        codeTFV.state = .otpCode
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func smsConfirmAction(_ sender: Any) {
        authService.confirmOtpCode(otpCode: codeTFV.textField.text ?? "") { (action) in
            switch action {
            case .failureWithString(let errorSTR):
                self.showAlertMessage(title: "", message: errorSTR)
            case .failure(let error):
                self.showAlertMessage(title: "", message: error.data)
            case .succes:
                break
            case .needSMSConfirm:
                break
            case .needPhoneNumber:
                break
            case .wrongOTP:
                self.showAlertMessage(title: "Ошибка", message: "Неправильный код")
            case .succesWithMassega(let message):
                self.showCustomAlertMessage(title: "", message: message, firstButtonTitle: "ОК", secondButtonTitle: nil, firstBTHandler: { [unowned self] in
                    if let phoneValue = self.phone {
                        self.login(phone: phoneValue, password: self.codeTFV.textField.text!, needBPM: false, user: nil)
                    }
                }, secondBTHandler: nil)
                
            case .showDetail:
                break
            case .login:
                break
            case .loginWithBPM(_):
                break
            case .loginWithSocialJoinAccount(_):
                break
            case .loginWithSocialBPM(_):
                break
            case .updateUserDataJoinAccount(_):
                break
            case .succesEditUser:
                break
            case .loginAfterRegistration(_):
                break
            }
        }
    }
    

}
