//
//  BarcodeVC.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/31/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import BarcodeScanner

class BarcodeVC: BarcodeScanner.BarcodeScannerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Штрих код"
        addLeftButton()
        messageViewController.textLabel.text = "Поднесите камеру к штрих коду. Сканирование начнется автоматически."
    }
    
    var leftButton: UIBarButtonItem? {
        return UIBarButtonItem(image: UIImage(named: "backButton")?.withRenderingMode(.alwaysOriginal),
                               style: .plain,
                               target: self,
                               action: #selector(leftButtonAction))
    }
    
    func addLeftButton() {
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func leftButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
}

