//
//  NewLoginViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/28/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD
import GoogleSignIn
import ok_ios_sdk
import VK_ios_sdk
import FacebookCore
import FacebookLogin
import AuthenticationServices

class NewLoginViewController: BaseViewController, AuthorizationUIActionsProtocol {
    
    var authService = AuthorizationServices()
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var phoneTFV: Smart24TextFieldView!
    @IBOutlet weak var passwordTFV: Smart24TextFieldView!
    @IBOutlet weak var appleButton: UIButton!
    
    var phone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.makeCorner()
        phoneTFV.state = .phone(code: "375")
        phoneTFV.textField.text = phone?.deletingPrefix("+").deletingPrefix("375").group(by: [2,3,2,2])
        passwordTFV.state = .password
        hideKeyboardWhenTappedAround()
        if #available(iOS 13, *) {
            appleButton.isHidden = false
        } else {
            appleButton.isHidden = true
        }
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        GeneralViewModel().openNewForgotPasswordVC()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        login(phone: phoneTFV.textField.text ?? "", password: passwordTFV.textField.text ?? "", needBPM: false, user: nil)
    }
    
    @IBAction func googleAuth(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = Constants.gClientID
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookAuth(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        MBProgressHUD.showAdded(to: view, animated: true)

        loginManager.logIn(permissions: ["publish_actions"], from: self) { [weak self] (loginResult, error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            guard let token = loginResult?.token else { return }
            self.loginSocial(socialName: .facebook, token: token.tokenString, serverAuthCode: nil)
        }
    }
    
    var isVKLogging = false
    @IBAction func vkAuth(_ sender: Any) {
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        VKSdk.forceLogout()
        MBProgressHUD.showAdded(to: view, animated: true)
        isVKLogging = false 
        VKSdk.authorize(["wall"])
    }
    
    @IBAction func okAuth(_ sender: Any) {
        OKSDK.clearAuth()
        MBProgressHUD.showAdded(to: view, animated: true)
        OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS","LONG_ACCESS_TOKEN"], success: { [weak self] (data) in
            guard let self = self else { return }
            guard let dataArray = data as? Array<String> else { return }
            print("\(dataArray)")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.loginSocial(socialName: .odnoklassniki, token: dataArray.first)
        }) { (error) in
            self.showAlertMessage(title: "Ошибка", message: "\(String(describing: error))")
        }
    }
    
    @available(iOS 13.0, *)
    @IBAction func appleButton(_ sender: Any) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}

extension NewLoginViewController: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            guard let token = appleIDCredential.identityToken else { return }
            guard let tokenString = String(data: token, encoding: .utf8) else { return }
            self.loginSocial(socialName: .apple, token: tokenString)
        default:
            break
        }
    }
}

extension NewLoginViewController: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    

}

extension NewLoginViewController: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        VKCaptchaViewController.captchaControllerWithError(captchaError).present(in: self)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        MBProgressHUD.hide(for: view, animated: true)
        if result.token != nil, !isVKLogging {
            isVKLogging = true
            loginSocial(socialName: .vkontakte, token: result.token.accessToken)
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

extension NewLoginViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user == nil {
            return
        }
        loginSocial(socialName: .google, token: user.authentication.accessToken)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
