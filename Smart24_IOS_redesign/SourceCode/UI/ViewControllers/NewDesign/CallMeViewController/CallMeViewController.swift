//
//  CallMeViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/10/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class CallMeViewController: BaseMenuViewController {

    @IBOutlet weak var nameView: Smart24TextFieldView!
    @IBOutlet weak var phoneView: Smart24TextFieldView!
    @IBOutlet weak var timeView: Smart24TimeView!
    @IBOutlet weak var technicsCategoryView: Smart24TextFieldView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameView.state = .standart
        phoneView.state = .phone(code: "375")
        technicsCategoryView.state = .list(list: [])
        timeView.setupDates(start: Calendar.current.date(byAdding: Calendar.Component.minute, value: 5, to: Date())!,
                            end: Calendar.current.date(byAdding: Calendar.Component.minute, value: 35, to: Date())!)
        scrollView.isUserInteractionEnabled = true
        scrollView.isExclusiveTouch = true
        scrollView.canCancelContentTouches = true
        scrollView.delaysContentTouches = true
        
        MBProgressHUD.showAdded(to: view, animated: true)
        PRSService().getPRSInfo(complition: { [weak self] (prsEntity) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.technicsCategoryView.state = .list(list: prsEntity.technicCategories)
        }) {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        tabBar.selectedItem = tabBar.items![2]
        setUser()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func leftButtonAction() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func setUser() {
        if let user = UserEntity.current {
            nameView.textField.text = user.firstname + " " + user.surname
            phoneView.textField.text = user.phone.replacingOccurrences(of: "375", with: "").group(by: [2,3,2,2])
        }
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        guard let responderView = UIResponder.currentFirstResponder as? UIView else { return }
        let responderFrame = responderView.convert(responderView.frame, to: view)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = UIEdgeInsets.zero
            scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        } else if keyboardFrame.origin.y < responderFrame.origin.y {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            if scrollView.contentSize.height - scrollView.bounds.size.height + scrollView.contentInset.bottom > responderFrame.maxY - keyboardFrame.origin.y {
                scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y + (responderFrame.maxY - keyboardFrame.origin.y)), animated: true)
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            } else {
                scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y + (scrollView.contentSize.height - scrollView.bounds.size.height + scrollView.contentInset.bottom - 20)), animated: true)
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
            }
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        }
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        networkProvider.request(ApiManager.orderCallBack(uid: (UserEntity.current?.uid)!,
                                                         serviceID: "",
                                                         username: nameView.textField.text!,
                                                         phone: phoneView.textField.text!,
                                                         comment: textView.text,
                                                         timeFrom: timeView.startTimeTextField.text!,
                                                         timeTo: timeView.endTimetextField.text!)) { (result) in
                                                            switch result {
                                                            case .success(let response):
                                                                do {
                                                                    if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                                                                        if json["success"] as? Bool == true {
                                                                            let alert = CustomAlertViewController.alertVC(alertText: "Ваша заявка принята!\nЭксперт свяжется с Вами в указанное время.",
                                                                                                                          okHandler: {
                                                                                self.navigationController?.popViewController(animated: true)
                                                                            },
                                                                                                                          cancelHandler: nil, isNeedScrollToBottom: false)
                                                                            self.present(alert, animated: true, completion: nil)
                                                                        } else if let errors = json["errors"] as? [[String: AnyObject]], errors.count > 0 {
                                                                            
                                                                            self.showAlertMessage(title: "", message: errors.first!["data"] as! String)
                                                                        }
                                                                    }
                                                                } catch {
                                                                    self.showAlertMessage(title: "", message: "При заказе звонка произошла ошибка")
                                                                }
                                                                
                                                            case .failure(let error):
                                                                self.showSmart24Error(error: error)
                                                            }
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
