//
//  CallPopupViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 7/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class CallPopupViewController: UIViewController {

    @IBOutlet weak var firstOperatorView: UIView!
    @IBOutlet weak var secondOperatorView: UIView!
    @IBOutlet weak var thirdOperatorView: UIView!
    
    @IBOutlet weak var firstOperatorPhoneLabel: UILabel!
    @IBOutlet weak var secondOperatorPhoneView: UILabel!
    @IBOutlet weak var thirdOperatorPhoneLabel: UILabel!
    
    var writeToChat: (() -> Void)?
    var requestCall: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstOperatorPhoneLabel.underline()
        secondOperatorPhoneView.underline()
        thirdOperatorPhoneLabel.underline()
        
        firstOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callFirstOperator)))
        secondOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callSecondOperator)))
        thirdOperatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callThirdOperator)))
    }
    
    @objc func callFirstOperator() {
        makeColl(firstOperatorPhoneLabel.text ?? "")
    }
    
    @objc func callSecondOperator() {
        makeColl(secondOperatorPhoneView.text ?? "")
    }
    @objc func callThirdOperator() {
        makeColl(thirdOperatorPhoneLabel.text ?? "")
    }
    
    @IBAction func writeToChat(_ sender: Any) {
        dismiss(animated: true, completion: writeToChat)
    }
    
    @IBAction func requestCall(_ sender: Any) {
        dismiss(animated: true, completion: requestCall)
    }
    
    func makeColl(_ phone: String) {
        dismiss(animated: true, completion: nil)
        let phoneStr = phone.replacingOccurrences(of: " ", with: "")
        guard let url = URL(string: "tel://\(phoneStr)"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func cancelTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

