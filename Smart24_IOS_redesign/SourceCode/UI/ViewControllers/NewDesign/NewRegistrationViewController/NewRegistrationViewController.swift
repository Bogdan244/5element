//
//  NewRegistrationViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/28/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class NewRegistrationViewController: UITableViewController, AuthorizationUIActionsProtocol {
    
    enum State {
        case registration
        case login
        case socialLogin(socialName: SocialName, accessToken: String?, serverAuthCode: String?, phone: String?)
        
        var title: String {
            switch self {
            case .registration:
                return "Регистрация"
            case .login:
                return "Авторизация"
            case .socialLogin(_, _, _, _):
                return "Авторизация"
            }
        }
    }
    
    var authService = AuthorizationServices()
    var state: State = .registration
    
    @IBOutlet weak var registrationButton: UIButton!
    
    @IBOutlet weak var nameTFV: Smart24TextFieldView!
    @IBOutlet weak var secondNameTFV: Smart24TextFieldView!
    @IBOutlet weak var phoneNumberTFV: Smart24TextFieldView!
    @IBOutlet weak var genderTF: Smart24TextFieldView!
    @IBOutlet weak var cityTF: Smart24TextFieldView!
    @IBOutlet weak var birthdayTF: Smart24TextFieldView!
    
    var city: CityEntity?
    var date: Date?
    var user: ShortUserEntity?
    var needBPM: Bool = false
    var phone: String?
    var password: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.keyboardDismissMode = .interactive
        tableView.tableFooterView = UIView()
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.179, green: 0.179, blue: 0.179, alpha: 1)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton")?.withRenderingMode(.alwaysOriginal),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(backAction))
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 20) as Any]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        configurate()
        setup()
    }
    
    private func configurate() {
        title = state.title
        registrationButton.makeCorner()
        nameTFV.state = .standart
        secondNameTFV.state = .standart
        phoneNumberTFV.state = .phone(code: "375")
        genderTF.state = .list(list: Gender.allCases.map { $0.rawValue })
        cityTF.state = .searchCities
        birthdayTF.state = .birthday(date: nil)
        cityTF.textField.didPickCity = { [weak self] city in
            self?.city = city
        }
        birthdayTF.textField.didChangeDate = { [weak self] date in
            self?.date = date
        }
    }
    
    private func setup() {
        if needBPM {
            nameTFV.textField.text = user?.firstname
            secondNameTFV.textField.text = user?.surname
            phoneNumberTFV.textField.text = user?.phone?.deletingPrefix("+").deletingPrefix("375").group(by: [2,3,2,2])
            cityTF.textField.text = user?.city?.cityName
            city = user?.city
            birthdayTF.state = .birthday(date: user?.birthday)
            date = user?.birthday
            genderTF.textField.text = user?.gender?.rawValue
        } else {
            phoneNumberTFV.textField.text = phone?.deletingPrefix("+").deletingPrefix("375").group(by: [2,3,2,2])
        }
        phoneNumberTFV.isUserInteractionEnabled = false
        phoneNumberTFV.rightButton.isHidden = true
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func registrationAction(_ sender: Any) {
        guard nameTFV.textField.text?.isEmpty == false else {
            showAlertMessage(title: nil, message: "Поле имя обязательно")
            return
        }
        guard secondNameTFV.textField.text?.isEmpty == false else {
            showAlertMessage(title: nil, message: "Поле фамилия обязательно")
            return
        }
        
        switch state {
        case .registration:
            registration(name: nameTFV.textField.text,
                         surname: secondNameTFV.textField.text,
                         phone: phoneNumberTFV.textField.text ?? "",
                         password: nil,
                         needBPM: needBPM,
                         gender: Gender(rawValue: genderTF.textField.text ?? ""),
                         cityID: city?.cityExternalID,
                         birthday: date.serverTimeInterval)
        case .login:
            login(phone: phoneNumberTFV.textField.text ?? "",
                  password: password,
                  needBPM: needBPM,
                  gender: Gender(rawValue: genderTF.textField.text ?? ""),
                  cityID: city?.cityExternalID,
                  birthday: date.serverTimeInterval,
                  name: nameTFV.textField.text,
                  surname: secondNameTFV.textField.text)
        case let .socialLogin(socialName: socialName, accessToken: accessToken, serverAuthCode: serverAuthCode, phone: _):
            loginSocial(socialName: socialName,
                        token: accessToken,
                        serverAuthCode: serverAuthCode,
                        joinAccount: nil,
                        needBPM: needBPM,
                        gender: Gender(rawValue: genderTF.textField.text ?? ""),
                        cityID: city?.cityExternalID,
                        birthday: date.serverTimeInterval,
                        name: nameTFV.textField.text,
                        surname: secondNameTFV.textField.text,
                        phone: phoneNumberTFV.textField.text ?? "")
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        backAction()
    }
    
}
