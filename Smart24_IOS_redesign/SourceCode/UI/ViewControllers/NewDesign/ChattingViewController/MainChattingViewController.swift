//
//  MainChattingViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 4/15/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation
import MessageInputBar

class MainChattingViewController: ChattingViewController {
    
    var bottomLayoutAnchor : NSLayoutConstraint?
    var messageCollectionBottomAnchor: NSLayoutConstraint?
    
    override var inputAccessoryView: UIView? {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manualyAddMessageInputBar()
        
    }
    
    @objc private func keyboardHide(notification: Notification) {
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        bottomLayoutAnchor?.constant = -1
        view.layoutIfNeeded()
    }
    
    @objc private func keyboardShow(notification: Notification) {
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        bottomLayoutAnchor?.constant = -(view.frame.height - keyboardFrame.minY)
        view.layoutIfNeeded()
    }
    
    @objc private func keyboardChangeFrame(notification: Notification) {
        let userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        if keyboardFrame.height > 88 {
            bottomLayoutAnchor?.constant = -(view.frame.height - keyboardFrame.minY)
        } else {
            bottomLayoutAnchor?.constant = -1
        }
        view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let previusMessageCollectionBottomAnchor = messageCollectionBottomAnchor?.constant
        messageCollectionBottomAnchor?.constant = -messageInputBar.frame.height + (bottomLayoutAnchor?.constant ?? 0)
        updateViewConstraints()
        if previusMessageCollectionBottomAnchor != -messageInputBar.frame.height + (bottomLayoutAnchor?.constant ?? 0) {
            self.messagesCollectionView.scrollToBottom(animated: true)
        }
    }
    
    func manualyAddMessageInputBar() {
        messageInputBar.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(messageInputBar)
        bottomLayoutAnchor = messageInputBar.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1)
        bottomLayoutAnchor?.isActive = true
        messageInputBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        messageInputBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        //messageInputBar.heightAnchor.constraint(equalToConstant: 88).isActive = true
        messageInputBar.heightAnchor.constraint(greaterThanOrEqualToConstant: 88).isActive = true
        view.clipsToBounds = true
        messagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 88, right: 0)
        messagesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: messagesCollectionView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: messagesCollectionView, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: messagesCollectionView, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1, constant: 0)
        messageCollectionBottomAnchor = messagesCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -messageInputBar.frame.height)
        messageCollectionBottomAnchor?.isActive = true
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint])
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint])
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        messageInputBar.inputTextView.endEditing(true)
    }
}

extension MessageInputBarDelegate where Self: MainChattingViewController {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        for component in inputBar.inputTextView.components {
            if let str = component as? String {
                let message = ChattingMessage(text: str, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                ChattingService.shared.sendMessage(message: str, complition: { (dict) in
                    self.messageList.append(message)
                    self.messagesCollectionView.reloadData()
                    self.messagesCollectionView.scrollToBottom(animated: false)
                }) { (error) in
                    self.showAlertMessage(title: "Ошибка", message: error.localizedDescription)
                }
            } else if let img = component as? UIImage {
                // let message = ChattingMessage(image: img, sender: currentSender(), messageId: UUID().uuidString, date: Date())
            }
        }
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom(animated: true)
    }
    
    
    func messageInputBar(_ inputBar: MessageInputBar, didChangeIntrinsicContentTo size: CGSize) {
        inputBar.frame = CGRect(x: inputBar.frame.minX, y: inputBar.frame.minY, width: size.width, height: size.height > 88 ? size.height : 88)
    }
}
