//
//  ChattingViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/14/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MessageKit
import MessageInputBar
import MBProgressHUD
import Reachability
import Kingfisher

open class MyCustomCell: UICollectionViewCell {
    open func configure(with message: MessageType, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {
        //self.contentView.backgroundColor = UIColor.red
    }
}

open class MyCustomMessagesFlowLayout: MessagesCollectionViewFlowLayout {
    
    lazy open var customMessageSizeCalculator = CustomMessageSizeCalculator(layout: self)

    override open func cellSizeCalculatorForItem(at indexPath: IndexPath) -> CellSizeCalculator {
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        if case .custom = message.kind {
            return customMessageSizeCalculator
        }
        return super.cellSizeCalculatorForItem(at: indexPath);
    }
}

open class CustomMessageSizeCalculator: TextMessageSizeCalculator {
    
    open override func messageContainerSize(for message: MessageType) -> CGSize {
        let maxWidth = messageContainerMaxWidth(for: message)
        
        var messageContainerSize: CGSize
        let attributedText: NSAttributedString
        
        switch message.kind {
        case .attributedText(let text):
            attributedText = text
        case .text(let text), .emoji(let text):
            attributedText = NSAttributedString(string: text, attributes: ChattingMessage.attributedForMessage)
        case .custom(let data):
            if let notice = data as? NoticeMessage {
                attributedText = NSAttributedString(string: notice.text, attributes: ChattingMessage.attributedForSystemMessage)
            } else if let videoLink = data as? VideoLinkMessage {
                attributedText = NSAttributedString(string: videoLink.text, attributes: ChattingMessage.attributedForSystemMessage)
            } else {
                fatalError("fatal Error with custom cell")
            }
        default:
            fatalError("messageContainerSize received unhandled MessageDataType: \(message.kind)")
        }
        let constraintBox = CGSize(width: maxWidth, height: .greatestFiniteMagnitude)
        let rect = attributedText.boundingRect(with: constraintBox, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).integral
        messageContainerSize = rect.size
        
        return messageContainerSize
    }
    
}

class ChattingViewController: MessagesViewController, MessageInputBarDelegate {
    
    let refreshControl = UIRefreshControl()
    
    var messageList = [ChattingMessage]()
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
    
    override func viewDidLoad() {
        messagesCollectionView = MessagesCollectionView(frame: .zero, collectionViewLayout: MyCustomMessagesFlowLayout())
        messagesCollectionView.register(UINib(nibName: "ChattingSystemMessageCell", bundle: nil), forCellWithReuseIdentifier: "ChattingSystemMessageCell")
        messagesCollectionView.keyboardDismissMode = .none
        super.viewDidLoad()
        ChattingService.shared.delegate = self
        configurateNavigationText()
        
        configureMessageInputBar()
        configurateMassegeCollectionView()
        
        loadMessages()
        
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        messagesCollectionView.addSubview(refreshControl)
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messagesLayoutDelegate = self
    }
    
    @objc func handleRefresh() {
        updateMessages()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        revealViewController()?.panGestureRecognizer()?.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        revealViewController()?.panGestureRecognizer()?.isEnabled = true
    }
    
    func loadMessages() {
        ChattingService.shared.getMessages(page: nil, complition: { [weak self] (messages) in
            guard let self = self else { return }
            self.messageList = messages
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom(animated: true)
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.showAlertMessage(title: "Ошибка", message: error.localizedDescription)
        }
    }
    
    func updateMessages() {
        ChattingService.shared.nextMessages(complition: { [weak self] (messages) in
            guard let self = self else { return }
            self.messageList = messages + self.messageList
            self.messagesCollectionView.reloadData()
        }) { [weak self] (error) in
            self?.showAlertMessage(title: "Ошибка", message: error.localizedDescription)
        }
    }
    
    func configurateMassegeCollectionView() {
        scrollsToBottomOnKeyboardBeginsEditing = true 
        //maintainPositionOnKeyboardFrameChanged = true
        messagesCollectionView.backgroundColor = .clear
        view.backgroundColor = .clear
        messageInputBar.becomeFirstResponder()
        let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout
        layout?.setMessageOutgoingAvatarSize(CGSize(width: 36, height: 36))
        layout?.setMessageIncomingAvatarSize(CGSize(width: 36, height: 36))
    }
    
    func configurateNavigationText() {
        navigationController?.navigationBar.barTintColor = UIColor(rgb: 0x2E2D2D)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 20) as Any]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.isTranslucent = true
        messageInputBar.separatorLine.isHidden = true
       
        messageInputBar.inputTextView.placeholderTextColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 2, bottom: 8, right: 36)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 7, bottom: 8, right: 36)
        messageInputBar.inputTextView.layer.masksToBounds = true
        messageInputBar.inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.inputTextView.keyboardAppearance = .dark
        messageInputBar.inputTextView.placeholderLabel.text = "Напишите сообщение..."
        
        configureInputBarItems()
    }
    
    private func configureInputBarItems() {
        messageInputBar.setRightStackViewWidthConstant(to: 30, animated: false)
        
        messageInputBar.sendButton.backgroundColor = UIColor(rgb: 0xCF2B2B)
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        messageInputBar.sendButton.setSize(CGSize(width: 30, height: 30), animated: false)
        messageInputBar.sendButton.layer.cornerRadius = 15
        messageInputBar.sendButton.image = UIImage(named: "newSendIcon")?.withRenderingMode(.alwaysOriginal)
        messageInputBar.sendButton.title = ""
        messageInputBar.textViewPadding.right = -38
      
        let bottomItems = [cameraButton(), galleryButton(), .flexibleSpace]
        messageInputBar.textViewPadding.bottom = 8
        messageInputBar.setStackViewItems(bottomItems, forStack: .bottom, animated: false)
    }
    
    private func cameraButton() -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: "photoIcon")?.withRenderingMode(.alwaysOriginal)
                $0.setSize(CGSize(width: 25, height: 25), animated: false)
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onTouchUpInside { [unowned self] _ in
                let imagePickerVC = UIImagePickerController()
                imagePickerVC.delegate = self
                imagePickerVC.allowsEditing = true
                imagePickerVC.sourceType = .camera
                self.present(imagePickerVC, animated: true, completion: nil)
        }
    }
    
    private func galleryButton() -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: "galleryIcon")?.withRenderingMode(.alwaysOriginal)
                $0.setSize(CGSize(width: 25, height: 25), animated: false)
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onSelected {
                $0.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
            }.onDeselected {
                $0.tintColor = UIColor(white: 0.8, alpha: 1)
            }.onTouchUpInside { [unowned self] _ in
                let imagePickerVC = UIImagePickerController()
                imagePickerVC.delegate = self
                imagePickerVC.allowsEditing = true
                imagePickerVC.sourceType = .photoLibrary
                self.present(imagePickerVC, animated: true, completion: nil)
        }
    }
    
    override open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let messagesDataSource = messagesCollectionView.messagesDataSource else {
            fatalError("Ouch. nil data source for messages")
        }
        
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        if case .custom = message.kind {
            let cell = messagesCollectionView.dequeueReusableCell(ChattingSystemMessageCell.self, for: indexPath)
            cell.configure(with: message, at: indexPath, and: messagesCollectionView)
            cell.delegate = self
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }

}

extension ChattingViewController: ChattingSystemMessageCellDelegate {
    
    func didTapVideoLink(cell: ChattingSystemMessageCell) {
        if let index = messagesCollectionView.indexPath(for: cell)?.section {
            switch messageList[index].kind {
            case .custom(let data):
                if let message = data as? VideoLinkMessage {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RTCVideoChatViewController") as! RTCVideoChatViewController
                    viewController.roomName = message.link
                    present(viewController, animated: true, completion: nil)
                }
            default:
                break
            }
        }
    }
    
}

extension ChattingViewController: MessagesDataSource {
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        return MessageLabel.defaultAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation]
    }
    
    func currentSender() -> Sender {
        return Sender(id: "user", displayName: UserEntity.current?.firstname ?? "")
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }

    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
}

extension ChattingViewController: MessageCellDelegate {
    
    
    
}

extension ChattingViewController: MessagesLayoutDelegate {
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 7
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        switch message.kind {
        case .custom(_):
            return 3
        default:
            return 18
        }
    }
    
}

extension ChattingViewController: MessagesDisplayDelegate {
    
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        switch message.kind {
        case .photo(let mediaType):
            guard let url = mediaType.url else { return }
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url)
        default:
            break
        }
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .white
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .black
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.image = nil
        let message = messageList[indexPath.section]
        if message.sender == currentSender() {
            guard let url = URL(string: UserEntity.current?.userPhoto ?? "") else { return }
            avatarView.kf.setImage(with: ImageResource(downloadURL: url, cacheKey: url.absoluteString))
        }
    }
}

extension MessageInputBarDelegate where Self: ChattingViewController {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {

        for component in inputBar.inputTextView.components {
            if let str = component as? String {
                let message = ChattingMessage(text: str, sender: currentSender(), messageId: UUID().uuidString, date: Date())
                ChattingService.shared.sendMessage(message: str, complition: { [weak self] (dict) in
                    self?.messageList.append(message)
                    self?.messagesCollectionView.reloadData()
                    self?.messagesCollectionView.scrollToBottom(animated: false)
                }) { [weak self] (error) in
                    self?.showAlertMessage(title: "Ошибка", message: error.localizedDescription)
                }
            } else if let img = component as? UIImage {
               // let message = ChattingMessage(image: img, sender: currentSender(), messageId: UUID().uuidString, date: Date())
            }
        }
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom(animated: true)
    }
    
    
    func messageInputBar(_ inputBar: MessageInputBar, didChangeIntrinsicContentTo size: CGSize) {
        inputBar.frame = CGRect(x: inputBar.frame.minX, y: inputBar.frame.minY, width: size.width, height: size.height > 88 ? size.height : 88)
    }
}

extension ChattingViewController: ChattingServiceDelegate {
    
    func didUpdateImage(forChatMessage: ChattingMessage) {
        
    }
    
    func didReceiveMessage() {
        AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1000), nil)
        loadMessages()
    }
}

extension ChattingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            picker.dismiss(animated: true) {
                let resizeImage = UIImage.resizeImage(image: pickedImage, targetSize: CGSize(width: 720, height: 960))
                if let imageBase64 = resizeImage.pngData()?.base64EncodedString() {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    ChattingService.shared.uploadFile(imageBase64: imageBase64, complition: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let message = ChattingMessage(image: resizeImage, sender: self.currentSender(), messageId: UUID().uuidString, date: Date())
                        self.messageList.append(message)
                        self.messagesCollectionView.reloadData()
                        self.messagesCollectionView.scrollToBottom(animated: false)
                    }, failure: { (error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    })
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        messagesCollectionView.scrollToBottom()
    }
    
}
