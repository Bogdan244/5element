//
//  NewGiftsListViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/10/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewGiftsListViewController: BaseMenuViewController, GiftUIProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [UserGift]()
    private var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if GiftsService.shared.notViewed > 0 {
            activateGifts(didActivateGift: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MBProgressHUD.hide(for: view, animated: true)
        MBProgressHUD.showAdded(to: view, animated: true)
        GiftsService.shared.getUserGifts(complition: { [weak self] (userGifts) in
            guard let self = self else { return }
            self.isFirstLoad = false
            MBProgressHUD.hide(for: self.view, animated: true)
            self.dataSource = userGifts.gifts
            self.tableView.reloadData()
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.isFirstLoad = false
            self.tableView.reloadData()
        }
    }
    
}

extension NewGiftsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.count == 0, !isFirstLoad {
            let placeholder: GiftPlaceholderView = .initFromNib()
            placeholder.activateAction = {
                PresenterImpl().openNewActivateMyServiceVC(didActivateService: nil)
            }
            tableView.backgroundView = placeholder
        } else {
            tableView.backgroundView = nil
        }
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiftListCell") as! GiftListCell
        cell.configurate(gift: dataSource[indexPath.row])
        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if dataSource[indexPath.row].viewed == 0 {
            GiftsService.shared.viewGiftCode(id: dataSource[indexPath.row].id, complition: {
                
            }) {
                
            }
        }
        PresenterImpl().openGiftVC(gift: dataSource[indexPath.row], giftID: dataSource[indexPath.row].id)
    }
    
}
