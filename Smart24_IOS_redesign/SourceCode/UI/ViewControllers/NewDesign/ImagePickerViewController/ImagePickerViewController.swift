//
//  ImagePickerViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/21/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class ImagePickerViewController: UIViewController {

    @IBOutlet weak var makePhotoView: UIView!
    @IBOutlet weak var galleryPhotoView: UIView!
    
    var makePhotoHandler: (() -> ())?
    var galleryHandler: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makePhotoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(makePhotoAction)))
        galleryPhotoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(galleryAction)))
    }

    @IBAction func touchCross() {
        dismiss(animated: true) {
            
        }
    }
    
    @objc func makePhotoAction() {
        dismiss(animated: true, completion: makePhotoHandler)
        print("makePhoto")
    }
    
    @objc func galleryAction() {
        dismiss(animated: true, completion: galleryHandler)
        print("gallery")
    }
    
}
