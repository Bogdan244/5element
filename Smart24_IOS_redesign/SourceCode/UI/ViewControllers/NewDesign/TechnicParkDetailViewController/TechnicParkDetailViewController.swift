//
//  TechnicParkDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/12/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol TechnicParkDetailViewControllerDelegate: class {
    func viewController(_ viewController: TechnicParkDetailViewController, removeProductAt index: Int)
    func viewController(_ viewController: TechnicParkDetailViewController, didEditAt index: Int)
}

class TechnicParkDetailViewController: BaseMenuViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var needRepairBT: UIButton!
    @IBOutlet weak var needInstallBT: UIButton!
    @IBOutlet weak var technicParkView: TechnicParkDropDownView!
    
    var dataSource = [TechnicParkEntity]()
    var startIndex = IndexPath(item: 0, section: 0)
    weak var delegate: TechnicParkDetailViewControllerDelegate?
    
    fileprivate var needsDelayedScrolling = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        needRepairBT.layer.cornerRadius = needRepairBT.frame.height / 2
        needInstallBT.layer.cornerRadius = needInstallBT.frame.height / 2
        needInstallBT.addShadow(height: 7)
        needRepairBT.addShadow(height: 7)
        setupUIforDataSource(index: startIndex.item)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        needsDelayedScrolling = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.needsDelayedScrolling {
            self.needsDelayedScrolling = false
            collectionView.scrollToItem(at: startIndex, at: .right, animated: false)
        }
    }
    
    override var rightBarButtons: [UIBarButtonItem] {
        return [clearButton, editButton]
    }
    
    lazy var clearButton = UIBarButtonItem(image: UIImage(named: "clearGiftImage"), style: .plain, target: self, action: #selector(clear))

    
    @objc func clear() {
        showCustomAlertMessage(title: nil, message: "Вы действительно хотите удалить технику?",
                               firstButtonTitle: "УДАЛИТЬ",
                               secondButtonTitle: "ОТМЕНА",
                               firstBTHandler: { [weak self] in
                                guard let self = self else { return }
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                let position = self.calculateScrollViewPosition(scrollView: self.collectionView)
                                guard position < self.dataSource.count else { return }
                                let technic = self.dataSource[position]
                                TechnicParkService().removeProductFromTechnicPark(technic: technic, complition: { [weak self] (item) in
                                    guard let self = self else { return }
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    guard let error = item.errors.first else {
                                        self.delegate?.viewController(self, removeProductAt: position)
                                        return
                                    }
                                    self.showAlertMessage(title: "Ошибка", message: error.data, complition: nil, cancel: nil)
                                }, failure: {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    self.showAlertMessage(title: "Ошибка связи с сервером", message: "Попробуйте позже", complition: nil, cancel: nil)
                                })
        }, secondBTHandler: { },
           isNeedScrollToBottom: false,
           isLinksEnabled: false)

    }
    
    lazy var editButton = UIBarButtonItem(image: UIImage(named: "editTechnic"), style: .plain, target: self, action: #selector(edit))
    
    @objc func edit() {
        let position = self.calculateScrollViewPosition(scrollView: self.collectionView)
        guard position < self.dataSource.count else { return }
        let technic = self.dataSource[position]
        MBProgressHUD.showAdded(to: view, animated: true)
        TechnicParkService().getTechnicParkInfo(complition: { [weak self] (technicParkInfo) in
            guard let self = self else { return }
            let categorie = technicParkInfo.technicCategories.lazy.compactMap{ $0.childrens }.joined().compactMap{ $0 }.first(where: { $0.id == technic.technicCategory.id })
            MBProgressHUD.hide(for: self.view, animated: false)
            guard let categorieValue = categorie else { return }
            let didEdit = { [weak self] in
                guard let self = self else { return }
                self.delegate?.viewController(self, didEditAt: position)
            }
            PresenterImpl().openAddTechnicToParkDetailVC(technikParkInfo: technicParkInfo, technicCategories: categorieValue, technicParkEntity: technic, didEdit: didEdit)
        }) {
            MBProgressHUD.hide(for: self.view, animated: false)
        }
    }
    
    func setupUIforDataSource(index: Int) {
        let prs = dataSource[index].technicParkPRSEntity
        technicParkView.setPRS(technicParkPRS: prs)
        prs == nil ? addRightButtons() : removeRightButtons()
        let installStatus = dataSource[index].technicCategory.installAvailable
        switch installStatus {
        case .available:
            needInstallBT.isHidden = false
        default:
            needInstallBT.isHidden = true
        }
    }
    
    @IBAction func needRepairAction(_ sender: Any) {
        var technic: TechnicParkPRSEntity?
        let position = calculateScrollViewPosition(scrollView: collectionView)
        if position < dataSource.count {
            technic = dataSource[position].technicParkPRSEntity
        }
        
        PresenterImpl().openRequestRepairEquipmentVC(didCreateRequestForInstall: nil, technicParkPRSEntity: technic)
    }
    
    @IBAction func needInstallAction(_ sender: Any) {
        PresenterImpl().openRequestInstallEquipmentVC()
    }
}

extension TechnicParkDetailViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    private func calculateScrollViewPosition(scrollView: UIScrollView) -> Int {
        return Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let position = calculateScrollViewPosition(scrollView: scrollView)
        setupUIforDataSource(index: position)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TechnicParkDetailCell", for: indexPath) as! TechnicParkDetailCell
        cell.setTechnicParkEntity(entity: dataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
