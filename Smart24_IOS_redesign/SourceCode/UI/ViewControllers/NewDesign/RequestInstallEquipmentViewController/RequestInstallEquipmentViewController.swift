//
//  RequestInstallEquipmentViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/24/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class SendButton: UIButton {
    override var isEnabled: Bool {
        didSet {
            alpha = isEnabled ? 1.0 : 0.5
        }
    }
}

class RequestInstallLabel: UILabel {
    override var isEnabled: Bool {
        didSet {
            alpha = isEnabled ? 1.0 : 0.5
        }
    }
}

class RequestInstallEquipmentViewController: BaseMenuViewController {

    @IBOutlet weak var serviceNumberLabel: RequestInstallLabel!
    @IBOutlet weak var equipmentLabel: RequestInstallLabel!
    @IBOutlet weak var modelLabel: RequestInstallLabel!
    @IBOutlet weak var fioLabel: RequestInstallLabel!
    @IBOutlet weak var phoneNumberLabel: RequestInstallLabel!
    @IBOutlet weak var dateLabel: RequestInstallLabel!
    @IBOutlet weak var serviceNumberTFView: Smart24TextFieldView!
    @IBOutlet weak var equipmentTFView: Smart24TextFieldView!
    @IBOutlet weak var modelTFView: Smart24TextFieldView!
    @IBOutlet weak var fioTFView: Smart24TextFieldView!
    @IBOutlet weak var phoneNumberTFView: Smart24TextFieldView!
    @IBOutlet weak var dateTFView: Smart24TextFieldView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sendButton: SendButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupState(isEnabled: false)
        serviceNumberTFView.state = .serviceNumber
        serviceNumberTFView.isComplete = { [weak self] isComplete in
            self?.setupState(isEnabled: isComplete)
        }
        equipmentTFView.state = .list(list: [String]())
        modelTFView.state = .standart
        fioTFView.state = .standart
        phoneNumberTFView.state = .phone(code: "375")
        dateTFView.state = .date(time: Date(), minimumDate: Date())
        RSService.shared.getRequestServicesInfo(complition: { [weak self] (requestServiceInfo) in
            guard let self = self else { return }
            self.equipmentTFView.state = .list(list: requestServiceInfo.technicInstallCategories)
        }) {
        
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = UserEntity.current {
            fioTFView.textField.text = user.firstname + " " + user.surname
            phoneNumberTFView.textField.text = user.phone.replacingOccurrences(of: "375", with: "").group(by: [2,3,2,2])
        }
    }
    
    private func setupState(isEnabled: Bool) {
        equipmentLabel.isEnabled = isEnabled
        modelLabel.isEnabled = isEnabled
        fioLabel.isEnabled = isEnabled
        phoneNumberLabel.isEnabled = isEnabled
        dateLabel.isEnabled = isEnabled
        sendButton.isEnabled = isEnabled
        equipmentTFView.isEnabled = isEnabled
        modelTFView.isEnabled = isEnabled
        fioTFView.isEnabled = isEnabled
        phoneNumberTFView.isEnabled = isEnabled
        dateTFView.isEnabled = isEnabled
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @IBAction func sendActionButton(_ sender: Any) {
        
        guard let serviceNumber = serviceNumberTFView.textField.text, serviceNumber.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Уникальный номер сервиса' обьязательное")
            return
        }
        
        guard let technicCategory = equipmentTFView.textField.text, technicCategory.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Вид техники' обьязательное")
            return
        }
        
        guard let name = fioTFView.textField.text, name.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'ФИО' обьязательное")
            return
        }
        
        guard let model = modelTFView.textField.text, model.count > 2 else {
            showAlertMessage(title: nil, message: "Поле 'Модель устройства' обьязательное и должно быть длинее двух символов")
            return
        }
        
        guard let phone = phoneNumberTFView.textField.text, phone.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Ваш номер телефона' обьязательное")
            return
        }
        
        let date = dateTFView.textField.serverDate()
        guard date.count > 0 else {
            showAlertMessage(title: nil, message: "Поле 'Ваш адрес' обьязательное")
            return
        }
        
        RSService.shared.createRequestToMasterInstall(serviceNumber: serviceNumber, name: name, model: model, phone: phone, technicCategory: technicCategory, date: date, complition: { [weak self] (baseEntity) in
            if baseEntity.errors.count > 0 {
                self?.showAlertMessage(title: nil, message: baseEntity.errors.first!.data)
            } else {
                self?.showAlertMessage(title: nil, message: "Сервис «Установка» принят в работу. Ожидайте, пожалуйста, звонок от Мастера", complition: {
                    self?.navigationController?.popViewController(animated: true)
                    }, cancel: nil)
            }
        }) { [weak self] in
            guard let self = self else { return }
            self.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
