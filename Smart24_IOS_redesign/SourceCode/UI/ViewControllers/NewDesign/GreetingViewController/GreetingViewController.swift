//
//  GreetingViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/26/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD
import GoogleSignIn
import ok_ios_sdk
import VK_ios_sdk
import FacebookCore
import FacebookLogin
import AuthenticationServices

class GreetingViewController: UIViewController, AuthorizationUIActionsProtocol {

    var authService = AuthorizationServices()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var vkButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var appleButton: UIButton!
    
    fileprivate let generalViewModel = GeneralViewModel()
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterButton.makeCorner()
        registrationButton.makeCorner()
        scrollView.delegate = self
        scrollView.addSubview(ScrollingView.technicalSupportView)
        scrollView.addSubview(ScrollingView.stockBonusesPresents)
        scrollView.addSubview(ScrollingView.evaluateTheConsultation)
        scrollView.addSubview(ScrollingView.technicalSupportView)
        if #available(iOS 13.0, *) {
            appleButton.isHidden = false
        } else {
            appleButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(handlerTimer), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func handlerTimer() {
        scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x + scrollView.frame.width, y: 0), animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for (index, view) in scrollView.subviews.enumerated() {
            view.frame = CGRect(x: scrollView.frame.width * CGFloat(index), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
        }
        scrollView.contentSize.width = scrollView.frame.width * CGFloat(scrollView.subviews.count)
    }

    @IBAction func enterButtonAction(_ sender: Any) {
        generalViewModel.openNewLoginVC()
    }
    
    @IBAction func registrationButtonAction(_ sender: Any) {
        PresenterImpl().openRegistrationStart()
    }
    
    @IBAction func googleAuth(_ sender: Any) {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = Constants.gClientID
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookAuth(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        MBProgressHUD.showAdded(to: view, animated: true)
        loginManager.logIn(permissions: ["publish_actions"], from: self) { [weak self] (loginResult, error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            guard let token = loginResult?.token else { return }
            self.loginSocial(socialName: .facebook, token: token.tokenString, serverAuthCode: nil)
        }
    }
    
    var isVKLogging = false
    
    @IBAction func vkAuth(_ sender: Any) {
        VKSdk.instance()?.unregisterDelegate(self)
        VKSdk.instance()?.uiDelegate = nil
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        
        VKSdk.forceLogout()
        MBProgressHUD.showAdded(to: view, animated: true)
        isVKLogging = false
        VKSdk.authorize(["wall"])
    }
    
    @IBAction func okAuth(_ sender: Any) {
        OKSDK.clearAuth()
        MBProgressHUD.showAdded(to: view, animated: true)
        OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS","LONG_ACCESS_TOKEN"], success: { [weak self] (data) in
            guard let self = self else { return }
            guard let dataArray = data as? Array<String> else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.loginSocial(socialName: .odnoklassniki, token: dataArray.first)
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.showAlertMessage(title: "Ошибка", message: "\(String(describing: error))")
            
        }
    }
    @available(iOS 13.0, *)
    @IBAction func appleButton(_ sender: Any) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
}

extension GreetingViewController: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            guard let token = appleIDCredential.identityToken else { return }
            guard let tokenString = String(data: token, encoding: .utf8) else { return }
            self.loginSocial(socialName: .apple, token: tokenString)
        default:
            break
        }
    }
}

extension GreetingViewController: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    

}


extension GreetingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        
        if x >= scrollView.frame.size.width * 3 {
            self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    
}

extension GreetingViewController: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        VKCaptchaViewController.captchaControllerWithError(captchaError).present(in: self)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        MBProgressHUD.hide(for: view, animated: true)
        
        if result.token != nil, !isVKLogging {
            isVKLogging = true
            loginSocial(socialName: .vkontakte, token: result.token.accessToken)
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

extension GreetingViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user == nil {
            return
        }
        loginSocial(socialName: .google, token: user.authentication.accessToken)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

//extension GreetingViewController: GIDSignInUIDelegate {
//
//    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        present(viewController, animated: true, completion: nil)
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
//        dismiss(animated: true, completion: nil)
//        MBProgressHUD.hide(for: view, animated: true)
//    }
//}
