//
//  NewBonussesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/8/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewBonussesViewController: BaseMenuViewController {
    
    @IBOutlet weak var firstBonusContainer: UIView!
    @IBOutlet weak var secondBonusContainer: UIView!
    @IBOutlet weak var thirdBonusContainer: UIView!
    @IBOutlet weak var activeBonuseLabel: UILabel!
    @IBOutlet weak var nonActiveBonuseLabel: UILabel!
    @IBOutlet weak var nextCancelingSumLabel: UILabel!
    @IBOutlet weak var nextCancelingDateLabel: UILabel!
    @IBOutlet weak var nextActivationSumLabel: UILabel!
    @IBOutlet weak var nextActivationDateLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    
    @IBOutlet weak var adsView: AdsView!
    var bonusCardEntity: BonusCardEntity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        guard let card = bonusCardEntity.cards.first else { return }
        activeBonuseLabel.text = card.activeBonuses != nil ? "\(Double(card.activeBonuses!) / 100)" : ""
        nonActiveBonuseLabel.text = card.nonActiveBonuses != nil ? "\(Double(card.nonActiveBonuses!) / 100)" : ""
        nextCancelingSumLabel.text = card.nextCancelingSum != nil ? "\(Double(card.nextCancelingSum!) / 100)" : ""
        nextCancelingDateLabel.text = card.nextCancelingDate != nil ? convertDateString(dateString: card.nextCancelingDate!) : ""
        nextActivationSumLabel.text = card.nextActivationSum != nil ? "\(Double(card.nextActivationSum!) / 100)" : ""
        nextActivationDateLabel.text = card.nextActivationDate != nil ? convertDateString(dateString: card.nextActivationDate!) : ""
        cardNumberLabel.text = card.card.group(by: 3, separator: " ")
        firstBonusContainer.addShadow(height: 3)
        secondBonusContainer.addShadow(height: 3)
        thirdBonusContainer.addShadow(height: 3)
        setupAds()
    }
    
    private func convertDateString(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let subString = dateString.prefix(10)
        if let date = dateFormatter.date(from: String(subString)) {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter.string(from: date)
            
        }
        return String(subString)
    }
    
    private func setupAds() {
        AuthorizationServices().updateUserInfo(didUpdate: {
        }) { [weak self] (ads) in
            guard let self = self else { return }
            self.adsView.setAds(ads: ads)
        }
    }
    
    @IBAction func deactivateCardAction(_ sender: Any) {
        guard let cardID = bonusCardEntity.cards.first?.id else { return }
        showCustomAlertMessage(title: nil,
                               message: "Вы действительно хотите отвязать карту?",
                               firstButtonTitle: "Да",
                               secondButtonTitle: "Нет",
                               firstBTHandler: { [unowned self] in
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                PRSService().deactivateBonusCard(cardID: cardID, complition: {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    self.navigationController?.popToRootViewController(animated: true)
                                }, failure: {
                                    self.showAlertMessage(title: "Ошибка", message: "Ошибка связи с сервером")
                                })
            }, secondBTHandler: {
            
            })
    }
    
}
