//
//  ActivateBonussesViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/3/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class ActivateBonussesViewController: BaseViewController {

    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Мои бонусы"
        activateButton.layer.cornerRadius = 15
        activateButton.layer.shadowColor = UIColor(rgb: 0xCF2B2B).cgColor
        activateButton.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        activateButton.layer.shadowOpacity = 0.4
        activateButton.layer.shadowRadius = 5.0
        let attributedString = NSMutableAttributedString(string: "Привяжи бонусную карту\n«5 элемент» и следи\nза балансом бонусов")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        textLabel.attributedText = attributedString
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        revealViewController()?.panGestureRecognizer()?.isEnabled = false
    }
    
    @IBAction func activateAction(_ sender: Any) {
        GeneralViewModel().openActivateBonuseAlert { [weak self] (cardNumber, phone) in
            guard let self = self else { return }
            MBProgressHUD.showAdded(to: self.view, animated: true)
            PRSService().activateBonusCard(cardNumber: cardNumber, phone: phone, complition: { [weak self] (bonusCard) in
                guard let self = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                if bonusCard.errors.count > 0 {
                    self.showCustomAlertMessage(title: nil, message: "Бонусная карта не обнаружена.\nПроверьте внесенные данные.", firstButtonTitle: "ПРОВЕРИТЬ", firstBTHandler: nil, secondBTHandler: nil)
                } else {
                    GeneralViewModel().openBonussesVC(bonuses: bonusCard)
                }
            }, failure: {
                
            })
        }

    }
}
