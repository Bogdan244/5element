//
//  RequestServiceDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/30/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class RequestServiceDetailViewController: BaseMenuViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var codeLB: UILabel!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var descriptionLB: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var service: ServiceEntity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.backgroundColor = navigationController?.navigationBar.barTintColor
        
        switch service.type {
        case .prsRequest:
            codeLB.text = service.code
            titleLB.text = service.productCategory.count > 0 ? service.productCategory : "Заявка"
            descriptionLB.text = service.productModule
        case .technicStatus:
            codeLB.text = service.actNumber
            titleLB.text = service.productCategory.count > 0 ? service.productCategory : "Заявка"
            descriptionLB.text = service.technic
        default:
            break
        }
    }
}

extension RequestServiceDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestServiceDetailStatusCell") as! RequestServiceDetailStatusCell
        cell.setService(service: service)
        return cell
    }
    
    
}
