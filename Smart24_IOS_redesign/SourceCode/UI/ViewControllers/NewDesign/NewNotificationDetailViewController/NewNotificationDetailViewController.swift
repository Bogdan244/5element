//
//  NewNotificationDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewNotificationDetailViewController: BaseMenuViewController {

    var notification: NewNotificationEntity!
    
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var activateButtonView: UIView!
    @IBOutlet weak var borderedContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: BorderedLabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override var rightBarButtons: [UIBarButtonItem] {
        return [UIBarButtonItem(image: UIImage(named: "clearGiftImage"),
                                style: .plain,
                                target: self,
                                action: #selector(clearNotification))]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        borderedContentView.layer.cornerRadius = 10
        borderedContentView.layer.borderWidth = 1
        borderedContentView.layer.borderColor = UIColor(rgb: 0xC4C4C4).cgColor
        setupViews()
        setupActivateButton()
    }
    
    @objc func clearNotification() {
        MBProgressHUD.showAdded(to: view, animated: true)
        NotificationsService.shared.removeNotification(id: notification.id, complition: { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.navigationController?.popViewController(animated: true)
        }) { [weak self] in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    private func setupViews() {
        let date = Date(timeIntervalSince1970: TimeInterval(notification.publishDateUnix))
        let df = DateFormatter()
        df.dateFormat = "EEEE, dd MMMM yyyy"
        dateLabel.text = df.string(from: date)
        df.dateFormat = "HH:mm"
        timeLabel.text = df.string(from: date)
        if let insurance = notification.insurance, notification.type == .insurance {
            let text = ("<span style=\"font-size: 16\">" + (insurance.description?.attributedHtmlString?.string ?? "") + "</span>").attributedHtmlString
            descriptionLabel.attributedText = text
        } else {
            descriptionLabel.text = notification.description
        }
    }
    
    private func setupActivateButton() {
        activateButton.makeCorner()
        activateButton.addShadow(height: 10,
                                 color: activateButton.backgroundColor ?? .black,
                                 opacity: 0.15)
        if notification.insurance != nil, notification.type == .insurance {
            activateButtonView.isHidden = false
            activateButton.addTarget(self, action: #selector(activateInsurance), for: .touchUpInside)
        } else {
            activateButtonView.isHidden = true
        }
    }
    
    @objc func activateInsurance() {
        guard let insurance = notification.insurance else { return }
        PresenterImpl().openInsuranceActivateVC(insurance: insurance, notification: notification)
    }
    
}

class BorderedLabel: UILabel {
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25))
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -15,
                                          left: -15,
                                          bottom: -15,
                                          right: -15)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)))
    }
}

