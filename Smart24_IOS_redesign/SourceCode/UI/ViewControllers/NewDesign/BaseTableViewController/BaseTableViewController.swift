//
//  BaseTableViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 4/2/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class BaseTableViewController: UITableViewController, UIGestureRecognizerDelegate {
    
    var leftButton: UIBarButtonItem? {
        return UIBarButtonItem(image: UIImage(named: "backButton")?.withRenderingMode(.alwaysOriginal),
                               style: .plain,
                               target: self,
                               action: #selector(leftButtonAction))
    }
    
    var rightBarButtons: [UIBarButtonItem] {
        return [UIBarButtonItem]()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateNavigationText()
        addLeftButton()
        addRightButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        revealViewController()?.panGestureRecognizer()?.isEnabled = false
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func configurateNavigationText() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(rgb: 0x2E2E2D)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 20) as Any]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func addLeftButton() {
        if leftButton != nil {
            navigationItem.leftBarButtonItem = leftButton
        }
    }
    
    func addRightButtons() {
        navigationItem.rightBarButtonItems = rightBarButtons
    }
    
    @objc func leftButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
}


