//
//  InsuranceViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/28/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class InsuranceActivateViewController: BaseMenuViewController {
    let insuranceService = InsuranceService()
    var notification: NewNotificationEntity!
    var insurance: InsuranceEntity!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var surnameTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var nameTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var middleNameTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var imeiTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var brandTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var modelTextFieldView: Smart24TextFieldView!
    @IBOutlet weak var priceTextFieldView: Smart24TextFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        surnameTextFieldView.state = .cyrillicOnly
        nameTextFieldView.state = .cyrillicOnly
        middleNameTextFieldView.state = .cyrillicOnly
        imeiTextFieldView.state = .imei
        brandTextFieldView.state = .notEditable
        modelTextFieldView.state = .notEditable
        priceTextFieldView.state = .notEditable
        surnameTextFieldView.textField.text = insurance.surname
        nameTextFieldView.textField.text = insurance.name
        middleNameTextFieldView.textField.text = insurance.patronymic
        brandTextFieldView.textField.text = insurance.brand
        modelTextFieldView.textField.text = insurance.model
        priceTextFieldView.textField.text = insurance.price
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func activateInsurance() {
        insuranceService.activateInsurance(id: insurance.prsId ?? 0,
                                           imei: imeiTextFieldView.textField.text ?? "",
                                           name: nameTextFieldView.textField.text ?? "",
                                           middleName: middleNameTextFieldView.textField.text ?? "",
                                           surname: surnameTextFieldView.textField.text ?? "",
                                           notificationId: notification.id, complition: { [weak self] insurance in
                                            self?.navigationController?.popToRootViewController(animated: true)
                                            PresenterImpl().openInsuranceVC(insurance: insurance, didRemoveInsurance: nil)
        },
                                           failure: { error in
                                            self.showErrorAlert(error: error)
                                            self.showSmart24Error(error: error)
        })
    }
    
    @IBAction func cancelButtonTapAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func activateButtonTapAction(_ sender: Any) {
        if imeiTextFieldView.textField.text?.count != 15 {
            guard let vc = UIApplication.shared.keyWindow?.rootViewController else { return }
            vc.showAlertMessage(title: nil, message: "Imei-код введен неверно.", complition: { }, cancel: nil)
        } else {
            showCustomAlertMessage(title: insurance.termsTitle,
                                   message: insurance.terms ?? "",
                                   firstButtonTitle: "СОГЛАСЕН",
                                   secondButtonTitle: "ОТМЕНА",
                                   firstBTHandler: {
                                    self.activateInsurance()
            },
                                   secondBTHandler: {
                                    
            },
                                   isNeedScrollToBottom: true,
                                   isLinksEnabled: true)
        }
        
    }
}
