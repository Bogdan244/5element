//
//  WebViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/1/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class WebViewController: BaseMenuViewController {

    @IBOutlet weak var webViewContentView: UIView!
    private var webView: WKWebView!
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Договор"
        webView = WKWebView()
        webView.navigationDelegate = self
        webViewContentView.addSubview(webView)
        //  webView.allowsBackForwardNavigationGestures = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url,
                                     cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad,
                                     timeoutInterval: 30)
            self.webView.load(request)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = self.webViewContentView.bounds
    }
    
}

extension WebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showAdded(to: webView, animated: true)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        MBProgressHUD.hide(for: webView, animated: true)
    }
    
}
