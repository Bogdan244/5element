//
//  StartRegistrationViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bogdan on 26.05.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import GoogleSignIn
import ok_ios_sdk
import VK_ios_sdk
import FacebookCore
import FacebookLogin
import AuthenticationServices

class StartRegistrationViewController: BaseViewController, AuthorizationUIActionsProtocol {
    
    var state: NewRegistrationViewController.State = .registration
    
    var authService = AuthorizationServices()
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var appleButton: UIButton!
    
    @IBOutlet weak var phoneNumberTFV: Smart24TextFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurate()
    }
    
    private func configurate() {
        title = state.title
        registrationButton.makeCorner()
        phoneNumberTFV.state = .phone(code: "375")
        if #available(iOS 13, *) {
            appleButton.isHidden = false
        } else {
            appleButton.isHidden = true
        }
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func registrationAction(_ sender: Any) {
        switch state {
        case .registration:
            registration(phone: phoneNumberTFV.textField.text ?? "")
        case .socialLogin(socialName: let socialName, accessToken: let accessToken, serverAuthCode: let serverAuthCode, phone: _):
            loginSocial(socialName: socialName, token: accessToken, serverAuthCode: serverAuthCode, phone: phoneNumberTFV.textField.text ?? "")
        case .login:
            break
        }
    }
    
    @IBAction func googleAuth(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = Constants.gClientID
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookAuth(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        MBProgressHUD.showAdded(to: view, animated: true)

        loginManager.logIn(permissions: ["publish_actions"], from: self) { [weak self] (loginResult, error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            guard let token = loginResult?.token else { return }
            self.loginSocial(socialName: .facebook, token: token.tokenString, serverAuthCode: nil)
        }
    }
    
    var isVKLogging = false
    @IBAction func vkAuth(_ sender: Any) {
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        VKSdk.forceLogout()
        MBProgressHUD.showAdded(to: view, animated: true)
        isVKLogging = false
        VKSdk.authorize(["wall"])
    }
    
    @IBAction func okAuth(_ sender: Any) {
        OKSDK.clearAuth()
        MBProgressHUD.showAdded(to: view, animated: true)
        OKSDK.authorize(withPermissions: ["VALUABLE_ACCESS","LONG_ACCESS_TOKEN"], success: { [weak self] (data) in
            guard let self = self else { return }
            guard let dataArray = data as? Array<String> else { return }
            print("\(dataArray)")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.loginSocial(socialName: .odnoklassniki, token: dataArray.first)
        }) { (error) in
            self.showAlertMessage(title: "Ошибка", message: "\(String(describing: error))")
        }
    }
    
    @available(iOS 13.0, *)
    @IBAction func appleButton(_ sender: Any) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}

extension StartRegistrationViewController: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            guard let token = appleIDCredential.identityToken else { return }
            guard let tokenString = String(data: token, encoding: .utf8) else { return }
            self.loginSocial(socialName: .apple, token: tokenString)
        default:
            break
        }
    }
}

extension StartRegistrationViewController: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    

}

extension StartRegistrationViewController: VKSdkDelegate, VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        VKCaptchaViewController.captchaControllerWithError(captchaError).present(in: self)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        MBProgressHUD.hide(for: view, animated: true)
        if result.token != nil, !isVKLogging {
            isVKLogging = true
            loginSocial(socialName: .vkontakte, token: result.token.accessToken)
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

extension StartRegistrationViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user == nil {
            return
        }
        loginSocial(socialName: .google, token: user.authentication.accessToken)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
