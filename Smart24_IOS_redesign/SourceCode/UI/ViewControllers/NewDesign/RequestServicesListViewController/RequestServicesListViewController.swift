//
//  RequestServicesListViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/29/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import MBProgressHUD

class RequestServicesListViewController: BaseMenuViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var floatingActionButton: UIButton!
    
    let refreshControl = UIRefreshControl()
    var dataSource = [ServiceEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        floatingActionButton.makeCircle()
        floatingActionButton.layer.shadowColor = UIColor.black.cgColor
        floatingActionButton.layer.shadowOffset = CGSize(width: 0, height: 14)
        floatingActionButton.layer.shadowOpacity = 0.1
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 80, right: 0)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        updateDataSource()
        refreshControl.endRefreshing()
    }

    @IBAction func didSelectFloatingActionButton(_ sender: Any) {
        PresenterImpl().openRequestService(didEnterEquipmentCode: { [weak self] (code) in
            guard let strongSelf = self else { return }
            MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
            RSService.shared.addTechnicToRepair(code: code, complition: { () in
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.updateDataSource()
            }) { (text) in
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.showAlertMessage(title: nil, message: text)
            }
        }) { [weak self] in
            self?.updateDataSource()
        }
    }
    
    func updateDataSource() {
        MBProgressHUD.showAdded(to: view, animated: true)
        RSService.shared.getRequestServices(complition: { [weak self] (serviceArray) in
            guard let strongSelf = self else { return }
            strongSelf.dataSource = serviceArray.services
            strongSelf.tableView.reloadData()
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }
    }
}

extension RequestServicesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestServiceCell") as! RequestServiceCell
        cell.configurate(service: dataSource[indexPath.row])
        cell.didSelectHistory = { [unowned self] in
            PresenterImpl().openRequestHistoryVC(service: self.dataSource[indexPath.row])
        }
        return cell
    }
    
}
