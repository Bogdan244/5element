//
//  NewActivateMyServiceViewController.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/31/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class NewActivateMyServiceViewController: BaseViewController {

    @IBOutlet weak var activateBT: UIButton!
    @IBOutlet weak var cancelBT: UIButton!
    @IBOutlet weak var textFieldView: Smart24TextFieldView!
    
    var didActivateServcie: ((Bool) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldView.state = .codeWithQR
        activateBT.makeCorner()
        cancelBT.makeCorner()
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func activationAction(_ sender: Any) {
        if textFieldView.textField.text?.count == 16 {
            RSService.shared.activateMartletCode(code: textFieldView.textField.text!, complition: { [weak self] (entity) in
                if entity.errors.count > 0 {
                    self?.showAlertMessage(title: nil, message: (entity.errors.first?.data)!)
                } else {
                    self?.navigationController?.popViewController(animated: true)
                    self?.didActivateServcie?(false)
                }
            }) { [weak self] in
                self?.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
            }
        } else {
            RSService.shared.activateCode(code: textFieldView.textField.text!, complition: { [weak self] (prsEntity) in
                if prsEntity.errors.count > 0 {
                    self?.showAlertMessage(title: nil, message: (prsEntity.errors.first?.data)!)
                } else {
                    self?.navigationController?.popViewController(animated: true)
                    self?.didActivateServcie?(prsEntity.gift)
                }
            }) { [weak self] in
                self?.showAlertMessage(title: nil, message: "Ошибка связи с сервером")
            }
        }
    }
    
    
}
