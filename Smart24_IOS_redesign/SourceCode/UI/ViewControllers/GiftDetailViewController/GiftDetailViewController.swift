//
//  GiftDetailViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 21.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class GiftDetailViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var consultationButton: UIButton!
    
    typealias selectGift = ((_ index: Int) -> Void)
    var didSelectGift: selectGift?
    
    var giftCode = GiftCodeEntity()
    
    private var profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateViews()
    }

    private func configurateViews() {
        titleLabel.text = giftCode.activationSuccessText
        codeLabel.text = giftCode.code
        consultationButton.layer.cornerRadius = 5
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 2
        cancelButton.layer.borderColor = UIColor.red.cgColor
        contentView.layer.cornerRadius = 5
    }
    
    @IBAction func cancelHandler(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func consultationAction(_ sender: Any) {
        dismiss(animated: false) {
            ProfileViewModel().openChatVC()
        }
    }
}
