//
//  MenuTableViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import MBProgressHUD

class MenuTableViewController: UITableViewController {
    
    //MARK:- private properties
    private var user: UserEntity?
    
    private let headerView  = MenuHeaderView.instanciateFromNib()
    private let footerView  = MenuFooterView.instanciateFromNib()
    private let viewModel = GeneralViewModel()
    
    private let profileViewModel = ProfileViewModel()
    private let bonusesViewModel = BonusesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableHeaders()
        
        if let navigationController = revealViewController().frontViewController as? Smart24NavigationController,
            let mainViewController = navigationController.topViewController as? MainViewController {
            
            mainViewController.openMenuButton.setImage(R.image.close_menu_icon(), for: .normal)
        }
        tableView.reloadData()
        revealViewController().panGestureRecognizer().isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = revealViewController().frontViewController as? Smart24NavigationController,
            let mainViewController = navigationController.topViewController as? MainViewController {
            
            mainViewController.openMenuButton.setImage(R.image.menu_icon(), for: .normal)
        }
    }
    
    private func configureTableView() {
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tableView.backgroundView = UIImageView(image: R.image.menu_background())
        tableView.backgroundColor = UIColor.black
        tableView.register(MainMenuCell.self)
        tableView.register(ReferenceMenuCell.self)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = 50

        footerView.activateButton.addTarget(self, action: #selector(showActivateServiceAlert), for: .touchUpInside)
    }
    
    private func configureTableHeaders() {
        user = profileViewModel.getSelfEntity()
                                                                    
        headerView.settingsButton.addTarget(self, action: #selector(openSettings), for: .touchUpInside)
        headerView.updateViewWithModel(model: user)
    }
    
    @objc func openSettings() {
        profileViewModel.openChangeDataVC()
        revealViewController().revealToggle(animated: false)
        revealViewController().panGestureRecognizer().isEnabled = false
    }
    
    @objc func showActivateServiceAlert() {
        let refreshAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        refreshAlert.addAction(UIAlertAction(title: "Написать в тех. поддержку", style: .destructive, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.revealViewController().revealToggle(animated: false)
            self.viewModel.openChatVC()
        }))

        refreshAlert.addAction(UIAlertAction(title: "Заказать обратный звонок", style: .default, handler: { (action: UIAlertAction!) in
            let stroybarod = UIStoryboard(name: "Menu", bundle: nil)
            let viewcontroller = stroybarod.instantiateViewController(withIdentifier: "OrderCallViewController")
            if let navigationController = self.revealViewController().frontViewController as? Smart24NavigationController {
                self.revealViewController().revealToggle(animated: false)
                navigationController.pushViewController(viewcontroller, animated: true)
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        self.present(refreshAlert, animated: true) {
            
        }
    }
    
    private func activateServices(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.activateService(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                switch events {
                case .completed:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationSuccess)
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.activationFailed)
                default:
                    break
                }
        }
    }
    
    private func openBonuses() {
        MBProgressHUD.showAdded(to: view, animated: false)
        
        bonusesViewModel.getBonuses()
            .observe(on: UIScheduler())
            .start { [weak self] (event) in
                guard let strongSelf = self else { return }
                
                switch event {
                case let .value(value):
                    if let bonuse = value {
                        strongSelf.bonusesViewModel.openBonusesVC(bonuses: bonuse)
                    } else {
                        strongSelf.bonusesViewModel.openActivateBonuseVC()
                    }
                    
                default:
                    break
                }
                MBProgressHUD.hide(for: strongSelf.view, animated: false)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 6 : 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: MainMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = Constants.profile
                cell.leftImageView.image = R.image.white_user_icon()
            case 1:
                cell.nameLabel.text = Constants.bonuses
                cell.leftImageView.image = R.image.bonuses()
            case 2:
                cell.nameLabel.text = Constants.newsVCTitle
                cell.leftImageView.image = R.image.store()
            case 3:
                cell.nameLabel.text = Constants.howItWork
                cell.leftImageView.image = R.image.stockPrice()
            case 4:
                cell.nameLabel.text = Constants.gifts
                cell.leftImageView.image = R.image.present()
                if let number = profileViewModel.getSelfEntity()?.codesNotViewed {
                    cell.showBadge(number: number)
                }
            case 5:
                cell.nameLabel.text = Constants.services
                cell.leftImageView.image = R.image.services()
            default:
                break
            }
            return cell
            
        } else {
            let cell: ReferenceMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = Constants.developers
                cell.rightImageView.image = R.image.developers_icon()
            case 1:
                cell.nameLabel.text = Constants.exit
                cell.rightImageView.image = R.image.exit_icon()
                cell.layer.addBorder(edge: .bottom, color: .gray, thickness: 0.5, opacity: 0.5)
            default:
                break
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 1 ? 20 : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 20 : 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?  {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        return backgroundView
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        return backgroundView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 50 : 40
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var isRevealPanGestureEnable = false
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                profileViewModel.openProfileVC()
            case 1:
                self.openBonuses()
            case 2:
                isRevealPanGestureEnable = true
                UIApplication.shared.openURL(URL(string:"http://5element.by/shops/")!)
                tableView.deselectRow(at: indexPath, animated: true)
            case 3:
                isRevealPanGestureEnable = true
                UIApplication.shared.openURL(URL(string:"https://5element.by/actions")!)
                tableView.deselectRow(at: indexPath, animated: true)
            case 4:
                profileViewModel.openMyGiftsVC()
            case 5:
                profileViewModel.openMyServices()
            default:
                break
            }
            
        } else {
            switch indexPath.row {
            case 0:
                profileViewModel.openDevelopersVC()
            case 1:
                MBProgressHUD.showAdded(to: view, animated: true)
                networkProvider.request(ApiManager.logOut(), completion: { (result) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    switch result {
                    case .success(_):
                        self.profileViewModel.presentLoginVC()
                    case .failure(_):
                        break
                    }
                })
            default:
                break
            }
            
        }
        
        revealViewController().revealToggle(animated: false)
        revealViewController().panGestureRecognizer().isEnabled = isRevealPanGestureEnable
    }
}
