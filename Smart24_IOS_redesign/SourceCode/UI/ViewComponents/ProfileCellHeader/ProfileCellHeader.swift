//
//  ProfileCellHeader.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ProfileCellHeader: UIView, NibLoadableView {

    @IBOutlet weak var activateButton: LoginButton!
    @IBOutlet weak var notificationsLabel: UILabel!
    
    override func awakeFromNib() {
        self.activateButton.layer.borderColor = UIColor(red: 42.0/255.0, green: 166.0/255.0, blue: 76.0/255.0, alpha: 1.0).cgColor
    }
}
