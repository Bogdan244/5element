//
//  NotificationCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit
import Kingfisher

class NotificationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var imageSeparator: UIView!
    @IBOutlet weak var imageBackroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        stateImageView.image = nil
        imageBackroundView.backgroundColor = .white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configurate(notification: NewNotificationEntity) {
        titleLabel.text = notification.title
        if let insuranceDescription = notification.insurance?.description {
            descriptionLabel.text = insuranceDescription.attributedHtmlString?.string
        } else {
            descriptionLabel.text = notification.description
        }
        descriptionLabel.textColor = .black
        backgroundColor = .white
        let date = Date(timeIntervalSince1970: TimeInterval(notification.publishDateUnix))
        let df = DateFormatter()
        df.dateFormat = "dd MMMM"
        dateLabel.text = df.string(from: date)
        if notification.read == 0 {
            titleLabel.font = UIFont(name: "Roboto-Bold", size: 16)
            guard let imageURL = URL(string: notification.imageActiveURL) else { return }
            let imageResource = ImageResource(downloadURL: imageURL)
            stateImageView?.kf.setImage(with: imageResource)
            imageBackroundView?.backgroundColor = UIColor(rgb: 0xCF2B2B)
            imageSeparator.isHidden = true
        } else {
            titleLabel.font = UIFont(name: "Roboto-Regular", size: 16)
            guard let imageURL = URL(string: notification.imageDefaultURL) else { return }
            let imageResource = ImageResource(downloadURL: imageURL)
            stateImageView?.kf.setImage(with: imageResource)
            imageBackroundView?.backgroundColor = .white
            imageSeparator.isHidden = false
        }
        
    }
    
}
