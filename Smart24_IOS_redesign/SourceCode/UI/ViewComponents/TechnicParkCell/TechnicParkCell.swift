//
//  TechnicParkCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class TechnicParkCell: UITableViewCell {

    @IBOutlet weak var technicContentView: UIView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var descriptionLB: UILabel!
    @IBOutlet weak var prsLogo: UIStackView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        prsLogo.isHidden = false
        logoImageView.kf.setImage(with: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        technicContentView.layer.shadowColor = UIColor.black.cgColor
        technicContentView.layer.shadowOffset = CGSize(width: 0, height: 5)
        technicContentView.layer.shadowOpacity = 0.1
        technicContentView.layer.cornerRadius = 2
    }

    func setTechnic(technic: TechnicParkEntity) {
        titleLB.text = technic.technicCategory.name
        descriptionLB.text = technic.model
        logoImageView.image = UIImage(named: "Smart_logo")
        if let imageStr = technic.technicCategory.imageBig, let imageURL = URL(string: imageStr) {
            logoImageView.kf.setImage(with: imageURL)
        }
        if technic.technicParkPRSEntity != nil {
            prsLogo.isHidden = false
        } else {
            prsLogo.isHidden = true
        }
    }
    
}
