//
//  TechnicParkDetailCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/12/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class TechnicParkDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var descriptionLB: UILabel!
   
    func setTechnicParkEntity(entity: TechnicParkEntity) {
        if let imageStr = entity.technicCategory.image, let imageUrl = URL(string: imageStr) {
            imageView.kf.setImage(with: imageUrl)
        }
        titleLB.text = entity.technicCategory.name
        descriptionLB.text = entity.model
    }
    
}
