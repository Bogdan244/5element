//
//  RequestServiceDetailStatusCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/11/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class RequestServiceDetailStatusCell: UITableViewCell {

    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLB: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var serviceContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        serviceContentView.layer.shadowColor = UIColor.black.cgColor
        serviceContentView.layer.shadowOffset = CGSize(width: 0, height: 5)
        serviceContentView.layer.shadowOpacity = 0.1
        serviceContentView.layer.cornerRadius = 2
    }
    
    func setService(service: ServiceEntity) {
        switch service.type {
        case .prsRequest:
            dateLB.text = "\(service.createdAt)"
            title.text = "\(service.productCategory)"
            descriptionLB.text = "\(service.name)"
            if service.shoppingTime.count > 0 {
                startDate.text = "Сервис приобретен: \(service.shoppingTime)"
            } else {
                startDate.text = ""
            }
            if service.shoppingTime.count > 0 {
                endDate.text = "Дата окончания сервиса: \(service.expired)"
            } else {
                endDate.text = ""
            }
        case .technicStatus:
            dateLB.text = ""
            title.text = ""
            descriptionLB.text = service.status
            startDate.text = ""
            endDate.text = ""
        default:
            break
        }
    }
    
}
