//
//  LoginTextField.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import UIKit

class LoginTextField: UITextField {
    
    let bottomInfoLayer = CALayer()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderStyle = .none
        autocorrectionType = .yes
        keyboardType = .default
        returnKeyType = .done
        clearButtonMode = .whileEditing
        contentVerticalAlignment = .center
        
        bottomInfoLayer.backgroundColor = UIColor.white.cgColor
        layer.addSublayer(bottomInfoLayer)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        bottomInfoLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width, height: 1)
    }
    
    func setLeftImage(image: UIImage?) {
        let passwordImageView          = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        passwordImageView.image        = image
        passwordImageView.contentMode  = .scaleAspectFit
        leftView                       = passwordImageView
        leftViewMode                   = .always
    }
}
