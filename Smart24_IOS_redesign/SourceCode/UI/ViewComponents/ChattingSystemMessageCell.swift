//
//  ChattingSystemMessageCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/17/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import MessageKit

protocol ChattingSystemMessageCellDelegate: class {
    func didTapVideoLink(cell: ChattingSystemMessageCell)
}

class ChattingSystemMessageCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    weak var delegate: ChattingSystemMessageCellDelegate?
    
    func configure(with message: MessageType, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {
        switch message.kind {
        case .custom(let data):
            if let notice = data as? NoticeMessage {
                self.label.attributedText = NSAttributedString(string: notice.text,
                                                               attributes: ChattingMessage.attributedForSystemMessage)
            } else if let videoLink = data as? VideoLinkMessage {
                addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapCell)))
                self.label.attributedText = NSAttributedString(string: videoLink.text,
                                                               attributes: ChattingMessage.attributedForVideoLinkMessage)
            } else {
                fatalError("fatal Error with custom cell")
            }
        default:
            break
        }
    }
    
    @objc func tapCell() {
        delegate?.didTapVideoLink(cell: self)
    }
 
}
