//
//  RepairsStatusCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/15/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class RepairsStatusCell: UITableViewCell {

    @IBOutlet weak var techniqueLabel: UILabel!
  //  @IBOutlet weak var diagnosticResultLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    let boldAttr = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)]
    let defaultAttr = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
    
    func configurateCell(with status: Status) {
        
        techniqueLabel.attributedText = self.createText(boldText: "Техника: ", nonBoldText: status.technic)
        //diagnosticResultLabel.attributedText = self.createText(boldText: "Результат диагностики: ", nonBoldText: status.diagnosticResult)
        statusLabel.attributedText = self.createText(boldText: "Статус: ", nonBoldText: status.status, color: UIColor(red: 50/255, green: 205/255, blue: 50/255, alpha: 1))
    }
    
    private func createText(boldText: String, nonBoldText: String, color: UIColor = .black) -> NSAttributedString {
        let boldAttr = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)]
        let defaultAttr = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15),
                           NSAttributedString.Key.foregroundColor: color]
        let attrString = NSMutableAttributedString(string: boldText, attributes: boldAttr)
        let attrString2 = NSMutableAttributedString(string: nonBoldText, attributes: defaultAttr)
        attrString.append(attrString2)
        return attrString
    }
}
