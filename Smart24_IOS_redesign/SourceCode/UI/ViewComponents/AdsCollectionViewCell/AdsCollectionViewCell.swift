//
//  AdsCollectionViewCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 7/8/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

class AdsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var adsImageView: UIImageView!
    
    var ads: Ads! {
        didSet {
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(redirect)))
        }
    }
    
    func configurate(with ads: Ads) {
        self.ads = ads
        if let url = URL(string: ads.imageLink) {
            adsImageView.kf.setImage(with: url)
        }
    }
    
    @objc func redirect() {
        if let url = URL(string: ads.redirectLink) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
}

