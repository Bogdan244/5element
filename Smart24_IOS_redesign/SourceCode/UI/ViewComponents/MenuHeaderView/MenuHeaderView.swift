//
//  MenuHeaderView.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/23/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher

class MenuHeaderView: UIView, NibLoadableView {
    
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var settingsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userAvatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        userAvatarImageView.backgroundColor = UIColor.gray
    }
    
    func updateViewWithModel(model: AnyObject?) {
        guard let owner = model as? UserEntity else {
            return
        }
        
        usernameLabel.text = owner.firstname

        if let userAvatarUrl = URL(string: owner.userPhoto) {
            userAvatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                            placeholder: nil,
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
        }
    }
}

