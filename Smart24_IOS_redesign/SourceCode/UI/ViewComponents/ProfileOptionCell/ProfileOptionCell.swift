//
//  ProfileOptionCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ProfileOptionCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateViewWithModel(model: AnyObject) {
        guard let services = model as? ActiveServicesEntity else { return }
        titleLabel.text         = services.name
        let dateString          = services.expired_time.getStringDate(dateFormat: "dd.MM.yyyy")
        descriptionLabel.text   = "Сервис действительный до \(dateString)"
    }
}
