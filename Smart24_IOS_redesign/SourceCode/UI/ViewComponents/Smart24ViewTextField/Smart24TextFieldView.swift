//
//  Smart24TextFieldView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/4/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit
import BarcodeScanner

class Smart24TextFieldView: UIView {

    @IBOutlet weak var textField: Smart24TextField!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var rightBarcodeButton: UIButton?
    @IBOutlet weak var phonePlaceholder: Smart24TextField!
    
    var isComplete: ((Bool) -> Void)? {
        didSet {
           textField.isComplete = isComplete
        }
    }
    
    var state: Smart24TextFieldState = .undef {
        didSet {
            setupRightButton()
            textField.textFieldState = state
            setupLayers()
        }
    }
    
    var isEnabled = true {
        didSet {
            alpha = isEnabled ? 1 : 0.5
            isUserInteractionEnabled = isEnabled
        }
    }
    
    var underlineLayer = CALayer()
    var phoneSpaceLayer = CALayer()
    
    private func setupLayers() {
        switch state {
        case .phone(code: _):
            phoneSpaceLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            phoneSpaceLayer.frame = CGRect(x: 0, y: bounds.maxY - 1, width: phonePlaceholder.frame.width, height: 1)
            underlineLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            underlineLayer.frame = CGRect(x: textField.frame.minX, y: bounds.maxY - 1, width: frame.width - textField.frame.minX, height: 1)
        default:
            phoneSpaceLayer.removeFromSuperlayer()
            underlineLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            underlineLayer.frame = CGRect(x: 0, y: bounds.maxY - 1, width: frame.width, height: 1)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.addSublayer(underlineLayer)
        layer.addSublayer(phoneSpaceLayer)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        setupLayers()
    }
    
    private func setupRightButton() {
        switch state {
        case .date(time: _), .birthday(date: _):
            rightButton.setImage(UIImage(named: "calendarImage"), for: .normal)
            rightButton.addTarget(self, action: #selector(downArrowAction), for: .touchUpInside)
        case .list(list: _), .searchCities:
            rightButton.setImage(UIImage(named: "downArrowImage"), for: .normal)
            rightButton.addTarget(self, action: #selector(downArrowAction), for: .touchUpInside)
        case .password:
            rightButton.setImage(UIImage(named: "eye"), for: .normal)
            rightButton.addTarget(self, action: #selector(eyeAction), for: .touchUpInside)
        case .otpCode:
            rightButton.setImage(nil, for: .normal)
        case .codeWithQR:
            rightBarcodeButton?.addTarget(self, action: #selector(barcodeAction), for: .touchUpInside)
            rightButton.addTarget(self, action: #selector(crossAction), for: .touchUpInside)
        case .undef:
            break
        case .notEditable:
            rightButton.isHidden = true
        case .searchList(list: _):
            rightButton.setImage(UIImage(named: "downArrowImage"), for: .normal)
        default:
            rightButton.isHidden = true
            rightButton.setImage(UIImage(named: "cross"), for: .normal)
            textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
            textField.didSetText = { [weak self] _ in self?.textFieldDidChange()}
            rightButton.addTarget(self, action: #selector(crossAction), for: .touchUpInside)
        }
    }
    
    @objc func textFieldDidChange() {
        rightButton.isHidden = !(textField.superText?.isEmpty == false)
    }
    
    @objc func downArrowAction() {
        textField.becomeFirstResponder()
    }
    
    @objc func crossAction() {
        textField.crossAction()
        textFieldDidChange()
    }
    
    @objc func eyeAction() {
        textField.eyeAction()
    }
 
    @objc func barcodeAction() {
        PresenterImpl().openBarcodeScannerVC(delegate: self)
    }
}

extension Smart24TextFieldView: BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
    
    func scanner(_ controller: BarcodeScanner.BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        textField.text = code
        controller.navigationController?.popViewController(animated: true)
    }
    
    func scanner(_ controller: BarcodeScanner.BarcodeScannerViewController, didReceiveError error: Error) {
        controller.resetWithError(message: "Произошла ошибка")
    }
    
    func scannerDidDismiss(_ controller: BarcodeScanner.BarcodeScannerViewController) {
        controller.navigationController?.popViewController(animated: true)
    }
}
