//
//  NewsCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher

class NewsCell: UITableViewCell, ReusableView, NibLoadableView  {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var newsImageView: UIImageView!

    func updateViewWithModel(model: AnyObject?) {
        guard let news = model as? NewsEntity else {
            return
        }
        
        titleLable.text = news.title
        dateLabel.text = news.publish_date.getDate(dateFormat: "dd MMM YYYY")

        if let newsUrl = URL(string: news.header_image) {
            newsImageView.kf.setImage(with: ImageResource(downloadURL: newsUrl, cacheKey: nil),
                                            placeholder: nil,
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
        }
    }
}
