//
//  CopyTextButton.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 30.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class CopyTextButton: UIButton {
    
    private var copyText: String = ""
    private let alertText = "Текст добавлен в буфер"
    
    func setCopyText(text: String, for controlEvants: UIControl.Event) {
        copyText = text
        addTarget(self, action: #selector(copyTextAction), for: controlEvants)
    }
    
    @objc private func copyTextAction() {
        UIPasteboard.general.string = copyText
        UIViewController.showAlert(with: alertText)
    }
    
    func copyText(text: String?) {
        UIPasteboard.general.string = text
        UIViewController.showAlert(with: alertText)
    }

}
