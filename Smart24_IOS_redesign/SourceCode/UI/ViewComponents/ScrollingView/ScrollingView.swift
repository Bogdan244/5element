//
//  ScrollingView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/26/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class ScrollingView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    static func instantiate(image: UIImage, title: String, text: NSAttributedString) -> ScrollingView {
        let view: ScrollingView = initFromNib()
        view.imageView.image = image
        view.textLabel.attributedText = text
        view.titleLabel.text = title
        view.textLabel.textAlignment = NSTextAlignment.center
        return view
    }
    
    static var technicalSupportView: ScrollingView {
        let boldAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16) as Any]
        let defaultAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16) as Any]
        let attrString = NSMutableAttributedString(string: "Удаленная", attributes: boldAttr)
        let attrString2 = NSMutableAttributedString(string: " техническая\nподдержка", attributes: defaultAttr)
        attrString.append(attrString2)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.5
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        return ScrollingView.instantiate(image: UIImage(named: "greetingFirst")!, title: "ТЕХНИЧЕСКАЯ ПОДДЕРЖКА\n24/7", text: attrString)
    }

    static var stockBonusesPresents: ScrollingView {
        let boldAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16) as Any]
        let defaultAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16) as Any]
        let attrString = NSMutableAttributedString(string: "Не упусти", attributes: defaultAttr)
        let attrString2 = NSMutableAttributedString(string: " выгоду!\n", attributes: boldAttr)
        attrString.append(attrString2)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.5
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        return ScrollingView.instantiate(image: UIImage(named: "greetingSecond")!, title: "АКЦИИ, БОНУСЫ,\nПОДАРКИ", text: attrString)
    }
 
    static var evaluateTheConsultation: ScrollingView {
        let boldAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 16) as Any]
        let defaultAttr = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16) as Any]
        let attrString = NSMutableAttributedString(string: "Присылай", attributes: boldAttr)
        let attrString2 = NSMutableAttributedString(string: " предложения\nпо улучшению сервиса", attributes: defaultAttr)
        attrString.append(attrString2)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.5
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        return ScrollingView.instantiate(image: UIImage(named: "greetingThird")!, title: "ОЦЕНИВАЙ\nКОНСУЛЬТАЦИИ!", text: attrString)
    }
    
}
