//
//  AdsView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 3/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class AdsView: UIView {
    @IBOutlet weak var adsCollectionView: UICollectionView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    var timer: Timer?
    var ads: [Ads] = []
    var currentPage: Int = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        adsCollectionView.delegate = self
        adsCollectionView.dataSource = self
    }
    
    func setAds(ads: [Ads]) {
        if ads.count > 1 {
            self.ads = [ads.last!] + ads + [ads.first!]
            adsCollectionView.setContentOffset(CGPoint(x: adsCollectionView.frame.width * CGFloat(currentPage), y: 0), animated: false)
            startTimer()
            leftButton.isHidden = false
            rightButton.isHidden = false
        } else {
            self.ads = ads
            leftButton.isHidden = true
            rightButton.isHidden = true
        }
        adsCollectionView.reloadData()
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(handlerTimer), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func resetTimer(){
        stopTimer()
        startTimer()
    }
    
    @IBAction func leftScrollButton(_ sender: Any) {
        resetTimer()
        if ads.count > 1 {
            adsCollectionView.setContentOffset(CGPoint(x: CGFloat(currentPage - 1) * adsCollectionView.frame.width, y: 0), animated: true)
        }
    }
    
    @IBAction func rightScrollButton(_ sender: Any) {
        resetTimer()
        if ads.count > 1 {
            adsCollectionView.setContentOffset(CGPoint(x: CGFloat(currentPage + 1) * adsCollectionView.frame.width, y: 0), animated: true)
        }
    }
    
    @objc func handlerTimer() {
        adsCollectionView.setContentOffset(CGPoint(x: adsCollectionView.contentOffset.x + adsCollectionView.frame.width, y: 0), animated: true)
    }
}

extension AdsView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ads.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdsCollectionViewCell", for: indexPath) as! AdsCollectionViewCell
        cell.configurate(with: ads[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let numberOfCells = ads.count
        if numberOfCells == 1 {
            return
        }
        currentPage = Int((scrollView.contentOffset.x / frame.width).rounded())
        if scrollView.contentOffset.x >= scrollView.frame.size.width * CGFloat(numberOfCells - 1) {
            scrollView.contentOffset = CGPoint(x: scrollView.frame.size.width, y: 0)
        } else if scrollView.contentOffset.x <= 0 {
            scrollView.contentOffset = CGPoint(x: scrollView.frame.size.width * CGFloat(numberOfCells - 2), y: 0)
        }
    }
    
    public func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        stopTimer()
    }
    
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        startTimer()
    }
}
