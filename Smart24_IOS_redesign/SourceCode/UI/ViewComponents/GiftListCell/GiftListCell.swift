//
//  GiftListCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/10/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class GiftListCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var selectedCellView: UIView!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        cellView.layer.shadowOpacity = 0.1
        cellView.layer.shadowRadius = 3.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configurate(gift: UserGift) {
        titleLabel.text = gift.title
        descriptionLabel.text = gift.description
        selectedCellView.isHidden = gift.viewed == 1 ? true : false 
    }
    
}
