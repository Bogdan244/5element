//
//  MainMenuCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/23/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class MainMenuCell: UITableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var rightBadge: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
    
    func showBadge(number: Int) {
        configurateBadge()
        rightBadge.text = "\(number)"
        if number > 0 {
            rightBadge.isHidden = false
        } else {
            rightBadge.isHidden = true
        }
    }
    
    func hideBadge() {
        rightBadge.isHidden = true
    }
    
    private func configurateBadge() {
        rightBadge.layer.cornerRadius = rightBadge.frame.size.width / 2
        rightBadge.layer.borderWidth = 1
        rightBadge.layer.borderColor = UIColor.white.cgColor
        rightBadge.clipsToBounds = true
        rightBadge.adjustsFontSizeToFitWidth = true
    }
}
