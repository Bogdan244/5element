//
//  Smart24TextField.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/29/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

enum Smart24TextFieldState {
    case undef
    case standart
    case phone(code: String)
    case password
    case otpCode
    case time(time: Date?, minimumDate: Date?)
    case list(list: [String])
    case bonusCard
    case date(time: Date?, minimumDate: Date?)
    case codeWithQR
    case email
    case searchList(list: [String])
    case searchCities
    case serviceNumber
    case notEditable
    case imei
    case noNumber
    case cyrillicOnly
    case birthday(date: Date?)
}

class Smart24TextField: UITextField {
    
    lazy var pickerView = UIPickerView()
    lazy var datePickerView = UIDatePicker()
    
    // work only for <searchCities>
    var didPickCity: ((CityEntity) -> Void)?
    // work only for <time(time: Date?, minimumDate: Date?)>
    var didChangeDate: ((Date) -> Void)?
    // work only for <serviceNumber>
    var isComplete: ((Bool) -> Void)?
    
    var didSetText: ((String?) -> Void)?
    
    var isImeiAlertShowing = false
    var textFieldState: Smart24TextFieldState = .undef {
        didSet {
            setup()
        }
    }
    
    var superText: String? {
        return super.text
    }
    
    override var text: String? {
        set {
            switch textFieldState {
            case .cyrillicOnly:
                if newValue?.isCyrillic ?? true {
                    super.text = newValue
                    didSetText?(newValue)
                }
            default:
                super.text = newValue
                didSetText?(newValue)
            }
        }
        get {
            switch textFieldState {
            case .phone(let code):
                return code + super.text!.replacingOccurrences(of: " ", with: "")
            case .bonusCard:
                return super.text!.replacingOccurrences(of: " ", with: "")
            default:
                return super.text
            }
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        keyboardAppearance = .dark
        clipsToBounds = false
        
        switch textFieldState {
        case .cyrillicOnly:
            self.delegate = self
        case .noNumber:
            self.delegate = self
        case .phone(_):
            self.delegate = self
            keyboardType = .phonePad
        case .password:
            isSecureTextEntry = true
        case .otpCode:
            keyboardType = .numberPad
            if #available(iOS 12.0, *) {
                textContentType = .oneTimeCode
            }
        case .time(let date, let minimumDate):
            keyboardAppearance = .default
            datePickerView.minimumDate = minimumDate
            datePickerView.datePickerMode = UIDatePicker.Mode.time
            datePickerView.minuteInterval = 1
            datePickerView.setTextColor(color: .white)
            tintColor = UIColor.clear
            inputView = datePickerView
            inputView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
            if let dateValue = date {
                datePickerView.setDate(dateValue, animated: false)
                handleTimePicker(sender: datePickerView)
            }
            addOkButton()
        case .date(time: let date, minimumDate: let minimumDate):
            keyboardAppearance = .default
            datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
            datePickerView.minuteInterval = 30
            datePickerView.minimumDate = minimumDate
            datePickerView.setTextColor(color: .white)
            tintColor = UIColor.clear
            inputView = datePickerView
            inputView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            if let dateValue = date {
                datePickerView.setDate(dateValue, animated: false)
                handleDatePicker(sender: datePickerView)
            }
            addOkButton()
        case .birthday(date: let date):
            keyboardAppearance = .default
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            datePickerView.timeZone = TimeZone(secondsFromGMT: 0)
            datePickerView.setTextColor(color: .white)
            tintColor = UIColor.clear
            inputView = datePickerView
            inputView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            datePickerView.addTarget(self, action: #selector(handleBirthdayPicker(sender:)), for: .valueChanged)
            if let dateValue = date {
                datePickerView.setDate(dateValue, animated: false)
                handleBirthdayPicker(sender: datePickerView)
            }
            addOkButton()
        case .list(_):
            delegate = self
            keyboardAppearance = .default
            pickerView.dataSource = self
            pickerView.delegate = self
            tintColor = UIColor.clear
            inputView = pickerView
            inputView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            addOkButton()
        case .bonusCard:
            delegate = self
            keyboardType = .numberPad
        case .codeWithQR:
            delegate = self
            keyboardType = .default
        case .email:
            keyboardType = .emailAddress
        case .searchList(list: _), .searchCities:
            delegate = self
        case .serviceNumber:
            delegate = self
            keyboardType = .numberPad
        case .notEditable:
            isUserInteractionEnabled = false
        case .imei:
            delegate = self
        default:
            break
        }
    }
    
    private func addOkButton() {
        let toolBar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        toolBar.barStyle = UIBarStyle.black
        let flexsibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(okButtonHandler))
        doneButton.tintColor = .white
        toolBar.items = [flexsibleSpace, doneButton]
        inputAccessoryView = toolBar
    }
    
    @objc func okButtonHandler() {
        resignFirstResponder()
    }
    
    func serverDate() -> String {
        let date = datePickerView.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    @objc func handleTimePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        text = dateFormatter.string(from: sender.date)
        didChangeDate?(sender.date)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMM HH:mm"
        text = dateFormatter.string(from: sender.date)
    }
    
    @objc func handleBirthdayPicker(sender: UIDatePicker) {
        text = DateFormatter.bithdayFormatter.string(from: sender.date)
        didChangeDate?(sender.date)
    }
    
    @objc func crossAction() {
        text = ""
    }
    
    @objc func eyeAction() {
        isSecureTextEntry = !isSecureTextEntry
        if isFirstResponder {
            becomeFirstResponder()
        }
    }

}

extension Smart24TextField: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textFieldState {
        case .imei:
            guard let vc = UIApplication.shared.keyWindow?.rootViewController else { return true }
            if !isImeiAlertShowing {
                vc.showAlertMessage(attributedString: Constants.imeiAttributed, complition: { [weak self] in
                    self?.isImeiAlertShowing = true
                    self?.becomeFirstResponder()
                }, cancel: nil)
            }
            return isImeiAlertShowing
        default:
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textFieldState {
        case .list(list: let list):
            if pickerView.selectedRow(inComponent: 0) == 0 {
                textField.text = list[0]
            }
        case .searchList(list: let items):
            resignFirstResponder()
            PresenterImpl().openSearchVC(items: items, delegate: self)
        case .searchCities:
            resignFirstResponder()
            PresenterImpl().openSearchCitiesVC(delegate: self)
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textFieldState {
        case .cyrillicOnly:
            return string.isCyrillic
        case .noNumber:
            return string.rangeOfCharacter(from: .decimalDigits) == nil
        case .phone(code: _):
            if range.length > 0 {
                return true
            }
            if range.location == 2 || range.location == 6 || range.location == 9 {
                text = super.text! + " " + string
                return false
            }
            if range.location > 11 {
                return false
            }
            return true
        case .bonusCard:
            if range.length > 0 {
                return true
            }
            if range.location == 3 || range.location == 7 || range.location == 11 {
                text = super.text! + " " + string
                return false
            }
            if range.location > 14 {
                return false
            }
            return true
        case .codeWithQR:
            if range.location > 15 {
                return false
            }
                return true
        case .serviceNumber:
            isComplete?(range.location - range.length >= 11)
            if range.location > 11 {
                return false
            }
            return true
        case .imei:
            if range.location > 14 && range.length == 0 {
                return false
            }
            return true
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textFieldState {
        case .imei:
            if textField.text?.rangeOfCharacter(from: .decimalDigits) != nil && textField.text?.count != 15 {
                guard let vc = UIApplication.shared.keyWindow?.rootViewController else { return }
                vc.showAlertMessage(title: nil, message: "Imei-код введен неверно.", complition: { }, cancel: nil)
            }
        default:
            break
        }
    }
    
}

extension Smart24TextField: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch textFieldState {
        case .list(let list):
            return list.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch textFieldState {
        case .list(let list):
            return NSAttributedString(string: list[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor.white as Any])
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch textFieldState {
        case .list(let list):
            list.count > row ? text = list[row] : nil
        default:
            break
        }
    }
}

extension Smart24TextField: SearchViewControllerDelegate {
    func didFindString(searchViewController: SearchViewController, searchString: String) {
        searchViewController.navigationController?.popViewController(animated: true)
        text = searchString
    }
}

extension Smart24TextField: SearchCityViewControllerDelegate {
    func searchCityViewController(searchViewController: SearchCityViewController, didSelect searchCity: CityEntity) {
        searchViewController.navigationController?.popViewController(animated: true)
        text = searchCity.cityName
        didPickCity?(searchCity)
    }

}
