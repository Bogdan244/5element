//
//  TechnicParkDropDownView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/18/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class TechnicParkDropDownView: UIStackView {
    
    enum State {
        case open
        case close
    }
    
    @IBOutlet weak var prsLabel: UILabel!
    @IBOutlet weak var expirePRS: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var state = State.close
    
    override func awakeFromNib() {
        super.awakeFromNib()
        infoView.isHidden = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
    }
    
    @objc func tap() {
        switch state {
        case .close:
            open()
        case .open:
            close()
        }
    }
    
    func open() {
        imageView.image = UIImage(named: "upArrow")
        infoView.isHidden = false
        state = .open
    }
    
    func close() {
        imageView.image = UIImage(named: "downArrow")
        infoView.isHidden = true
        state = .close
    }
    
    func setPRS(technicParkPRS: TechnicParkPRSEntity?) {
        if let prs = technicParkPRS {
            isHidden = false
            expirePRS.text = prs.expired
            prsLabel.text = prs.name
        } else {
            isHidden = true
        }
    }
}
