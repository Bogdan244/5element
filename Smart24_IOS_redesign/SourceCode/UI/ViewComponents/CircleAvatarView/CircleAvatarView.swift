//
//  CircleAvatarView.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/27/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher
import Foundation

class CircleAvatarView: UIView {
    
    let countButton                 = UIButton(type: .custom)
    let changePhotoButton           = UIButton()
    
    private let avatarImageView     = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        changePhotoButton.frame = bounds
        changePhotoButton.backgroundColor = UIColor.white
        changePhotoButton.setImage(R.image.black_photo_icon(), for: .normal)
        
        avatarImageView.frame  = bounds
        avatarImageView.layer.borderWidth = 2
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarImageView.center = CGPoint(x: changePhotoButton.frame.midX,
                                         y: changePhotoButton.frame.midY)
        
        countButton.setBackgroundImage(R.image.red_circle_icon(), for: .normal)
        countButton.setTitle("0", for: .normal)
        countButton.titleLabel?.adjustsFontSizeToFitWidth = true
        countButton.layer.borderWidth = 2
        countButton.layer.borderColor = UIColor.white.cgColor
        countButton.frame = CGRect(x: changePhotoButton.frame.minX,
                                   y: changePhotoButton.frame.maxY * 0.65,
                                   width: 25,
                                   height: 25)
        
        addSubview(changePhotoButton)
        addSubview(avatarImageView)
        addSubview(countButton)
        
        avatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        changePhotoButton.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        countButton.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        
        backgroundColor = UIColor.clear
    }
    
    func updateViewWithModel(model: AnyObject?) {
        guard let userEntity = model as? UserEntity else {
            return
        }
        
        if let userAvatarUrl = URL(string: userEntity.userPhoto) {
            avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                        placeholder: nil,
                                        options: nil,
                                        progressBlock: nil,
                                        completionHandler: nil)
        }
    }
}
