//
//  AddTechnicSectionHeaderView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/13/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

protocol AddTechnicSectionHeaderViewDelegate: class {
    
    func addTechnicSectionHeaderView(_ view: AddTechnicSectionHeaderView, didSelect section: Int)
    
}

class AddTechnicSectionHeaderView: UITableViewHeaderFooterView {

    weak var delegate: AddTechnicSectionHeaderViewDelegate?
    var section: Int!
    
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var dropDownImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundImageView.makeCircle()
        backgroundImageView.clipsToBounds = true
        backgroundImageView.backgroundColor = UIColor(red: 207/255, green: 43/255, blue: 43/255, alpha: 1)
        headerContentView.makeCircle()
        headerContentView.layer.shadowColor = UIColor.black.cgColor
        headerContentView.layer.shadowOffset = CGSize(width: 0, height: 5)
        headerContentView.layer.shadowOpacity = 0.1
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickHandler)))
    }
    
    @objc func clickHandler() {
        delegate?.addTechnicSectionHeaderView(self, didSelect: section)
    }
    
    func configurate(with techicCategory: TechnicCategories, section: Int) {
        titleLB.text = techicCategory.name
        imageView.image = UIImage(named: "dropDownArrow")
        if let imageUrl = URL(string: techicCategory.image) {
            imageView.kf.setImage(with: imageUrl)
        }
        self.section = section
        
    }
    
    static func instantiate(imageStr: String?, title: String, section: Int) -> AddTechnicSectionHeaderView {
        let view: AddTechnicSectionHeaderView = initFromNib()
        view.titleLB.text = title
        view.section = section
        //view.bounds = bounds
        if let imageStrValue = imageStr, let imageUrl = URL(string: imageStrValue) {
            view.imageView.kf.setImage(with: imageUrl)
        }
        return view
    }
    
}
