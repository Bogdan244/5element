//
//  Smart24TimeView.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/4/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class Smart24TimeView: UIView {
    
    @IBOutlet weak var startTimeTextField: Smart24TextField!
    @IBOutlet weak var endTimetextField: Smart24TextField!
    var underlineLayer = CALayer()
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.addSublayer(underlineLayer)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        underlineLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        underlineLayer.frame = CGRect(x: 0, y: layer.bounds.maxY - 1, width: layer.frame.width, height: 1)
    }
    
    func setupDates(start: Date, end: Date) {
        startTimeTextField.textFieldState = .time(time: start, minimumDate: start)
        startTimeTextField.didChangeDate = { [weak self] date in
            guard let self = self else { return }
            let newDate = date.addingTimeInterval(1800)
            self.endTimetextField.textFieldState = .time(time: nil, minimumDate: newDate)
            if self.endTimetextField.datePickerView.date.timeIntervalSince(date) < 1800 {
                self.endTimetextField.textFieldState = .time(time: newDate, minimumDate: newDate)
            }
        }
        endTimetextField.textFieldState = .time(time: end, minimumDate: end)
    }
    
}
