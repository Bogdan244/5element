//
//  ProfileHeaderView.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/27/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView, NibLoadableView {
    
    @IBOutlet weak var circleAvatarView: CircleAvatarView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!
    @IBOutlet weak var activeSarvicesLabel: UILabel!
    
    func updateViewWithModel(model: AnyObject?) {
        guard let userEntity = model as? UserEntity else {
            return
        }
        
        userNameLabel.text = userEntity.firstname
        userPhoneLabel.text = userEntity.phone
        
        circleAvatarView.updateViewWithModel(model: userEntity)
    }
    
    func setMessagesCount(count: Int?) {
        if let count = count, count != 0 {
            circleAvatarView.countButton.setTitle(String(count), for: .normal)
            circleAvatarView.countButton.isHidden = false
        } else {
            circleAvatarView.countButton.isHidden = true
        }
    }
}
