//
//  RequestServiceCell.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 1/29/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import UIKit

class RequestServiceCell: UITableViewCell {

    @IBOutlet weak var serviceContentView: UIView!
    @IBOutlet weak var actNumberLB: UILabel!
    @IBOutlet weak var workStatusLB: UILabel!
    @IBOutlet weak var requestLB: UILabel!
    @IBOutlet weak var descriptionLB: UILabel!
    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var historyBT: UIButton!
    @IBOutlet weak var updateBT: UIButton!
    
    var didSelectHistory: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        serviceContentView.layer.shadowColor = UIColor.black.cgColor
        serviceContentView.layer.shadowOffset = CGSize(width: 0, height: 5)
        serviceContentView.layer.shadowOpacity = 0.1
        serviceContentView.layer.cornerRadius = 2
        workStatusLB.layer.cornerRadius = 10
        workStatusLB.clipsToBounds = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        updateBT.isHidden = false
        workStatusLB.backgroundColor = .white
        descriptionLB.text = ""
        dateLB.text = ""
        requestLB.text = ""
        actNumberLB.text = "" 
    }
    
    func configurate(service: ServiceEntity) {
        switch service.type {
        case .prsRequest:
            requestLB.text = service.productCategory
            descriptionLB.text = service.name
            if service.expired.count > 0 {
                dateLB.text = "Сервис действительный до \(service.expired)"
            } else {
                dateLB.text = ""
            }
            
            actNumberLB.text = service.code
            updateBT.isHidden = false
        case .technicStatus:
            descriptionLB.text = ""
            requestLB.text = service.technic
            actNumberLB.text = service.actNumber
            updateBT.isHidden = true
        default:
            break
        }
        workStatusLB.text = "   \(service.statusShort)   "
        if service.statusShort == "Завершено" {
            workStatusLB.backgroundColor = UIColor(red: 117/255, green: 183/255, blue: 107/255, alpha: 1)
        } else {
            workStatusLB.backgroundColor = UIColor(red: 254/255, green: 182/255, blue: 59/255, alpha: 1)
        }
    }
    
    @IBAction func historyButton(_ sender: Any) {
        didSelectHistory?()
    }
    
}
