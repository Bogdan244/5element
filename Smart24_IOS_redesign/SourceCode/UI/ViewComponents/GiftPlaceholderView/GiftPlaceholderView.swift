//
//  GiftPlaceholder.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 19.03.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation

class GiftPlaceholderView: UIView, NibLoadableView {
    
    var activateAction: (() -> Void)?
    
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activateButton.layer.cornerRadius = 15
        activateButton.layer.shadowColor = UIColor(rgb: 0xCF2B2B).cgColor
        activateButton.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        activateButton.layer.shadowOpacity = 0.4
        activateButton.layer.shadowRadius = 5.0
        let attributedString = NSMutableAttributedString(string: "Что бы получить подарок активируйте сервис \"Защита+\" в своем профиле")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        textLabel.attributedText = attributedString
    }
    
    @IBAction func activateAction(_ sender: Any) {
        activateAction?()
    }
    
}
