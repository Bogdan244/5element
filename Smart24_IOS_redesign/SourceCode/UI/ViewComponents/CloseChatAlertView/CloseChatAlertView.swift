//
//  CloseChatAlertView.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/6/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class CloseChatAlertView: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var goodServiceButton: UIButton!
    @IBOutlet weak var badServiceButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var backgroundAlertView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var goodServiceImageView: UIImageView!
    @IBOutlet weak var badServiceImageView: UIImageView!
    
    var dialogId = ""
    
    @IBAction func closeButtonAction(_ sender: Any) {
        view.removeFromSuperview()
        removeFromParent()
        messageTextView.text = Constants.commentPlaceholder
    }
    
    @IBAction func goodServiceButtonAction(_ sender: Any) {
        goodServiceImageView.image = R.image.circle_blue_check_icon()
        badServiceImageView.image = R.image.circle_white_check_icon()
        currentGrade = ChatGrade.good
    }
    
    @IBAction func badServiceButtonAction(_ sender: Any) {
        goodServiceImageView.image = R.image.circle_white_check_icon()
        badServiceImageView.image = R.image.circle_blue_check_icon()
        currentGrade = ChatGrade.bad
    }
    
    var currentGrade = ChatGrade.good
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        backgroundAlertView.hideKeyboardWhenTappedAround()
        
        messageTextView.text = Constants.commentPlaceholder
        messageTextView.textColor = UIColor.lightGray
        messageTextView.delegate = self
        
        backgroundAlertView.layer.masksToBounds = true
        backgroundAlertView.layer.cornerRadius = 10
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }
    
    func getMessage() -> String? {
        if messageTextView.text != "" && messageTextView.text != Constants.commentPlaceholder {
            return messageTextView.text
        }
        
        return nil
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Constants.commentPlaceholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func show(message: String) {
        
        if let topVC = UIApplication.topViewController() {
            view.frame = topVC.view.bounds
            
            topVC.addChild(self)
            topVC.view!.addSubview(view)
            didMove(toParent: topVC)
            
            questionLabel.text = message
        }
    }
    
}
