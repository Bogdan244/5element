//
//  Constants.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation

enum Constants {
    
    //TEST SERVER
//    static let BASE_URL          = "https://sodevb.so24.net/api"
//    static let BONUS_URL         = "https://sodevb.so24.net/base/api/bonuses/"
//    static let GIFTS_URL         = "https://sodevb.so24.net/base/api/gifts/"
//    static let MARTLET_URL       = "https://sodevb.so24.net/base/api/martlet/"
//    static let CALLBACK_URL      = "https://sodevb.so24.net/base/api/"
//    static let AUTH_URL          = "https://sodevb.so24.net/base/api/"
//    static let NOTIFICATIONS_URL = "https://sodevb.so24.net/base/api/notifications"
//    static let BASE5ELEMEMT_URL  = "https://sodevb.so24.net/base/api/five_element"
//    static let client_secret     = "Vgwptt1uHSG1uylODbppkplYQJvoMYaqwoSi6bGl"
//    static let client_id =  3
    
    //TEST SERVER 2
//    static let BASE_URL          = "https://sodevp.so24.net/api"
//    static let BONUS_URL         = "https://sodevp.so24.net/base/api/bonuses/"
//    static let GIFTS_URL         = "https://sodevp.so24.net/base/api/gifts/"
//    static let MARTLET_URL       = "https://sodevp.so24.net/base/api/martlet/"
//    static let CALLBACK_URL      = "https://sodevp.so24.net/base/api/"
//    static let AUTH_URL          = "https://sodevp.so24.net/base/api/"
//    static let NOTIFICATIONS_URL = "https://sodevp.so24.net/base/api/notifications"
//    static let BASE5ELEMEMT_URL  = "https://sodevp.so24.net/base/api/five_element"
//    static let client_secret     = "Vgwptt1uHSG1uylODbppkplYQJvoMYaqwoSi6bGl"
//    static let client_id =  3
    
//    INSURANCE TEST SERVER
//        static let BASE_URL          = "https://sotest.so24.net/api"
//        static let BONUS_URL         = "https://sotest.so24.net/base/api/bonuses/"
//        static let GIFTS_URL         = "https://sotest.so24.net/base/api/gifts/"
//        static let MARTLET_URL       = "https://sotest.so24.net/base/api/martlet/"
//        static let CALLBACK_URL      = "https://sotest.so24.net/base/api/"
//        static let AUTH_URL          = "https://sotest.so24.net/base/api/"
//        static let NOTIFICATIONS_URL = "https://sotest.so24.net/base/api/notifications"
//        static let BASE5ELEMEMT_URL  = "https://sotest.so24.net/base/api/five_element"
//        static let client_secret     = "5ju7v4ZHbeb9UGjc1sr8QWfz4saPfKBe5AdTWfLe"
//        static let client_id =  1
    
//     PRODACTION SERVER
//    static let BASE_URL          = "https://so24.net/api"
//    static let BONUS_URL         = "https://so24.net/base/api/bonuses/"
//    static let GIFTS_URL         = "https://so24.net/base/api/gifts/"
//    static let MARTLET_URL       = "https://so24.net/base/api/martlet/"
//    static let CALLBACK_URL      = "https://so24.net/base/api/"
//    static let AUTH_URL          = "https://so24.net/base/api/"
//    static let NOTIFICATIONS_URL = "https://so24.net/base/api/notifications"
//    static let BASE5ELEMEMT_URL  = "https://so24.net/base/api/five_element"
//
//    static let client_secret     = "2wv9AOM6yKdA9LG9I1T8sPv2Aa7mqIJ8CD73EACl"
//    static let client_id         = "8"
    
    // NEW PRODACTION SERVER
//        static let BASE_URL          = "https://sodevb.so24.net/api"
//        static let BONUS_URL         = "https://sodevb-admin.so24.net/api/bonuses/"
//        static let GIFTS_URL         = "https://sodevb-admin.so24.net/api/gifts/"
//        static let MARTLET_URL       = "https://sodevb-admin.so24.net/api/martlet/"
//        static let CALLBACK_URL      = "https://sodevb-admin.so24.net/api/"
//        static let AUTH_URL          = "https://sodevb-admin.so24.net/api/"
//        static let NOTIFICATIONS_URL = "https://sodevb-admin.so24.net/api/notifications"
//        static let BASE5ELEMEMT_URL  = "https://sodevb-admin.so24.net/api/five_element"
//
//        static let client_secret     = "TuoHkudfINMWHIeKDj43plieCQro1c3DWogmNlSF"
//        static let client_id         = "7"
    
    // хуй знает что
//    static let BASE_URL          = "https://sodevp-admin.so24.net/api"
//    static let BONUS_URL         = "https://sodevp-admin.so24.net/api/bonuses/"
//    static let GIFTS_URL         = "https://sodevp-admin.so24.net/api/gifts/"
//    static let MARTLET_URL       = "https://sodevp-admin.so24.net/api/martlet/"
//    static let CALLBACK_URL      = "https://sodevp-admin.so24.net/api/"
//    static let AUTH_URL          = "https://sodevp-admin.so24.net/api/"
//    static let NOTIFICATIONS_URL = "https://sodevp-admin.so24.net/api/notifications"
//    static let BASE5ELEMEMT_URL  = "https://sodevp-admin.so24.net/api/five_element"
//
//    static let client_secret     = "TuoHkudfINMWHIeKDj43plieCQro1c3DWogmNlSF"
//    static let client_id         = "7"

    static let BASE_URL          = "https://partner.so24.net/api"
    static let BONUS_URL         = "https://partner.so24.net/api/bonuses/"
    static let GIFTS_URL         = "https://partner.so24.net/api/gifts/"
    static let MARTLET_URL       = "https://partner.so24.net/api/martlet/"
    static let CALLBACK_URL      = "https://partner.so24.net/api/"
    static let AUTH_URL          = "https://partner.so24.net/api/"
    static let NOTIFICATIONS_URL = "https://partner.so24.net/api/notifications"
    static let BASE5ELEMEMT_URL  = "https://partner.so24.net/api/five_element"

    static let client_secret     = "2wv9AOM6yKdA9LG9I1T8sPv2Aa7mqIJ8CD73EACl"
    static let client_id         = "8"
    
    static let userPrimaryKey = "user"
    
    //app config
    static let appName = "Защита+ IOS"
    static let partnerName = "Защита+ IOS"
    static let versionApp = "VersionApp=1.1.3"
    
    //vkontakte
    static let vkId                     = "6201411"
    //static let vkScope: Set<VK.Scope>   = [.wall]
    static let vkAvatarSize             = "photo_50"
    
    //odnoklassniki
    static let okAppKey  = ""
    static let okAppId   = ""
    
    //google+
    static let gClientID = "134039058855-efrbfvpus8cjcluh17ic09tk0qdkbbt7.apps.googleusercontent.com"
    
    //FaceBook
    static let fbGraphPath = "me"
    static let fbParameters: [String : Any] = ["fields": "id, name, picture"]
    
    static let smart24UA_phone      = "tel://0800600001"
    
    // SIP
    static let sipNumber                = "8012@80.209.239.116"
    static let sipUsername              = "5005"
    static let sipPassword              = "Ahg3Ai3SdSil9K"
    static let sipDomain                = "80.209.239.116"
    static let sipProxy                 = "sip:80.209.239.116"
    static let connectDurationInterval  = 1.0
    static let sipCallStateKey          = "callState"
    
    // RTMP
    static let rtmpUrl                     = "rtmp://rtmp.so24.net/video/"
    static let videoMicrophoneOff: Float   = 0.000001
    static let videoMicrophoneOn: Float    = 1
    static let startVideoMessage           = "start video"
    
    static let firstRun             = "firstRun"
    static let savedPhone           = "savedPhone"
    static let savedPassword        = "savedPassword"
    
    static let currentUserId        = "id"
    static let lastBackupDate       = "lastBackupDate"
    static let backupPeriod         = "backupPeriod"
    static let setOnBackupPeriod    = "setOnBackupPeriod"
    static let dialog_id            = "dialogId"
    static let emptyDialogId        = "0"
    static let timestamp            = "timestamp"
    static let keyChainValue        = "key"
    static let scrollObserverKey    = "contentSize"
    static let expertCallId         = "expertCallId"
    
    static let phoneCharacters      = "+0123456789"
    static let phoneCharactersCount = 13
    static let teamViewerQS         = "https://itunes.apple.com/us/app/teamviewer-quicksupport/id661649585"
    static let teamViewLink         = "https://www.teamviewer.com/ru/download/ios/"
    static let wezomLink            = "https://wezom.mobi/"
    static let freshDigitalLink     = "http://www.fresh-d.net/"
    static let smartlLink           = "https://so24.net"
    
    // error types
    static let nodialogError        = 31
    static let notAuthError         = 20
    static let userNotFoundError    = 10
    
    static let notificationError        = "Не удалось удалить уведомление, проверте интернет соединение"
    static let notificationsExist       = "Сообщения и уведомления"
    static let notificationsNotExist    = "Сообщения и уведомления отсутствуют."
    static let servicesExist            = "Ваши активные сервисы"
    static let servicesNotExist         = "Нет активных сервисов."
    
    // messages
    static let ok                       = "Ок"
    static let goToChat                 = "Перейти"
    static let error                    = "Ошибка"
    static let unreachableMessage       = "У Вас отключен Интернет. Часть функций будет недоступна!"
    static let unreachableCreateBackup  = "У Вас отключен Интернет. Невозможно сохранить контакты!"
    static let unreachableRestoreBackup = "У Вас отключен Интернет. Невозможно восстановить контакты!"
    static let allowAccess              = "Разрешить доступ"
    static let cantCallSIPMessage       = "Не удалось дозвониться к оператору"
    static let cantStartVideoMessage    = "Не удалось создать видеоконференцию, попробуйте позже."
    static let cantSendMessage          = "Не удалось отправить сообщение, проверте интернет подключение"
    static let shialdMessage            = "Ваше устройство защищено! Вы можете управлять устройством и отслеживать его из личного кабинета на сайте so24.net (должен быть включен Интернет)"
    static let closeCallMessage         = "Не удалось подключится к серверу, проверте интернет соединение."
    static let errorGettingData         = "Не удалось получить данные с сервера"
    static let registrationMessage      = "Регистрация прошла успешно"
    static let savedMessage             = "Данные успешно сохранены"
    static let authorizationFailMessage = "Не удалось авторизироваться"
    static let backupLastActivation     = "Последняя активация"
    static let backupErrorMessage       = "Ошибка сохранения данных. Облачное хранилище недоступно."
    static let dayBackupMessage         = "Один раз в день"
    static let weekBackupMessage        = "Один раз в неделю"
    static let monthBackupMessage       = "Один раз в месяц"
    static let yearBackupMessage        = "Один раз в год"
    static let contactsSaving           = "Сохранение контактов..."
    static let restoreContacts          = "Восстановление контактов..."
    static let backupButtonState        = "backupButtonState"
    static let backupButtonDayIndex     = "backupButtonDayIndex"
    static let backupDate               = "backupDate"
    static let backupDateFormat         = "dd MMM YYYY, H:mm"
    static let profileDateFormat        = "dd.MM.yyyy"
    static let phoneModel               = "iPhone"
    static let notExixstChatMessage     = "У вас нет активных диалогов"
    static let dialogClosedSuccess      = "Диалог был успешно завершен"
    static let dialogClosedByOperator   = "Диалог был завершен оператором"
    static let dialogClosed             = "Диалог был завершен"
    static let newMessageFromOperator   = "Новое сообщение от эксперта"
    static let closeChatMessage         = "Завершить активный чат с экспертом?"
    static let closeChatQuestion        = "Закрыть чат с экспертом?"
    static let evaluateDialog           = "Оцените диалог с экспертом"
    static let commentPlaceholder       = "Ваш комментарий"
    static let enterSMSCode             = "Введите код из SMS"
    static let dataUpdating             = "Обновление данных"
    static let confirmBackupMessage     = "Вы уверены что хотите сохранить ваши контакты на сервер?"
    static let confirmRestoreBackup     = "Вы уверены что хотите восстановить ваши контакты?"
    
    // controllersTitle
    static let backupPeriodTitle        = "Период копирования"
    static let chatVCTitle              = "Центр поддержки"
    static let forgotPasswordVC         = "Восстановление пароля"
    static let howItWorkVCTitle         = "Акции"
    static let newsVCTitle              = "Магазины"
    static let registrationVCTitle      = "Регистрация"
    
    // ChangeDataViewController
    static let changePhoto              = "Изменить фото"
    static let camera                   = "Камера"
    static let galery                   = "Галерея"
    static let close                    = "Отменить"
    static let editingTitle             = "Редактирование"
    
    // MenuTableViewController
    static let profile                  = "Профиль"
    static let bonuses                  = "Мои бонусы"
    static let backup                   = "Резервная копия"
    static let howItWork                = "Акции"
    static let utility                  = "Утилита"
    static let developers               = "Разработчики"
    static let exit                     = "Выход"
    static let gifts                    = "Мои подарки"
    static let services                 = "Мои сервисы"
    
    // ProfileViewController
    static let activationSuccess        = "Сервис успешно активирована"
    static let activationFailed         = "Не удалось активировать сервис"
    static let enterActivationCode      = "Введите уникальный номер сервиса"
    static let enterActivationCodePlaceholder      = "Уникальный номер сервиса"
    static let scanCodeAction      = "Сканировать код"
    static let activationCode           = "Код активации:"
    static let delete                   = "УДАЛИТЬ"
    
    // VideoViewController
    static let continueChat            = "Продолжить чат"
    static let closeChat               = "Завершить"
    static let send                    = "Отправить"
    
    static let imei                    = "Для ввода IMEI-кода устройства зайдите в меню настроек телефона, перейдите в «Общие», а затем в меню «О телефоне», длительно нажмите на поле «IMEI» до появления всплывающей кнопки «Скопировать». Скопируйте IMEI-код и вернитесь в приложение «Защита+» на страницу Страхование. Длительно нажмите на поле «Imei-код устройства» и вставьте скопированный код."

    static var imeiAttributed: NSAttributedString {
        let attributedText = NSMutableAttributedString(string: imei)
        var ranges: [NSRange] = []
        ranges.append(NSRange(attributedText.string.range(of: "Общие")!, in: attributedText.string))
        ranges.append(NSRange(attributedText.string.range(of: "IMEI")!, in: attributedText.string))
        ranges.append(NSRange(attributedText.string.range(of: "Скопировать")!, in: attributedText.string))
        ranges.append(NSRange(attributedText.string.range(of: "Защита+")!, in: attributedText.string))
        ranges.append(NSRange(attributedText.string.range(of: "Imei-код устройства")!, in: attributedText.string))
        ranges.forEach {
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Roboto-Bold", size: 14) as Any, range: $0)
        }
        return attributedText
    }
    
}

enum SocialType: Int {
    case googlePlus
    case facebook
    case vk
    case ok
}

enum ChatGrade: String {
    case good = "good"
    case bad = "bad"
}

enum IsBackupExist: String {
    case not = "0"
    case exist = "1"
}

enum SmartDensity: Int {
    case for2x = 7
    case for3x = 12
}

enum IsVideoStart: Int {
    case not
    case start
}

public enum SocialName: String {
    case google
    case facebook
    case vkontakte
    case odnoklassniki
    case apple
}

public enum Field {
    case brand(brand: String)
    case model(brand: String, model: String)
    
    public var rawValue: String {
        switch self {
        case .brand(brand: _):
            return "brand"
        case .model(brand: _, model: _):
            return "model"
        }
    }
}

public enum FeedbackType: String {
    case Thanks = "thanks"
    case Remark = "comment"
    case Suggest = "sentence"
}
