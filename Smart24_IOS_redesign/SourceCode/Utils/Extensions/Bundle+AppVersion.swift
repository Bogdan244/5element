//
//  Bundle+AppVersion.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/29/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

extension Bundle {
    
    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? "0"
    }
    
    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? "0"
    }

    var appVersion: String {
        return versionNumber + "." + buildNumber
    }
    
}


