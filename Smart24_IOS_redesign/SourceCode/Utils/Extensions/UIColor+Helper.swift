//
//  UIColor+Helper.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/6/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation

extension UIColor {
    convenience private init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    
    convenience init?(hexString: String) {
        let hexColor = hexString.replacingOccurrences(of: "#", with: "")
        guard let hexInt = Int(hexColor, radix: 16) else { return nil }
        self.init(rgb: hexInt)
    }
    
}
