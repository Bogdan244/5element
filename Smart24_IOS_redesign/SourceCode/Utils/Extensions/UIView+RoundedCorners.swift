//
//  UIView+RoundedCorners.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/27/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    func roundedView(border: Bool, borderColor: UIColor?, borderWidth: CGFloat?, cornerRadius: CGFloat?) {
        
        if let radius = cornerRadius {
            layer.cornerRadius = radius
        } else {
            layer.cornerRadius = frame.size.width * 0.5
        }
        
        if border {
            
            if let borderColor = borderColor {
                layer.borderColor = borderColor.cgColor
            } else {
                layer.borderColor = UIColor.black.cgColor;
            }
            
            if let borderWidth = borderWidth {
                layer.borderWidth = borderWidth
            } else {
                layer.borderWidth = 0.0
            }
        }
        
        layer.masksToBounds = true
    }
    
    func makeCorner() {
        layer.cornerRadius = 20
    }
    
    func makeCircle() {
        layer.cornerRadius = frame.height / 2
    }
    
    func addShadow(height: Int, color: UIColor = .black, opacity: Float = 0.1) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: height)
        layer.shadowOpacity = opacity
    }
    
}
