//
//  UIViewController+AlertMessage.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showCustomAlertMessage(title: String? = nil,
                                message: String,
                                firstButtonTitle: String? = nil,
                                secondButtonTitle: String? = nil,
                                firstBTHandler: (() -> Void)?,
                                secondBTHandler: (() -> Void)?,
                                isNeedScrollToBottom: Bool = false,
                                isLinksEnabled: Bool = false) {
        var text = message
        if let titleValue = title, titleValue.count > 0 {
            text = titleValue + "\n" + message
        }
        let alert = CustomAlertViewController.alertVC(alertText: text, okHandler: firstBTHandler, cancelHandler: secondBTHandler, isNeedScrollToBottom: isNeedScrollToBottom, isLinksEnabled: isLinksEnabled)
        if let firstButtonTitleValue = firstButtonTitle {
            alert.okButtonTitle = firstButtonTitleValue
        }
        if let secondButtonTitleValue = secondButtonTitle {
            alert.cancelButtonTitle = secondButtonTitleValue
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func showCustomAlertMessage(attributedString: NSAttributedString? = nil,
                                firstButtonTitle: String? = nil,
                                secondButtonTitle: String? = nil,
                                firstBTHandler: (() -> Void)?,
                                secondBTHandler: (() -> Void)?,
                                isNeedScrollToBottom: Bool = false,
                                isLinksEnabled: Bool = false){
        let alert = CustomAlertViewController.alertVC(alertText: attributedString, okHandler: firstBTHandler, cancelHandler: secondBTHandler, isNeedScrollToBottom: isNeedScrollToBottom, isLinksEnabled: isLinksEnabled)
        if let firstButtonTitleValue = firstButtonTitle {
            alert.okButtonTitle = firstButtonTitleValue
        }
        if let secondButtonTitleValue = secondButtonTitle {
            alert.cancelButtonTitle = secondButtonTitleValue
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(attributedString: NSAttributedString? = nil,
                          firstButtonTitle: String? = nil,
                          secondButtonTitle: String? = nil,
                          complition: (() -> Void)?,
                          cancel: (() -> Void)?) {
        showCustomAlertMessage(attributedString: attributedString,
                               firstButtonTitle: firstButtonTitle,
                               secondButtonTitle: secondButtonTitle,
                               firstBTHandler: complition,
                               secondBTHandler: cancel)
    }
    
    func showAlertMessage(title: String?, message: String, complition: (() -> Void)?, cancel: (() -> Void)?) {
        showCustomAlertMessage(title: title, message: message, firstBTHandler: complition, secondBTHandler: cancel)
    }
    
    func showAlertMessage(title: String?, message: String) {
        showAlertMessage(title: title, message: message, complition: nil, cancel: nil)
    }
    
    func showErrorAlert(error: Swift.Error) {
        showAlertMessage(title: "Ошибка", message: error.localizedDescription)
    }
    
    func showSmart24Error(error: ErrorEntity) {
        
        let alertController = UIAlertController(title: "", message: error.data, preferredStyle: .alert)
        let ok = UIAlertAction(title: Constants.ok, style: .default) { action in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
    
    func showSmart24Error(error: Error) {
        
        let alertController = UIAlertController(title: Constants.error, message: error.localizedDescription, preferredStyle: .alert)
        let ok = UIAlertAction(title: Constants.ok, style: .default) { action in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
}

