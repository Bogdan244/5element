//
//  UIDatePicker+UIColor.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 12/10/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

extension UIDatePicker {
    
    func setTextColor(color: UIColor) {
        self.setValue(color, forKeyPath: "textColor")
    }
    
}

extension UIResponder {
    
    private static weak var _currentFirstResponder: UIResponder?
    
    static var currentFirstResponder: UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        
        return _currentFirstResponder
    }
    
    @objc func findFirstResponder(_ sender: Any) {
        UIResponder._currentFirstResponder = self
    }
}

