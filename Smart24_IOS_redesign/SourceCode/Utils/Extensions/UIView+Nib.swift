//
//  UIView+Nib.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 11/26/18.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class func initFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
    }
}
