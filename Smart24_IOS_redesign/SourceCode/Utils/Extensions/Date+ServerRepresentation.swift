//
//  Date+ServerRepresentation.swift
//  Smart24_IOS_redesign
//
//  Created by Bogdan on 16.06.2020.
//  Copyright © 2020 Vitya. All rights reserved.
//

import Foundation

extension Date {
    var serverRepresentation: String? {
        let df: DateFormatter = {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            return df
        }()
        return df.string(from: self)
    }
}

extension Optional where Wrapped == Date {
    var serverTimeInterval: Int? {
        guard let date = self else { return nil }
        return Int(date.timeIntervalSince1970)
    }
}

extension DateFormatter {
    static var bithdayFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }
}
