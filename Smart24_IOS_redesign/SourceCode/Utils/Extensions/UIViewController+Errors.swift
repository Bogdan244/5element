//
//  UIViewController+Errors.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/26/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func errorAlert(alertType: CloudSyncType) {
        
        var alert = UIAlertController()
        
        switch alertType {
            
        case .success:
            alert = UIAlertController(title: "Online Support & Security", message: "Копия адрессной книги создана.",
                                      preferredStyle: .alert)
            
        case .error:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка сохранения данных. \nОблачное хранилище недоступно.\nПовторите попытку позже",
                                      preferredStyle: .alert)
            
        case .unavailable:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка сохранения данных. \nОблачное хранилище недоступно.",
                                      preferredStyle: .alert)
        case .noContacts:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка сохранения данных. \nКонтакты отсутствуют.",
                                      preferredStyle: .alert)
        case .restoreSuccess:
            alert = UIAlertController(title: "Online Support & Security", message: "Последняя копия адрессной книги\nвосстановлена.",
                                      preferredStyle: .alert)
        case .restoreFailure:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка восстановления адрессной книги.",
                                      preferredStyle: .alert)
        case .backgroundBackupAccessDenied:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка. Приложение не сможет сохранять копию адрессной книги. Работа в фоновом режиме запрещена.",
                                      preferredStyle: .alert)
        case .backgroundBackupAccessLimited:
            alert = UIAlertController(title: "Online Support & Security", message: "Ошибка. Приложение не сможет сохранять копию адрессной книги. Работа в фоновом режиме ограничена.",
                                      preferredStyle: .alert)
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        present(alert, animated: true, completion: nil)
    }
}
