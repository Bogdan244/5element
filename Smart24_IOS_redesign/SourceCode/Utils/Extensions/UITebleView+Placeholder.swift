//
//  UITebleView+Placeholder.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 29.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import Foundation


extension UITableView {
    
    func emptyMessage(message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0),
                          size: CGSize(width: bounds.size.width,
                                       height: bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        backgroundView = messageLabel;
    }
}
