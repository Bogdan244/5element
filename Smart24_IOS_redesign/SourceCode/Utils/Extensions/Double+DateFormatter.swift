//
//  Double+DateFormatter.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/29/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation

extension Double {
    
    func getStringDate(dateFormat: String) -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current 
        return dateFormatter.string(from: date)
    }
}

extension String {
    
    func getDate(dateFormat: String) -> String? {
        if let doubleTime = Double(self) {
            let date = Date(timeIntervalSince1970: doubleTime)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            dateFormatter.timeZone = TimeZone.current
            return dateFormatter.string(from: date)
        }
        
        return nil
    }
}

extension Int {
    
    func timeFormatted() -> String {
        let seconds: Int = self % 60
        let minutes: Int = (self / 60) % 60
        let hours: Int = self / 3600
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
}
