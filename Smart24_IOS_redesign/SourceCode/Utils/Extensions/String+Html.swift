//
//  String+Html.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 10/17/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

extension String {
    
    var utfData: Data {
        return Data(utf8)
    }
    
    var attributedHtmlString: NSAttributedString? {
            return try? NSAttributedString(data: utfData,
                                          options: [ .documentType: NSAttributedString.DocumentType.html,
                                                     .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
    }
}
