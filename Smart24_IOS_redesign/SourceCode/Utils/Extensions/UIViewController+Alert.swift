//
//  UIViewController+Alert.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 5/26/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

extension UIViewController {
    
    static func showAlert(with text: String, duration: TimeInterval = 1) {
        guard let vc = UIApplication.shared.topMostViewController() else { return }
        let label = alertViewForVC(vc: vc, with: text)
        vc.view.addSubview(label)
        
        UIView.animate(withDuration: 0.5, animations: {
            label.alpha = 1
        }) { (_) in
            UIView.animate(withDuration: duration, animations: {
                label.alpha = 0
            }, completion: { (_) in
                label.removeFromSuperview()
            })
        }
        
    }
    
    private static func alertViewForVC(vc: UIViewController, with text: String) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: vc.view.frame.width - 40, height: 0))
        label.text = text
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.77)
        label.clipsToBounds = true
        label.alpha = 0
        label.sizeToFit()
        label.layer.cornerRadius = 5
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.black.cgColor
        label.bounds.size = CGSize(width: label.bounds.size.width + 20, height: label.bounds.size.height + 10)
        label.center = vc.view.center
        
        return label
    }
    
}
