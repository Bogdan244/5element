//
//  String+Grouped.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/20/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

extension String {
    
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    
    
    func group(by groupSize: Int = 3, separator: String = " ") -> String {
        if count <= groupSize { return self }
        return String(self.prefix(groupSize)) + separator + String(self[String.Index(encodedOffset: groupSize)...]).group(by: groupSize, separator: separator)

    }
    
    
    
    func group(by groupSize: [Int], separator: String = " ") -> String {
        if groupSize.reduce(0, +) > count {
            print("String so short to grouped by \(groupSize)")
            return self
        }
        var str = String(self.prefix(groupSize[0]))
        var sum = groupSize[0]
        
        for index in 1..<groupSize.count {
            
            let startIndex = String.Index(encodedOffset: sum)
            let endIndex = String.Index(encodedOffset: sum + groupSize[index] - 1)
            
            str = str + separator + String(self[startIndex...endIndex])
            sum = sum + groupSize[index]
        }
        
        return str
    }
}


