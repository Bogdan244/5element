//
//  String+Validator.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation

extension String {
    
    var isValidEmail: Bool {
        let filterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", filterString)
        return emailTest.evaluate(with: self)
    }
    
    var isCyrillic: Bool {
        let lower = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

        for c in self {
            if !lower.contains(c.lowercased()) {
                return false
            }
        }
        return true
    }
    
}
