//
//  UILabel+AdjustFontSize.swift
//  Smart24_IOS_redesign
//
//  Created by admin on 2/5/19.
//  Copyright © 2019 Vitya. All rights reserved.
//

import Foundation

extension UILabel {
    
    func adjustWidthFont() {
        let str = text ?? ""
        let strWidth = str.width(withConstrainedHeight: bounds.height, font: font)
        if strWidth > bounds.width {
            font = font.withSize(font.pointSize - 1)
            adjustWidthFont()
        }
    }
    
}
