//
//  ErrorFactory.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 1/20/17.
//  Copyright © 2017 Vitya. All rights reserved.
//

import ObjectMapper

protocol EntityCreatable {
    func createEntity(dictionary: [String: AnyObject]) -> ErrorEntity?;
}

public class ErrorFactory: EntityCreatable {
    
    static func createEntity(error: NewError) -> ErrorEntity {
        return ErrorEntity(code: error.code, message: error.message, data: error.data)
    }
    
    func createEntity(dictionary: [String: AnyObject]) -> ErrorEntity? {
        
        guard let jsonArray = dictionary[MapperKey.errors] as? [AnyObject],
            let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
            let error = errors.first else {
                return nil
        }
        
        return error
    }
    
    static func createEntity(dictionary: [String: AnyObject]?) -> ErrorEntity? {
        
        guard let jsonArray = dictionary?[MapperKey.errors] as? [AnyObject],
            let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
            let error = errors.first else {
                return nil
        }
        
        return error
    }
    
    static func error(text: String) -> ErrorEntity {
        return ErrorEntity(text: text)
    }
    
    static var serverError: ErrorEntity {
        return ErrorEntity(text: "Ошибка сервера")
    }
}
