// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable operator_usage_whitespace
extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

// swiftlint:disable identifier_name line_length type_body_length
struct ColorName {
  let rgbaValue: UInt32
  var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2ecc71"></span>
  /// Alpha: 100% <br/> (0x2ecc71ff)
  static let loginButtonNormal = ColorName(rgbaValue: 0x2ecc71ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e9e7e5"></span>
  /// Alpha: 100% <br/> (0xe9e7e5ff)
  static let loginTextFiledColor = ColorName(rgbaValue: 0xe9e7e5ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#1cb0f6"></span>
  /// Alpha: 100% <br/> (0x1cb0f6ff)
  static let blueProgress = ColorName(rgbaValue: 0x1cb0f6ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ddf1d8"></span>
  /// Alpha: 100% <br/> (0xddf1d8ff)
  static let chatMessageColor = ColorName(rgbaValue: 0xddf1d8ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#dde6ef"></span>
  /// Alpha: 100% <br/> (0xdde6efff)
  static let lightBlueProgress = ColorName(rgbaValue: 0xdde6efff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#db001b"></span>
  /// Alpha: 100% <br/> (0xdb001bff)
  static let topBarColor = ColorName(rgbaValue: 0xdb001bff)
}
// swiftlint:enable identifier_name line_length type_body_length

extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}

