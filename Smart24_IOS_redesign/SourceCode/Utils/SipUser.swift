//
//  SipUser.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 1/9/17.
//  Copyright © 2017 Vitya. All rights reserved.
//

import Foundation

class SipUser: NSObject, SIPEnabledUser {
    
    var sipAccount: String
    var sipPassword: String
    var sipDomain: String
    var sipProxy: String
    var sipRegisterOnAdd: Bool
    
    init(sipAccount: String, sipPassword: String, sipDomain: String, sipProxy: String) {
        self.sipAccount = Keys.SIP.Account
        self.sipPassword = Keys.SIP.Password
        self.sipDomain = Keys.SIP.Domain
        self.sipProxy = Keys.SIP.Proxy
        sipRegisterOnAdd = false
        super.init()
    }
    
    convenience override init() {
        self.init(sipAccount: Keys.SIP.Account, sipPassword: Keys.SIP.Password, sipDomain: Keys.SIP.Domain, sipProxy: Keys.SIP.Proxy)
    }
}
